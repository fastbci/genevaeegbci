**Description**

FastBCI, previously GenevaEegBci, is a C++ application under MIT license.
It provides a EEG driven radial keyboard with word predictions.
It uses ANT Eego amplifiers and can be used with 32, 64 or 128 electrodes caps.
It is available in English, German and French via a configuration file.

----

**Configuration**

- The Matlab API dll folder needs to be copied locally to take benefit of the 32 bits Matlab API and be able to build and run.
The path of this folder needs to be added to the Windows path.

- The Intel Math Kernel Library (MKL) needs to be copied in the Geneva EEG BCI\lib folder.

- Both libraries are available on the NAS or the Dropbox.

----

**Branches description**

- The master branch is configured to provide an EXE

- The processing-testing branch is configured to perform unit tests and integration tests by using the application as a DLL