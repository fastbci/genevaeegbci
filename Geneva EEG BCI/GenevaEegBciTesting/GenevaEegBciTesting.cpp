// GenevaEegBciTesting.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
// Author: Christoph Pokorny
// Date:   11/2018
// Desc:   Testing environment to develop/test C++ functions and compare results with MATLAB

#include <iostream>

// Standard libs
#include <stdlib.h>
#include <mat.h>  // MAT-File API Library
#include <chrono>
#include <string>
#include <memory>
#include <numeric>

#ifndef MATHLIBRARY_H
#include "../GenevaEegBci/MathLibrary.h"
#endif

#ifndef MATLABLIBRARY_H
#include "../GenevaEegBci/MatlabLibrary.h"
#endif

#ifndef SIZETVECTOR_H
#include "../GenevaEegBci/SizeTVector.h"
#endif

#include "../GenevaEegBci/DataBuffer.h"
#include "../GenevaEegBci/EegData.h"
#include "../GenevaEegBci/SerialRationalTransferFunctionFilter.h"
#include "../GenevaEegBci/ChannelInterpolationAlgorithm.h"
#include "../GenevaEegBci/EogCorrectionAlgorithm.h"
#include "../GenevaEegBci/CarAlgorithm.h"

#include "../GenevaEegBci/RealImaginaryStftAlgorithm.h"
#include "../GenevaEegBci/DenoisingAlgorithm.h"
#include "../GenevaEegBci/FrequencyBandSelector.h"
#include "../GenevaEegBci/SpectralSourceReconstruction.h"
#include "../GenevaEegBci/SpectralAdaptiveFeatureNormalization.h"

#include "../GenevaEegBci/SerialSGolayFilterAlgorithm.h"
#include "../GenevaEegBci/CorticalSourceReconstruction.h"
#include "../GenevaEegBci/AdaptiveFeatureNormalizationAlgorithm.h"

#include "../GenevaEegBci/FeatureTimeSelector.h"
#include "../GenevaEegBci/MldaClassifier.h"
#include "../GenevaEegBci/ConsolidateDecisionAlgorithm.h"

using namespace std;

// Preprocessing
void fillDataBuffer(double *dataBuffer, mxArray *matData, unsigned int sampleId);

// Mat files
mxArray *loadMatData(string);
mxArray* loadMatVariable(string fileName, string variableName);
void saveMatEegEogData(string, double *, double *, size_t, size_t, size_t);
void saveMatDouble3dData(string fileName, string dataName, double* data, size_t dim1Len, size_t dim2Len, size_t dim3Len);
void saveMatDoubleMatrixData(string fileName, string dataName, double* data, size_t numSamples, size_t numChannels);

const unsigned int SAMPLING_RATE = 1024;
const unsigned int BUFFER_DURATION = 1; // in s

ofstream logFile;

int main()
{
    cout << "Starting online BCI test...\n";

    // Create and open Dialog log file
    time_t t = time(0);
    struct tm* now = localtime(&t);

    string buffer(80, '\0');
    strftime(&buffer[0], buffer.size(), "%F_%H-%M-%S.txt", now);

    logFile = ofstream("logDlg_" + buffer, std::ios::out);

    vector<long long> preprocessingDuration;
    vector<long long> timeFrequencyDuration;
    vector<long long> stftDuration;
    vector<long long> mldaClassifierDuration;
    vector<long long> featureTimeSelectorDuration;
    vector<long long> processedFeatureDuration;
    vector<long long> adaptiveFeatureNormalizationDuration;
    vector<long long> corticalSourceReconstructionDuration;
    vector<long long> spectralBinsMeanDuration;
    vector<long long> spectralAmplitudeDuration;
    vector<long long> freqBandSelectorDuration;
    vector<long long> spectralSourceReconstructionDuration;
    vector<long long> sGolayDuration;
    vector<long long> spectralAdaptiveFeatureNormalizationDuration;
    vector<long long> denoisingDuration;
    
    auto timeStart = chrono::steady_clock::now();

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Configuration

    // EegData class init
    string channelSelector_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateChannelSelector.mat";
    CEegData eegData(channelSelector_calibrationFile);
    ChannelConfig channelConfig = eegData.GetChannelConfig();

    const size_t numberOfEegChannels = channelConfig.eegChannelsWithRef.size();
    const size_t numberOfEogChannels = 2; // always two channels: eogBipHor/eogBipVert
    

    // High-pass eeg filter to remove recording drifts
    string eegSerialRationalTransferFunctionFilter_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateSerialRationalTransferFunctionFilterEeg.mat";
    CSerialRationalTransferFunctionFilter eegSerialRationalTransferFunctionFilter(eegSerialRationalTransferFunctionFilter_calibrationFile);


    // High-pass eog filter to remove recording drifts
    string eogSerialRationalTransferFunctionFilter_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateSerialRationalTransferFunctionFilterEog.mat";
    CSerialRationalTransferFunctionFilter eogSerialRationalTransferFunctionFilter(eogSerialRationalTransferFunctionFilter_calibrationFile);


    // Channel interpolation
    string channelInterpolationAlgorithm_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateChannelInterpolationAlgorithm.mat";
    CChannelInterpolationAlgorithm channelInterpolationAlgorithm(channelInterpolationAlgorithm_calibrationFile);


    // EOG correction
    string eogCorrectionAlgorithm_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateEogCorrectionAlgorithm.mat";
    CEogCorrectionAlgorithm eogCorrectionAlgorithm(eogCorrectionAlgorithm_calibrationFile);


    // CAR algorithm
    string carAlgorithm_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateCarAlgorithm.mat";
    CCarAlgorithm carAlgorithm(carAlgorithm_calibrationFile, eegData.getReferenceChannelIndex());


    // Real & imaginary stft algorithm
    string realImaginaryStftAlgorithm_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateRealImaginaryStftAlgorithm.mat";
    CRealImaginaryStftAlgorithm realImaginaryStftAlgorithm(realImaginaryStftAlgorithm_calibrationFile);


    // Denoising algorithm
    string denoisingAlgorithm_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateDenoisingAlgorithm.mat";
    CDenoisingAlgorithm denoisingAlgorithm(denoisingAlgorithm_calibrationFile);


    // Real frequency band selector
    string realFrequencyBandSelector_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateRealFrequencyBandSelector.mat";
    CFrequencyBandSelector realFrequencyBandSelector(realFrequencyBandSelector_calibrationFile);


    // Imginary frequency band selector
    string imaginaryFrequencyBandSelector_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateImaginaryFrequencyBandSelector.mat";
    CFrequencyBandSelector imaginaryFrequencyBandSelector(imaginaryFrequencyBandSelector_calibrationFile);


    // Real spectral source reconstruction
    string realSpectralSourceReconstruction_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateRealSpectralSourceReconstruction.mat";
    CSpectralSourceReconstruction realSpectralSourceReconstruction(realSpectralSourceReconstruction_calibrationFile);


    // Imaginary spectral source reconstruction
    string imaginarySpectralSourceReconstruction_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateImaginarySpectralSourceReconstruction.mat";
    CSpectralSourceReconstruction imaginarySpectralSourceReconstruction(realSpectralSourceReconstruction_calibrationFile);


    // Spectral adaptive feature normalization
    string spectralAdaptiveFeatureNormalization_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateRealSpectralAdaptiveFeatureNormalization.mat";
    CSpectralAdaptiveFeatureNormalization spectralAdaptiveFeatureNormalization(spectralAdaptiveFeatureNormalization_calibrationFile);

	// Spectral feature time selector
	string spectralFeatureTimeSelector_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateSpectralFeatureTimeSelector.mat";
	CFeatureTimeSelector spectralFeatureTimeSelector(spectralFeatureTimeSelector_calibrationFile);

    // Savitzky-Golay filter
    string serialSGolayFilterAlgorithm_calibrationFile = "..\\IntegrationTestData\\integrationTestData_SerialSGolayFilterAlgorithm.mat";
    CSerialSGolayFilterAlgorithm serialSGolayFilterAlgorithm(serialSGolayFilterAlgorithm_calibrationFile);


    // Cortical source reconstruction
    string corticalSourceReconstruction_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateCorticalSourceReconstruction.mat";
    CCorticalSourceReconstruction corticalSourceReconstruction(corticalSourceReconstruction_calibrationFile);


    // Adaptive feature normalization
    string adaptiveFeatureNormalizationAlgorithm_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateAdaptiveFeatureNormalizationAlgorithm.mat";
    CAdaptiveFeatureNormalizationAlgorithm adaptiveFeatureNormalizationAlgorithm(adaptiveFeatureNormalizationAlgorithm_calibrationFile);


    // Feature time selector
    string featureTimeSelector_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateFeatureTimeSelector.mat";
    CFeatureTimeSelector featureTimeSelector(featureTimeSelector_calibrationFile);


    // Decoder
    string mldaClassifier_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateMldaClassifier.mat";
    CMldaClassifier mldaClassifier(mldaClassifier_calibrationFile);


    // Decision consolidator
    string consolidateDecisionAlgorithm_calibrationFile = "..\\IntegrationTestData\\integrationTestData_calibrateConsolidateDecisionAlgorithm.mat";
    CConsolidateDecisionAlgorithm consolidateDecisionAlgorithm(consolidateDecisionAlgorithm_calibrationFile);


    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Load test signals

    // Input file
    //string signalFileName = "..\\IntegrationTestData\\integrationTestData_testSignals.mat";
    string signalFileName = "..\\IntegrationTestData\\ChristophTestData_eegSignals.mat";
    //string signalFileName = "..\\IntegrationTestData\\ChristophTestData_eegSignals.mat";


    //// Load numberOfEegChannels from .mat file
    //mxArray* matNumberOfEegChannels = CMatlabLibrary::loadMatVariable(signalFileName, "numberOfEegChannels");
    //assert(NULL != matNumberOfEegChannels);

    //// Verify that the size is ok
    //assert(1 == static_cast<int>(mxGetN(matNumberOfEegChannels)));
    //assert(1 == static_cast<int>(mxGetM(matNumberOfEegChannels)));

    //double* dataElements = static_cast<double*>(mxGetData(matNumberOfEegChannels));
    ////const size_t numberOfEegChannels = static_cast<size_t>(dataElements[0]);
    //assert(numberOfEegChannels == static_cast<size_t>(dataElements[0]));


    //// Load matNumberOfEogChannels from .mat file
    //mxArray* matNumberOfEogChannels = CMatlabLibrary::loadMatVariable(signalFileName, "numberOfEogChannels");
    //assert(NULL != matNumberOfEogChannels);

    //// Verify that the size is ok
    //assert(1 == static_cast<int>(mxGetN(matNumberOfEogChannels)));
    //assert(1 == static_cast<int>(mxGetM(matNumberOfEogChannels)));

    //dataElements = static_cast<double*>(mxGetData(matNumberOfEogChannels));
    //assert(numberOfEogChannels == static_cast<size_t>(dataElements[0]));


    //// Load matNumberOfEmgChannels from .mat file
    //mxArray* matNumberOfEmgChannels = CMatlabLibrary::loadMatVariable(signalFileName, "numberOfEmgChannels");
    //assert(NULL != matNumberOfEmgChannels);

    //// Verify that the size is ok
    //assert(1 == static_cast<int>(mxGetN(matNumberOfEmgChannels)));
    //assert(1 == static_cast<int>(mxGetM(matNumberOfEmgChannels)));

    //dataElements = static_cast<double*>(mxGetData(matNumberOfEmgChannels));
    //const size_t numberOfEmgChannels = static_cast<size_t>(48);
    //const size_t numberOfOtherChannels = static_cast<size_t>(3);

    const size_t numberOfChannels = static_cast<size_t>(178);

    // Load matNumberOfEogChannels from .mat file
    mxArray* matNumberOfProcessingCalls = CMatlabLibrary::loadMatVariable(signalFileName, "numberOfProcessingCalls");
    assert(NULL != matNumberOfProcessingCalls);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfProcessingCalls)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfProcessingCalls)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfProcessingCalls));
    const size_t numberOfProcessingCalls = static_cast<size_t>(dataElements[0]);


    // Load the numberOfInterpolatedChannels from .mat file
    mxArray* matNumberOfSamples = CMatlabLibrary::loadMatVariable(signalFileName, "numberOfSamples");
    assert(NULL != matNumberOfSamples);

    // Verify that the size is ok
    assert(numberOfProcessingCalls == mxGetM(matNumberOfSamples));
    assert(1 == static_cast<int>(mxGetN(matNumberOfSamples)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfSamples));
    size_t* numberOfSamples = new size_t[numberOfProcessingCalls];
    memset(numberOfSamples, 0, numberOfProcessingCalls * sizeof(size_t));
    for (size_t cellIndex = 0; cellIndex < numberOfProcessingCalls; ++cellIndex)
    {
        numberOfSamples[cellIndex] = static_cast<size_t>(dataElements[cellIndex]);
    }


    // Store data in data Buffer
    CDoubleVector*** testSignalBuffer = new CDoubleVector**[numberOfProcessingCalls];
    mxArray* matTestSignals = CMatlabLibrary::loadMatVariable(signalFileName, "testSignals");
    assert(NULL != matTestSignals);
    mxArray* signalCellPointer = 0;

    for (size_t cellIndex = 0; cellIndex < numberOfProcessingCalls; ++cellIndex)
    {
        signalCellPointer = mxGetCell(matTestSignals, cellIndex);
        assert(NULL != signalCellPointer);

        if (numberOfSamples[cellIndex] > 0)
        {
            // Verify that the size is ok
            assert(numberOfChannels == mxGetM(signalCellPointer));
            assert(numberOfSamples[cellIndex] = mxGetN(signalCellPointer));

            dataElements = static_cast<double*>(mxGetData(signalCellPointer));
            testSignalBuffer[cellIndex] = new CDoubleVector*[numberOfSamples[cellIndex]];
            for (size_t testIndex = 0; testIndex < numberOfSamples[cellIndex]; ++testIndex)
            {
                testSignalBuffer[cellIndex][testIndex] = new CDoubleVector(numberOfChannels, dataElements + testIndex * numberOfChannels);
            }
        }
        else
        {
            testSignalBuffer[cellIndex] = 0;
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Process the signals

    CDoubleVector* eegSignals = new CDoubleVector(numberOfEegChannels, 0.0);
    CDoubleVector* eogSignals = new CDoubleVector(numberOfEogChannels, 0.0);
    CDoubleVector* filteredEegSignals = new CDoubleVector(numberOfEegChannels, 0.0);
    CDoubleVector* filteredEogSignals = new CDoubleVector(numberOfEogChannels, 0.0);
    CDoubleVector* interpolatedSignal = new CDoubleVector(numberOfEegChannels, 0.0);
    CDoubleVector* eogCorrectedSignal = new CDoubleVector(numberOfEegChannels, 0.0);
    vector<CDoubleMatrix*> helpStftReal;
    vector<CDoubleMatrix*> helpStftImaginary;
    CDoubleMatrix* realDenoisedSpectralMatrix = new CDoubleMatrix(realFrequencyBandSelector.getNumberOfSpectralBins(), realFrequencyBandSelector.getNumberOfChannels(), 0.0);
    CDoubleMatrix* imaginaryDenoisedSpectralMatrix = new CDoubleMatrix(imaginaryFrequencyBandSelector.getNumberOfSpectralBins(), imaginaryFrequencyBandSelector.getNumberOfChannels(), 0.0);
    CDoubleMatrix* helpRealSpectralMatrix = new CDoubleMatrix(realFrequencyBandSelector.getNumberOfSelectedBins(), realFrequencyBandSelector.getNumberOfChannels(), 0.0);
    CDoubleMatrix* helpImaginarySpectralMatrix = new CDoubleMatrix(imaginaryFrequencyBandSelector.getNumberOfSelectedBins(), imaginaryFrequencyBandSelector.getNumberOfChannels(), 0.0);
    CDoubleMatrix* realSpectralCorticalActivity = new CDoubleMatrix(realSpectralSourceReconstruction.getNumberOfSpectralBins(), realSpectralSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleMatrix* imaginarySpectralCorticalActivity = new CDoubleMatrix(imaginarySpectralSourceReconstruction.getNumberOfSpectralBins(), imaginarySpectralSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleMatrix* spectralAmplitude = new CDoubleMatrix(realSpectralSourceReconstruction.getNumberOfSpectralBins(), realSpectralSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleMatrix* normalizedSpectralAmplitude = new CDoubleMatrix(realSpectralSourceReconstruction.getNumberOfSpectralBins(), realSpectralSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleVector* meanSpectralAmplitude = new CDoubleVector(realSpectralSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    vector<CDoubleVector*> filteredEEG;
    CDoubleVector* corticalActivity = new CDoubleVector(corticalSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleVector* normalizedCorticalActivity = new CDoubleVector(corticalSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleVector* timeFeatureVector = new CDoubleVector(corticalSourceReconstruction.getNumberOfCorticalSources() * featureTimeSelector.getNumberOfTaps(), 0.0);
	CDoubleVector* spectralFeatureVector = new CDoubleVector(corticalSourceReconstruction.getNumberOfCorticalSources() * spectralFeatureTimeSelector.getNumberOfTaps(), 0.0);

    double* consolidatedIntefaceCommand = new double[numberOfProcessingCalls];
    size_t decoderOutput = 0;
    bool isReadyRealImaginaryStftAlgorithm = false;
    bool isReadySerialSGolayFilterAlgorithm = false;

    auto processStart = chrono::steady_clock::now();
    auto processEnd = chrono::steady_clock::now();
    auto processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();

    for (size_t cellIndex = 0; cellIndex < numberOfProcessingCalls; ++cellIndex)
    {
        artifactPresence = false;

        for (size_t testIndex = 0; testIndex < numberOfSamples[cellIndex]; ++testIndex)
        {
            processStart = chrono::steady_clock::now();
         
            // Channel selector
            eegData.channelSelector(testSignalBuffer[cellIndex][testIndex]->getValues(), eegSignals->getValues(), eogSignals->getValues());

            // High-pass filtering
            if (!eegSerialRationalTransferFunctionFilter.process(filteredEegSignals, eegSignals) | !eogSerialRationalTransferFunctionFilter.process(filteredEogSignals, eogSignals))
            {
                continue;
            }

            // Channel interpolation
            channelInterpolationAlgorithm.process(interpolatedSignal, filteredEegSignals);

            // Eog correction
            eogCorrectionAlgorithm.process(eogCorrectedSignal, interpolatedSignal, filteredEogSignals);

            // Threshold artifact detection
			eegData.artefactThreshold(eogCorrectedSignal->getValues(), numberOfEegChannels);

            // Common average referencing
            carAlgorithm.process(eegSignals->getValues(), eogCorrectedSignal->getValues());

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            preprocessingDuration.push_back(processTimeElapsed);

            if (isReadyRealImaginaryStftAlgorithm && isReadySerialSGolayFilterAlgorithm)
            {
                // Spectral decomposition
                realImaginaryStftAlgorithm.process(helpStftReal, helpStftImaginary, eegSignals);

                // Measure elapsed time
                processEnd = chrono::steady_clock::now();
                processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
                processStart = processEnd;
                stftDuration.push_back(processTimeElapsed);

                // Savitzky-Golay filtering
                serialSGolayFilterAlgorithm.process(filteredEEG, eegSignals);

                // Measure elapsed time
                processEnd = chrono::steady_clock::now();
                processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
                processStart = processEnd;
                sGolayDuration.push_back(processTimeElapsed);
            }
            else
            {
                isReadyRealImaginaryStftAlgorithm = realImaginaryStftAlgorithm.addToBuffer(eegSignals);

                isReadySerialSGolayFilterAlgorithm = serialSGolayFilterAlgorithm.addToBuffer(eegSignals);
            }
        }

        assert(helpStftReal.size() == filteredEEG.size());
        for (size_t outputIndex = 0; outputIndex < helpStftReal.size(); outputIndex++)
        {
            processStart = chrono::steady_clock::now();

            // Spectral denoising
            denoisingAlgorithm.process(realDenoisedSpectralMatrix, imaginaryDenoisedSpectralMatrix, helpStftReal.at(outputIndex), helpStftImaginary.at(outputIndex));
            delete helpStftReal.at(outputIndex);
            helpStftReal.at(outputIndex) = 0;
            delete helpStftImaginary.at(outputIndex);
            helpStftImaginary.at(outputIndex) = 0;

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            denoisingDuration.push_back(processTimeElapsed);

            // Frequency band selector for real spectrum
            realFrequencyBandSelector.process(helpRealSpectralMatrix, realDenoisedSpectralMatrix);

            // Frequency band selector for imaginary spectrum
            realFrequencyBandSelector.process(helpImaginarySpectralMatrix, imaginaryDenoisedSpectralMatrix);

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            freqBandSelectorDuration.push_back(processTimeElapsed);

            // Spectral source reconstruction for the real spectrum
            realSpectralSourceReconstruction.process(realSpectralCorticalActivity, helpRealSpectralMatrix);

            // Spectral source reconstruction for the imaginary spectrum
            imaginarySpectralSourceReconstruction.process(imaginarySpectralCorticalActivity, helpImaginarySpectralMatrix);

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            spectralSourceReconstructionDuration.push_back(processTimeElapsed);

            // Calculate the spectral amplitude of cortical activity
            CMathLibrary::elementWiseAbsoluteValue(spectralAmplitude, realSpectralCorticalActivity[0], imaginarySpectralCorticalActivity[0]);

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            spectralAmplitudeDuration.push_back(processTimeElapsed);

            // Perform the spectral adaptive feature normalization
            spectralAdaptiveFeatureNormalization.process(normalizedSpectralAmplitude, spectralAmplitude, eegData.GetIsArtefactDetected());

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            spectralAdaptiveFeatureNormalizationDuration.push_back(processTimeElapsed);

            // Calculate the mean over all the selected spectral bins
            CMathLibrary::matrixMeanAcrossFirstDimension(meanSpectralAmplitude, normalizedSpectralAmplitude[0]);

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            spectralBinsMeanDuration.push_back(processTimeElapsed);

            // Cortical source reconstruction
            corticalSourceReconstruction.process(corticalActivity, filteredEEG.at(outputIndex));
            delete filteredEEG.at(outputIndex);
            filteredEEG.at(outputIndex) = 0;

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            corticalSourceReconstructionDuration.push_back(processTimeElapsed);

            // Adaptive feature normalization
            adaptiveFeatureNormalizationAlgorithm.process(normalizedCorticalActivity[0], corticalActivity[0], eegData.GetIsArtefactDetected());

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            adaptiveFeatureNormalizationDuration.push_back(processTimeElapsed);

			if (!adaptiveFeatureNormalizationAlgorithm.getIsInitialized() || !spectralAdaptiveFeatureNormalization.getIsInitialized())
			{
				continue;
			}

			// Add spectral and MRCP features to time selector buffers
			featureTimeSelector.addToBuffer(normalizedCorticalActivity);
			spectralFeatureTimeSelector.addToBuffer(meanSpectralAmplitude);

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            processedFeatureDuration.push_back(processTimeElapsed);
        }

        helpStftReal.clear();
        helpStftImaginary.clear();
        filteredEEG.clear();


        if (featureTimeSelector.getIsBufferFilled() && spectralFeatureTimeSelector.getIsBufferFilled())
        {
            processStart = chrono::steady_clock::now();

            // Feature time selectors
            featureTimeSelector.generateFeatureVector(timeFeatureVector);
			spectralFeatureTimeSelector.generateFeatureVector(spectralFeatureVector);

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            processStart = processEnd;
            featureTimeSelectorDuration.push_back(processTimeElapsed);

            // Decoder
            decoderOutput = mldaClassifier.process(timeFeatureVector, spectralFeatureVector);

            // Measure elapsed time
            processEnd = chrono::steady_clock::now();
            processTimeElapsed = chrono::duration_cast<chrono::microseconds>(processEnd - processStart).count();
            mldaClassifierDuration.push_back(processTimeElapsed);
        }
        else
        {
            decoderOutput = 0;
        }

        consolidatedIntefaceCommand[cellIndex] = consolidateDecisionAlgorithm.process(decoderOutput,eegData.GetIsArtefactDetected(),numberOfSamples[cellIndex]);
    }

    for (auto it = preprocessingDuration.begin(); it != preprocessingDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = stftDuration.begin(); it != stftDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = denoisingDuration.begin(); it != denoisingDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = freqBandSelectorDuration.begin(); it != freqBandSelectorDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = spectralSourceReconstructionDuration.begin(); it != spectralSourceReconstructionDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = spectralAmplitudeDuration.begin(); it != spectralAmplitudeDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = spectralAdaptiveFeatureNormalizationDuration.begin(); it != spectralAdaptiveFeatureNormalizationDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = spectralBinsMeanDuration.begin(); it != spectralBinsMeanDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = corticalSourceReconstructionDuration.begin(); it != corticalSourceReconstructionDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = adaptiveFeatureNormalizationDuration.begin(); it != adaptiveFeatureNormalizationDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = processedFeatureDuration.begin(); it != processedFeatureDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = featureTimeSelectorDuration.begin(); it != featureTimeSelectorDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }

    logFile << "\n0\n\n";

    for (auto it = mldaClassifierDuration.begin(); it != mldaClassifierDuration.end(); ++it)
    {
        logFile << *it << "\n";
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Save results

    // Create and open Dialog log file
    t = time(0);
    now = localtime(&t);

    string buffer2(80, '\0');
    strftime(&buffer2[0], buffer2.size(), "%F_%H-%M-%S.mat", now);
    
    CMatlabLibrary::saveMatDoubleMatrixData("..\\IntegrationTestData\\IntegrationTest_testResults_"+buffer2, "decodingResult", consolidatedIntefaceCommand, numberOfProcessingCalls, 1);


    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Cleanup

    delete eegSignals;
    delete eogSignals;
    delete filteredEegSignals;
    delete filteredEogSignals;
    delete interpolatedSignal;
    delete eogCorrectedSignal;
    delete helpRealSpectralMatrix;
    delete helpImaginarySpectralMatrix;
    delete realDenoisedSpectralMatrix;
    delete imaginaryDenoisedSpectralMatrix;
    delete realSpectralCorticalActivity;
    delete imaginarySpectralCorticalActivity;
    delete spectralAmplitude;
    delete normalizedSpectralAmplitude;
    delete meanSpectralAmplitude;
    delete corticalActivity;
    delete normalizedCorticalActivity;
    delete timeFeatureVector;
	delete spectralFeatureVector;
    delete[] consolidatedIntefaceCommand;

    for (size_t cellIndex = 0; cellIndex < numberOfProcessingCalls; ++cellIndex)
    {
        for (size_t testIndex = 0; testIndex < numberOfSamples[cellIndex]; ++testIndex)
        {
            if (testSignalBuffer[cellIndex][testIndex] != 0)
            {
                delete testSignalBuffer[cellIndex][testIndex];
            }
        }

        if (testSignalBuffer[cellIndex] != 0)
        {
            delete[] testSignalBuffer[cellIndex];
        }
    }
    delete[] testSignalBuffer;
    delete[] numberOfSamples;

    // Measure elapsed time
    auto timeEnd = chrono::steady_clock::now();
    auto timeElapsed = chrono::duration_cast<chrono::seconds>(timeEnd - timeStart).count();
    cout << "Elapsed time: " << timeElapsed << "s\n";
}

/*===============================================================================================*/
void fillDataBuffer(double *dataBuffer, mxArray *matData, unsigned int sampleId)
{
    size_t numChannels = mxGetN(matData);
	size_t numSamples = mxGetM(matData);
	double* dataElements = static_cast<double*>(mxGetData(matData));

	// Extract data frames <#channelsx1> from matData
	// (data in matData are stored channel-wise, but needed sample/frame-wise)
	for (unsigned int jChannel = 0; jChannel < numChannels; jChannel++)
	{
        dataBuffer[jChannel] = dataElements[sampleId + jChannel * numSamples];
	}
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

/*===============================================================================================*/
mxArray* loadMatData(string fileName)
{
    string varName = "data";

    cout << "Loading file: " << fileName << "\n";
    MATFile *matFile = matOpen(fileName.c_str(), "r");
    if (matFile == NULL)
    {
        printf("ERROR: Error opening file %s\n", fileName.c_str());
        return NULL;
    }

    // Read data
    mxArray *matData = matGetVariable(matFile, varName.c_str());
    if (matData == NULL)
    {
        printf("ERROR: MATLAB array called '%s' not found in file %s\n", varName.c_str(), fileName.c_str());
    }

    // Close .mat file
    if (matClose(matFile) != 0)
    {
        printf("ERROR: Error closing file %s\n", fileName.c_str());

        if (matData != NULL)
        {
            mxDestroyArray(matData);
            matData = NULL;
        }
    }

    return matData;
}

/*===============================================================================================*/
mxArray* loadMatVariable(string fileName, string variableName)
{
	std::cout << "Loading file: " << fileName << "\n";
	MATFile *matFile = matOpen(fileName.c_str(), "r");
	if (matFile == NULL)
	{
		printf("ERROR: Error opening file %s\n", fileName.c_str());
		return NULL;
	}

	// Read data
	mxArray *matData = matGetVariable(matFile, variableName.c_str());
	if (matData == NULL)
	{
		printf("ERROR: MATLAB array called '%s' not found in file %s\n", variableName.c_str(), fileName.c_str());
	}

	// Close .mat file
	if (matClose(matFile) != 0)
	{
		printf("ERROR: Error closing file %s\n", fileName.c_str());

		if (matData != NULL)
		{
			mxDestroyArray(matData);
			matData = NULL;
		}
	}

	return matData;
}

/*===============================================================================================*/
void saveMatDoubleMatrixData(string fileName, string dataName, double* data, size_t numSamples, size_t numChannels)
{
	// Update the output .mat file. If the file does not exist, create it.
	MATFile *matOutputFile = matOpen(fileName.c_str(), "u");
	if (matOutputFile == NULL)
	{
		matOutputFile = matOpen(fileName.c_str(), "w");
		if (matOutputFile == NULL)
		{
			printf("ERROR: Error opening file %s\n", fileName.c_str());
			return;
		}
	}

	// Store double matrix data
	mxArray *matData = mxCreateDoubleMatrix(numSamples, numChannels, mxREAL);
	double* dataElements = static_cast<double*>(mxGetData(matData));
	memcpy(dataElements, data, numChannels * numSamples * sizeof(double));
	if (matPutVariable(matOutputFile, dataName.c_str(), matData) != 0)
	{
		printf("ERROR: Writing variable %s file %s\n", dataName.c_str(), fileName.c_str());
	}
	mxDestroyArray(matData);
	matData = NULL;
	dataElements = NULL;

	// Close file
	if (matClose(matOutputFile) != 0)
	{
		printf("ERROR: Error closing file %s\n", fileName.c_str());
		return;
	}

	return;
}


/*===============================================================================================*/
void saveMatDouble3dData(string fileName, string dataName, double* data, size_t dim1Len, size_t dim2Len, size_t dim3Len)
{
	// Update the output .mat file. If the file does not exist, create it.
	MATFile *matOutputFile = matOpen(fileName.c_str(), "u");
	if (matOutputFile == NULL)
	{
		matOutputFile = matOpen(fileName.c_str(), "w");
		if (matOutputFile == NULL)
		{
			printf("ERROR: Error opening file %s\n", fileName.c_str());
			return;
		}
	}

	// Store double 3d data
	size_t dimLen[3];
	dimLen[0] = dim1Len;
	dimLen[1] = dim2Len;
	dimLen[2] = dim3Len;

	mxArray *matData = mxCreateNumericArray(3, dimLen, mxDOUBLE_CLASS, mxREAL);
	double* dataElements = static_cast<double*>(mxGetData(matData));
	memcpy(dataElements, data, dim1Len * dim2Len * dim3Len * sizeof(double));
	if (matPutVariable(matOutputFile, dataName.c_str(), matData) != 0)
	{
		printf("ERROR: Writing variable %s file %s\n", dataName.c_str(), fileName.c_str());
	}
	mxDestroyArray(matData);
	matData = NULL;
	dataElements = NULL;

	// Close file
	if (matClose(matOutputFile) != 0)
	{
		printf("ERROR: Error closing file %s\n", fileName.c_str());
		return;
	}

	return;
}

/*===============================================================================================*/
void saveMatEegEogData(string fileName, double *eegBuffer, double *eogBuffer, size_t numSamples, size_t numEegChannels, size_t numEogChannels)
{
    // Create output .mat file
    MATFile *matOutputFile = matOpen(fileName.c_str(), "w");
    if (matOutputFile == NULL)
    {
        printf("ERROR: Error opening file %s\n", fileName.c_str());
        return;
    }

    // Store EEG data
    string varName = "eegData";
    mxArray *eegData = mxCreateDoubleMatrix(numSamples, numEegChannels, mxREAL);
    double* eegDataElements = static_cast<double*>(mxGetData(eegData));
    memcpy(eegDataElements, eegBuffer, numEegChannels * numSamples * sizeof(double));
    if (matPutVariable(matOutputFile, varName.c_str(), eegData) != 0)
    {
        printf("ERROR: Writing variable %s file %s\n", varName.c_str(), fileName.c_str());
    }
    mxDestroyArray(eegData);
    eegData = NULL;
    eegDataElements = NULL;

    // Store EOG data
    varName = "eogData";
    mxArray *eogData = mxCreateDoubleMatrix(numSamples, numEogChannels, mxREAL);
    double* eogDataElements = static_cast<double*>(mxGetData(eogData));
    memcpy(eogDataElements, eogBuffer, numEogChannels * numSamples * sizeof(double));
    if (matPutVariable(matOutputFile, varName.c_str(), eogData) != 0)
    {
        printf("ERROR: Writing variable %s file %s\n", varName.c_str(), fileName.c_str());
    }
    mxDestroyArray(eogData);
    eogData = NULL;
    eogDataElements = NULL;

    // Close file
    if (matClose(matOutputFile) != 0)
    {
        printf("ERROR: Error closing file %s\n", fileName.c_str());
        return;
    }

    return;
}