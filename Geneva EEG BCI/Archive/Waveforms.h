#pragma once
#include "WaveformEntry.h"

class Waveforms
{
public:
	Waveforms(void);
	~Waveforms(void);

public:
	const static int default_numberOfEntries = 10;
	int numberOfEntries;
	WaveformEntry entries[default_numberOfEntries];
};

