#ifndef SAVITZKYGOLAYFILTERALGORITHM_H
#define SAVITZKYGOLAYFILTERALGORITHM_H

#ifndef DATABUFFER_H
#include "DataBuffer.h"
#endif

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#ifndef FFTWTRANSFORM_H
#include "FftwTransform.h"
#endif

#ifndef FFTWREVERSETRANSFORM_H
#include "FftwReverseTransform.h"
#endif

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Savitzky-Golay filter algorithm
* @ingroup coreimpl
* @author joerg
*
* Implementation detail: There is a windowing-effect if the input time series alone
* is folded with the smoothing kernel. It shows that all values are shifted by half
* the kernel window size to the right. This effect is removed in this implementation
* by padding the input time series before and after with zeros. Input sizes are doubled
* by this approach (2*m_windowSize == m_bufferSize). Same applies for the kernel.
*/
class DLL_EXPORT CSavitzkyGolayFilterAlgorithm
{
private:
    /**
    * Simple and hopefully efficient implementation of ISavitzkyGolayResult.
    */
    class CResult
    {
        friend CSavitzkyGolayFilterAlgorithm;
    private:
        CResult(const size_t& windowSize, const size_t& numberOfChannels);
        CResult(const CResult& other);

    public:
        virtual ~CResult();

    public:
        size_t getWindowSize() const;

        size_t getNumberOfChannels() const;

        const double* const getChannel(const size_t& channelNumber) const;
	
        const double* const getTimes() const;

		/////////////////////////////////////
		void getBuffer(double**);
		//////////////////////////////////////


    private:
        void setWindow(const size_t& channelNumber, const double* const window);
        void setTimes(const double* const times);

        /// not implemented
        CResult& operator=(const CResult& other);

    private:
        const size_t  m_windowSize;
        const size_t  m_numberOfChannels;
        double*       m_windows;
        double*       m_times;
    };

public:
    /**
    * Constructor.
    *
    * @param leftWidth        How many time steps to the left should be used for smoothing
    * @param rightWidth       How many time steps to the right should be used for smoothing
    * @param polynomialOrder  order of the polynomial used for smoothing
    * @param windowSize       length of the time series that has to be smoothed,, must be odd.
    * @param numberOfChannels number of channels
    */
    CSavitzkyGolayFilterAlgorithm(const size_t& leftWidth
        , const size_t& rightWidth
        , const size_t& polynomialOrder
        , const size_t& windowSize
		, const size_t& stepSize
        , const size_t& numberOfChannels);

    /// destructor
    virtual ~CSavitzkyGolayFilterAlgorithm();

public:
    /**
    * Expects one IAmplifierFrame in input. Every windowSize-th step a smoothing 
    * is performed and result contains a ISavitzkyGolayResult instance. Otherwise 
    * result is empty.
    */
    void process(const size_t& noOfSamples, double** result, double** input);

    /**
    * Helper function. Calculates the Savitzky-Golay coefficients.
    * TODO: find out, if this method needs to be public.
    *
    * @param leftWidth        How many time steps to the left should be used for smoothing
    * @param rightWidth       How many time steps to the right should be used for smoothing
    * @param polynomialOrder  order of the polynomial used for smoothing
    */
    static CDoubleVector getCoefficients(const size_t& leftWidth, const size_t& rightWidth, const size_t& polynomialOrder);

	/*
    void reset();

    size_t getNumberOfInputData(); 

    size_t getNumberOfOutputData();

    std::string getInputDataName(const size_t& inputIndex);

    std::string getOutputDataName(const size_t& outputIndex);
	*/

private:
    void initialize(const size_t& leftWidth, const size_t& rightWidth, const size_t& polynomialOrder);
    /// not implemented
    CSavitzkyGolayFilterAlgorithm& operator=(const CSavitzkyGolayFilterAlgorithm& other);

private:
    /// polynomial order
    const size_t              m_polynomialOrder;
    /// all m_windowSize steps a smoothing is performed
    const size_t              m_windowSize;
    /** 
    * always twice of m_windowSize if m_windowSize is even, 2 * m_windowsSize - 1 otherwise.
    * (to avoid windowing-effect, explanation in class description)
    */
    const size_t              m_bufferSize;
	/// all m_windowSize steps a smoothing is performed
	const size_t			  m_stepSize;
    /// real spectrum of Savitzky Golay coefficients
    CDoubleVector             m_realSpectrum;
    /// imaginary spectrum of Savitzky Golay coefficients
    CDoubleVector             m_imagSpectrum;  
    /// for Fourier transform
    CFftwTransform*        m_fft;
    /// for inverse Fourier transform
    CFftwReverseTransform* m_ifft;
    /// buffer for the values of the frames that have not been smoothed yet
    CDataBuffer       m_buffer;
    /// counts how much frames have arrived since last smoothing operation
    size_t                    m_timeStep;
    /// helper variable. kept as member for the sake of speed.
    CDoubleVector             m_realProduct;
    /// helper variable. kept as member for the sake of speed.
    CDoubleVector             m_imagProduct;    
    /// helper variable. kept as member for the sake of speed.
    CDoubleVector             m_inputRealSpectrum;
    /// helper variable. kept as member for the sake of speed.
    CDoubleVector             m_inputImagSpectrum;
};

#endif // SAVITZKYGOLAYFILTERALGORITHM_H

