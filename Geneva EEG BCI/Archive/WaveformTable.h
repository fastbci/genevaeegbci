#pragma once
#include "WaveformTableEntry.h"

class WaveformTable
{
public:
	WaveformTable(void);
	WaveformTable(int en);
	~WaveformTable(void);

public:
	const static int default_numberOfEntries = 90;
	int numberOfEntries;
	WaveformTableEntry entries[default_numberOfEntries];
};

