///*  linreg.h
//Linear Regression calculation class
//
//
//This class implements a standard linear regression on
//experimental data using a least squares fit to a straight
//line graph.  Calculates coefficients a and b of the equation:
//
//y = a + b * x
//
//for data points of x and y.  Also calculates the coefficient of
//determination, the coefficient of correlation, and standard
//error of estimate.
//
//The value n (number of points) must be greater than 2 to
//calculate the regression.  This is primarily because the
//standard error has a (N-2) in the denominator.
//
//Check haveData() to see if there is enough data in
//LinearRegression to get values.
//
//You can think of the x,y pairs as 2 dimensional points.
//The class Point2D is included to allow pairing x and y
//values together to represent a point on a plane.
//
//*/
//
//#ifndef _LINREG_H_
//#define _LINREG_H_
//
//#include <fstream>
//#include <iostream>
//#include <stdio.h>
//
//#include <math.h>
//#include <algorithm>
//#include <sstream>
//#include <vector>
//#include <string>
//
//#pragma once
//
//#define PI 3.1416
//
//
//// **************** Point2D ******************
//
//class Point2D
//{
//public:
//	Point2D(double X = 0.0, double Y = 0.0) : x(X), y(Y) { }
//
//	void setPoint(double X, double Y) { x = X; y = Y; }
//	void setX(double X) { x = X; }
//	void setY(double Y) { y = Y; }
//
//	double getX() const { return x; }
//	double getY() const { return y; }
//
//private:
//	double x, y;
//
//};
//
//
//
//// *******************************************************
////					LINEAR REGRESSION 
//// *******************************************************
//
//
//
//class LinearRegression
//{
////friend ostream& operator<<(ostream& out, LinearRegression& lr);
//
//public:
//	// Constructor using an array of Point2D objects
//	// This is also the default constructor
//	LinearRegression(Point2D *p = 0, long size = 0);
//
//	// Constructor using arrays of x values and y values
//	LinearRegression(double *x, double *y, long size = 0);
//
//	virtual void addXY(const double& x, const double& y);
//	void addPoint(const Point2D& p) { addXY(p.getX(), p.getY()); }
//
//	// Must have at least 3 points to calculate
//	// standard error of estimate.  Do we have enough data?
//	int haveData() const { return (n > 2 ? 1 : 0); }
//	long items() const { return n; }
//
//
//	virtual double getA() const { return a; }
//	virtual double getB() const { return b; }
//
//	double getCoefDeterm() const  { return coefD; }
//	double getCoefCorrel() const { return coefC; }
//	double getStdErrorEst() const { return stdError; }
//
//
//	virtual double estimateY(double x) const { return (a + b * x); }
//
//protected:
//	long n;             // number of data points input so far
//	double sumX, sumY;  // sums of x and y
//	double sumXsquared, // sum of x squares
//		sumYsquared;	// sum y squares
//	double sumXY;       // sum of x*y
//
//	double a, b;        // coefficients of f(x) = a + b*x
//	double coefD,       // coefficient of determination
//		coefC,			// coefficient of correlation
//		stdError;		// standard error of estimate
//
//	void Calculate();   // calculate coefficients
//	void Calculate_Ed();
//};
//
//
//
//
//// *******************************************************
////							LMS 
//// *******************************************************
//
//class LMS{
//
//	// _y = sample of de-noised signal
//	// _W = vector of coefficients for FIR filter
//	// _Wsize = order of the filter
//	// _mu = convergence parameter (how fast the filter fits the signal)
//	// _alpha = forgetting factor (between  0 y 1)
//	// _Px = auxiliary vector with previous samples given to the filter (L-1 the given now)
//	// _sigma = power normalisation factor (difference between alg. LMSN and LMS).
//private:
//
//	int _Wsize;
//	int _idx;
//
//	double _y; // assume 1-dimensional for now
//	double _sigma;
//	double _alpha;
//	double _mu;
//	//vector<double> _W;
//	//vector<double> _Px;
//	//vector<double> _alpha_vec;
//	//vector<double> _sigma_vec;
//
//	double *_W;
//	double *_Px;
//	double *_alpha_vec;
//	double *_sigma_vec;
//
//
//public:
//
//	LMS(){};  // Default constructor
//	
//	~LMS(){
//		//_W.clear();
//		//_Px.clear();
//		//_alpha_vec.clear();
//		//_sigma_vec.clear();
//
//		delete[] _W;
//		_W = 0;
//		delete[] _Px;
//		_Px = 0;		
//		delete[] _alpha_vec;
//		_alpha_vec = 0;		
//		delete[] _sigma_vec;
//		_sigma_vec = 0;		
//	}
//	
//	LMS(const int &); 
//	LMS(const int &, const double& , const double& , const double& ); // constructor
//	LMS(const int &, const double& , double *, double *); // constructor for vectors (multDim)
//
//	virtual double eval(const double &, const double &); 
//	virtual void getPrediction(double &);
//	virtual bool eval_multDim(double [], const double &d);
//	virtual double getWeight(const int &);
//	virtual void increase_stepSize(const double &);
//
//};
//
//
//// *******************************************************
////						BUTTER FILTER 
//// *******************************************************
//
//// CALCULATION OF COEFS 
//double *ComputeNumCoeffs(int FilterOrder,double Lcutoff, double Ucutoff, double *DenC);
//double *ComputeDenCoeffs( int FilterOrder, double Lcutoff, double Ucutoff );
//
//// SIGNAL FILTERING FUNCTION
//void butterFilter(int ord, double *a, double *b, int np, std::vector<double> &x, double *y);
//void butterFilter_ed(int ord, double a[], double b[], int np, double *x, double *y);
//
//
//
//
//#endif  // end of linreg.h
//
