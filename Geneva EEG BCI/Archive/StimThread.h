#pragma once

ref class StimThread
{
public:
	static bool Run;
	static int ThreadCount;
	static void ThreadProc();
};
