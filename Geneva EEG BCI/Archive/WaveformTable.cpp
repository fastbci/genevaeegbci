#include "stdafx.h"
#include "WaveformTable.h"


WaveformTable::~WaveformTable(void)
{
}

WaveformTable::WaveformTable()
{
	numberOfEntries = 0;
	for(int i=0; i<default_numberOfEntries; i++)
	{
		WaveformTableEntry e = WaveformTableEntry();
		entries[i] = e;
	}
}

// General Constructor
WaveformTable::WaveformTable(int en)
{
	numberOfEntries = min(en,default_numberOfEntries); // never more than "default_numberOfEntries"
	for(int i=0; i<en; i++)
	{
		WaveformTableEntry e = WaveformTableEntry();
		entries[i] = e;
	}
}