#include "LfpMldaClassifier.h"

#include "stdafx.h"
#include "ap.h"
#include <memory.h>
#include <stdio.h>
#include <iostream>
#include <assert.h>
#include <math.h>

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

CLfpMldaClassifier::CLfpMldaClassifier(std::string modelFileName
	, const size_t& sampleRate
    , const size_t& numberOfChannels
    , const size_t& windowSize
	, const size_t& fftwindowSize)
: m_prodBuffer(0)
, m_templateSec(0)
, m_extendedFeatures(0)
, m_tmpProb(0)
, m_tmpDistVect(0)
, m_classMeans(0)
, m_covInvMatrix(0)
, m_currentState(0)
, m_stateCounter(0)
, m_sampleInd(0)
, m_tmpLogProb(0)
, m_fileExpectation(0)
, m_modelType(2)
, m_sgfFrameLen(0)
, m_sgfTemplate(0)
, m_windowSize(windowSize)
, m_sampleRate(sampleRate)
, m_featureDataBuffer(numberOfChannels, windowSize, CDataBuffer::LAST_IN_AT_END)
, m_tmpDataBuffer(0)
, m_fft(new CFftwTransform(fftwindowSize))
, m_fftWinLen(0)
, m_fftWinFunc(0)
, m_fftNorma(0)
, m_freqBottom(0)
, m_freqTop(0)
, m_lfpFeaType(0)
, m_realSpectrum(ceil(fftwindowSize / 2 + 1))
, m_imagSpectrum(ceil(fftwindowSize / 2 + 1))
, m_feature(0)
{
	double nclass;
	double nfeat;
	double expect_th;
	double refper;
	double noOfTaps;
	double modelType;

	double lfpFeaType;
	double neuralLpfSgLength;
	double fftWinLength;
	double freqBottom;
	double freqTop;

	FILE* file = fopen(modelFileName.c_str(), "r");
	fscanf(file, "%le", &nclass);
	fscanf(file, "%le", &nfeat);
	fscanf(file, "%le", &expect_th);
	fscanf(file, "%le", &refper);
	fscanf(file, "%le", &noOfTaps);
	fscanf(file, "%le", &modelType);

	fscanf(file, "%le", &lfpFeaType);
	fscanf(file, "%le", &neuralLpfSgLength);
	fscanf(file, "%le", &fftWinLength);
	fscanf(file, "%le", &freqBottom);
	fscanf(file, "%le", &freqTop);

	m_noOfClass = size_t(nclass);
	m_noOfFeatures = size_t(nfeat);
	m_expectationThreshold = expect_th;
	m_refractoryPeriodStep = round(refper * m_sampleRate);
	m_noOfTaps = size_t(noOfTaps);
	m_modelType = size_t(modelType);
	
	m_sampleInd = new size_t[m_noOfTaps];
	m_tmpLogProb = new double[m_noOfClass];
	m_expectation = new double[m_noOfClass];

	m_lfpFeaType = size_t(lfpFeaType);
	m_sgfFrameLen = size_t(neuralLpfSgLength);
	m_fftWinLen = size_t(fftWinLength);
	m_freqBottom = size_t(freqBottom);
	m_freqTop = size_t(freqTop);
	//m_expectationThreshold =  0.6;
	if (m_lfpFeaType == 2)
	{
		m_noOfChannels = size_t(m_noOfFeatures / m_noOfTaps / 2);
	}
	else
	{
		m_noOfChannels = size_t(m_noOfFeatures / m_noOfTaps);
	}
	m_maxWinLen = size_t(max(m_sgfFrameLen, m_fftWinLen));

	m_sgfTemplate = new double[m_sgfFrameLen];
	for (size_t i = 0; i < m_sgfFrameLen; i++)
	{
		fscanf(file, "%le", &m_sgfTemplate[i]);
	}

	m_fftWinFunc = new double[m_fftWinLen];
	for (size_t i = 0; i < m_fftWinLen; i++)
	{
		fscanf(file, "%le", &m_fftWinFunc[i]);
	}

	m_noOfFreq = size_t(ceil(m_fftWinLen/2+1));
	m_fftNorma = new double*[m_noOfChannels];
	for (size_t ch = 0; ch <m_noOfChannels; ch++)
	{
		m_fftNorma[ch] = new double[m_noOfFreq];
		for (size_t fr = 0; fr < m_noOfFreq; fr++)
		{
			fscanf(file, "%le", &m_fftNorma[ch][fr]);
		}
	}

	m_templateSec = new double[m_noOfTaps];
	for (size_t i = 0; i<m_noOfTaps; i++)
	{
		fscanf(file, "%le", &m_templateSec[i]);
	}

	m_prodBuffer = new double[m_noOfFeatures];

	m_classMeans = new double*[m_noOfClass];
	for (size_t cl = 0; cl<m_noOfClass; cl++)
	{
		m_classMeans[cl] = new double[m_noOfFeatures];
		for (size_t ft = 0; ft<m_noOfFeatures; ft++)
		{
			fscanf(file, "%le", &m_classMeans[cl][ft]);
		}
	}

	m_covInvMatrix = new double*[m_noOfFeatures];
	for (size_t i = 0; i<m_noOfFeatures; i++)
	{
		m_covInvMatrix[i] = new double[m_noOfFeatures];
	}

	for (size_t i = 0; i<m_noOfFeatures; i++)
	{
		for (size_t j = 0; j<m_noOfFeatures; j++)
		{
			fscanf(file, "%le", &m_covInvMatrix[j][i]);
		}
	}

	fclose(file);

	m_extendedFeatures = new double[m_noOfFeatures];
	m_tmpProb = new double[m_noOfClass];
	m_tmpDistVect = new double[m_noOfFeatures];

	m_tmpDataBuffer = new double*[numberOfChannels];
	
	m_fileExpectation = fopen("Expectation.txt", "w");
	m_feature = fopen("Features.txt", "w");
}


CLfpMldaClassifier::~CLfpMldaClassifier()
{

	if (m_prodBuffer != 0)
	{
		delete[] m_prodBuffer;
		m_prodBuffer = 0;
	}

	if (m_templateSec != 0)
	{
		delete[] m_templateSec;
		m_templateSec = 0;
	}

	if (m_extendedFeatures != 0)
	{
		delete[] m_extendedFeatures;
		m_extendedFeatures = 0;
	}

	if (m_tmpProb != 0)
	{
		delete[] m_tmpProb;
		m_tmpProb = 0;
	}

	if (m_tmpDistVect != 0)
	{
		delete[] m_tmpDistVect;
		m_tmpDistVect = 0;
	}

	if (m_sampleInd != 0)
	{
		delete[] m_sampleInd;
		m_sampleInd = 0;
	}

	if (m_tmpLogProb != 0)
	{
		delete[] m_tmpLogProb;
		m_tmpLogProb = 0;
	}

	if (m_expectation != 0)
	{
		delete[] m_expectation;
		m_expectation = 0;
	}

	if (m_sgfTemplate != 0)
	{
		delete[] m_sgfTemplate;
		m_sgfTemplate = 0;
	}

	if (m_tmpDataBuffer != 0)
	{
		delete[] m_tmpDataBuffer;
		m_tmpDataBuffer = 0;
	}

	if (m_fftWinFunc != 0)
	{
		delete[] m_fftWinFunc;
		m_fftWinFunc = 0;
	}

	if (m_fftNorma != 0)
	{
		for (size_t i = 0; i<m_noOfChannels; i++)
		{
			delete[] m_fftNorma[i];
			m_fftNorma[i] = 0;
		}
		delete[] m_fftNorma;
		m_fftNorma = 0;
	}

	if (m_classMeans != 0)
	{
		for (size_t i = 0; i<m_noOfClass; i++)
		{
			delete[] m_classMeans[i];
			m_classMeans[i] = 0;
		}
		delete[] m_classMeans;
		m_classMeans = 0;
	}

	if (m_covInvMatrix != 0)
	{
		for (size_t i = 0; i<m_noOfFeatures; i++)
		{
			delete[] m_covInvMatrix[i];
			m_covInvMatrix[i] = 0;
		}
		delete[] m_covInvMatrix;
		m_covInvMatrix = 0;
	}

	if (m_fileExpectation != 0)
	{
		fclose(m_fileExpectation);
		m_fileExpectation = 0;
	}

	if (m_feature != 0)
	{
		fclose(m_feature);
		m_feature = 0;
	}

	if (m_fft != 0)
	{
		delete m_fft;
		m_fft = 0;
	}
}

int CLfpMldaClassifier::getNoOfClass()
{
	return m_noOfClass;
}

size_t CLfpMldaClassifier::getModelType()
{
	return m_modelType;
}

size_t CLfpMldaClassifier::getFeatureType()
{
	return m_lfpFeaType;
}

double CLfpMldaClassifier::calcCovInvProd(double *X)
{
	double prod;

	//C*X
	for (size_t i = 0; i<m_noOfFeatures; i++)
	{
		prod = 0;
		for (size_t j = 0; j < m_noOfFeatures; j++)
		{
			prod = prod + X[j] * m_covInvMatrix[i][j];
		}
		m_prodBuffer[i] = prod;
	}

	//Y'*X
	prod = 0;
	for (size_t i = 0; i<m_noOfFeatures; i++)
	{
		prod = X[i] * m_prodBuffer[i] + prod;
	}

	return prod;
}

void CLfpMldaClassifier::calcExpectation(double* featureVector, double* expectation)
{
	for (size_t cl = 0; cl<m_noOfClass; cl++)
	{
		for (size_t ft = 0; ft<m_noOfFeatures; ft++)
		{
			m_tmpDistVect[ft] = featureVector[ft] - m_classMeans[cl][ft];
		}
		m_tmpProb[cl] = -calcCovInvProd(m_tmpDistVect) / 2;
	}

	// INIT
	memset(expectation, 0, m_noOfClass * sizeof(double));

	// LOOP for calculation of expectations
	double sum;
	for (size_t cl = 0; cl < m_noOfClass; cl++)
	{
		sum = 0.0;
		for (size_t na = 0; na<m_noOfClass; na++)
		{
			m_tmpLogProb[na] = m_tmpProb[na] - m_tmpProb[cl];
			sum = sum + exp(m_tmpLogProb[na]);
		}

		expectation[cl] = 1.0 / sum;
	}
}

void CLfpMldaClassifier::timeReslovedSelector(const size_t& noOfSamples, double** inputBuffer)
{
	//add current data buffer to featureDataBuffer
	m_featureDataBuffer.append(noOfSamples, inputBuffer);
	
	//retrieving the data from the buffer
	m_featureDataBuffer.getBuffer(m_tmpDataBuffer);

	CDoubleVector tmpLfcFeature(m_noOfChannels*m_noOfTaps, 0.0);
	CDoubleVector tmpHfcFeature(m_noOfChannels*m_noOfTaps, 0.0);
	//decide the center of sgf and fft window to extract features
	for (size_t tp = 0; tp<m_noOfTaps; tp++)
	{
		m_sampleInd[tp] = int(round(m_templateSec[tp] * m_sampleRate)) + m_windowSize - 1 - floor((m_maxWinLen-1)/2);
	}
	
	
	//extract Low-frequency-components
	if (m_lfpFeaType != 1)
	{
		double prod;
		for (size_t ch = 0; ch<m_noOfChannels; ch++)
		{
			for (size_t tp = 0; tp<m_noOfTaps; tp++)
			{
				prod = 0;
				for (size_t i = 0; i<m_sgfFrameLen; i++)
				{
					prod = m_tmpDataBuffer[ch][m_sampleInd[tp]- (m_sgfFrameLen-1)/2 + i] * m_sgfTemplate[i] + prod;
				}

				tmpLfcFeature[ch * m_noOfTaps + tp] = prod;
			}
		}
	}

	//extract High-frequency-components
	if (m_lfpFeaType != 0)
	{
		CDoubleVector timeSeries(m_fftWinLen, 0.0);
		size_t noOfLeftHalfPts = m_fftWinLen - 1 - floor((m_fftWinLen - 1) / 2);
		double hfcsum;

		for (size_t ch = 0; ch<m_noOfChannels; ch++)
		{
			for (size_t tp = 0; tp<m_noOfTaps; tp++)
			{
				hfcsum = 0;
				for (size_t i = 0; i<m_fftWinLen; i++)
				{
					timeSeries[i] = m_tmpDataBuffer[ch][m_sampleInd[tp] - noOfLeftHalfPts + i] * m_fftWinFunc[i];
				}
				
				m_fft->doFourierTransform(m_realSpectrum, m_imagSpectrum, timeSeries);

				for (size_t fr = m_freqBottom - 1; fr < m_freqTop; fr++)
				{
					hfcsum = hfcsum + sqrt(m_realSpectrum[fr]* m_realSpectrum[fr] + m_imagSpectrum[fr]* m_imagSpectrum[fr]) / sqrt(m_sampleRate) / m_fftNorma[ch][fr];
				}
				tmpHfcFeature[ch * m_noOfTaps + tp] = sqrt(hfcsum/(m_freqTop-m_freqBottom+1));
			}
		}
	}

	//assign tmpfeatures to extended feature according to different LFP feature type
	if (m_lfpFeaType == 0)
	{
		for (size_t i = 0; i < m_noOfFeatures; i++)
		{
			m_extendedFeatures[i] = tmpLfcFeature[i];
		}
	}
	else if (m_lfpFeaType == 1)
	{
		for (size_t i = 0; i < m_noOfFeatures; i++)
		{
			m_extendedFeatures[i] = tmpHfcFeature[i];
		}
	}
	else
	{
		for (size_t i = 0; i < m_noOfFeatures; i++)
		{
			if (i < m_noOfFeatures / 2)
			{
				m_extendedFeatures[i] = tmpLfcFeature[i];
			}
			else
			{
				m_extendedFeatures[i] = tmpHfcFeature[i - (m_noOfFeatures / 2)];
			}
		}
	}

	/*fprintf(m_feature, "\n start \n");
	fprintf(m_feature, "%d\n ", noOfSamples);
    for (size_t i = 0; i<m_noOfFeatures; i++)
	{
		fprintf(m_feature, "%f, ", m_extendedFeatures[i]);
	}
	fprintf(m_feature, "end\n\n");*/

}

int CLfpMldaClassifier::detectState(const size_t& noOfSamples, double** inputBuffer)
{
	int outState = 0;

	timeReslovedSelector(noOfSamples, inputBuffer);
	calcExpectation(m_extendedFeatures, m_expectation);

	if (m_logExpectationValues)
	{
		for (size_t cl = 0; cl<m_noOfClass; cl++)
		{
			fprintf(m_fileExpectation, "%f, ", m_expectation[cl]);
		}
		fprintf(m_fileExpectation, "\n");
	}

	switch (m_modelType)
	{
	case 0:
		outState = stateMachineUnilateral();
		break;

	case 1:
		outState = stateMachineUnilateral();
		if (outState > 0)
		{
			outState = outState + 2;
		}
		break;

	case 2:
		outState = stateMachineBilateral();
		break;

	case 3:
		outState = stateMachineUnilateral();
		// Change the states 1 and 2 into states 2 (right foot swing / left foot stance) and 4 (left foot swing / right foot stance)
		if (outState > 0)
		{
			outState = 2 * outState;
		}
		break;

	case 4:
		outState = stateMachineUnilateral();
		// Change the states 1 and 2 into states 2 (right foot swing / left foot stance) and 4 (left foot swing / right foot stance)
		if (outState > 0)
		{
			outState = 2 * outState - 1;
		}
		break;
	}

	return outState;
}

int CLfpMldaClassifier::stateMachineUnilateral(void)
{
	int outState = 0; // tells the main application whether to modify the current stimulation or not: 1 - no change; all other indices - change the state to that index

	switch (m_currentState)
	{

	case 0:  //currently neither in stance nor swing
		if (m_expectation[1] >= m_expectationThreshold)
		{
			m_currentState = 1;
			outState = 1;
			m_stateCounter = 0;
		}
		else if (m_expectation[2] >= m_expectationThreshold)
		{
			m_currentState = 2;
			outState = 2;
			m_stateCounter = 0;
		}
		break;

	case 1: //currently in stance
		if (m_expectation[2] >= m_expectationThreshold)
		{
			m_currentState = 2;
			outState = 2;
			m_stateCounter = 0;
		}
		else
		{
			m_stateCounter = m_stateCounter + 1;
			outState = 0;
			if (m_stateCounter >= m_refractoryPeriodStep)
			{
				m_currentState = 0;
				m_stateCounter = 0;
			}
		}
		break;

	case 2: //currently in swing
		if (m_expectation[1] >= m_expectationThreshold)
		{
			m_currentState = 1;
			outState = 1;
			m_stateCounter = 0;
		}
		else
		{
			m_stateCounter = m_stateCounter + 1;
			outState = 0; //I'm still in swing so i send NONE as out
			if (m_stateCounter >= m_refractoryPeriodStep)
			{
				m_currentState = 0;
				m_stateCounter = 0;
			}
		}
		break;
	}

	return outState;
}

int CLfpMldaClassifier::stateMachineBilateral(void)
{
	int outState = 0;

	switch (m_currentState)
	{

	case 0:  //currently no stimulation event
		for (size_t st = 1; st<m_noOfClass; st++)
		{
			if (m_expectation[st] >= m_expectationThreshold)
			{
				m_currentState = st;
				outState = st;
				m_stateCounter = 0;
				break;
			}
		}
		break;

	default: //currently in a stimulation state
		bool stateChanged = false;
		for (size_t st = 1; st<m_noOfClass; st++)
		{
			if ((st != m_currentState) & (m_expectation[st] >= m_expectationThreshold))
			{
				m_currentState = st;
				outState = st;
				m_stateCounter = 0;
				stateChanged = true;
				break;
			}
		}

		if (!stateChanged)
		{
			m_stateCounter = m_stateCounter + 1;
			outState = 0;
			if (m_stateCounter >= m_refractoryPeriodStep)
			{
				m_currentState = 0;
				m_stateCounter = 0;
			}
			break;
		}
	}

	return outState;
}
