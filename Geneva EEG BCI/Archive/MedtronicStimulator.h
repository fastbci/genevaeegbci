///***************************************************************************
//
//// From the TERMINAL: 	 
//// curl -v --data-urlencode -d "http://128.179.164.231:9090/therapy/state=off?";
//*/
//
//// 1. Add security (min max channels, amps etc)
//// 2. automatize IP address
//// Why does it chrash if no connection?
//
//// Check: Modify/erase 1 line of table without re.writing everything? Possible?
//
//// 3. Balckrock EMG code
//
//#ifndef _LINKMEDTRONIC_
//#define _LINKMEDTRONIC_
//#endif
//
//#pragma once
//#include "afxcmn.h"
//#include "afxwin.h"
//#include <stdio.h>
//#include <stdlib.h>
//#include <iostream>
//#include <cmath>
//#include <string>
////#include <curl/curl.h>
//
//#include "WaveformTable.h"
//#include "Waveforms.h"
//
//
//struct response_data
//{
//	std::string readBuffer;
//
//	// ERROR MESSAGES
//	char delay[6]; 
//	char status;
//	CString extract_ErrorFromResponse(std::string response);
//
//	// PERFORMANCE COUNTERS
//	static const size_t m_noOfPerfCounters = 4;
//	static const size_t m_bufferSize = 100;
//
//	size_t noOfPastIDs;
//	size_t queueCount;
//	size_t commandID;
//	size_t succesfulIDs[m_bufferSize];
//	LONGLONG succesfullPerfCount[m_bufferSize][m_noOfPerfCounters];
//	void extract_dataFromResponse(std::string response);
//};
//
//class CMedtronicStimulator
//{
//public:
//	CMedtronicStimulator(void);
//	CMedtronicStimulator(std::string);
//	virtual ~CMedtronicStimulator(void);
//
//private:
//	char* int2charPointer(const int val);
//	char* double2charPointer(const float val);
//	static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp);
//
//public:
//	static int bin2dec(const bool activeChannels[10]);
//	void stim_changeState(char *state);
//	void stim_changeAmplitude(int waveformNum_val, double amplitude_val);
//	void stim_changePulseWidth(int waveformNum_val, double pulsewidth_val);
//	void stim_changeFrequency( double frequency_val, int modeAccurate_flag);
//	void stim_updateTable(int writeIndex_val , int runIndex_val, WaveformTable table);
//	void stim_updateTable_onlyIndex(int runIndex_val);
//	response_data stim_updateTable_onlyIndexWithResponse(int runIndex_val);
//	void stim_updateTable_IndexAndFreq(int runIndex_val, int freqVal);
//	void setIP(std::string);
//
//	std::string m_initializeResponse;
//	int stim_initialize(int isCVMode , Waveforms waveforms, WaveformTable table);
//
//private:
//	std::string m_IP;
////	CURL* m_curl;
//};
