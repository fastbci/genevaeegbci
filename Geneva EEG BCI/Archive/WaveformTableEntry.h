#pragma once
class WaveformTableEntry
{
public:
	WaveformTableEntry(void);
	WaveformTableEntry(int Dur, int En, int Freq);
	~WaveformTableEntry(void);

public:
	int Duration;
	int Enables;
	int Frequency;
};
