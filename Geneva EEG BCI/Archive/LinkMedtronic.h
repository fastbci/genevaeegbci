/***************************************************************************

// From the TERMINAL: 	 
// curl -v --data-urlencode -d "http://128.179.164.231:9090/therapy/state=off?";
*/

// 1. Add security (min max channels, amps etc)
// 2. automatize IP address
// Why does it chrash if no connection?

// Check: Modify/erase 1 line of table without re.writing everything? Possible?

// 3. Balckrock EMG code

#ifndef _LINKMEDTRONIC_
#define _LINKMEDTRONIC_

#include "afxcmn.h"
#include "afxwin.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <cmath>
#include <string>
#include <curl/curl.h>
#include "cbhwlib3.8.h" //Blackrock lib 


//std::string url_IP = "128.179.166.236";
//std::string url_IP = "128.179.164.231";
//std::string url_IP = "127.0.0.1";
std::string url_IP = "10.6.5.183";
//std::string url_IP = "192.168.137.13";


using namespace std;

// --- GENERAL FUNCTIONS

char * int2charPointer(const int val)
{
	string s = to_string(static_cast<long long>(val));

	char* p = new char[s.length()];
	strcpy(p, s.c_str());

	return p;
}

char * double2charPointer(const float val)
{
	string s = to_string(static_cast<long double>(val));

	char* p = new char[s.length()];
	strcpy(p, s.c_str());

	return p;
}


int bin2dec(const bool activeChannels[10] )
{
	double channelsDecimal = 0;
	for(int i=0;i<10;i++){
		channelsDecimal += int(activeChannels[i])*pow(2.0,i) ;
	}
	return int(channelsDecimal);
}

/*
int bin2dec(const bool activeChannels[5] )
{
	int channelsDecimal = activeChannels[0]*16 + activeChannels[1]*8 + activeChannels[2]*4 + activeChannels[3]*2 + activeChannels[4]*1;

	return channelsDecimal;
}
*/

// --- FUNCTION TO SAVE RETURNING DATA

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

struct response_data
{
	char delay[6]; 
	char status;

	void extract_dataFromResponse(string response)
	{
		size_t beg_response = response.find("{");
		size_t end_response = response.find("}");
		size_t separator_response = response.find_last_of(",");

		size_t length_delay = separator_response - (beg_response+17);
		size_t l = response.copy(delay,length_delay,beg_response+17);
		delay[length_delay]='\0';

		status = response[separator_response+10];
	}
};



// --- STIMULATOR CLASSES & FUNCTIONS

class WaveformTableEntry
{
public:
	int Duration;
	int Enables;
	int Frequency;

	// Default Constructor
	WaveformTableEntry()
		: Duration(0)
		, Enables(0)
		, Frequency(40)
	{
	}

	// General Constructor
	WaveformTableEntry(int Dur, int En, int Freq)
	{
		Duration=Dur;
		Enables=En;
		Frequency = Freq;
	}
};

class WaveformTable
{
public:
	const static int default_numberOfEntries = 90;
	int numberOfEntries;
	WaveformTableEntry entries[default_numberOfEntries];

	// Default Constructor
	WaveformTable()
	{
		numberOfEntries = 0;
		for(int i=0; i<default_numberOfEntries; i++)
		{
			WaveformTableEntry e = WaveformTableEntry();
			entries[i] = e;
		}
	}

	// General Constructor
	WaveformTable(int en)
	{
		numberOfEntries = min(en,default_numberOfEntries); // never more than "default_numberOfEntries"
		for(int i=0; i++; i<en)
		{
			WaveformTableEntry e = WaveformTableEntry();
			entries[i] = e;
		}
	}

};



void stim_changeState(char *state)
{
	CURL *curl;
	CURLcode res;

	static string readBuffer;
	//response_data rd;


	// In windows, this will init the winsock stuff
	curl_global_init(CURL_GLOBAL_ALL);

	// get a curl handle 
	curl = curl_easy_init();

	if(curl)
	{
		// TURN ON VERBOSE
		curl_easy_setopt(curl, CURLOPT_VERBOSE,1L);

		// OPTIONS
		curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 0);
		curl_easy_setopt(curl, CURLOPT_HEADER, 1);


		readBuffer.clear();
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer); 


		// DEFINE DATA
		std::string url_h1 = "http://";
		std::string url_h2 = ":9090/therapy?state=";
		//char *state = "off";

		char url[100];   // array to hold the full URL.
		strcpy(url,url_h1.c_str()); 
		strcat(url,url_IP.c_str());
		strcat(url,url_h2.c_str());
		strcat(url,state);

		curl_easy_setopt(curl, CURLOPT_URL, url );
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");

		// PERFORM REQUEST
		res = curl_easy_perform(curl);

		// Check for errors
		if(res != CURLE_OK)
		{
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
				curl_easy_strerror(res));
		}


		// Get info returnd from stimulator
		//rd.extract_dataFromResponse(readBuffer);

		// always cleanup 
		curl_easy_cleanup(curl);

	}

	curl_global_cleanup();

	//return rd;
}

void stim_changeAmplitude(int waveformNum_val, double amplitude_val)
{
	CURL *curl;
	CURLcode res;

	static string readBuffer;
	//response_data rd;


	// Set values as char* to be sent over HTTP
	char* waveformNum = int2charPointer(waveformNum_val);
	char* amplitude = double2charPointer(amplitude_val);


	// In windows, this will init the winsock stuff
	curl_global_init(CURL_GLOBAL_ALL);

	// get a curl handle 
	curl = curl_easy_init();

	if(curl) {

		// TURN ON VERBOSE
		curl_easy_setopt(curl, CURLOPT_VERBOSE,1L);

		// OPTIONS
		curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 0);
		curl_easy_setopt(curl, CURLOPT_HEADER, 1);

		readBuffer.clear();
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer); 

		// DEFINE DATA
		char * url_h1 = "http://";
		char * url_h2 = ":9090/therapy/modifyamplitude?";
		char *waveformTxt = "waveform=";
		char *amplitudeTxt = "&amplitude=";

		char url[100];   // array to hold the full URL.
		strcpy(url,url_h1); 
		strcat(url, url_IP.c_str());
		strcat(url, url_h2); 
		strcat(url,waveformTxt); strcat(url, waveformNum );
		strcat(url,amplitudeTxt); strcat(url, amplitude );


		curl_easy_setopt(curl, CURLOPT_URL, url );
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");

		// PERFORM REQUEST
		res = curl_easy_perform(curl);

		// Check for errors
		if(res != CURLE_OK)
		{
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
				curl_easy_strerror(res));
		}

		// Get info returnd from stimulator
		//rd.extract_dataFromResponse(readBuffer);

		// always cleanup 
		curl_easy_cleanup(curl);

	}

	curl_global_cleanup();

	//return rd;
}

void stim_changeFrequency( double frequency_val, int modeAccurate_flag)
{
	CURL *curl;
	CURLcode res;

	static string readBuffer;
	//response_data rd;


	// Set values as char* to be sent over HTTP
	char* frequency = double2charPointer(frequency_val);
	char* mode;
	if( modeAccurate_flag == 0){
		mode = "false";
	}
	else{
		mode = "true";
	}



	// In windows, this will init the winsock stuff
	curl_global_init(CURL_GLOBAL_ALL);

	// get a curl handle 
	curl = curl_easy_init();

	if(curl) {

		// TURN ON VERBOSE
		curl_easy_setopt(curl, CURLOPT_VERBOSE,1L);

		// OPTIONS
		curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 0);
		curl_easy_setopt(curl, CURLOPT_HEADER, 1);

		readBuffer.clear();
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer); 

		// DEFINE DATA
				// DEFINE DATA
		char * url_h1 = "http://";
		char * url_h2 = ":9090/therapy/modifyfrequency?";
		char *frequencyTxt = "frequency=";
		char *modeTxt = "&fast=";

		char url[100];   // array to hold the full URL.
		strcpy(url,url_h1); 
		strcat(url, url_IP.c_str());
		strcat(url, url_h2); 

		strcat(url,frequencyTxt); strcat(url, frequency);
		strcat(url,modeTxt); strcat(url, mode);

		curl_easy_setopt(curl, CURLOPT_URL, url );
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");

		// PERFORM REQUEST
		res = curl_easy_perform(curl);

		// Check for errors
		if(res != CURLE_OK)
		{
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
				curl_easy_strerror(res));
		}

		// Get info returnd from stimulator
		// rd.extract_dataFromResponse(readBuffer);

		// always cleanup 
		curl_easy_cleanup(curl);

	}

	curl_global_cleanup();

	//return rd;
}

void stim_updateTable(int writeIndex_val , int runIndex_val, WaveformTable table)
{

	CURL *curl;
	CURLcode res;

	static string readBuffer;
	//response_data rd;


	// Set values as char* to be sent over HTTP
	char* writeIndex = int2charPointer(writeIndex_val);
	char* runIndex = int2charPointer(runIndex_val);


	// In windows, this will init the winsock stuff
	curl_global_init(CURL_GLOBAL_ALL);

	// get a curl handle 
	curl = curl_easy_init();

	if(curl)
	{
		// TURN ON VERBOSE
		curl_easy_setopt(curl, CURLOPT_VERBOSE,1L);

		// OPTIONS
		curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 0);
		curl_easy_setopt(curl, CURLOPT_HEADER, 1);

		readBuffer.clear();
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer); 


		// DEFINE DATA
		char * url_h1 = "http://";
		char * url_h2 = ":9090/therapy/updatewaveformtable?";
		char *writeIndexTxt = "writeIndex=";
		char *runIndexTxt = "&runIndex=";
		char *tableTxt = "&table=";

		char url[1000];   // array to hold the URL.
		strcpy(url,url_h1); 
		strcat(url,url_IP.c_str());
		strcat(url,url_h2);
		strcat(url,writeIndexTxt); strcat(url, writeIndex );
		strcat(url,runIndexTxt); strcat(url, runIndex );
		strcat(url,tableTxt); 

		// The table needs to be urlencoded... 
		// Save it in a separate array, and concatenate at the end once encoded
		char *entriesTxt_start = "{'Entries':[";
		char *entriesTxt_end = "]}";

		char url_table[1000]; 
		strcpy(url_table,entriesTxt_start); 

		int N = table.numberOfEntries;
		for( int i=0; i<N; i++)
		{
			strcat(url_table, "{'Duration':");
			strcat(url_table, int2charPointer(table.entries[i].Duration) );
			strcat(url_table, ",'Enables':");
			strcat(url_table, int2charPointer(table.entries[i].Enables));
			strcat(url_table, ",'Frequency':");
			strcat(url_table, int2charPointer(table.entries[i].Frequency));
			strcat(url_table, "}");

			if( i!=N-1 )
			{
				strcat(url_table, ",");
			}
		}
		strcat(url_table,entriesTxt_end); 

		// url-encoding

		/*
		char* testUrl = "{'Entries':[{'Duration':100,'Enables':1,'Frequency':40},{'Duration':100,'Enables':1,'Frequency':40},{'Duration':100,'Enables':1,'Frequency':40}]}";
		char *urlTable2pointer = &testUrl[0];
		urlTable2pointer = curl_easy_escape(curl , urlTable2pointer , 0);
		strcat(url,urlTable2pointer);
		curl_easy_setopt(curl, CURLOPT_URL, url );
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
		*/

		char *urlTable2pointer = &url_table[0];
		urlTable2pointer = curl_easy_escape(curl , urlTable2pointer , 0);

		// Concatenate everything
		strcat(url,urlTable2pointer);

		curl_easy_setopt(curl, CURLOPT_URL, url );
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");

		// PERFORM REQUEST
		res = curl_easy_perform(curl);

		// Check for errors
		if(res != CURLE_OK)
		{
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
				curl_easy_strerror(res));
		}

		// Get info returnd from stimulator
		//rd.extract_dataFromResponse(readBuffer);

		// always cleanup 
		curl_easy_cleanup(curl);

	}

	curl_global_cleanup();

	//return rd;
}

void stim_updateTable_onlyIndex(int runIndex_val)
{

	CURL *curl;
	CURLcode res;

	static string readBuffer;
	//response_data rd;


	// Set values as char* to be sent over HTTP
	char* runIndex = int2charPointer(128 + runIndex_val);
	// char* runIndex = int2charPointer(runIndex_val);

	// In windows, this will init the winsock stuff
	curl_global_init(CURL_GLOBAL_ALL);

	// get a curl handle 
	curl = curl_easy_init();

	if(curl)
	{
		// TURN ON VERBOSE
		curl_easy_setopt(curl, CURLOPT_VERBOSE,1L);

		// OPTIONS
		curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 0);
		curl_easy_setopt(curl, CURLOPT_HEADER, 1);

		readBuffer.clear();
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer); 


		// DEFINE DATA
		char * url_h1 = "http://";
		char * url_h2 = ":9090/therapy/updatewaveformtable?";
		char *writeIndexTxt = "writeIndex=";
		char *runIndexTxt = "&runIndex=";
		char *tableTxt = "&table={}";
		// char *tableTxt = "&table={'Entries':[{�Duration�:25,�Enables�:15,�Frequency�:80}]}";


		char url[1000];   // array to hold the URL.
		strcpy(url,url_h1); 
		strcat(url,url_IP.c_str());
		strcat(url,url_h2);
		strcat(url,writeIndexTxt);
		strcat(url, "0" );
		strcat(url,runIndexTxt);
		strcat(url, runIndex );
		strcat(url,tableTxt); 

		curl_easy_setopt(curl, CURLOPT_URL, url );
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");

		// PERFORM REQUEST
		res = curl_easy_perform(curl);

		// Check for errors
		if(res != CURLE_OK)
		{
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
				curl_easy_strerror(res));
		}

		// Get info return from stimulator
		// rd.extract_dataFromResponse(readBuffer);

		// always cleanup 
		curl_easy_cleanup(curl);
	}

	curl_global_cleanup();
}

void stim_updateTable_IndexAndFreq(int runIndex_val, int freqVal)
{

	CURL *curl;
	CURLcode res;

	static string readBuffer;
	//response_data rd;


	// Set values as char* to be sent over HTTP
	char* runIndex = int2charPointer(128+runIndex_val);
	char* freqText = int2charPointer(freqVal);


	// In windows, this will init the winsock stuff
	curl_global_init(CURL_GLOBAL_ALL);

	// get a curl handle 
	curl = curl_easy_init();

	if(curl) {

		// TURN ON VERBOSE
		curl_easy_setopt(curl, CURLOPT_VERBOSE,1L);

		// OPTIONS
		curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 0);
		curl_easy_setopt(curl, CURLOPT_HEADER, 1);

		readBuffer.clear();
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer); 


		// DEFINE DATA
		char * url_h1 = "http://";
		char * url_h2 = ":9090/therapy/updatewaveformtableandfrequency?";
		char *frequencyTxt = "frequency=";
		char *writeIndexTxt = "&writeIndex=";
		char *runIndexTxt = "&runIndex=";
		char *tableTxt = "&table={}";


		char url[1000];   // array to hold the URL.
		strcpy(url,url_h1); 
		strcat(url,url_IP.c_str());
		strcat(url,url_h2);
		strcat(url,frequencyTxt); strcat(url, freqText );
		strcat(url,writeIndexTxt); strcat(url, "0" );
		strcat(url,runIndexTxt); strcat(url, runIndex );
		strcat(url,tableTxt); 

		curl_easy_setopt(curl, CURLOPT_URL, url );
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");

		// PERFORM REQUEST
		res = curl_easy_perform(curl);

		// Check for errors
		if(res != CURLE_OK)
		{
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
				curl_easy_strerror(res));
		}

		// Get info returnd from stimulator
		//rd.extract_dataFromResponse(readBuffer);

		// always cleanup 
		curl_easy_cleanup(curl);

	}

	curl_global_cleanup();

	//return rd;
}



// --- MAIN
/*
response_data rd;

// 1. STATE OF THE STIMULATOR
char *state = "on"; // either: "on" / "off"
rd = stim_changeState(state);


// AMPLITUDE OF A PARTICULAR CHANNEL 
int channel= 2;  // Int [1:5]
double amplitude= 5.0; // Double [0-10.5V]
rd = stim_changeAmplitude( channel, amplitude );


// UPDATE TABLE
int writeIndex = 0; // Starts writing at this index - Value [0-69]
int runIndex = 0; // Value [0-69].

WaveformTable table = WaveformTable(2); // Create new table with 2 entries (70 entries max by default)
int Dur; 

Dur = 500; bool activeChannels[5] = {1,0,0,0,1};
table.entries[0] = WaveformTableEntry( Dur, bin2dec(activeChannels) ); // Update first entry
Dur = 600; bool activeChannels2[5] = {1,0,0,0,0};
table.entries[1] = WaveformTableEntry( Dur, bin2dec(activeChannels2) ); // Update second entry	stim_updateTable(writeIndex, runIndex, table);

rd = stim_updateTable(writeIndex, runIndex, table);

// Change one entry of table
Dur = 250;
table.entries[0] = WaveformTableEntry( Dur, bin2dec(activeChannels) ); // Update first entry
rd = stim_updateTable(writeIndex, runIndex, table); 

system("PAUSE");

*/


#endif