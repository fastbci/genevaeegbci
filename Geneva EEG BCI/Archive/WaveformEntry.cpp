#include "stdafx.h"
#include "WaveformEntry.h"


WaveformEntry::WaveformEntry(void)
{
}


WaveformEntry::WaveformEntry(double amp, double pw, int elec[16])
{
	amplitude = amp;
	pulseWidth = pw;
	for (int i=0; i<17; i++)
	{
		electrode[i] = elec[i];
	}
}


WaveformEntry::~WaveformEntry(void)
{
}
