#include "StdAfx.h"
#include "StimThread.h"
#using <mscorlib.dll>
#include <iostream>
#include <conio.h>   // For _kbhit()

// [C++]
// Compile using /clr option.
using namespace System;
using namespace System::Threading;
using namespace std;


// Simple threading scenario:  Start a Shared method running
// on a second thread.
public ref class StimThread
{
public:

	static bool Run = true;
	static int ThreadCount = 0;
   // The ThreadProc method is called when the thread starts.
   // It loops ten times, writing to the console and yielding 
   // the rest of its time slice each time, and then ends.
	static void ThreadProc()
	{
		Run = true;
		ThreadCount++;
		int myThreadID = ThreadCount;

		cout << "Thread #" << myThreadID << " instantiated" << endl;

		while(Run)
		{
			cout << "Thread #" << myThreadID << " doing stuff" << endl;
			Thread::Sleep( 200 );
		}

		cout << "Thread #" << myThreadID << " exiting" << endl;
   }

};