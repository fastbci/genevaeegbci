//#include "stdafx.h"
//#include "MedtronicStimulator.h"
//
//#include <iostream>
//#include <fstream>
//
//using namespace std;
//
//CMedtronicStimulator::CMedtronicStimulator(void)
//: m_IP("127.0.0.1")//"192.168.137.14")//m_IP("192.168.137.14") //"10.6.5.183")//"127.0.0.1")//
//{
//	// In windows, this will init the winsock stuff
//	curl_global_init(CURL_GLOBAL_ALL);
//}
//
//
//CMedtronicStimulator::CMedtronicStimulator(std::string myIP)
//: m_IP(myIP.c_str())
//{
//	// In windows, this will init the winsock stuff
//	curl_global_init(CURL_GLOBAL_ALL);
//}
//
//
//CMedtronicStimulator::~CMedtronicStimulator(void)
//{
//	curl_global_cleanup();
//}
//
//
//char* CMedtronicStimulator::int2charPointer(const int val)
//{
//	/*std::string s = std::to_string(static_cast<long long>(val));
//
//	char* p = new char[s.length()];
//	strcpy(p, s.c_str());
//
//	return p;*/
//
//	char* p = new char[10];
//	sprintf_s(p, 10,"%i", val);
//
//	return p;
//}
//
//char* CMedtronicStimulator::double2charPointer(const float val)
//{
//	/*std::string s = std::to_string(static_cast<long double>(val));
//
//	char* p = new char[s.length()];
//	strcpy(p, s.c_str());
//
//	return p;*/
//
//	char* p = new char[10];
//	sprintf_s(p, 10,"%.1f", val);
//
//	return p;
//}
//
//
//int CMedtronicStimulator::bin2dec(const bool activeChannels[10] )
//{
//
//	double channelsDecimal = 0;
//	for(int i=0;i<10;i++)
//	{
//		channelsDecimal += int(activeChannels[i]<<i);
//	}
//
//	//channelsDecimal = int(activeChannels[0])*1 + int(activeChannels[1])*2 +int(activeChannels[2])*4 +int(activeChannels[3])*8 +int(activeChannels[4])*16 +int(activeChannels[5])*32 +int(activeChannels[6])*64 +int(activeChannels[7])*128 +int(activeChannels[8])*256 +int(activeChannels[9])*512;
//
//	return int(channelsDecimal);
//}
//
//
//// --- FUNCTION TO SAVE RETURNING DATA
//size_t CMedtronicStimulator::WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
//{
//	((std::string*)userp)->append((char*)contents, size * nmemb);
//	return size* nmemb;
//}
//
//	
//
//void CMedtronicStimulator::stim_changeState(char *state)
//{
//
//	CURLcode res;
//
//	static std::string readBuffer;
//
//	// get a m_curl handle 
//	m_curl = curl_easy_init();
//
//	if(m_curl)
//	{
//		// TURN ON VERBOSE
//		curl_easy_setopt(m_curl, CURLOPT_VERBOSE,1L);
//
//		// OPTIONS
//		curl_easy_setopt(m_curl, CURLOPT_AUTOREFERER, 0);
//		curl_easy_setopt(m_curl, CURLOPT_HEADER, 1);
//
//
//		readBuffer.clear();
//		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
//		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &readBuffer); 
//
//
//		// DEFINE DATA
//		std::string url_h1 = "http://";
//		std::string url_h2 = ":9090/therapy?state=";
//		//char *state = "off";
//
//		char url[100];   // array to hold the full URL.
//		strcpy(url,url_h1.c_str()); 
//		strcat(url,m_IP.c_str());
//		strcat(url,url_h2.c_str());
//		strcat(url,state);
//
//		curl_easy_setopt(m_curl, CURLOPT_URL, url );
//		curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "");
//
//
//		
//		// PERFORM REQUEST
//		res = curl_easy_perform(m_curl);
//
//
//		// Check for errors
//		if(res != CURLE_OK)
//		{
//			fprintf(stderr, "curl_easy_perform() failed: %s\n",
//				curl_easy_strerror(res));
//		}
//
//		// always cleanup 
//		curl_easy_cleanup(m_curl);
//
//	}
//}
//
//void CMedtronicStimulator::stim_changeAmplitude(int waveformNum_val, double amplitude_val)
//{
//	CURLcode res;
//
//	static std::string readBuffer;
//
//	// get a m_curl handle 
//	m_curl = curl_easy_init();
//
//	// Set values as char* to be sent over HTTP
//	char* waveformNum = int2charPointer(waveformNum_val);
//	char* amplitude = double2charPointer(amplitude_val);
//
//	if(m_curl)
//	{
//		// TURN ON VERBOSE
//		curl_easy_setopt(m_curl, CURLOPT_VERBOSE,1L);
//
//		// OPTIONS
//		curl_easy_setopt(m_curl, CURLOPT_AUTOREFERER, 0);
//		curl_easy_setopt(m_curl, CURLOPT_HEADER, 1);
//
//		readBuffer.clear();
//		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
//		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &readBuffer); 
//
//		// DEFINE DATA
//		char * url_h1 = "http://";
//		char * url_h2 = ":9090/therapy/modifyamplitude?";
//		char *waveformTxt = "waveform=";
//		char *amplitudeTxt = "&amplitude=";
//
//		char url[100];   // array to hold the full URL.
//		strcpy(url,url_h1); 
//		strcat(url, m_IP.c_str());
//		strcat(url, url_h2); 
//		strcat(url,waveformTxt); strcat(url, waveformNum );
//		strcat(url,amplitudeTxt); strcat(url, amplitude );
//
//
//		//// WRITE TO FILE (FABIEN WAGNER, JUNE 2015)
//		////MessageBox(NULL, CString(url), _T("Command sent:"), MB_OK);
//		//string filename;
//		//time_t rawtime;
//		//time ( &rawtime );
//		//struct tm * timePtr = localtime ( &rawtime );
//		//string date= asctime (timePtr);
//
//		//// clean (symbols ":", " ")
//		//string str2remove;
//
//		//str2remove = ":";
//		//while(date.find(str2remove)!=string::npos)
//		//	date.replace(date.find(str2remove),str2remove.length(),"");
//
//		//str2remove = " ";
//		//while(date.find(str2remove)!=string::npos)
//		//	date.replace(date.find(str2remove),str2remove.length(),"");
//		//date = date.substr(0, 15);
//
//		//// Append names... FILE 1
//		//string fileName1 = "AmplitudeChange"; 
//		//filename = fileName1.insert(fileName1.length(),"_");
//		//filename = filename.insert(filename.length(),date);
//		//filename = filename.insert(filename.length(),".txt");
//
//		//ofstream out(filename.c_str());
//		//if(! out)
//		//	{  
//		//		cout<<"Cannot open output file\n";
//		//	}
//		//out.write(url, sizeof(url));
//		//out.close();
//		//// END WRITE TO FILE
//
//
//		curl_easy_setopt(m_curl, CURLOPT_URL, url );
//		curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "");
//
//		// PERFORM REQUEST
//		res = curl_easy_perform(m_curl);
//
//		// Check for errors
//		if(res != CURLE_OK)
//		{
//			fprintf(stderr, "curl_easy_perform() failed: %s\n",
//				curl_easy_strerror(res));
//		}
//
//		// always cleanup 
//		curl_easy_cleanup(m_curl);
//
//	}
//}
//
//void CMedtronicStimulator::stim_changeFrequency( double frequency_val, int modeAccurate_flag)
//{
//	CURLcode res;
//	
//	static std::string readBuffer;
//
//	// Set values as char* to be sent over HTTP
//	char* frequency = double2charPointer(frequency_val);
//	char* mode;
//	if( modeAccurate_flag == 0)
//	{
//		mode = "false";
//	}
//	else
//	{
//		mode = "true";
//	}
//
//	// get a m_curl handle 
//	m_curl = curl_easy_init();
//
//	if(m_curl)
//	{
//
//		// TURN ON VERBOSE
//		curl_easy_setopt(m_curl, CURLOPT_VERBOSE,1L);
//
//		// OPTIONS
//		curl_easy_setopt(m_curl, CURLOPT_AUTOREFERER, 0);
//		curl_easy_setopt(m_curl, CURLOPT_HEADER, 1);
//
//		readBuffer.clear();
//		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
//		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &readBuffer); 
//
//		// DEFINE DATA
//		char * url_h1 = "http://";
//		char * url_h2 = ":9090/therapy/modifyfrequency?";
//		char *frequencyTxt = "frequency=";
//		char *modeTxt = "&fast=";
//
//		char url[100];   // array to hold the full URL.
//		strcpy(url,url_h1); 
//		strcat(url, m_IP.c_str());
//		strcat(url, url_h2); 
//
//		strcat(url,frequencyTxt); strcat(url, frequency);
//		strcat(url,modeTxt); strcat(url, mode);
//
//		curl_easy_setopt(m_curl, CURLOPT_URL, url );
//		curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "");
//
//		// PERFORM REQUEST
//		res = curl_easy_perform(m_curl);
//
//		// Check for errors
//		if(res != CURLE_OK)
//		{
//			fprintf(stderr, "curl_easy_perform() failed: %s\n",
//				curl_easy_strerror(res));
//		}
//
//		// always cleanup 
//		curl_easy_cleanup(m_curl);
//
//	}
//}
//
//void CMedtronicStimulator::stim_updateTable(int writeIndex_val , int runIndex_val, WaveformTable table)
//{
//
//	CURLcode res;
//
//	static std::string readBuffer;
//
//	// get a m_curl handle 
//	m_curl = curl_easy_init();
//
//	// Set values as char* to be sent over HTTP
//	char* writeIndex = int2charPointer(writeIndex_val);
//	char* runIndex = int2charPointer(runIndex_val);
//
//	if(m_curl)
//	{
//		// TURN ON VERBOSE
//		curl_easy_setopt(m_curl, CURLOPT_VERBOSE,1L);
//
//		// OPTIONS
//		curl_easy_setopt(m_curl, CURLOPT_AUTOREFERER, 0);
//		curl_easy_setopt(m_curl, CURLOPT_HEADER, 1);
//
//		readBuffer.clear();
//		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
//		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &readBuffer); 
//
//		// DEFINE DATA
//		char * url_h1 = "http://";
//		char * url_h2 = ":9090/therapy/updatewaveformtable?";
//		char *writeIndexTxt = "writeIndex=";
//		char *runIndexTxt = "&runIndex=";
//		char *tableTxt = "&table=";
//
//		char url[1000];   // array to hold the URL.
//		strcpy(url,url_h1); 
//		strcat(url,m_IP.c_str());
//		strcat(url,url_h2);
//		strcat(url,writeIndexTxt); strcat(url, writeIndex );
//		strcat(url,runIndexTxt); strcat(url, runIndex );
//		strcat(url,tableTxt); 
//
//		// The table needs to be urlencoded... 
//		// Save it in a separate array, and concatenate at the end once encoded
//		char *entriesTxt_start = "{'Entries':[";
//		char *entriesTxt_end = "]}";
//
//		char url_table[1000]; 
//		strcpy(url_table,entriesTxt_start); 
//
//		int N = table.numberOfEntries;
//		for( int i=0; i<N; i++)
//		{
//			strcat(url_table, "{'Duration':");
//			strcat(url_table, int2charPointer(table.entries[i].Duration) );
//			strcat(url_table, ",'Enables':");
//			strcat(url_table, int2charPointer(table.entries[i].Enables));
//			strcat(url_table, ",'Frequency':");
//			strcat(url_table, int2charPointer(table.entries[i].Frequency));
//			strcat(url_table, "}");
//
//			if( i!=N-1 )
//			{
//				strcat(url_table, ",");
//			}
//		}
//		strcat(url_table,entriesTxt_end); 
//
//		char *urlTable2pointer = &url_table[0];
//		urlTable2pointer = curl_easy_escape(m_curl , urlTable2pointer , 0);
//
//		// Concatenate everything
//		strcat(url,urlTable2pointer);
//
//		curl_easy_setopt(m_curl, CURLOPT_URL, url );
//		curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "");
//
//		// PERFORM REQUEST
//		res = curl_easy_perform(m_curl);
//
//		// Check for errors
//		if(res != CURLE_OK)
//		{
//			fprintf(stderr, "curl_easy_perform() failed: %s\n",
//				curl_easy_strerror(res));
//		}
//
//		// always cleanup 
//		curl_easy_cleanup(m_curl);
//
//	}
//}
//
//void CMedtronicStimulator::stim_updateTable_onlyIndex(int runIndex_val)
//{
//
//	CURLcode res;
//
//	static std::string readBuffer;
//
//	// get a m_curl handle 
//	m_curl = curl_easy_init();
//
//	// Set values as char* to be sent over HTTP
//	char* runIndex = int2charPointer(128 + runIndex_val);
//
//	if(m_curl)
//	{
//		// TURN ON VERBOSE
//		curl_easy_setopt(m_curl, CURLOPT_VERBOSE,1L);
//
//		// OPTIONS
//		curl_easy_setopt(m_curl, CURLOPT_AUTOREFERER, 0);
//		curl_easy_setopt(m_curl, CURLOPT_HEADER, 1);
//
//		readBuffer.clear();
//		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
//		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &readBuffer); 
//
//
//		// DEFINE DATA
//		char * url_h1 = "http://";
//		char * url_h2 = ":9090/therapy/updatewaveformtable?";
//		char *writeIndexTxt = "writeIndex=";
//		char *runIndexTxt = "&runIndex=";
//		char *tableTxt = "&table={}";
//		// char *tableTxt = "&table={'Entries':[{�Duration�:25,�Enables�:15,�Frequency�:80}]}";
//
//
//		char url[1000];   // array to hold the URL.
//		strcpy(url,url_h1); 
//		strcat(url,m_IP.c_str());
//		strcat(url,url_h2);
//		strcat(url,writeIndexTxt);
//		strcat(url, "0" );
//		strcat(url,runIndexTxt);
//		strcat(url, runIndex );
//		strcat(url,tableTxt); 
//
//		curl_easy_setopt(m_curl, CURLOPT_URL, url );
//		curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "");
//
//		// PERFORM REQUEST
//		res = curl_easy_perform(m_curl);
//
//		// Check for errors
//		if(res != CURLE_OK)
//		{
//			fprintf(stderr, "curl_easy_perform() failed: %s\n",
//				curl_easy_strerror(res));
//		}
//
//		// always cleanup 
//		curl_easy_cleanup(m_curl);
//	}
//}
//
//response_data CMedtronicStimulator::stim_updateTable_onlyIndexWithResponse(int runIndex_val)
//{
//
//	CURLcode res;
//	static std::string readBuffer;
//	response_data rd;
//
//	// get a m_curl handle 
//	m_curl = curl_easy_init();
//
//	// Set values as char* to be sent over HTTP
//	char* runIndex = int2charPointer(128 + runIndex_val);
//	// char* runIndex = int2charPointer(runIndex_val);
//
//	if(m_curl)
//	{
//		// TURN ON VERBOSE
//		curl_easy_setopt(m_curl, CURLOPT_VERBOSE,1L);
//
//		// OPTIONS
//		curl_easy_setopt(m_curl, CURLOPT_AUTOREFERER, 0);
//		curl_easy_setopt(m_curl, CURLOPT_HEADER, 1);
//
//		readBuffer.clear();
//		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
//		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &readBuffer); 
//
//		// DEFINE DATA
//		char * url_h1 = "http://";
//		char * url_h2 = ":9090/therapy/updatewaveformtable?";
//		char *writeIndexTxt = "writeIndex=";
//		char *runIndexTxt = "&runIndex=";
//		char *tableTxt = "&table={}";
//
//
//		char url[1000];   // array to hold the URL.
//		strcpy(url,url_h1); 
//		strcat(url,m_IP.c_str());
//		strcat(url,url_h2);
//		strcat(url,writeIndexTxt);
//		strcat(url, "0" );
//		strcat(url,runIndexTxt);
//		strcat(url, runIndex );
//		strcat(url,tableTxt); 
//
//		curl_easy_setopt(m_curl, CURLOPT_URL, url );
//		curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "");
//
//		// PERFORM REQUEST
//		res = curl_easy_perform(m_curl);
//
//		// Check for errors
//		if(res != CURLE_OK)
//		{
//			fprintf(stderr, "curl_easy_perform() failed: %s\n",
//				curl_easy_strerror(res));
//		}
//
//		// Get info return from stimulator
//		rd.extract_dataFromResponse(readBuffer);
//
//		// always cleanup 
//		curl_easy_cleanup(m_curl);
//	}
//
//	return rd;
//}
//
//void CMedtronicStimulator::stim_updateTable_IndexAndFreq(int runIndex_val, int freqVal)
//{
//
//	CURLcode res;
//
//	static std::string readBuffer;
//
//	// Set values as char* to be sent over HTTP
//	char* runIndex = int2charPointer(128+runIndex_val);
//	char* freqText = int2charPointer(freqVal);
//
//	if(m_curl)
//	{
//
//		// TURN ON VERBOSE
//		curl_easy_setopt(m_curl, CURLOPT_VERBOSE,1L);
//
//		// OPTIONS
//		curl_easy_setopt(m_curl, CURLOPT_AUTOREFERER, 0);
//		curl_easy_setopt(m_curl, CURLOPT_HEADER, 1);
//
//		readBuffer.clear();
//		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
//		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &readBuffer); 
//
//
//		// DEFINE DATA
//		char * url_h1 = "http://";
//		char * url_h2 = ":9090/therapy/updatewaveformtableandfrequency?";
//		char *frequencyTxt = "frequency=";
//		char *writeIndexTxt = "&writeIndex=";
//		char *runIndexTxt = "&runIndex=";
//		char *tableTxt = "&table={}";
//
//
//		char url[1000];   // array to hold the URL.
//		strcpy(url,url_h1); 
//		strcat(url,m_IP.c_str());
//		strcat(url,url_h2);
//		strcat(url,frequencyTxt); strcat(url, freqText );
//		strcat(url,writeIndexTxt); strcat(url, "0" );
//		strcat(url,runIndexTxt); strcat(url, runIndex );
//		strcat(url,tableTxt); 
//
//		curl_easy_setopt(m_curl, CURLOPT_URL, url );
//		curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "");
//
//		// PERFORM REQUEST
//		res = curl_easy_perform(m_curl);
//
//		// Check for errors
//		if(res != CURLE_OK)
//		{
//			fprintf(stderr, "curl_easy_perform() failed: %s\n",
//				curl_easy_strerror(res));
//		}
//
//		// always cleanup 
//		curl_easy_cleanup(m_curl);
//
//	}
//}
//
//void CMedtronicStimulator::setIP(std::string newIP)
//{
//	m_IP.assign(newIP.c_str());
//}
//
//
//void CMedtronicStimulator::stim_changePulseWidth(int waveformNum_val, double pulseWidth_val)
//{
//	CURLcode res;
//
//	static std::string readBuffer;
//	//response_data rd;
//
//	// get a m_curl handle 
//	m_curl = curl_easy_init();
//
//
//	// Set values as char* to be sent over HTTP
//	char* waveformNum = int2charPointer(waveformNum_val);
//	char* pulseWidth = int2charPointer(pulseWidth_val);
//
//	if(m_curl)
//	{
//		// TURN ON VERBOSE
//		curl_easy_setopt(m_curl, CURLOPT_VERBOSE,1L);
//
//		// OPTIONS
//		curl_easy_setopt(m_curl, CURLOPT_AUTOREFERER, 0);
//		curl_easy_setopt(m_curl, CURLOPT_HEADER, 1);
//
//		readBuffer.clear();
//		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
//		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &readBuffer); 
//
//		// DEFINE DATA
//		char * url_h1 = "http://";
//		char * url_h2 = ":9090/therapy/modifypulsewidth?";
//		char *waveformTxt = "waveform=";
//		char *pulseWidthTxt = "&pulsewidth=";
//
//		char url[100];   // array to hold the full URL.
//		strcpy_s(url, sizeof(url), url_h1); 
//		strcat_s(url, sizeof(url), m_IP.c_str());
//		strcat_s(url, sizeof(url), url_h2); 
//		strcat_s(url, sizeof(url), waveformTxt); 
//		strcat_s(url, sizeof(url), waveformNum );
//		strcat_s(url, sizeof(url), pulseWidthTxt); 
//		strcat_s(url, sizeof(url), pulseWidth );
//
//
//		//// WRITE TO FILE (FABIEN WAGNER, JUNE 2015)
//		////MessageBox(NULL, CString(url), _T("Command sent:"), MB_OK);
//		//string filename;
//		//time_t rawtime;
//		//time ( &rawtime );
//		//struct tm * timePtr = localtime ( &rawtime );
//		//string date= asctime (timePtr);
//
//		//// clean (symbols ":", " ")
//		//string str2remove;
//
//		//str2remove = ":";
//		//while(date.find(str2remove)!=string::npos)
//		//	date.replace(date.find(str2remove),str2remove.length(),"");
//
//		//str2remove = " ";
//		//while(date.find(str2remove)!=string::npos)
//		//	date.replace(date.find(str2remove),str2remove.length(),"");
//		//date = date.substr(0, 15);
//
//		//// Append names... FILE 1
//		//string fileName1 = "PulseWidthChange"; 
//		//filename = fileName1.insert(fileName1.length(),"_");
//		//filename = filename.insert(filename.length(),date);
//		//filename = filename.insert(filename.length(),".txt");
//
//		//ofstream out(filename.c_str());
//		//if(! out)
//		//	{  
//		//		cout<<"Cannot open output file\n";
//		//	}
//		//out.write(url, sizeof(url));
//		//out.close();
//		//// END WRITE TO FILE
//
//
//		curl_easy_setopt(m_curl, CURLOPT_URL, url );
//		curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "");
//
//		// PERFORM REQUEST
//		res = curl_easy_perform(m_curl);
//
//		// Check for errors
//		if(res != CURLE_OK)
//		{
//			fprintf(stderr, "curl_easy_perform() failed: %s\n",
//				curl_easy_strerror(res));
//		}
//
//		// Write error to file
//		/*ofstream out("readBuffer.txt");
//		if(! out)
//			{  
//				out<<"Cannot open output file\n";
//			}
//		out << readBuffer;
//		out.close();*/
//
//		// Get info returnd from stimulator
//		//rd.extract_dataFromResponse(readBuffer);
//
//		// always cleanup 
//		curl_easy_cleanup(m_curl);
//
//	}
//
//	delete[] waveformNum;
//	delete[] pulseWidth;
//
//	//return rd;
//}
//
//
//int CMedtronicStimulator::stim_initialize(int isCVMode, Waveforms waveforms, WaveformTable table)
//{
//
//	CURLcode res;
//
//	static std::string readBuffer;
//	response_data rd;
//
//	// get a m_curl handle 
//	m_curl = curl_easy_init();
//	int res2 = 0;
//	
//	if(m_curl)
//	{
//		res2 = 1;
//
//		// TURN ON VERBOSE
//		curl_easy_setopt(m_curl, CURLOPT_VERBOSE,1L);
//
//		// OPTIONS
//		curl_easy_setopt(m_curl, CURLOPT_AUTOREFERER, 0);
//		curl_easy_setopt(m_curl, CURLOPT_HEADER, 1);
//
//		readBuffer.clear();
//		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, WriteCallback); // Prepare function to save what is returned by stimulator
//		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &readBuffer); 
//		
//		// DEFINE DATA
//		char *url_h1 = "http://";
//		char *url_h2 = ":9090/therapy/initialize?";
//		char *isCVModeTxt = "isCVMode=";
//		char *waveformsTxt = "&waveforms=";
//		char *tableTxt = "&table=";
//		char *trueTxt = "True";
//		char *falseTxt = "False";
//
//		char url[15000];   // array to hold the URL.
//		strcpy_s(url, sizeof(url), url_h1); 
//		strcat_s(url, sizeof(url), m_IP.c_str());
//		strcat_s(url, sizeof(url), url_h2);
//		strcat_s(url, sizeof(url), isCVModeTxt);
//		if (isCVMode==1)
//		{
//			strcat_s(url, sizeof(url), trueTxt);
//		}
//		else
//		{
//			strcat_s(url, sizeof(url), falseTxt);
//		}
//		
//		// Waveforms url-encoding
//		strcat_s(url, sizeof(url), waveformsTxt); 
//
//		char *entriesTxt_start = "{'Entries':[";
//		char *entriesTxt_end = "]}";
//
//		char url_waveforms[4000]; 
//		strcpy_s(url_waveforms, sizeof(url_waveforms), entriesTxt_start); 
//		//_mbcpy_s(url_waveforms, sizeof(url_waveforms), entriesTxt_start); 
//
//
//		int N = waveforms.numberOfEntries;
//
//		for( int i=0; i<N; i++)
//		{
//			strcat_s(url_waveforms, sizeof(url_waveforms), "{'amplitude':");
//			char* ampPointer = double2charPointer(waveforms.entries[i].amplitude);
//			strcat_s(url_waveforms, sizeof(url_waveforms), ampPointer);
//			strcat_s(url_waveforms, sizeof(url_waveforms), ",'pulsewidth':");
//			char* pulseWidthPointer = int2charPointer(waveforms.entries[i].pulseWidth);
//			strcat_s(url_waveforms, sizeof(url_waveforms), pulseWidthPointer);
//			strcat_s(url_waveforms, sizeof(url_waveforms), ",'electrodes':[");
//
//			for (int j=0; j<17; j++)
//			{
//				if (j>0)
//				{
//					strcat_s(url_waveforms, sizeof(url_waveforms), ",");
//				}
//				int el = (waveforms.entries[i]).electrode[j];
//				if (el==-1)
//				{
//					strcat_s(url_waveforms, sizeof(url_waveforms), "'-'");
//				}
//				else if (el==1)
//				{
//					strcat_s(url_waveforms, sizeof(url_waveforms), "'+'");
//				}
//				else 
//				{
//					strcat_s(url_waveforms, sizeof(url_waveforms), "'0'");
//				}
//			}
//			strcat_s(url_waveforms, sizeof(url_waveforms), "]}");
//
//			if( i!=N-1 )
//			{
//				strcat_s(url_waveforms, sizeof(url_waveforms), ",");
//			}
//
//			delete[] pulseWidthPointer;
//			delete[] ampPointer;
//		}
//		strcat_s(url_waveforms, sizeof(url_waveforms), entriesTxt_end); 
//
//
//		//strcat_s(url_waveforms, sizeof(url_waveforms), "{'amplitude': 0.0, 'pulsewidth':10,'electrodes':['+', '-', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']},{'amplitude': 0.0, 'pulsewidth':300,'electrodes':['+', '-', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']},{'amplitude': 0.0, 'pulsewidth':300,'electrodes':['+', '-', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']},{'amplitude': 0.0, 'pulsewidth':300,'electrodes':['+', '-', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']},{'amplitude': 5.0, 'pulsewidth':900,'electrodes':['+', '-', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']},{'amplitude': 0.0, 'pulsewidth':300,'electrodes':['+', '-', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']},{'amplitude': 0.0, 'pulsewidth':300,'electrodes':['+', '-', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']},{'amplitude�: 0.0, 'pulsewidth':300,�electrodes�:['+', '-', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']},{'amplitude': 0.0, 'pulsewidth':300,'electrodes':['+', '-', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']},{'amplitude': 0.0, 'pulsewidth':300,'electrodes':['+', '-', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']} ] }");
//
//		char *urlWaveforms2pointer = &url_waveforms[0];
//		urlWaveforms2pointer = curl_easy_escape(m_curl , urlWaveforms2pointer , 0);
//		int jj=sizeof(url);
//		strcat_s(url, sizeof(url), urlWaveforms2pointer);
//		// End waveforms url-encoding
//		
//		// Table url-encoding
//		strcat_s(url, sizeof(url), tableTxt); 
//
//		//char *entriesTxt_start = "{'Entries':[";
//		//char *entriesTxt_end = "]}";
//
//		char url_table[10000]; 
//		strcpy_s(url_table, sizeof(url_table), entriesTxt_start); 
//
//		N = table.numberOfEntries;
//
//		for( int i=0; i<N; i++)
//		{
//			strcat_s(url_table, sizeof(url_table), "{'Duration':");
//			char* durationPointer = int2charPointer(table.entries[i].Duration);
//			strcat_s(url_table, sizeof(url_table), durationPointer );
//			strcat_s(url_table, sizeof(url_table), ",'Enables':");
//			char* enablePointer = int2charPointer(table.entries[i].Enables);
//			strcat_s(url_table, sizeof(url_table), enablePointer);
//			strcat_s(url_table, sizeof(url_table), ",'Frequency':");
//			char* frequencyPointer = int2charPointer(table.entries[i].Frequency);
//			strcat_s(url_table, sizeof(url_table), frequencyPointer);
//			strcat_s(url_table, sizeof(url_table), "}");
//
//			if( i!=N-1 )
//			{
//				strcat_s(url_table, sizeof(url_table), ",");
//			}
//
//			delete[] durationPointer;
//			delete[] enablePointer;
//			delete[] frequencyPointer;
//		}
//		strcat_s(url_table, sizeof(url_table), entriesTxt_end); 
//
//
//		char *urlTable2pointer = &url_table[0];
//		urlTable2pointer = curl_easy_escape(m_curl , urlTable2pointer , 0);
//
//		strcat_s(url, sizeof(url), urlTable2pointer);
//		// End table url-encoding
//
//
//		// WRITE TO FILE (FABIEN WAGNER, JUNE 2015)
//		//MessageBox(NULL, CString(url), _T("Command sent:"), MB_OK);
//		string filename;
//		time_t rawtime;
//		time ( &rawtime );
//		struct tm * timePtr = localtime ( &rawtime );
//		string date= asctime (timePtr);
//
//		// clean (symbols ":", " ")
//		string str2remove;
//
//		str2remove = ":";
//		while(date.find(str2remove)!=string::npos)
//			date.replace(date.find(str2remove),str2remove.length(),"");
//
//		str2remove = " ";
//		while(date.find(str2remove)!=string::npos)
//			date.replace(date.find(str2remove),str2remove.length(),"");
//		date = date.substr(0, 15);
//
//		// Append names... FILE 1
//		string fileName1 = "Txtfiles/Parameters"; 
//		filename = fileName1.insert(fileName1.length(),"_");
//		filename = filename.insert(filename.length(),date);
//		filename = filename.insert(filename.length(),".txt");
//
//		ofstream out(filename.c_str());
//		if(! out)
//			{  
//				cout<<"Cannot open output file\n";
//			}
//		out.write(url, sizeof(url));
//		out.close();
//		// END WRITE TO FILE
//
//
//		curl_easy_setopt(m_curl, CURLOPT_URL, url );
//		curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "");
//
//		// PERFORM REQUEST
//		res = curl_easy_perform(m_curl);
//
//		// Check for errors
//		if(res != CURLE_OK)
//		{
//			//fprintf(stderr, "curl_easy_perform() failed: %s\n",
//				//curl_easy_strerror(res));
//
//			CString str(curl_easy_strerror(res));
//			MessageBox(NULL, str, _T("Error CURL"), MB_OK);
//			res2 = 0;
//			m_initializeResponse = "Error CURL";
//		}
//		else
//		{
//			m_initializeResponse = readBuffer;
//			CString msg = rd.extract_ErrorFromResponse(readBuffer);
//			CString msg_beg = msg.Mid(0, 5);
//			int res3=msg_beg.Compare(L"ERROR");
//			
//			// Write error to file
//			ofstream out("readBuffer.txt");
//			if(! out)
//				{  
//					out<<"Cannot open output file\n";
//				}
//			out << readBuffer;
//			out.close();
//
//			if (msg_beg.Compare(L"ERROR") == 0)
//			{
//				// Write error to file
//				/*ofstream out("readBuffer.txt");
//				if(! out)
//					{  
//						out<<"Cannot open output file\n";
//					}
//				out << readBuffer;
//				out.close();*/
//
//				// Display error in MessageBox
//				MessageBox(NULL, msg, _T("Error NRP"), MB_OK);
//				res2 = 0;
//			}
//		}
//
//		// always cleanup 
//		curl_easy_cleanup(m_curl);
//
//		// Delete pointers
//		//delete urlTable2pointer;
//		//delete urlWaveforms2pointer;
//
//	}
//
//	return res2;
//}
//
//
//CString response_data::extract_ErrorFromResponse(std::string response)
//{
//	size_t beg_msg = response.find("Message")+10;
//	size_t end_msg = response.find(",", beg_msg)-2;
//	char msg[200];
//	response.copy(msg, end_msg-beg_msg+1, beg_msg);
//	msg[end_msg-beg_msg+1] = '\0';
//	CString res =  CString(msg);
//	return res;
//}
//
//void response_data::extract_dataFromResponse(std::string response)
//	{
//		// OutputDebugStringA(response.c_str());
//
//		//MessageBoxA(NULL, response.c_str(), "Response from stim", MB_OK);
//
//
//		noOfPastIDs = 0;
//		size_t beg_ID = response.find("Id") + 4;
//		size_t end_ID = response.find(",", beg_ID);
//		commandID = atoi(response.substr(beg_ID, end_ID - beg_ID).c_str());
//
//		beg_ID = response.find("QueueCount") + 12;
//		end_ID = response.find(",", beg_ID);
//		queueCount = atoi(response.substr(beg_ID, end_ID - beg_ID).c_str());
//
//		beg_ID = response.find("SuccessfulIds");
//		if (beg_ID != response.npos)
//		{
//			beg_ID = response.find("[", beg_ID) + 1;
//			end_ID = response.find("]", beg_ID);
//		}
//
//		if (end_ID > beg_ID)
//		{
//			size_t nextComma;
//			bool runLoop = true;
//			while (runLoop)
//			{
//				nextComma = response.find(",", beg_ID);
//				if ((nextComma == response.npos) | (nextComma > end_ID))
//				{
//					nextComma = end_ID;
//					runLoop = false;
//				}
//				succesfulIDs[noOfPastIDs] = atoi(response.substr(beg_ID, nextComma - beg_ID).c_str());
//				noOfPastIDs = noOfPastIDs + 1;
//				beg_ID = nextComma + 1;
//			}
//		}
//
//		beg_ID = response.find("PerfCounters");
//		if (beg_ID != response.npos)
//		{
//			size_t nextComma;
//			beg_ID = response.find("[", beg_ID) + 1;
//			for (size_t idc = 0; idc < noOfPastIDs; ++idc)
//			{
//				beg_ID = response.find("[", beg_ID) + 1;
//				end_ID = response.find("]", beg_ID);
//				for (size_t pc = 0; pc < m_noOfPerfCounters - 1; ++pc)
//				{
//					nextComma = response.find(",", beg_ID);
//					std::string testSting = response.substr(beg_ID, nextComma - beg_ID).c_str();
//					succesfullPerfCount[idc][pc] = _atoi64(response.substr(beg_ID, nextComma - beg_ID).c_str());
//					beg_ID = nextComma + 1;
//				}
//				succesfullPerfCount[idc][m_noOfPerfCounters - 1] = _atoi64(response.substr(beg_ID, end_ID - beg_ID).c_str());
//			}
//		}
//	}
