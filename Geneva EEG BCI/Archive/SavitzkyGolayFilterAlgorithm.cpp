#include "SavitzkyGolayFilterAlgorithm.h"

#include <math.h>
#include <sstream>
#include <assert.h>
#include <iostream>
#include <stdexcept>

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif


using namespace std;

CSavitzkyGolayFilterAlgorithm::CSavitzkyGolayFilterAlgorithm(const size_t& leftWidth
	, const size_t& rightWidth
	, const size_t& polynomialOrder
	, const size_t& windowSize
	, const size_t& stepSize
	, const size_t& numberOfChannels)
: m_polynomialOrder  (polynomialOrder)
, m_windowSize       (windowSize)
, m_stepSize		 (stepSize)
, m_bufferSize       (2 * int(windowSize) - 1)
, m_realSpectrum     (windowSize)
, m_imagSpectrum     (windowSize)
, m_fft              (new CFftwTransform(m_bufferSize))
, m_ifft             (new CFftwReverseTransform(m_bufferSize))
, m_buffer           (numberOfChannels, windowSize, CDataBuffer::LAST_IN_AT_END)
, m_timeStep         (0)
, m_realProduct      (windowSize)
, m_imagProduct      (windowSize)
, m_inputRealSpectrum(windowSize)
, m_inputImagSpectrum(windowSize)
{
	initialize(leftWidth, rightWidth, polynomialOrder);
}

CSavitzkyGolayFilterAlgorithm::~CSavitzkyGolayFilterAlgorithm()
{
	if(m_fft != 0)
	{
		delete m_fft;
		m_fft = 0;
	}

	if(m_ifft != 0)
	{
		delete m_ifft;
		m_ifft = 0;
	}
}

void CSavitzkyGolayFilterAlgorithm::initialize(const size_t& leftWidth, const size_t& rightWidth, const size_t& polynomialOrder)
{
	if(m_windowSize < (leftWidth + rightWidth + 1))
	{
		throw runtime_error("windowSize must be >= leftWidth + rightWidth + 1");
	}
	if((leftWidth + rightWidth) % 2 != 0)
	{
		throw runtime_error("leftWidth + rightWidth must be even");
	}

	const size_t spectrumSize = m_windowSize;
	assert(m_realSpectrum.getSize() == spectrumSize);
	assert(m_imagSpectrum.getSize() == spectrumSize);
	assert(m_fft  != 0);
	assert(m_ifft != 0);
	assert(m_timeStep == 0);
	assert(m_realProduct.getSize() == spectrumSize);
	assert(m_imagProduct.getSize() == spectrumSize);
	assert(m_inputRealSpectrum.getSize() == spectrumSize);
	assert(m_inputImagSpectrum.getSize() == spectrumSize);

	const CDoubleVector coeffs = getCoefficients(leftWidth, rightWidth, polynomialOrder);    
	CFftwTransform fft(m_bufferSize);
	CDoubleVector timeSeries(m_bufferSize, 0.0);

	const size_t startIndex = m_bufferSize / 4u - coeffs.getSize() / 2u;
	for(size_t index = 0; index < coeffs.getSize(); ++index)
	{
		timeSeries[startIndex + index] = coeffs[index];
	}

	fft.doFourierTransform(m_realSpectrum, m_imagSpectrum, timeSeries);     
}

void CSavitzkyGolayFilterAlgorithm::process(const size_t& noOfSamples, double** result, double** input)
{
	++m_timeStep;
	///////////////////////////////////////
	m_buffer.append(noOfSamples, input);
	///////////////////////////////////////

	if(m_timeStep == m_stepSize)
	{
		const size_t numberOfChannels = m_buffer.getNumberOfChannels();
		CResult* smoothed = new CResult(m_windowSize, numberOfChannels);
		for(size_t channelNumber = 0; channelNumber < numberOfChannels; ++channelNumber)
		{
			// prepare data for transformation into fourier space: pad with zeros to the left and right
			const size_t spectrumSize = m_realSpectrum.getSize();
			CDoubleVector timeSeries(m_bufferSize, 0.0);
			const size_t halfWindowSize = m_windowSize / 2u;
			memcpy(timeSeries.getValues() + halfWindowSize, m_buffer.getChannel(channelNumber), m_windowSize * sizeof(double));

			// transform into fourier space
			m_fft->doFourierTransform(m_inputRealSpectrum, m_inputImagSpectrum, timeSeries);

			// convolve with coefficients
			const double* const a = m_inputRealSpectrum.getValues();
			const double* const b = m_inputImagSpectrum.getValues();
			const double* const c = m_realSpectrum.getValues();
			const double* const d = m_imagSpectrum.getValues();
			for(size_t index = 0; index < spectrumSize; ++index)
			{
				// multiply two complex numbers: (a+ib) * (c+id) = (a*c+b*d) + i*(a*d + c*b) 
				m_realProduct[index] = a[index] * c[index] - b[index] * d[index];
				m_imagProduct[index] = a[index] * d[index] + c[index] * b[index];
			}

			// reverse transformation into time domain
			m_ifft->doInverseFourierTransform(timeSeries, m_realProduct, m_imagProduct);

			// result is located in the second half of the time series (due to zero-padding)
			smoothed->setWindow(channelNumber, timeSeries.getValues() + m_windowSize - 1);

		}

		assert(smoothed->getWindowSize() == m_buffer.getCapacity());
		// smoothed->setTimes(m_buffer.getTimes());

		/////////////////////////////////////////////////////
		smoothed->getBuffer(result);
	    //////////////////////////////////////////////////////

		//  result.push_back(smoothed);  
		m_timeStep = 0;
	}


	// return 0;
}

CDoubleVector CSavitzkyGolayFilterAlgorithm::getCoefficients(const size_t& leftWidth, const size_t& rightWidth, const size_t& polynomialOrder)
{
	const size_t frameSize = leftWidth + rightWidth + 1;
	if(!(frameSize % 2 == 1))
	{
		throw runtime_error("leftWidth + rightWidth + 1 must be odd!");
	}
	if(!(frameSize > polynomialOrder))
	{
		throw runtime_error("polynomial order must be smaller than leftWidth + rightWidth + 1!");
	}

	CDoubleMatrix vandermonde(frameSize, polynomialOrder + 1, 1.0);
	for(size_t col = 1; col < vandermonde.getColumnSize(); ++col)
	{
		double val = - double(leftWidth);
		for(size_t row = 0; row < vandermonde.getRowSize(); ++row, ++val)
		{
			vandermonde.at(row, col) = pow(val, double(col));
		}          
	}

	// calculate inv(vand^T * vand) * vand^T
	CDoubleMatrix toInvert(polynomialOrder + 1, polynomialOrder + 1);
	CDoubleMatrix inverted(polynomialOrder + 1, polynomialOrder + 1);
	CDoubleMatrix coeffs(polynomialOrder + 1, frameSize);
	CDoubleMatrix vandermondeTransposed = vandermonde.getTransposed();
	CMathLibrary::multMatrixMatrix(&toInvert, vandermondeTransposed, vandermonde);
	CMathLibrary::invertMatrix(inverted, toInvert);
	CMathLibrary::multMatrixMatrix(&coeffs, inverted, vandermondeTransposed);
	CDoubleVector result(frameSize);
	for(size_t col = 0; col < coeffs.getColumnSize(); ++col)
	{
		result[col] = coeffs.at(0, col);
		//////////////////////////////////////////////
		// Why only use first raw ?  for smoothing only, don't need derivative
		//////////////////////////////////////////////////////
	}
	return result;
}

/*
void CSavitzkyGolayFilterAlgorithm::reset()
{
	m_timeStep = 0;
	assert(m_windowSize == m_buffer.getCapacity());
}

size_t CSavitzkyGolayFilterAlgorithm::getNumberOfInputData()
{
	return 1;
}

size_t CSavitzkyGolayFilterAlgorithm::getNumberOfOutputData()
{
	return 1;
}

std::string CSavitzkyGolayFilterAlgorithm::getInputDataName(const size_t& inputIndex)
{
	if(inputIndex != 0)
	{
		ostringstream message;
		message << "CSavitzkyGolayFilterAlgorithm::getInputDataName(): inputIndex too big (>0): " << inputIndex;
		throw runtime_error(message.str());
	}
	return "unsmoothed";
}

std::string CSavitzkyGolayFilterAlgorithm::getOutputDataName(const size_t& outputIndex)
{
	if(outputIndex != 0)
	{
		ostringstream message;
		message << "CSavitzkyGolayFilterAlgorithm::getOutputDataName(): outputIndex too big (>0): " << outputIndex;
		throw runtime_error(message.str());
	}
	return "smoothed";
}
*/

CSavitzkyGolayFilterAlgorithm::CResult::CResult(const size_t& windowSize, const size_t& numberOfChannels)
	: m_windowSize      (windowSize)
	, m_numberOfChannels(numberOfChannels)
	, m_windows         (new double[windowSize * numberOfChannels])
	, m_times           (new double[windowSize])
{
	assert(m_windowSize       != 0);
	assert(m_numberOfChannels != 0);
	assert(m_windows          != 0);
	assert(m_times            != 0);
	memset(m_windows, 0, m_windowSize * m_numberOfChannels * sizeof(double));
	memset(m_times,   0, m_windowSize * sizeof(double));
}

CSavitzkyGolayFilterAlgorithm::CResult::CResult(const CResult& other)
	: m_windowSize      (other.m_windowSize)
	, m_numberOfChannels(other.m_numberOfChannels)
	, m_windows         (new double[other.m_windowSize * other.m_numberOfChannels])
	, m_times           (new double[other.m_windowSize])
{
	assert(m_windowSize       != 0);
	assert(m_numberOfChannels != 0);
	assert(m_windows          != 0);
	memcpy(m_windows, other.m_windows, m_windowSize * m_numberOfChannels * sizeof(double));
	memcpy(m_times,   other.m_times,   m_windowSize * sizeof(double));
}

CSavitzkyGolayFilterAlgorithm::CResult::~CResult()
{
	if(m_windows != 0)
	{
		delete[] m_windows;
		m_windows = 0;
	}
	if(m_times != 0)
	{
		delete m_times;
		m_times = 0;
	}
}

size_t CSavitzkyGolayFilterAlgorithm::CResult::getWindowSize() const
{
	return m_windowSize;
}

size_t CSavitzkyGolayFilterAlgorithm::CResult::getNumberOfChannels() const
{
	return m_numberOfChannels;
}

const double* const CSavitzkyGolayFilterAlgorithm::CResult::getChannel(const size_t& channelNumber) const
{
	assert(channelNumber < m_numberOfChannels);
	return m_windows + channelNumber * m_windowSize;
}

const double* const CSavitzkyGolayFilterAlgorithm::CResult::getTimes() const
{
	return m_times;
}

void CSavitzkyGolayFilterAlgorithm::CResult::setWindow(const size_t& channelNumber, const double* const window)
{
	assert(channelNumber < m_numberOfChannels);
	assert(window != 0);
	memcpy(m_windows + channelNumber * m_windowSize, window, m_windowSize * sizeof(double));
}

void CSavitzkyGolayFilterAlgorithm::CResult::setTimes(const double* const times)
{
	assert(times != 0);
	memcpy(m_times, times, m_windowSize * sizeof(double));
}

//////////////////////////////////////////////////////////////////////////////////
void CSavitzkyGolayFilterAlgorithm::CResult::getBuffer(double** bufferPointer)
{
	for (size_t ch = 0; ch < m_numberOfChannels; ++ch)
	{
		bufferPointer[ch] = m_windows + ch * m_windowSize;
	}

}
/////////////////////////////////////////////////////////////////////////////////////