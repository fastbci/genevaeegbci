///*
//file linreg.cpp
//*/
//
//
//#include "stdafx.h"
//#include <iostream>
//#include <vector>
//#include <math.h>
//#include <stdio.h>
//#include <float.h>
//#include <complex>
//#include <math.h>
//
//using namespace std;
//
//#include "calcFunctions.h"
//
//// ********************************************************
////					Linear Regression
//// ********************************************************
//
//// --- Constructor using arrays (fixed size)
//LinearRegression::LinearRegression(Point2D *p, long size)
//{
//	long i;
//	a = b = sumX = sumY = sumXsquared = sumYsquared = sumXY = 0.0;
//	n = 0L;
//
//	if (size > 0L) // if size greater than zero there are data arrays
//		for (n = 0, i = 0L; i < size; i++)
//			addPoint(p[i]);
//}
//
//// --- Contructor usnig Point2D
//LinearRegression::LinearRegression(double *x, double *y, long size)
//{
//	long i;
//	a = b = sumX = sumY = sumXsquared = sumYsquared = sumXY = 0.0;
//	n = 0L;
//
//	if (size > 0L) // if size greater than zero there are data arrays
//		for (n = 0, i = 0L; i < size; i++)
//			addXY(x[i], y[i]);
//}
//
//void LinearRegression::addXY(const double& x, const double& y)
//{
//	n++;
//	sumX += x;
//	sumY += y;
//	sumXsquared += x * x;
//	sumYsquared += y * y;
//	sumXY += x * y;
//	Calculate();
//}
//
//void LinearRegression::Calculate()
//{
//	if (haveData())
//	{
//		if (abs( double(n) * sumXsquared - sumX * sumX) > DBL_EPSILON)
//		{
//			b = ( double(n) * sumXY - sumY * sumX) /
//				( double(n) * sumXsquared - sumX * sumX);
//			a = (sumY - b * sumX) / double(n);
//
//			double sx = b * ( sumXY - sumX * sumY / double(n) );
//			double sy2 = sumYsquared - sumY * sumY / double(n);
//			double sy = sy2 - sx;
//
//			coefD = sx / sy2;
//			coefC = sqrt(coefD);
//			stdError = sqrt(sy / double(n - 2));
//		}
//		else
//		{
//			a = b = coefD = coefC = stdError = 0.0;
//		}
//	}
//}
//
//
//
//
//
//
//
//// ********************************************************
////					LMS ALGORITHM
//// ********************************************************
//
//#define EPSI 0.0000001
//
//LMS::LMS(const int &Wsize)
//	: _Wsize(Wsize)
//	, _W (new double[Wsize])
//	, _Px (new double[Wsize])
//	, _alpha_vec (new double[Wsize])
//	, _sigma_vec (new double[Wsize])
//{
//	// By default set all values to 0
//	_y = 0.0;
//	_mu = 0.0;
//	_alpha = 0.0;
//	_sigma = 0.0;
//	_y = 0.0;
//
//	for(int copyLoop = 0; copyLoop < _Wsize; copyLoop++)
//	{
//		_W[copyLoop] = 0.0;
//		_Px[copyLoop] = 0.0;
//		_alpha_vec[copyLoop] = _alpha;
//		_sigma_vec[copyLoop] = _sigma;
//	}
//}
//
//LMS::LMS(const int& Wsize, const double& mu, const double& alpha, const double& sigma)
//	: _Wsize(Wsize)
//	, _y(0.0)
//	, _idx(-1)
//	, _W (new double[Wsize])
//	, _Px (new double[Wsize])
//	, _alpha_vec (new double[Wsize])
//	, _sigma_vec (new double[Wsize])
//{
//	_mu = mu;
//	_alpha = alpha;
//	_sigma = sigma;
//
//	memset(_W, 0, _Wsize * sizeof(double));
//	memset(_Px, 0, _Wsize * sizeof(double));
//	memset(_alpha_vec, 0, _Wsize * sizeof(double));
//	memset(_sigma_vec, 0, _Wsize * sizeof(double));
//
//}
//
//// Constructor for vectors (multDim)
//LMS::LMS(const int& Wsize, const double& mu, double *alpha, double *sigma)
//	: _Wsize(Wsize)
//	, _y(0.0)
//	, _idx(-1)
//	, _W (new double[Wsize])
//	, _Px (new double[Wsize])
//	, _alpha_vec (new double[Wsize])
//	, _sigma_vec (new double[Wsize])
//{
//	_mu = mu;
//	_alpha = 0.0;
//	_sigma = 0.0;
//	
//	memset(_W, 0, _Wsize * sizeof(double));
//	memset(_Px, 0, _Wsize * sizeof(double));
//	memcpy(_alpha_vec, alpha, _Wsize * sizeof(double));
//	memcpy(_sigma_vec, sigma, _Wsize * sizeof(double));
//	/*
//	_Wsize= Wsize;
//	 _W =new double[Wsize];
//	_Px =new double[Wsize];
// _alpha_vec= new double[Wsize];
//	_sigma_vec= new double[Wsize];
//	//constructor
//	_y = 0.0;
//	_mu = mu;
//	_alpha = 0.0; //not used
//	_sigma = 0.0; // not used
//	_y = 0.0;
//
//	_idx = -1;
//
//	_W = (double*)malloc(_Wsize*sizeof(double));
//	_Px = (double*)malloc(_Wsize*sizeof(double));
//	_alpha_vec = (double*)malloc(_Wsize*sizeof(double));
//	_sigma_vec = (double*)malloc(_Wsize*sizeof(double));
//
//
//	for(int copyLoop = 0; copyLoop < _Wsize; copyLoop++)
//	{
//		_W[copyLoop] = 0.0;
//		_Px[copyLoop] = 0.0;
//		_alpha_vec[copyLoop] = _alpha;
//		_sigma_vec[copyLoop] = _sigma;
//
//		//_W.push_back(W[copyLoop]);
//		//_Px.push_back( Px[copyLoop]);
//		//_alpha_vec.push_back( alpha );  // Put same values in all elements of vector
//		//_sigma_vec.push_back( sigma );
//	}
//	*/
//}
//
//double LMS::eval(const double &x, const double &d)
//{
//	// --- INPUTS:
//	// - x = sample of noisy signal
//	// - d = sample of desired signal
//
//
//
//	// --- Update Px with the new value;
//	_idx++;
//
//	if( _idx == _Wsize )
//		_idx = 0;
//
////	for(copyLoop = _Wsize-2; copyLoop>-1; copyLoop--)
////	{
////		_Px[copyLoop+1] = _Px[copyLoop];
////	}
////  _Px[0] = x;
//
//	_Px[_idx] = x;
//
//
//
//
//
//	// --- Evaluate output by multiplying by previous filter coefs
//	double y_tmp = 0;
//	int current_el =0;
//	for(int copyLoop = 0; copyLoop<_Wsize; copyLoop++)
//	{
//		current_el = _idx - copyLoop;
//		if( current_el < 0 )
//			current_el = current_el + _Wsize;
//
//		y_tmp = y_tmp + _Px[current_el]*_W[copyLoop];
//	}
//	_y = y_tmp;
//
//
//
//	// --- Evaluate error
//	double _e = d - _y;
//
//
//	// --- Calculate power and update sigma
//	_sigma = _alpha * pow(x,2) + (1 - _alpha) * _sigma;
//	if (_sigma == 0)
//		_sigma = _sigma + EPSI;
//
//	// Calculate new coefficients
//	for(int copyLoop = 0; copyLoop<_Wsize; copyLoop++)
//	{
//		current_el = _idx - copyLoop;
//		if( current_el < 0 )
//			current_el = current_el + _Wsize;
//
//		_W[copyLoop] = _W[copyLoop] + _mu*_e*_Px[current_el]/_sigma;
//	}
//
//	return _y;
//
//} // END LMS::eval
//
//bool LMS::eval_multDim(double x[], const double &d)
//
//{
//	// --- INPUTS:
//	// - x = sample of noisy signal (Multi-dimensional)
//	// - d = sample of desired signal
//
//
//	// --- Copy in Px with the new value;
//	int copyLoop;
//	for(copyLoop = 0; copyLoop<_Wsize; copyLoop++)
//		_Px[copyLoop] = x[copyLoop];
//
//
//	// --- Evaluate output by multiplying by previous filter coefs
//	double y_tmp = 0;
//	for(copyLoop = 0; copyLoop<_Wsize; copyLoop++)
//		y_tmp = y_tmp + _Px[copyLoop]*_W[copyLoop];
//	_y = y_tmp;
//
//
//
//	// --- Evaluate error
//	double _e = d - _y;
//
//
//	// --- Calculate power and update sigma
//	for(int i = 0; i<_Wsize; i++ )
//		_sigma_vec[i] = _alpha_vec[i]*pow(_Px[i],2) + (1-_alpha_vec[i])*_sigma_vec[i] + EPSI;
//
//
//
//	// Calculate new coefficients
//	for(int i = 0; i<_Wsize; i++)
//		_W[i] = _W[i] + _mu*_e*_Px[i]/_sigma_vec[i];
//
//
//	return true; // returns whether updating was successful. Need to retrieve the weights from the other method
//
//} // END LMS::eval_multDim()
//
//void LMS::getPrediction(double &y)
//{
//	y = _y;
//}
//
//double LMS::getWeight(const int &pos)
//{
//	if( pos <_Wsize )
//		return _W[pos];
//	else
//		return 0;
//}
//
//void LMS::increase_stepSize(const double &inc)
//{
//	_mu+=inc;
//	_alpha+=inc;
//}
//
//
//
//// ********************************************************
////					BUTTERWORTH FILTER
//// ********************************************************
//
//
//#define N 10 //The number of images which construct a time series for each pixel
//
//double *ComputeLP( int FilterOrder )
//{
//    double *NumCoeffs;
//    int m;
//    int i;
//
//    NumCoeffs = (double *)calloc( FilterOrder+1, sizeof(double) );
//    if( NumCoeffs == NULL ) return( NULL );
//
//    NumCoeffs[0] = 1;
//    NumCoeffs[1] = FilterOrder;
//    m = FilterOrder/2;
//    for( i=2; i <= m; ++i)
//    {
//        NumCoeffs[i] =(double) (FilterOrder-i+1)*NumCoeffs[i-1]/i;
//        NumCoeffs[FilterOrder-i]= NumCoeffs[i];
//    }
//    NumCoeffs[FilterOrder-1] = FilterOrder;
//    NumCoeffs[FilterOrder] = 1;
//
//    return NumCoeffs;
//}
//
//double *ComputeHP( int FilterOrder )
//{
//    double *NumCoeffs;
//    int i;
//
//    NumCoeffs = ComputeLP(FilterOrder);
//    if(NumCoeffs == NULL ) return( NULL );
//
//    for( i = 0; i <= FilterOrder; ++i)
//        if( i % 2 ) NumCoeffs[i] = -NumCoeffs[i];
//
//    return NumCoeffs;
//}
//
//double *TrinomialMultiply( int FilterOrder, double *b, double *c )
//{
//    int i, j;
//    double *RetVal;
//
//    RetVal = (double *)calloc( 4 * FilterOrder, sizeof(double) );
//    if( RetVal == NULL ) return( NULL );
//
//    RetVal[2] = c[0];
//    RetVal[3] = c[1];
//    RetVal[0] = b[0];
//    RetVal[1] = b[1];
//
//    for( i = 1; i < FilterOrder; ++i )
//    {
//        RetVal[2*(2*i+1)]   += c[2*i] * RetVal[2*(2*i-1)]   - c[2*i+1] * RetVal[2*(2*i-1)+1];
//        RetVal[2*(2*i+1)+1] += c[2*i] * RetVal[2*(2*i-1)+1] + c[2*i+1] * RetVal[2*(2*i-1)];
//
//        for( j = 2*i; j > 1; --j )
//        {
//            RetVal[2*j]   += b[2*i] * RetVal[2*(j-1)]   - b[2*i+1] * RetVal[2*(j-1)+1] +
//                c[2*i] * RetVal[2*(j-2)]   - c[2*i+1] * RetVal[2*(j-2)+1];
//            RetVal[2*j+1] += b[2*i] * RetVal[2*(j-1)+1] + b[2*i+1] * RetVal[2*(j-1)] +
//                c[2*i] * RetVal[2*(j-2)+1] + c[2*i+1] * RetVal[2*(j-2)];
//        }
//
//        RetVal[2] += b[2*i] * RetVal[0] - b[2*i+1] * RetVal[1] + c[2*i];
//        RetVal[3] += b[2*i] * RetVal[1] + b[2*i+1] * RetVal[0] + c[2*i+1];
//        RetVal[0] += b[2*i];
//        RetVal[1] += b[2*i+1];
//    }
//
//    return RetVal;
//}
//
//double *ComputeNumCoeffs(int FilterOrder,double Lcutoff, double Ucutoff, double *DenC)
//{
//    double *TCoeffs;
//    double *NumCoeffs;
//    std::complex <double> *NormalizedKernel;
//    double Numbers[11]={0,1,2,3,4,5,6,7,8,9,10};
//    int i;
//
//    NumCoeffs = (double *)calloc( 2*FilterOrder+1, sizeof(double) );
//    if( NumCoeffs == NULL ) return( NULL );
//
//    NormalizedKernel = (std::complex<double> *)calloc( 2*FilterOrder+1, sizeof(std::complex<double>) );
//    if( NormalizedKernel == NULL ) return( NULL );
//
//    TCoeffs = ComputeHP(FilterOrder);
//    if( TCoeffs == NULL ) return( NULL );
//
//    for( i = 0; i < FilterOrder; ++i)
//    {
//        NumCoeffs[2*i] = TCoeffs[i];
//        NumCoeffs[2*i+1] = 0.0;
//    }
//    NumCoeffs[2*FilterOrder] = TCoeffs[FilterOrder];
//    double cp[2];
//    double Bw, Wn;
//    cp[0] = 2*2.0*tan(PI * Lcutoff/ 2.0);
//    cp[1] = 2*2.0*tan(PI * Ucutoff / 2.0);
//
//    Bw = cp[1] - cp[0];
//    //center frequency
//    Wn = sqrt(cp[0]*cp[1]);
//    Wn = 2*atan2(Wn,4);
//    const std::complex<double> result = std::complex<double>(-1,0);
//
//    for(int k = 0; k<11; k++)
//    {
//        NormalizedKernel[k] = std::exp(-sqrt(result)*Wn*Numbers[k]);
//    }
//    double b=0;
//    double den=0;
//    for(int d = 0; d<11; d++)
//    {
//        b+=real(NormalizedKernel[d]*NumCoeffs[d]);
//        den+=real(NormalizedKernel[d]*DenC[d]);
//    }
//    for(int c = 0; c<11; c++)
//    {
//        NumCoeffs[c]=(NumCoeffs[c]*den)/b;
//    }
//
//    free(TCoeffs);
//    return NumCoeffs;
//}
//
//double *ComputeDenCoeffs( int FilterOrder, double Lcutoff, double Ucutoff )
//{
//    int k;            // loop variables
//    double theta;     // PI * (Ucutoff - Lcutoff) / 2.0
//    double cp;        // cosine of phi
//    double st;        // sine of theta
//    double ct;        // cosine of theta
//    double s2t;       // sine of 2*theta
//    double c2t;       // cosine 0f 2*theta
//    double *RCoeffs;     // z^-2 coefficients
//    double *TCoeffs;     // z^-1 coefficients
//    double *DenomCoeffs;     // dk coefficients
//    double PoleAngle;      // pole angle
//    double SinPoleAngle;     // sine of pole angle
//    double CosPoleAngle;     // cosine of pole angle
//    double a;         // workspace variables
//
//    cp = cos(PI * (Ucutoff + Lcutoff) / 2.0);
//    theta = PI * (Ucutoff - Lcutoff) / 2.0;
//    st = sin(theta);
//    ct = cos(theta);
//    s2t = 2.0*st*ct;        // sine of 2*theta
//    c2t = 2.0*ct*ct - 1.0;  // cosine of 2*theta
//
//    RCoeffs = (double *)calloc( 2 * FilterOrder, sizeof(double) );
//    TCoeffs = (double *)calloc( 2 * FilterOrder, sizeof(double) );
//
//    for( k = 0; k < FilterOrder; ++k )
//    {
//        PoleAngle = PI * (double)(2*k+1)/(double)(2*FilterOrder);
//        SinPoleAngle = sin(PoleAngle);
//        CosPoleAngle = cos(PoleAngle);
//        a = 1.0 + s2t*SinPoleAngle;
//        RCoeffs[2*k] = c2t/a;
//        RCoeffs[2*k+1] = s2t*CosPoleAngle/a;
//        TCoeffs[2*k] = -2.0*cp*(ct+st*SinPoleAngle)/a;
//        TCoeffs[2*k+1] = -2.0*cp*st*CosPoleAngle/a;
//    }
//
//    DenomCoeffs = TrinomialMultiply(FilterOrder, TCoeffs, RCoeffs );
//    free(TCoeffs);
//    free(RCoeffs);
//
//    DenomCoeffs[1] = DenomCoeffs[0];
//    DenomCoeffs[0] = 1.0;
//    for( k = 3; k <= 2*FilterOrder; ++k )
//        DenomCoeffs[k] = DenomCoeffs[2*k-2];
//
//
//    return DenomCoeffs;
//}
//
//void butterFilter(int ord, double *a, double *b, int np, vector<double> &x, double *y)
//{
//
//    int i,j;
//
//    y[0]=b[0] * x[0];
//
//    for (i=1;i<ord;i++)
//    {
//        y[i]=0.0;
//        for (j=0;j<i+1;j++)
//            y[i]=y[i]+b[j]*x[i-j];
//        for (j=0;j<i;j++)
//            y[i]=y[i]-a[j+1]*y[i-j-1];
//    }
//
//    for (i=ord;i<np;i++)
//    {
//        y[i]=0.0;
//        for (j=0;j<ord;j++)
//            y[i]=y[i]+b[j]*x[i-j];
//        for (j=0;j<ord;j++)
//            y[i]=y[i]-a[j+1]*y[i-j-1];
//    }
//	
//}
//
//
//
//void butterFilter_ed(int ord, double a[], double b[], int np, double *x, double *y)
//{
//	// B is Numerator and A is Denominator
//
//    int i,j;
//
//    y[0]=b[0] * x[0];
//
//    for (i=1;i<ord;i++)
//    {
//        y[i]=0.0;
//
//        for (j=0;j<i+1;j++)
//            y[i]=y[i]+b[j]*x[i-j];
//
//        for (j=0;j<i;j++)
//            y[i]=y[i]-a[j+1]*y[i-j-1];
//    }
//
//    for (i=ord;i<np;i++)
//    {
//        y[i]=0.0;
//
//        for (j=0;j<ord;j++)
//            y[i]=y[i]+b[j]*x[i-j];
//
//        for (j=0;j<ord-1;j++)
//            y[i]=y[i]-a[j+1]*y[i-j-1];
//    }
//	
//}
//
//
//
//
