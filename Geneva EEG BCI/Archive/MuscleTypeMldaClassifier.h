#include <string>
#include <stdio.h>
#pragma once
class CMuscleTypeMldaClassifier
{
public:
	CMuscleTypeMldaClassifier(std::string, const double);
	virtual ~CMuscleTypeMldaClassifier();
	int detectState(double* currentFeature);
	int getNoOfClass();
	void timeReslovedSelector(double* currentFeature);

private:
	double calcCovInvProd(double* X);
	void calcExpectation(double* featureVector, double* expectation);
	//void timeReslovedSelector(double* currentFeature);

private:
	double* m_prodBuffer;
	double* m_templateSec;
	double* m_extendedFeatures;
	double* m_tmpProb;
	double* m_tmpDistVect;
	double* m_tmpLogProb;
	double* m_expectation;
	double** m_covInvMatrix;
	double** m_classMeans;
	double** m_featureBuffer;
	size_t m_noOfFeatures;
	size_t m_noOfClass;
	size_t m_noOfTaps;
	size_t m_noOfChannels;
	size_t m_bufferSize;
	size_t m_bufferIndex;
	size_t* m_sampleInd;
	size_t* m_channelList;
	const double m_systemTimeStepSec;

	FILE* m_fileExpectation;
	static const bool m_logExpectationValues = false;
};

