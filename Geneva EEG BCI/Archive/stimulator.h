/**
* -stimulator.h-
* Header file for necessary structures
* 
*/

#pragma once

#define SR 24414.0625	// Sample Rate of RZ5 system
#define ms2s 24.4140625	// convert ms to samples

struct ch_info
{	
	double aAmp, aDur, cAmp, cDur, pInt, nPulses, phase, activity; 
	// anodic amplitude, anodic duration, cathodic ampliture, cathodic duration, ...
	//	pulse interval, number of pulses, phase (relative to ch 1), activity (reported from RZ5)
	CString desc;	// description
	int ch_on;	// these three may be redundant with array_status - something to change for style points in the future
	bool needs_reset, Tonic, ground;
};

struct ch_edits
{
	//CComboBox setting;	// electrode status
	CEdit desc;			// electrode location descriptions
	CEdit aAmp;			// anodic amplitudes
	CEdit aDur;			// anodic amplitudes
	//CEdit cAmp;			// cathodic amplitudes
	//CEdit cDur;			// cathodic amplitudes
	CEdit pInt;			// pulse train interval
	CEdit phase;		// electrode phases amplitudes
	CEdit nPulses;		// number of pulses in train
	//CEdit activity;		// electrode active or off
	CButton startBurst;	// start phasic train of pulses
	int channelNum;
};

struct fine_edits
{
	CComboBox channel;		// channel to adjust
	CComboBox attribute;	// feature to adjust
	CComboBox magnitude;	// size of adjustments
};

struct target_edit
{
	char channel[4];
	int feature, index_ch;
	double multiplier;
	bool monopolar;
};

struct over_shoot
{
	// if moving to individual overshoots, then move these to ch_info
	double osAmp, osDur;
	CEdit amp, dur;
	bool novelAmp, novelDur;
};

struct niko_searches
{
	// if moving to individual overshoots, then move these to ch_info
	CEdit window, window2;
	
};

struct nikoIdeas
{CString initialName;
int Tdt_System_mode_before_Recording;
CEdit hot, textBox_2, Amp_Incr;
CButton influButt_StimStart, influButt_StimStop,influButt_RecordStop,influButt_RecordStart;	// start phasic train of pulses
};
struct TextFiles
{clock_t start;
int HowOftenStart,record;
CString hiddenFile;
};