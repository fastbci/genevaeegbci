#include <string>
#include <stdio.h>

#ifndef DATABUFFER_H
#include "DataBuffer.h"
#endif

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#ifndef FFTWTRANSFORM_H
#include "FftwTransform.h"
#endif

#pragma once
class CLfpMldaClassifier
{
public:
	CLfpMldaClassifier(std::string modelFileName
		, const size_t& sampleRate
		, const size_t& numberOfChannels
		, const size_t& windowSize
		, const size_t& fftwindowSize);
	virtual ~CLfpMldaClassifier();
	int detectState(const size_t& noOfSamples, double** inputBuffer);
	int getNoOfClass();
	size_t getModelType();
	size_t getFeatureType();

private:
	double calcCovInvProd(double* X);
	void calcExpectation(double* featureVector, double* expectation);
	void timeReslovedSelector(const size_t& noOfSamples, double** inputBuffer);
	int stateMachineUnilateral(void);
	int stateMachineBilateral(void);

private:
	double* m_prodBuffer;
	double* m_templateSec;
	double* m_extendedFeatures;
	double* m_tmpProb;
	double* m_tmpDistVect;
	double* m_tmpLogProb;
	double* m_expectation;
	double** m_covInvMatrix;
	double** m_classMeans;
	size_t m_noOfFeatures;
	size_t m_noOfClass;
	size_t m_noOfTaps;
	size_t m_noOfChannels;
	size_t m_bufferSize;
	size_t m_bufferIndex;
	int m_currentState;
	size_t m_stateCounter;
	size_t m_refractoryPeriodStep;
	size_t* m_sampleInd;
	const size_t m_sampleRate;
	const size_t m_windowSize;
	double m_expectationThreshold;
	size_t m_modelType;			// Type of the model: 0 - Right unilateral; 1 - Left unilateral; 2 - Bilateral

	size_t m_sgfFrameLen;
	double* m_sgfTemplate;
	CDataBuffer  m_featureDataBuffer;
	double** m_tmpDataBuffer;

	CFftwTransform* m_fft;
	size_t m_fftWinLen;
	double* m_fftWinFunc;
	size_t m_noOfFreq;
	double** m_fftNorma;
	size_t m_freqBottom;
	size_t m_freqTop;
	size_t m_lfpFeaType;
	size_t m_maxWinLen;

	/// real spectrum of fft coefficients
	CDoubleVector             m_realSpectrum;
	/// imaginary spectrum of fft coefficients
	CDoubleVector             m_imagSpectrum;

	FILE* m_fileExpectation;
	FILE* m_feature;
	static const bool m_logExpectationValues = false;
};

