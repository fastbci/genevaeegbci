#include "stdafx.h"
#include "WaveformTableEntry.h"


WaveformTableEntry::WaveformTableEntry(void)
: Duration(0)
, Enables(0)
, Frequency(40)
{
}


WaveformTableEntry::~WaveformTableEntry(void)
{
}

WaveformTableEntry::WaveformTableEntry(int Dur, int En, int Freq)
{
	Duration = Dur;
	Enables = En;
	Frequency = Freq;
}