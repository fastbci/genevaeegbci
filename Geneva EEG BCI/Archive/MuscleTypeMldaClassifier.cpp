#include "stdafx.h"
#include "MuscleTypeMldaClassifier.h"
#include "ap.h"
#include <memory.h>
#include <stdio.h>
#include <algorithm>

CMuscleTypeMldaClassifier::CMuscleTypeMldaClassifier(std::string modelFileName, const double systemTimeStepSec)
	: m_prodBuffer(0)
	, m_templateSec(0)
	, m_extendedFeatures(0)
	, m_tmpProb(0)
	, m_tmpDistVect(0)
	, m_classMeans(0)
	, m_covInvMatrix(0)
	, m_featureBuffer(0)
	, m_sampleInd(0)
	, m_tmpLogProb(0)
	, m_systemTimeStepSec(systemTimeStepSec)
	, m_fileExpectation(0)
{
	double nclass;
	double nfeat;
	double noOfTaps;
	double chInd;

	FILE* file = fopen(modelFileName.c_str(), "r");
	fscanf(file, "%le", &nclass);
	fscanf(file, "%le", &nfeat);
	fscanf(file, "%le", &noOfTaps);

	m_noOfClass = size_t(nclass);
	m_noOfFeatures = size_t(nfeat);
	m_noOfTaps = size_t(noOfTaps);
	m_noOfChannels = size_t(m_noOfFeatures / m_noOfTaps);
	//m_expectationThreshold =  0.6;

	m_channelList = new size_t[m_noOfChannels];
	for (size_t ch = 0; ch<m_noOfChannels; ch++)
	{
		fscanf(file, "%le", &chInd);
		m_channelList[ch] = size_t(chInd) - 1;
	}

	m_sampleInd = new size_t[m_noOfTaps];
	m_tmpLogProb = new double[m_noOfClass];
	m_expectation = new double[m_noOfClass];

	m_templateSec = new double[m_noOfTaps];
	for (size_t i = 0; i<m_noOfTaps; i++)
	{
		fscanf(file, "%le", &m_templateSec[i]);
	}

	// load file that contains the parameters
	m_prodBuffer = new double[m_noOfFeatures];

	m_classMeans = new double*[m_noOfClass];
	for (size_t cl = 0; cl<m_noOfClass; cl++)
	{
		m_classMeans[cl] = new double[m_noOfFeatures];
		for (size_t ft = 0; ft<m_noOfFeatures; ft++)
		{
			fscanf(file, "%le", &m_classMeans[cl][ft]);
		}
	}

	m_covInvMatrix = new double*[m_noOfFeatures];
	for (size_t i = 0; i<m_noOfFeatures; i++)
	{
		m_covInvMatrix[i] = new double[m_noOfFeatures];
	}

	for (size_t i = 0; i<m_noOfFeatures; i++)
	{
		for (size_t j = 0; j<m_noOfFeatures; j++)
		{
			fscanf(file, "%le", &m_covInvMatrix[j][i]);
		}
	}

	fclose(file);

	m_bufferSize = size_t(-round(m_templateSec[m_noOfTaps - 1] / double(m_systemTimeStepSec))) + 1; //watch out values are negative
	m_bufferIndex = m_bufferSize - 1;
	m_featureBuffer = new double*[m_noOfChannels];
	for (size_t ch = 0; ch<m_noOfChannels; ch++)
	{
		m_featureBuffer[ch] = new double[m_bufferSize];
		memset(m_featureBuffer[ch], 0, m_bufferSize * sizeof(double));
	}

	m_extendedFeatures = new double[m_noOfFeatures];
	m_tmpProb = new double[m_noOfClass];
	m_tmpDistVect = new double[m_noOfFeatures];

	m_fileExpectation = fopen("Expectation.txt", "w");
}

CMuscleTypeMldaClassifier::~CMuscleTypeMldaClassifier()
{

	if (m_prodBuffer != 0)
	{
		delete[] m_prodBuffer;
		m_prodBuffer = 0;
	}

	if (m_templateSec != 0)
	{
		delete[] m_templateSec;
		m_templateSec = 0;
	}

	if (m_extendedFeatures != 0)
	{
		delete[] m_extendedFeatures;
		m_extendedFeatures = 0;
	}

	if (m_tmpProb != 0)
	{
		delete[] m_tmpProb;
		m_tmpProb = 0;
	}

	if (m_tmpDistVect != 0)
	{
		delete[] m_tmpDistVect;
		m_tmpDistVect = 0;
	}

	if (m_sampleInd != 0)
	{
		delete[] m_sampleInd;
		m_sampleInd = 0;
	}

	if (m_tmpLogProb != 0)
	{
		delete[] m_tmpLogProb;
		m_tmpLogProb = 0;
	}

	if (m_expectation != 0)
	{
		delete[] m_expectation;
		m_expectation = 0;
	}

	if (m_classMeans != 0)
	{
		for (size_t i = 0; i<m_noOfClass; i++)
		{
			delete[] m_classMeans[i];
			m_classMeans[i] = 0;
		}
		delete[] m_classMeans;
		m_classMeans = 0;
	}

	if (m_covInvMatrix != 0)
	{
		for (size_t i = 0; i<m_noOfFeatures; i++)
		{
			delete[] m_covInvMatrix[i];
			m_covInvMatrix[i] = 0;
		}
		delete[] m_covInvMatrix;
		m_covInvMatrix = 0;
	}

	if (m_featureBuffer != 0)
	{
		for (size_t i = 0; i<m_noOfChannels; i++)
		{
			delete[] m_featureBuffer[i];
			m_featureBuffer[i] = 0;
		}
		delete[] m_featureBuffer;
		m_featureBuffer = 0;
	}

	if (m_fileExpectation != 0)
	{
		fclose(m_fileExpectation);
		m_fileExpectation = 0;
	}
}

int CMuscleTypeMldaClassifier::getNoOfClass()
{
	return m_noOfClass;
}


double CMuscleTypeMldaClassifier::calcCovInvProd(double *X)
{
	double prod;

	//C*X
	for (size_t i = 0; i<m_noOfFeatures; i++)
	{
		prod = 0;
		for (size_t j = 0; j < m_noOfFeatures; j++)
		{
			prod = prod + X[j] * m_covInvMatrix[i][j];
		}
		m_prodBuffer[i] = prod;
	}

	//Y'*X
	prod = 0;
	for (size_t i = 0; i<m_noOfFeatures; i++)
	{
		prod = X[i] * m_prodBuffer[i] + prod;
	}

	return prod;
}

void CMuscleTypeMldaClassifier::calcExpectation(double* featureVector, double* expectation)
{
	for (size_t cl = 0; cl<m_noOfClass; cl++)
	{
		for (size_t ft = 0; ft<m_noOfFeatures; ft++)
		{
			m_tmpDistVect[ft] = featureVector[ft] - m_classMeans[cl][ft];
		}
		m_tmpProb[cl] = -calcCovInvProd(m_tmpDistVect) / 2;
	}

	// INIT
	memset(expectation, 0, m_noOfClass * sizeof(double));

	// LOOP for calculation of expectations
	double sum;
	for (size_t cl = 0; cl < m_noOfClass; cl++)
	{
		sum = 0.0;
		for (size_t na = 0; na<m_noOfClass; na++)
		{
			m_tmpLogProb[na] = m_tmpProb[na] - m_tmpProb[cl];
			sum = sum + exp(m_tmpLogProb[na]);
		}

		expectation[cl] = 1.0 / sum;
	}
}

void CMuscleTypeMldaClassifier::timeReslovedSelector(double* currentFeature)
{
	//current passed by reference and not by argument to allow modification
	m_bufferIndex++;
	if (m_bufferIndex >= m_bufferSize)
	{
		m_bufferIndex = 0;
	}

	for (size_t ch = 0; ch<m_noOfChannels; ch++)
	{
		m_featureBuffer[ch][m_bufferIndex] = currentFeature[m_channelList[ch]];
	}

	//retrieving the features from the buffer
	int tmpIndex;
	for (size_t tp = 0; tp<m_noOfTaps; tp++)
	{
		tmpIndex = int(round(m_templateSec[tp] / double(m_systemTimeStepSec))) + m_bufferIndex;
		if (tmpIndex < 0)
		{
			m_sampleInd[tp] = tmpIndex + m_bufferSize;
		}
		else
		{
			m_sampleInd[tp] = tmpIndex;
		}
	}

	for (size_t ch = 0; ch<m_noOfChannels; ch++)
	{
		for (size_t tp = 0; tp<m_noOfTaps; tp++)
		{
			m_extendedFeatures[ch * m_noOfTaps + tp] = m_featureBuffer[ch][m_sampleInd[tp]];
		}
	}
}

int CMuscleTypeMldaClassifier::detectState(double* currentFeature)
{
	int outState = 0;

	timeReslovedSelector(currentFeature);
	calcExpectation(m_extendedFeatures, m_expectation);

	if (m_logExpectationValues)
	{
		for (size_t cl = 0; cl<m_noOfClass; cl++)
		{
			fprintf(m_fileExpectation, "%f, ", m_expectation[cl]);
		}
		fprintf(m_fileExpectation, "\n");
	}

	outState = std::distance(m_expectation, std::max_element(m_expectation, m_expectation + m_noOfClass));

	return outState;
}

