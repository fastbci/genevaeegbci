#include "EogCorrectionAlgorithm.h"
#include <assert.h>

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

#ifndef MATLABLIBRARY_H
#include "../GenevaEegBci/MatlabLibrary.h"
#endif

using namespace std;


CEogCorrectionAlgorithm::CEogCorrectionAlgorithm(string calibrationFileName)
: m_regressionMatrix(0)
, m_numberOfNeuralChannels(0)
, m_numberOfEogChannels(0)
, m_helperVector(0)
{
    // Load numberOfSpectralBins from .mat file
    mxArray* matNumberOfNeuralChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfNeuralChannels");
    assert(NULL != matNumberOfNeuralChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfNeuralChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfNeuralChannels)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfNeuralChannels));
    m_numberOfNeuralChannels = static_cast<size_t>(dataElements[0]);


    // Load numberOfSignals from .mat file
    mxArray* matNumberOfEogChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfEogChannels");
    assert(NULL != matNumberOfEogChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfEogChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfEogChannels)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfEogChannels));
    m_numberOfEogChannels = static_cast<size_t>(dataElements[0]);


    // Load the projection matrix from .mat file
    mxArray* matRegressionMatrix = CMatlabLibrary::loadMatVariable(calibrationFileName, "regressionMatrix");
    assert(NULL != matRegressionMatrix);

    // Verify that the size is ok
    assert(m_numberOfNeuralChannels == mxGetM(matRegressionMatrix));
    assert(m_numberOfNeuralChannels + m_numberOfEogChannels + 1 == mxGetN(matRegressionMatrix));

    dataElements = static_cast<double*>(mxGetData(matRegressionMatrix));
    m_regressionMatrix = new CDoubleMatrix(m_numberOfNeuralChannels, m_numberOfNeuralChannels + m_numberOfEogChannels + 1, dataElements);

    m_helperVector = new CDoubleVector(m_numberOfNeuralChannels + m_numberOfEogChannels + 1, 1);
}


CEogCorrectionAlgorithm::~CEogCorrectionAlgorithm()
{
    if (m_regressionMatrix == 0)
    {
        delete m_regressionMatrix;
        m_regressionMatrix = 0;
    }

    if (m_helperVector == 0)
    {
        delete m_helperVector;
        m_helperVector = 0;
    }
}

void CEogCorrectionAlgorithm::process(CDoubleVector* const correctedSignal, const CDoubleVector * const neuralSignal, const CDoubleVector * const eogSignal)
{
    assert(eogSignal->getSize() == m_numberOfEogChannels);
    assert(neuralSignal->getSize() == m_numberOfNeuralChannels);
    assert(correctedSignal->getSize() == m_numberOfNeuralChannels);

    memcpy(m_helperVector->getValues() + 1, neuralSignal->getValues(), m_numberOfNeuralChannels * sizeof(double));
    memcpy(m_helperVector->getValues() + m_numberOfNeuralChannels + 1, eogSignal->getValues(), m_numberOfEogChannels * sizeof(double));

    CMathLibrary::multMatrixVector(correctedSignal, m_regressionMatrix[0], m_helperVector[0]);
}

size_t CEogCorrectionAlgorithm::getNumberOfNeuralChannels()
{
    return m_numberOfNeuralChannels;
}

size_t CEogCorrectionAlgorithm::getNumberOfEogChannels()
{
    return m_numberOfEogChannels;
}
