//#include "cbhwlib.h"
#include <string>

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Implementation of Common Average Reference algorithm
*/
class DLL_EXPORT CCarAlgorithm
{
public:
	/**
	* Constructor.
	*/
	CCarAlgorithm(const size_t totNumChannels
                , const size_t noOfCarChannels
                , const size_t noOfSelectedChannels
                , const size_t* const carChannels
                , const size_t* const selectedChannels
                , const size_t referenceChannel);

    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the EOG correction algorithm
    * @param referenceChannel           Reference channel
    */
    CCarAlgorithm(std::string calibrationFileName, const size_t referenceChannel);

	virtual ~CCarAlgorithm();

public:
	//void process(double*, INT16*);
	void process(double* result, double* input);
	void processArrayInPlace(const size_t noOfSamples, double** dataBuffer);

private:
    void setCarNorm(const size_t referenceChannel);

private: 
	/// total number of channels
	size_t m_totNumChannels;

	/// number of channels used for common average reference
	size_t m_noOfCarChannels;

    /// number used to divide the sum of signal accross channels
    double m_carNorm;

	/// number of channels provided in the output
	size_t m_noOfSelectedChannels;

	/// indices of common average reference channels.
	size_t* m_carChannelInd;

	/// indices of the output channels;
	size_t* m_selectedChannelInd;
};

