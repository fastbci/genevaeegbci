#ifndef ADAPTIVEFEATURENORMALIZATIONALGORITHM_H
#define ADAPTIVEFEATURENORMALIZATIONALGORITHM_H

#include <string>

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

class CDoubleVector;

/**
* @brief Filter algorithm that does normalizing to standard deviation of all selected channels.
*/
class DLL_EXPORT CAdaptiveFeatureNormalizationAlgorithm
{
public:
	/**
    * @brief constructor
    * @param means     must have same size as normFactors.
    * @param variances must have same size as means.
    * @param recursiveLength
    */
    CAdaptiveFeatureNormalizationAlgorithm(const CDoubleVector& means
                                            , const CDoubleVector& variances
                                            , const size_t recursiveLength);

	/**
	* @brief constructor
	* @param numberOfFeatures
	* @param initLength
	* @param recursiveLength
	*/
	CAdaptiveFeatureNormalizationAlgorithm(const size_t numberOfFeatures
                                            , const size_t initLength
		                                    , const size_t recursiveLength);

    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param modelFileName        Matlab file that holds the parameters needed to initialize the adaptive feature normalization algorithm
    */
    CAdaptiveFeatureNormalizationAlgorithm(std::string modelFileName);

	/// destructor
	virtual ~CAdaptiveFeatureNormalizationAlgorithm();

public:
	/**
	* @param input  Amplifier frames to normalize. It is expected that input
	*               contains only IAmplifierFrames.
	* @param output normalized IAmplifierFrames.
	* @throw runtime_error if not all IData members of input are of type IAmplifierFrame.
	*/
	void process(CDoubleVector& output, const CDoubleVector& input, const bool artifactPresence);

	/**
	* Resets the algorithm.
	*/
	void reset(const CDoubleVector& means, const CDoubleVector& standardDeviations);

	/**
	* Returns the safe normalization value
	*/
	double getSafeNormalizationValue() { return c_safeNormalizationValue; };

    /**
    * Returns the recursive length
    */
    size_t getRecursiveLength() { return m_recursiveLength; };

	/**
	* Returns the initialization length
	*/
	size_t getInitLength() { return m_initLength; };

	/**
	* Returns the initialization counter
	*/
	size_t getInitCounter() { return m_initCounter; };

	/**
	* Returns the initialization flag
	*/
	bool getIsInitialized() { return m_isInitialized; };

    /**
    * Returns the number of features
    */
    size_t getNumberOfFeatures() { return m_numberOfFeatures; };

private:
	/// safe value to prevent division with 0
	const double c_safeNormalizationValue = 1e-12;

	/// recursive length that controls the memory of the process
	size_t m_recursiveLength;

	/// initialization length to compute initial estimates on
	size_t m_initLength;

	/// current initialization step
	size_t m_initCounter;

	/// inicates if initial estimates are ready
	bool m_isInitialized;

	/// number of features == length of m_means and m_variances
	size_t m_numberOfFeatures;

	/// average values to subtract from each channel
	double* m_means;

	/// variance estimates
	double* m_variances;

	/// helper variable for initial variance estimates
	double* m_helpVariances;
};

#endif // ADAPTIVEFEATURENORMALIZATIONALGORITHM_H
