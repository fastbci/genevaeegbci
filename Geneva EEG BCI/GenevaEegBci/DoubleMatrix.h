#ifndef DOUBLEMATRIX_H
#define DOUBLEMATRIX_H

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
@brief encapsulate a Matrix of double
*/
class DLL_EXPORT CDoubleMatrix
{
public:
    /**
    Initializing the matrix with null.

    @param rowSize row Size, number of columns
    @param columnSize column size, number of rows
    */
    CDoubleMatrix(const size_t& rowSize, const size_t& columnSize);

    /**
    Initializing the matrix with a constant.

    @param rowSize      row size, number of columns
    @param columnSize   column size, number of rows
    @param initValue    initial Value for the matrix
    */
    CDoubleMatrix(const size_t& rowSize, const size_t& columnSize,
        const double& initValue);

    /**
    Initializing the matrix with a vector given by an pointer, the vector must have
    (rowSize * columnSize) valid elements. The matrix is filled column by column.

    @param rowSize      row Size, number of columns
    @param columnSize   column size, number of rows
    @param values       vector of values for initializing
    */
    CDoubleMatrix(const size_t& rowSize, const size_t& columnSize,
        const double* const values);
    
    /**
    Copy constructor

    @param other matrix to copy
    */
    CDoubleMatrix(const CDoubleMatrix& other);

    /**
    destructor
    */
    virtual ~CDoubleMatrix();

public:
    /**
    size of the Columns, number of rows
    */
    size_t getColumnSize() const;
    
    /**
    size of the rows, the number of columns
    */
    size_t getRowSize() const;
    
    /**
    Returns a Pointer,
    that points on the first element of the matrix.
    Order of the Elements is: first column, from top to bottom,
    then the second(top to bottom), and so on.
    The pointer is owned by the matrix.
    */
    double* const getValues() const;
    
    /**
    Returns a pointer to a column of this matrix.
    The pointer is owned by the matrix.

    @param columnIndex index of the column to get
    @return pointer to the column
    */
    double* const getColumnAt(size_t columnIndex) const;

    /**
    assignment operator
    */
    CDoubleMatrix& operator=(const CDoubleMatrix& other);

    /**
    PLEASE DOCUMENT
    @param toInsert
    @param columnOffset
    @param rowOffset
    */
    void insertAt(const CDoubleMatrix& toInsert, const size_t columnOffset,
        const size_t rowOffset);

    /**
    subtracts a other matrix from this matrix (this[i] = this[i] - other[i])
    @param other matrix to subtract 
    */
    void subtract(const CDoubleMatrix& other);

    /**
    adds a other matrix to this matrix (this[i] = this[i] + other[i])
    @param other matrix to add
    */
    void add(const CDoubleMatrix& other);

    /**
    adds a matrix with a factor to this matrix (this[i] = this[i] + (factor * other[i]))
    @param factor the factor
    @param other matrix to add
    */
    void addWeighted(const double& factor, const CDoubleMatrix& other);

    /**
    multiply factor to all values of the matrix
    @param factor value witch is multiplied to all values of the matrix
    */
    void mult(const double& factor);

    /**
    @return transposed matrix
    */
    CDoubleMatrix getTransposed() const;

    double& at(const size_t& row, const size_t& column) const;
	double& operator [] (size_t position);
    

private:
    const size_t m_columnSize;
    const size_t m_rowSize;
    double* m_values;
};

#endif // DOUBLEMATRIX_H