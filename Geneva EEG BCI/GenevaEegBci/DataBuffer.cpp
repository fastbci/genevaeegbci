#include "DataBuffer.h"

#include <assert.h>
#include <memory.h>
#include <stdexcept>

using namespace std;

CDataBuffer::CDataBuffer(const size_t& numberOfChannels
	, const size_t& capacity
	, const TimeOrder& timeOrder)
: m_numberOfChannels(numberOfChannels)
, m_capacity        (capacity)
, m_index           (capacity)
, m_bufferedSignal        (0)
, m_timeOrder       (timeOrder)
{
	assert(m_numberOfChannels > 0);
	assert(m_capacity > 0);
	assert(m_index == m_capacity);

	m_bufferedSignal = new double*[m_numberOfChannels];
	assert(m_bufferedSignal != 0);

	const size_t channelBufferSize = 2 * m_capacity;
	for(size_t channel = 0; channel < m_numberOfChannels ; ++channel)
	{
		m_bufferedSignal[channel] = new double[channelBufferSize];
		assert(m_bufferedSignal[channel] != 0);
		memset(m_bufferedSignal[channel], 0, channelBufferSize * sizeof(double));
	}
}

CDataBuffer::~CDataBuffer()
{
	for(size_t channel = 0; channel < m_numberOfChannels ; ++channel)
	{
		delete[] m_bufferedSignal[channel];
		m_bufferedSignal[channel] = 0;
	}
	delete[] m_bufferedSignal;
	m_bufferedSignal = 0;
}

size_t CDataBuffer::getNumberOfChannels() const
{
	return m_numberOfChannels;
}

size_t CDataBuffer::getCapacity() const
{
	return m_capacity;
}

double* CDataBuffer::getChannel(const size_t& channelNumber)
{
	if(channelNumber > m_numberOfChannels)
	{
		throw runtime_error("CDataBuffer::getChannel(): Channel number too big.");
	}
	else
	{
		if(m_timeOrder == LAST_IN_AT_BEGGINING)
		{
			return m_bufferedSignal[channelNumber] + m_index;
		}
		else
		{
			return m_bufferedSignal[channelNumber] + m_index - m_capacity + 1;
		}
	}
}

void CDataBuffer::getBuffer(double** bufferPointer)
{
	if(m_timeOrder == LAST_IN_AT_BEGGINING)
	{
		for (size_t ch = 0; ch < m_numberOfChannels; ++ch)
		{
			bufferPointer[ch] = m_bufferedSignal[ch] + m_index;
		}
	}
	else
	{
		for (size_t ch = 0; ch < m_numberOfChannels; ++ch)
		{
			bufferPointer[ch] = m_bufferedSignal[ch] + m_index - m_capacity + 1;
		}
	}
}

CDataBuffer::TimeOrder CDataBuffer::getTimeOrder() const
{
	return m_timeOrder;
}

void CDataBuffer::appendOneFrame(const double* const signal)
{
	if(m_timeOrder == LAST_IN_AT_BEGGINING)
	{
		if(m_index == 0)
		{
			m_index += m_capacity;
		}
		--m_index;
		for(size_t channelNumber = 0; channelNumber < m_numberOfChannels ; ++channelNumber)
		{
			m_bufferedSignal[channelNumber][m_index] = signal[channelNumber];
			m_bufferedSignal[channelNumber][m_index + m_capacity] = signal[channelNumber];
		}
	}
	else
	{
		if(m_index == 2 * m_capacity - 1)
		{
			m_index -= m_capacity;
		}
		++m_index;
		for(size_t channelNumber = 0; channelNumber < m_numberOfChannels ; ++channelNumber)
		{
			m_bufferedSignal[channelNumber][m_index] = signal[channelNumber];
			m_bufferedSignal[channelNumber][m_index - m_capacity] = signal[channelNumber];
		}
	}     
}

void CDataBuffer::append(const size_t& noOfSamples, const double* const* const signal)
{
	if(m_timeOrder == LAST_IN_AT_BEGGINING)
	{
		for (size_t si = 0; si < noOfSamples; ++si)
		{
			if(m_index == 0)
			{
				m_index += m_capacity;
			}
			--m_index;
			for(size_t channelNumber = 0; channelNumber < m_numberOfChannels ; ++channelNumber)
			{
				m_bufferedSignal[channelNumber][m_index] = signal[channelNumber][si];
				m_bufferedSignal[channelNumber][m_index + m_capacity] = signal[channelNumber][si];
			}
		}
	}
	else
	{
		for (size_t si = 0; si < noOfSamples; ++si)
		{
			if(m_index == 2 * m_capacity - 1)
			{
				m_index -= m_capacity;
			}
			++m_index;
			for(size_t channelNumber = 0; channelNumber < m_numberOfChannels ; ++channelNumber)
			{
				m_bufferedSignal[channelNumber][m_index] = signal[channelNumber][si];
				m_bufferedSignal[channelNumber][m_index - m_capacity] = signal[channelNumber][si];
			}
		}
	}     
}

void CDataBuffer::flushBuffer()
{
    for (size_t channelNumber = 0; channelNumber < m_numberOfChannels; ++channelNumber)
    {
        memset(m_bufferedSignal[channelNumber], 0.0, 2 * m_capacity * sizeof(double));
    }
}