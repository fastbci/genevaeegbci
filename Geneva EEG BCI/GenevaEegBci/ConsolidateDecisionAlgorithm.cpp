#include "ConsolidateDecisionAlgorithm.h"
#include <assert.h>

#ifndef MATLABLIBRARY_H
#include "MatlabLibrary.h"
#endif

using namespace std;

CConsolidateDecisionAlgorithm::CConsolidateDecisionAlgorithm(string calibrationFileName)
: m_decisionRefractoryPeriod(0)
, m_artifactRefractoryPeriod(0)
, m_decisionRefractoryCounter(0)
, m_artifactRefractoryCounter(0)
{
    // Load decision refractory period from the .mat file
    mxArray *matDecisionRefractoryPeriod = CMatlabLibrary::loadMatVariable(calibrationFileName, "decisionRefractoryPeriod");
    assert(NULL != matDecisionRefractoryPeriod);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matDecisionRefractoryPeriod)));
    assert(1 == static_cast<int>(mxGetM(matDecisionRefractoryPeriod)));

    double* dataElements = static_cast<double*>(mxGetData(matDecisionRefractoryPeriod));
    m_decisionRefractoryPeriod = static_cast<size_t>(dataElements[0]);


    // Load m_numberOfInitFilterings from the .mat file
    mxArray *matArtifactRefractoryPeriod = CMatlabLibrary::loadMatVariable(calibrationFileName, "artifactRefractoryPeriod");
    assert(NULL != matArtifactRefractoryPeriod);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matArtifactRefractoryPeriod)));
    assert(1 == static_cast<int>(mxGetM(matArtifactRefractoryPeriod)));

    dataElements = static_cast<double*>(mxGetData(matArtifactRefractoryPeriod));
    m_artifactRefractoryPeriod = static_cast<size_t>(dataElements[0]);

    m_decisionRefractoryCounter = m_decisionRefractoryPeriod;
    m_artifactRefractoryCounter = m_artifactRefractoryPeriod;
}


CConsolidateDecisionAlgorithm::~CConsolidateDecisionAlgorithm()
{
}


const size_t CConsolidateDecisionAlgorithm::process(const size_t decoderOutput, const bool artifactPresence, const size_t numberOfReceivedSamples)
{
	if (artifactPresence)
	{
		m_artifactRefractoryCounter = 0;
	}

    if ((artifactPresence && m_artifactRefractoryPeriod == 0) || m_artifactRefractoryCounter < m_artifactRefractoryPeriod || m_decisionRefractoryCounter < m_decisionRefractoryPeriod)
    {
		m_decisionRefractoryCounter += numberOfReceivedSamples;

		if (m_artifactRefractoryPeriod == 0 || !artifactPresence)
		{
			m_artifactRefractoryCounter += numberOfReceivedSamples;
		}

        return 0;
    }
    else if (decoderOutput > 0)
    {
        m_decisionRefractoryCounter = 0;
        return decoderOutput;
    }

    return 0;
}
