#ifndef SPECTRALSOURCERECONSTRUCTION_H
#define SPECTRALSOURCERECONSTRUCTION_H

#include <string>

class CDoubleMatrix;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Projection of spectrally decomposed neural signals into spectral activity of cortical sources
* @author Tom
*
* Implementation detail: The module is used to process each incoming spectral matrix
* into an output matrix.
*/
class DLL_EXPORT CSpectralSourceReconstruction
{
public:
    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the spectral source reconstruction
    */
    CSpectralSourceReconstruction(std::string calibrationFileName);

    /**
    * Destructor.
    */
    ~CSpectralSourceReconstruction();

    /**
    * Process function - used to calculate the activity of cortical sources from the neural signal.
    *
    * @param spectralCorticalActivity   The spectral activity of cortical sources
    * @param spectralNeuralActivity     The spectral activity of neural signals (e.g. EEG activity)
    */
    void process(CDoubleMatrix* const spectralCorticalActivity, const CDoubleMatrix * const spectralNeuralActivity);

    /**
    * Function that returns the number of neural signals.
    */
    size_t getNumberOfSpectralBins();

    /**
    * Function that returns the number of neural signals.
    */
    size_t getNumberOfChannels();

    /**
    * Function that returns the number of cortical sources.
    */
    size_t getNumberOfCorticalSources();

private:
    /// Matrix that projects neural signals into activity of cortical sources
    CDoubleMatrix* m_projectionMatrix;

    /// Number of spectral bins
    size_t m_numberOfSpectralBins;

    /// Number of neural signals
    size_t m_numberOfChannels;

    /// Number of cortical sources
    size_t m_numberOfCorticalSources;
};

#endif // SPECTRALSOURCERECONSTRUCTION_H


