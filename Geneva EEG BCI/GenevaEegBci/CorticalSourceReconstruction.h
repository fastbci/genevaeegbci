#ifndef CORTICALSOURCERECONSTRUCTION_H
#define CORTICALSOURCERECONSTRUCTION_H

#include <string>

class CDoubleVector;
class CDoubleMatrix;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Projection of neural signals into activity of cortical sources
* @author Tom
*
* Implementation detail: The module is used to process each incoming feature vector
* into an output vector.
*/
class DLL_EXPORT CCorticalSourceReconstruction
{
public:
    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the decoder
    */
    CCorticalSourceReconstruction(std::string calibrationFileName);

    /**
    * Destructor.
    */
    ~CCorticalSourceReconstruction();

    /**
    * Process function - used to calculate the activity of cortical sources from the neural signal.
    *
    * @param signalVector		        The neural signals (e.g. EEG activity)
    * @param corticalSourceActivity		The activity of cortical sources
    */
    void process(CDoubleVector* const corticalSourceActivity, const CDoubleVector * const signalVector);

    /**
    * Function that returns the number of neural signals.
    */
    size_t getNumberOfSignals();

    /**
    * Function that returns the number of cortical sources.
    */
    size_t getNumberOfCorticalSources();

private:
    /// Matrix that projects neural signals into activity of cortical sources
    CDoubleMatrix* m_projectionMatrix;

    /// Dimensinality of the neural signals
    size_t m_numberOfSignals;

    /// Number of cortical sources
    size_t m_numberOfCorticalSources;
};

#endif // CORTICALSOURCERECONSTRUCTION_H
