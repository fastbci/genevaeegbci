#include "SerialRationalTransferFunctionFilter.h"
#include <assert.h>

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#ifndef DATABUFFER_H
#include "DataBuffer.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

#ifndef MATLABLIBRARY_H
#include "MatlabLibrary.h"
#endif

using namespace std;



CSerialRationalTransferFunctionFilter::CSerialRationalTransferFunctionFilter(string calibrationFileName)
: m_numberOfChannels(0)
, m_numeratorFilterCoefficients(0)
, m_denominatorFilterCoefficients(0)
, m_leadingDenominator(1.0)
, m_rawBuffer(0)
, m_filteredBuffer(0)
, m_isRawBufferFilled(false)
, m_isFilteredBufferFilled(false)
, m_isFilterStable(false)
, m_numberOfBufferedRawSamples(0)
, m_numberOfBufferedFilteredSamples(0)
, m_numberOfFilteredSamples(0)
, m_helperRawChannelData(0)
, m_helperFilteredChannelData(0)
{
    // Load m_numberOfChannels from the .mat file
    mxArray *matNumberOfChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfChannels");
    assert(NULL != matNumberOfChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfChannels)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfChannels));
    m_numberOfChannels = static_cast<size_t>(dataElements[0]);


    // Load m_numberOfInitFilterings from the .mat file
    mxArray *matNumberOfInitFilterings = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfInitFilterings");
    assert(NULL != matNumberOfInitFilterings);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfInitFilterings)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfInitFilterings)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfInitFilterings));
    m_numberOfInitFilterings = static_cast<size_t>(dataElements[0]);


    // Load m_numeratorFilterCoefficients from the .mat file
    mxArray* matNumeratorFilterCoefficients = CMatlabLibrary::loadMatVariable(calibrationFileName, "numeratorFilterCoefficients");
    assert(NULL != matNumeratorFilterCoefficients);

    // Verify that the size is ok
    const size_t numberOfNumeratorFilterCoefficients = static_cast<int>(mxGetN(matNumeratorFilterCoefficients));
    assert(1 == static_cast<int>(mxGetM(matNumeratorFilterCoefficients)));

    dataElements = static_cast<double*>(mxGetData(matNumeratorFilterCoefficients));
    m_numeratorFilterCoefficients = new CDoubleVector(numberOfNumeratorFilterCoefficients, dataElements);
    m_helperRawChannelData = new CDoubleVector(numberOfNumeratorFilterCoefficients, 0.0);
    m_rawBuffer = new CDataBuffer(m_numberOfChannels, numberOfNumeratorFilterCoefficients, CDataBuffer::LAST_IN_AT_BEGGINING);


    // Load m_denominatorFilterCoefficients from the .mat file
    mxArray* matDenominatorFilterCoefficients = CMatlabLibrary::loadMatVariable(calibrationFileName, "denominatorFilterCoefficients");
    assert(NULL != matDenominatorFilterCoefficients);

    // Verify that the size is ok
    const size_t numberOfDenominatorFilterCoefficients = static_cast<int>(mxGetN(matDenominatorFilterCoefficients));
    assert(1 == static_cast<int>(mxGetM(matDenominatorFilterCoefficients)));

    dataElements = static_cast<double*>(mxGetData(matDenominatorFilterCoefficients));
    m_leadingDenominator = dataElements[0];

    if (numberOfDenominatorFilterCoefficients > 1)
    {
        m_denominatorFilterCoefficients = new CDoubleVector(numberOfDenominatorFilterCoefficients - 1, dataElements + 1);
        m_filteredBuffer = new CDataBuffer(m_numberOfChannels, numberOfDenominatorFilterCoefficients - 1, CDataBuffer::LAST_IN_AT_BEGGINING);
        m_helperFilteredChannelData = new CDoubleVector(numberOfDenominatorFilterCoefficients - 1, 0.0);
    }
    else
    {
        m_isFilteredBufferFilled = true;
    }
}


CSerialRationalTransferFunctionFilter::~CSerialRationalTransferFunctionFilter()
{
    if (m_numeratorFilterCoefficients == 0)
    {
        delete m_numeratorFilterCoefficients;
        m_numeratorFilterCoefficients = 0;
    }

    if (m_denominatorFilterCoefficients == 0)
    {
        delete m_denominatorFilterCoefficients;
        m_denominatorFilterCoefficients = 0;
    }

    if (m_rawBuffer == 0)
    {
        delete m_rawBuffer;
        m_rawBuffer = 0;
    }

    if (m_filteredBuffer == 0)
    {
        delete m_filteredBuffer;
        m_filteredBuffer = 0;
    }

    if (m_helperRawChannelData == 0)
    {
        delete m_helperRawChannelData;
        m_helperRawChannelData = 0;
    }

    if (m_helperFilteredChannelData == 0)
    {
        delete m_helperFilteredChannelData;
        m_helperFilteredChannelData = 0;
    }
}

const bool CSerialRationalTransferFunctionFilter::process(CDoubleVector* const filteredSignal, const CDoubleVector * const rawSignal)
{
    assert(rawSignal->getSize() == m_numberOfChannels);
    assert(filteredSignal->getSize() == m_numberOfChannels);
    m_rawBuffer->appendOneFrame(rawSignal->getValues());

    if (!m_isRawBufferFilled)
    {
        ++m_numberOfBufferedRawSamples;
        if (m_numberOfBufferedRawSamples == m_rawBuffer->getCapacity())
        {
            m_isRawBufferFilled = true;
        }
    }

    double numeratorPart = 0;
    double denominatorPart = 0;
    for (size_t channel = 0; channel < m_numberOfChannels; ++channel)
    {
        memcpy(m_helperRawChannelData->getValues(), m_rawBuffer->getChannel(channel), m_rawBuffer->getCapacity() * sizeof(double));
        numeratorPart = CMathLibrary::multVectorDotVector(m_numeratorFilterCoefficients[0], m_helperRawChannelData[0]);

        if (m_filteredBuffer != 0)
        {
            memcpy(m_helperFilteredChannelData->getValues(), m_filteredBuffer->getChannel(channel), m_filteredBuffer->getCapacity() * sizeof(double));
            denominatorPart = CMathLibrary::multVectorDotVector(m_denominatorFilterCoefficients[0], m_helperFilteredChannelData[0]);
        }

        filteredSignal->getValues()[channel] = (numeratorPart - denominatorPart) / m_leadingDenominator;
    }

    m_filteredBuffer->appendOneFrame(filteredSignal->getValues());

    if (!m_isFilteredBufferFilled)
    {
        ++m_numberOfBufferedFilteredSamples;
        if (m_numberOfBufferedFilteredSamples == m_filteredBuffer->getCapacity())
        {
            m_isFilteredBufferFilled = true;
        }
    }

    if (!m_isFilterStable)
    {
        ++m_numberOfFilteredSamples;
        if (m_numberOfFilteredSamples == m_numberOfInitFilterings)
        {
            m_isFilterStable = true;
        }
    }

    return m_isRawBufferFilled * m_isFilteredBufferFilled * m_isFilterStable;
}

const size_t CSerialRationalTransferFunctionFilter::getNumberOfChannels()
{
    return m_numberOfChannels;
}