#ifndef FEATURETIMESELECTOR_H
#define FEATURETIMESELECTOR_H

#include <vector>
#include <string>

class CSizeTVector;
class CDoubleVector;
class CDataBuffer;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Implementation of the time selector of the features
* @author Tom
*
* Implementation detail: The module holds the input features in a buffer and,
* for every cycle, return one set of features sampled at specific time points.
*/
class DLL_EXPORT CFeatureTimeSelector
{
public:
	/**
	* Constructor.
	*
	* @param featureTaps		Set of indeces that show which timepoints form the buffer to chose from
	* @param numberOfFeatures   Number of features in each sample
	*/
	CFeatureTimeSelector(const CSizeTVector* const featureTaps, const size_t numberOfFeatures);

    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the feature time selector
    */
    CFeatureTimeSelector(std::string calibrationFileName);

    /**
	* Destructor.
	*/
	~CFeatureTimeSelector();

	/**
	* The process function that outputs one set of features on every software cycle.
	*
	* @param output		Set of features aligned in the vector sampled from the buffer.
	*/
	void generateFeatureVector(CDoubleVector* output);

    /**
    * Add input frames to the buffer.
    *
    * @param input			input that contains a set of feature samples
    */
    const bool addToBuffer(std::vector<CDoubleVector*>& input);

	/**
	* Add one input frame to the buffer.
	*
	* @param input			input that contains a set of features
	*/
	const bool addToBuffer(CDoubleVector* input);

	/**
    * Flush the internal buffer of the class.
    */
    void flushBuffer();

    /**
    * Function that returns the number of taps.
    */
    size_t getNumberOfTaps();

    /**
    * Function that returns the number of input features.
    */
    size_t getNumberOfFeatures();

	/**
	* Function that returns if the buffer is filled.
	*/
	bool getIsBufferFilled() { return m_isBufferFilled; };

private:
	/// Number of feature taps
	size_t m_numberOfTaps;

	/// Feature taps
	CSizeTVector* m_featureTaps;

	/// Number of channels
	size_t m_numberOfFeatures;

	/// buffer for the feature values to be selected
	CDataBuffer* m_buffer;

    /// is the buffer filled with data
    bool  m_isBufferFilled;

    /// number of signal samples in the buffer
    size_t  m_numberOfBufferedSamples;
};

#endif // FEATURETIMESELECTOR_H
