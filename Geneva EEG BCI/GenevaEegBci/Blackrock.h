#include "cbsdk.h" // Blackrock library
#include <string>
#include <vector>

#pragma once
class CBlackrock
{
public:
	CBlackrock(void);
	virtual ~CBlackrock(void);

	/////////////
	// Methods
	/////////////

	int connection_init_EMG_secondVersion(void);
	int connection_init_EMG(void);
	int connection_init_neuro(void);
	int connection_init_neuro_double(void);
	int connection_init_spikes(void);
	void connection_end(void);
	void get_EMG_old(const int, const int, const int* const, INT16*, INT16*, INT16**);
	void get_EMG_secondVersion(const int* const, INT16**);
	void get_EMG(const size_t, const size_t* const, INT16**, size_t&);
	void get_neuro(const size_t, const size_t* const, INT16**, size_t&, INT16**, size_t&);
	void get_neuro_downsample(const size_t, const size_t* const, double**, const size_t, const size_t, size_t&, size_t&);
	void get_neuro_double(const size_t, const size_t* const, double**, size_t&);
	void get_spikes(UINT32***, double***, double*);
	UINT32 getTime(void);
	size_t getSampleRate(void);
	void setComment(UINT32, std::string);
	void setup_connection_blackrock(void);
	void flushBuffer(void);

	// Get analog data
	std::vector<std::string> getAnalogData();
private:
	/////////////
	// Methods
	/////////////

	/////////////
	// Variables
	/////////////

	// BlackRock identifier
	UINT32 m_blkrckInstance;

	// BlackRock data acquisition
	cbSdkTrialEvent m_trialEvent;
	cbSdkTrialCont m_trialCont;

	// Get channel label
	char m_label[cbNUM_ANALOG_CHANS][17];

	// Spikes
	const size_t m_spikeBufferSize;

	// Neuro
	const size_t m_neuroBufferSize;
	const size_t m_neuronalSampleRate;
	const size_t m_numNeuroChannels;

	// EMG
	const size_t m_emgBufferSize;
	const size_t m_emgSampleRate;
	const size_t m_numEmgChannels;
	std::vector<std::string> listAnalogChannels;

	// Variable for set comment
	static const UINT m_charset = 1;
};

