#include "FeatureTimeSelector.h"
#include <assert.h>

#ifndef DATABUFFER_H
#include "DataBuffer.h"
#endif

#ifndef SIZETVECTOR_H
#include "SizeTVector.h"
#endif

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#ifndef MATLABLIBRARY_H
#include "MatlabLibrary.h"
#endif


using namespace std;


CFeatureTimeSelector::CFeatureTimeSelector(const CSizeTVector* const featureTaps
										 , const size_t numberOfFeatures)
: m_numberOfTaps(featureTaps->getSize())
, m_featureTaps(new CSizeTVector(featureTaps->getSize(),featureTaps->getValues()))
, m_numberOfFeatures(numberOfFeatures)
, m_buffer(new CDataBuffer(numberOfFeatures, featureTaps->getMax() + 1, CDataBuffer::LAST_IN_AT_BEGGINING))
, m_isBufferFilled(false)
, m_numberOfBufferedSamples(0)
{
}

CFeatureTimeSelector::CFeatureTimeSelector(string calibrationFileName)
: m_numberOfTaps(0)
, m_featureTaps(0)
, m_numberOfFeatures(0)
, m_buffer(0)
, m_isBufferFilled(false)
, m_numberOfBufferedSamples(0)
{
    // Load numberOfFeatures from .mat file
    mxArray *matNumberOfFeatures = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfFeatures");
    assert(NULL != matNumberOfFeatures);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfFeatures)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfFeatures)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfFeatures));
    m_numberOfFeatures = static_cast<size_t>(dataElements[0]);


    // Load recursiveLength from .mat file
    mxArray* matNumberOfTaps = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfTaps");
    assert(NULL != matNumberOfTaps);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfTaps)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfTaps)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfTaps));
    m_numberOfTaps = static_cast<size_t>(dataElements[0]);


    // Load the initial feature means from .mat file
    mxArray* matFeatureTaps = CMatlabLibrary::loadMatVariable(calibrationFileName, "featureTaps");
    assert(NULL != matFeatureTaps);

    // Store data in data Buffer
    assert(m_numberOfTaps == mxGetM(matFeatureTaps));
    assert(1 == static_cast<int>(mxGetN(matFeatureTaps)));

    dataElements = static_cast<double*>(mxGetData(matFeatureTaps));
    m_featureTaps = new CSizeTVector(m_numberOfTaps);
    for (size_t tapIndex = 0; tapIndex < m_numberOfTaps; ++tapIndex)
    {
        m_featureTaps->getValues()[tapIndex] = static_cast<size_t>(dataElements[tapIndex]);
    }

    m_buffer = new CDataBuffer(m_numberOfFeatures, m_featureTaps->getMax() + 1, CDataBuffer::LAST_IN_AT_BEGGINING);
}

CFeatureTimeSelector::~CFeatureTimeSelector()
{
    if (m_featureTaps == 0)
    {
        delete m_featureTaps;
        m_featureTaps = 0;
    }

    if (m_buffer == 0)
    {
        delete m_buffer;
        m_buffer = 0;
    }
}

void CFeatureTimeSelector::generateFeatureVector(CDoubleVector* output)
{
	assert(output->getSize() == m_numberOfFeatures * m_numberOfTaps);
	double* featurePointer = output->getValues();
	for (size_t featureInd = 0; featureInd < m_numberOfFeatures; ++featureInd)
	{
		for (size_t tapInd = 0; tapInd < m_numberOfTaps; ++tapInd)
		{
			featurePointer[featureInd*m_numberOfTaps + tapInd] = m_buffer->getChannel(featureInd)[m_featureTaps->getValues()[tapInd]];
		}
	}
}

const bool CFeatureTimeSelector::addToBuffer(std::vector<CDoubleVector*>& input)
{
    /// Insert the new feature samples into the buffer
    for (size_t inputInd = 0; inputInd < input.size(); ++inputInd)
    {
        assert(input[inputInd]->getSize() == m_numberOfFeatures);
        m_buffer->appendOneFrame(input[inputInd]->getValues());
        delete input[inputInd];

        if (!m_isBufferFilled)
        {
            ++m_numberOfBufferedSamples;
            if (m_numberOfBufferedSamples == m_buffer->getCapacity())
            {
                m_isBufferFilled = true;
            }
        }
    }
    input.clear();

    return m_isBufferFilled;
}

const bool CFeatureTimeSelector::addToBuffer(CDoubleVector* input)
{
	/// Insert a new feature sample into the buffer
	assert(input->getSize() == m_numberOfFeatures);
	m_buffer->appendOneFrame(input->getValues());

	if (!m_isBufferFilled)
	{
		++m_numberOfBufferedSamples;
		if (m_numberOfBufferedSamples == m_buffer->getCapacity())
		{
			m_isBufferFilled = true;
		}
	}

	return m_isBufferFilled;
}

void CFeatureTimeSelector::flushBuffer()
{
    m_buffer->flushBuffer();
    m_isBufferFilled = false;
    m_numberOfBufferedSamples = 0;
}

size_t CFeatureTimeSelector::getNumberOfTaps()
{
    return m_numberOfTaps;
}

size_t CFeatureTimeSelector::getNumberOfFeatures()
{
    return m_numberOfFeatures;
}