#include "AdaptiveFeatureNormalizationAlgorithm.h"

#include <assert.h>
#include <stdexcept>
#include <sstream>
#include <fstream>

#ifndef MATLABLIBRARY_H
#include "MatlabLibrary.h"
#endif

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif


using namespace std;

CAdaptiveFeatureNormalizationAlgorithm::CAdaptiveFeatureNormalizationAlgorithm(const CDoubleVector& means
																			 , const CDoubleVector& variances
																			 , const size_t recursiveLength)
: m_numberOfFeatures(means.getSize())
, m_means(new double[means.getSize()])
, m_variances(new double[variances.getSize()])
, m_helpVariances(0)
, m_recursiveLength(recursiveLength)
, m_initLength(0)
, m_initCounter(0)
, m_isInitialized(true)
{
	if (variances.getSize() != means.getSize())
	{
		throw runtime_error("CAdaptiveFeatureNormalizationAlgorithm: means and variances don't have the same size.");
	}
	memcpy(m_means, means.getValues(), m_numberOfFeatures * sizeof(double));
	memcpy(m_variances, variances.getValues(), m_numberOfFeatures * sizeof(double));
}

CAdaptiveFeatureNormalizationAlgorithm::CAdaptiveFeatureNormalizationAlgorithm(const size_t numberOfFeatures
	, const size_t initLength
	, const size_t recursiveLength)
	: m_numberOfFeatures(numberOfFeatures)
	, m_initLength(initLength)
	, m_recursiveLength(recursiveLength)
	, m_initCounter(0)
	, m_isInitialized(false)
{
	// Initialize means and variances
	m_means = new double[m_numberOfFeatures];
	m_variances = new double[m_numberOfFeatures];
	m_helpVariances = new double[m_numberOfFeatures];

	memset(m_means, 0, m_numberOfFeatures * sizeof(double));
	memset(m_variances, 0, m_numberOfFeatures * sizeof(double));
	memset(m_helpVariances, 0, m_numberOfFeatures * sizeof(double));
}

CAdaptiveFeatureNormalizationAlgorithm::CAdaptiveFeatureNormalizationAlgorithm(string modelFileName)
: m_initCounter(0) 
, m_isInitialized(false)
{
    // Load numberOfFeatures from .mat file
    mxArray* matNumberOfFeatures = CMatlabLibrary::loadMatVariable(modelFileName, "numberOfFeatures");
    assert(NULL != matNumberOfFeatures);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfFeatures)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfFeatures)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfFeatures));
    m_numberOfFeatures = static_cast<size_t>(dataElements[0]);


    // Load recursiveLength from .mat file
    mxArray* matRecursiveLength = CMatlabLibrary::loadMatVariable(modelFileName, "recursiveLength");
    assert(NULL != matRecursiveLength);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matRecursiveLength)));
    assert(1 == static_cast<int>(mxGetM(matRecursiveLength)));

    dataElements = static_cast<double*>(mxGetData(matRecursiveLength));
    m_recursiveLength = static_cast<size_t>(dataElements[0]);


	// Load initLength from .mat file
	mxArray* matInitLength = CMatlabLibrary::loadMatVariable(modelFileName, "initLength");
	assert(NULL != matInitLength);

	// Verify that the size is ok
	assert(1 == static_cast<int>(mxGetN(matInitLength)));
	assert(1 == static_cast<int>(mxGetM(matInitLength)));

	dataElements = static_cast<double*>(mxGetData(matInitLength));
	m_initLength = static_cast<size_t>(dataElements[0]);
	
	assert(m_initLength > 0);


	// Initialize means and variances
	m_means = new double[m_numberOfFeatures];
	m_variances = new double[m_numberOfFeatures];
	m_helpVariances = new double[m_numberOfFeatures];

	memset(m_means, 0, m_numberOfFeatures * sizeof(double));
	memset(m_variances, 0, m_numberOfFeatures * sizeof(double));
	memset(m_helpVariances, 0, m_numberOfFeatures * sizeof(double));


    //// Load the initial feature means from .mat file
    //mxArray* matInitMeans = CMatlabLibrary::loadMatVariable(modelFileName, "initMeans");
    //assert(NULL != matInitMeans);

    //// Store data in data Buffer
    //assert(m_numberOfFeatures == mxGetM(matInitMeans));
    //assert(1 == static_cast<int>(mxGetN(matInitMeans)));

    //dataElements = static_cast<double*>(mxGetData(matInitMeans));
    //CDoubleVector* initMeans = new CDoubleVector(m_numberOfFeatures, dataElements);


    //// Load the initial feature variances from .mat file
    //mxArray* matInitVariances = CMatlabLibrary::loadMatVariable(modelFileName, "initVariances");
    //assert(NULL != matInitVariances);

    //// Store data in data Buffer
    //assert(m_numberOfFeatures == mxGetM(matInitVariances));
    //assert(1 == static_cast<int>(mxGetN(matInitVariances)));

    //dataElements = static_cast<double*>(mxGetData(matInitVariances));
    //CDoubleVector* initVariances = new CDoubleVector(m_numberOfFeatures, dataElements);

    //// Sanity check
    //if (initMeans->getSize() != initVariances->getSize())
    //{
    //    throw runtime_error("CAdaptiveFeatureNormalizationAlgorithm: means and variances don't have the same size.");
    //}

    //m_means = new double[initMeans->getSize()];
    //m_variances = new double[initVariances->getSize()];

    //memcpy(m_means, initMeans->getValues(), m_numberOfFeatures * sizeof(double));
    //memcpy(m_variances, initVariances->getValues(), m_numberOfFeatures * sizeof(double));
}

CAdaptiveFeatureNormalizationAlgorithm::~CAdaptiveFeatureNormalizationAlgorithm()
{
	delete[] m_means;
	m_means = 0;

	delete[] m_variances;
	m_variances = 0;

	delete[] m_helpVariances;
	m_helpVariances = 0;
}

void CAdaptiveFeatureNormalizationAlgorithm::process(CDoubleVector& output, const CDoubleVector& input, const bool artifactPresence)
{
	double* const values = input.getValues();
	double* outputVal = output.getValues();
	double diff2Mean;

	for (size_t featureInd = 0; featureInd < m_numberOfFeatures; ++featureInd)
	{
		if (!artifactPresence)
		{
			if (!m_isInitialized)
			{
				// Update the initial mean and variance estimates
				if (featureInd == 0)
				{
					m_initCounter++;
				}

				if (m_initCounter == 1)
				{
					m_means[featureInd] = values[featureInd];
					m_helpVariances[featureInd] = 0.0;
					m_variances[featureInd] = 0.0;
				}
				else
				{
					double oldMean = m_means[featureInd];
					m_means[featureInd] = m_means[featureInd] + (values[featureInd] - m_means[featureInd]) / m_initCounter;
					m_helpVariances[featureInd] = m_helpVariances[featureInd] + (values[featureInd] - oldMean) * (values[featureInd] - m_means[featureInd]);
					m_variances[featureInd] = m_helpVariances[featureInd] / (m_initCounter - 1);
				}

				if (featureInd == m_numberOfFeatures - 1 && m_initCounter == m_initLength)
				{
					m_isInitialized = true;
				}
			}
			else
			{
                if (m_recursiveLength > 0)
                {
				    // Update the mean estimate
				    m_means[featureInd] = ((m_recursiveLength - 1) * m_means[featureInd] + values[featureInd]) / m_recursiveLength;

				    // Update the variance estimate
				    diff2Mean = values[featureInd] - m_means[featureInd];
				    m_variances[featureInd] = ((m_recursiveLength - 1) * m_variances[featureInd]
					    + diff2Mean * diff2Mean) / m_recursiveLength;
                }
			}
		}

        if (m_recursiveLength > 0)
        {
            // Normalize the features
            outputVal[featureInd] = (values[featureInd] - m_means[featureInd]) / (sqrt(m_variances[featureInd]) + c_safeNormalizationValue);
        }
        else
        {
            // Special case: Don't apply any feature normalization
            outputVal[featureInd] = values[featureInd];
        }
	}
}


void CAdaptiveFeatureNormalizationAlgorithm::reset(const CDoubleVector& means
												 , const CDoubleVector& variances)
{
	if (variances.getSize() != means.getSize())
	{
		throw runtime_error("CAdaptiveFeatureNormalizationAlgorithm::reset means and variances don't have the same size.");
	}

	assert(means.getSize() == m_numberOfFeatures);
	memcpy(m_means, means.getValues(), m_numberOfFeatures * sizeof(double));

	assert(variances.getSize() == m_numberOfFeatures);
	memcpy(m_variances, variances.getValues(), m_numberOfFeatures * sizeof(double));

	m_isInitialized = true;
}
