#include "DoubleVector.h"

#include <memory.h>
#include <assert.h>


CDoubleVector::CDoubleVector(const size_t& size)
	: m_size(size)
	, m_values(0)
{
	if (size == 0)
	{
		throw std::invalid_argument("argument size of CDoubleVector have to be not zero");
	}
	else
	{
		m_values = new double[size];
		memset(m_values, 0, m_size * sizeof(double));
	}
}

CDoubleVector::CDoubleVector(const size_t& size, const double& initValue)
	: m_size(size)
	, m_values(0)
{
	if (size == 0)
	{
		throw std::invalid_argument("argument size of CDoubleVector have to be not zero");
	}
	else
	{
		m_values = new double[size];
		for(size_t index = 0; index < m_size; ++index)
		{
			m_values[index] = initValue;
		}		
	}    
}

CDoubleVector::CDoubleVector(const size_t& size, const double* const values)
	: m_size(size)
	, m_values(0)
{
	if (size == 0)
	{
		throw std::invalid_argument("argument \"size\" of CDoubleVector have to be bigger than zero");
	}
	else
	{
		m_values = new double[size];
		memcpy(m_values, values, size * sizeof(double));
	}    
}

CDoubleVector::CDoubleVector(const CDoubleVector& other)
	: m_size(other.m_size)
	, m_values(new double[m_size])
{
	// not nice but fast
	memcpy(m_values, other.getValues(), m_size * sizeof(double));
}

CDoubleVector::~CDoubleVector()
{
	if (m_values!=0)
	{
		delete [] m_values;
		m_values = 0;
	}
}

size_t CDoubleVector::getSize() const
{
	return m_size;
}

double* const CDoubleVector::getValues() const
{
	return m_values;
}

void CDoubleVector::mult(const double& factor)
{
	for(size_t row = 0; row < m_size; ++row)
	{
		m_values[row] *= factor;
	}
}

void CDoubleVector::add(const CDoubleVector& other)
{
	assert(m_size == other.getSize());

	const double* const otherValues = other.getValues();
	for(size_t row = 0; row < m_size; ++row)
	{
		m_values[row] += otherValues[row];
	}
}

void CDoubleVector::addWeighted(const double& factor, const CDoubleVector& other)
{
	assert(m_size == other.getSize());

	const double* const otherValues = other.getValues();
	for(size_t row = 0; row < m_size; ++row)
	{
		m_values[row] += factor * otherValues[row];
	}
}

void CDoubleVector::subtract(const CDoubleVector& other)
{
	assert(m_size == other.getSize());

	const double* const otherValues = other.getValues();
	for(size_t row = 0; row < m_size; ++row)
	{
		m_values[row] -= otherValues[row];
	}
}

CDoubleVector& CDoubleVector::operator=(const CDoubleVector& other)
{
	assert(other.getSize() == m_size);
	// not nice but fast
	memcpy(m_values, other.getValues(), m_size * sizeof(double));
	return *this;
}

double& CDoubleVector::operator[](const size_t& position)
{
	assert(position < m_size);
	return m_values[position];
}

const double& CDoubleVector::operator[](const size_t& position) const
{
	assert(position < m_size);
	return m_values[position];
}

bool CDoubleVector::operator==( const CDoubleVector& other )
{
	if(other.m_size != m_size)
	{
		return false;
	}
	for(size_t index = 0; index < m_size ; ++index)
	{
		if(other.m_values[index] != m_values[index])
		{
			return false;
		}
	}
	return true;
}

void CDoubleVector::findMinMax( double& minValue, double& maxValue )
{
	minValue=m_values[0];
	maxValue=m_values[0];
	for (size_t index = 0; index < m_size ; ++index)
	{
		if (m_values[index]>maxValue)
		{
			maxValue=m_values[index];
		}
		else if (m_values[index]<minValue)
		{
			minValue=m_values[index];
		}
	}
}