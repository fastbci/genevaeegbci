#ifndef SERIALRATIONALTRANSFERFUNCTIONFILTER_H
#define SERIALRATIONALTRANSFERFUNCTIONFILTER_H

#include <vector>
#include <string>

class CDoubleVector;
class CDataBuffer;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Implementation of the general rational transfer function filter used in the "filter" MATLAB function
* @author Tom
*
* Implementation detail: The module holds the feature coefficients, signal history and the filtered output history
* that is used to filter a new input. Once the buffers have been filled and the filter has been stabilized, the filter
* return one vector of values for every input vector.
*/
class DLL_EXPORT CSerialRationalTransferFunctionFilter
{
public:
    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the feature time selector
    */
    CSerialRationalTransferFunctionFilter(std::string calibrationFileName);

    /**
    * Destructor.
    */
    ~CSerialRationalTransferFunctionFilter();

    /**
    * Process function - used to filter the inputs.
    *
    * @param filteredSignal		The filtered signal
    * @param rawSignal		    The unfiltered raw signal (e.g. EEG activity)
    */
    const bool process(CDoubleVector* const filteredSignal, const CDoubleVector * const rawSignal);

    /**
    * Function that returns the number of signal channels.
    */
    const size_t getNumberOfChannels();


private:
    /// Number of signal channels
    size_t m_numberOfChannels;

    /// Number of filtered samples to stabilize the filter
    size_t m_numberOfInitFilterings;

    /// Numerator filter coefficiants
    CDoubleVector* m_numeratorFilterCoefficients;

    /// 2nd to last denominator filter coefficiants
    CDoubleVector* m_denominatorFilterCoefficients;

    /// Leading denominator coefficient
    double m_leadingDenominator;

    /// buffer for the history of the raw signals
    CDataBuffer* m_rawBuffer;

    /// buffer for the history of the filtered signals
    CDataBuffer* m_filteredBuffer;

    /// is the raw signal buffer filled with data
    bool  m_isRawBufferFilled;

    /// is the filtered signal buffer filled with data
    bool  m_isFilteredBufferFilled;

    /// is the filter considered stable
    bool  m_isFilterStable;

    /// number of raw signal samples in the buffer
    size_t  m_numberOfBufferedRawSamples;

    /// number of filtered signal samples in the buffer
    size_t  m_numberOfBufferedFilteredSamples;

    /// number of filtered samples
    size_t  m_numberOfFilteredSamples;

    /// helper vector that holds the raw channel data
    CDoubleVector* m_helperRawChannelData;

    /// helper vector that holds the filteres channel data
    CDoubleVector* m_helperFilteredChannelData;
};

#endif // SERIALRATIONALTRANSFERFUNCTIONFILTER_H

