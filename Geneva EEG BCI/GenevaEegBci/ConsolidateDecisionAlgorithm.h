#ifndef CONSOLIDATEDECISIONALGORITHM_H
#define CONSOLIDATEDECISIONALGORITHM_H

#include <string>


#ifndef DLL_EXPORT
#define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Implementation of the algorithm used to convert the output of a decoder into text-entry interface commands
* @author Tom
*/
class DLL_EXPORT CConsolidateDecisionAlgorithm
{
public:
   /**
   * Constructor that uses a matlab file for initialization.
   *
   * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the consolidate decision algorithm
   */
    CConsolidateDecisionAlgorithm(std::string calibrationFileName);

    /**
    * Destructor.
    */
    ~CConsolidateDecisionAlgorithm();

    /**
    * Process function - used to consolidate the decoder output.
    *
    * @param decoderOutput           decoder output to consolidate
    * @param artifactPresence        artifact detection flag
    * @param numberOfReceivedSamples number of samples received and processed
    */
    const size_t process(const size_t decoderOutput, const bool artifactPresence, const size_t numberOfReceivedSamples);

private:
    /// Decision refractory period
    size_t m_decisionRefractoryPeriod;

    /// Artifact refractory period
    size_t m_artifactRefractoryPeriod;

    /// Decision refractory period counter
    size_t m_decisionRefractoryCounter;

    /// Artifact refractory period counter
    size_t m_artifactRefractoryCounter;
};

#endif // CONSOLIDATEDECISIONALGORITHM_H

