#include "stdafx.h"
#include <stdexcept>
#include "CarAlgorithm.h"

#ifndef MATLABLIBRARY_H
#include "../GenevaEegBci/MatlabLibrary.h"
#endif

using namespace std;


CCarAlgorithm::CCarAlgorithm(const size_t totNumChannels 
	, const size_t noOfCarChannels 
	, const size_t noOfSelectedChannels 
	, const size_t* const carChannels
	, const size_t* const selectedChannels
    , const size_t referenceChannel)
: m_totNumChannels(totNumChannels)
, m_noOfCarChannels(noOfCarChannels)
, m_noOfSelectedChannels(noOfSelectedChannels)
, m_carChannelInd       (0)
, m_selectedChannelInd  (0)
, m_carNorm(0)
{
	if(totNumChannels == 0)
	{
		throw std::runtime_error("no input channels selected");
	}

	if(noOfCarChannels == 0)
	{
		throw std::runtime_error("no car channels selected");
	}

	if(noOfSelectedChannels == 0)
	{
		throw std::runtime_error("no output channels selected");
	}

	m_carChannelInd = new size_t[m_noOfCarChannels];
	memcpy(m_carChannelInd,carChannels,m_noOfCarChannels * sizeof(size_t));

	m_selectedChannelInd = new size_t[m_noOfSelectedChannels];
	memcpy(m_selectedChannelInd,selectedChannels,m_noOfSelectedChannels * sizeof(size_t));

    setCarNorm(referenceChannel);
}


CCarAlgorithm::CCarAlgorithm(string calibrationFileName, const size_t referenceChannel)
: m_totNumChannels(0)
, m_noOfCarChannels(0)
, m_noOfSelectedChannels(0)
, m_carChannelInd(0)
, m_selectedChannelInd(0)
, m_carNorm(0)
{
    // Load numberOfSpectralBins from .mat file
    mxArray* matNumberOfNeuralChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfNeuralChannels");
    assert(NULL != matNumberOfNeuralChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfNeuralChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfNeuralChannels)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfNeuralChannels));
    m_totNumChannels = static_cast<size_t>(dataElements[0]);

    if (m_totNumChannels == 0)
    {
        throw std::runtime_error("no input channels selected");
    }


    // Load numberOfSignals from .mat file
    mxArray* matNumberOfSelectedChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfSelectedChannels");
    assert(NULL != matNumberOfSelectedChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfSelectedChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfSelectedChannels)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfSelectedChannels));
    m_noOfSelectedChannels = static_cast<size_t>(dataElements[0]);

    if (m_noOfSelectedChannels == 0)
    {
        throw std::runtime_error("no output channels selected");
    }


    // Load numberOfSignals from .mat file
    mxArray* matNumberOfCarChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfCarChannels");
    assert(NULL != matNumberOfCarChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfCarChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfCarChannels)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfCarChannels));
    m_noOfCarChannels = static_cast<size_t>(dataElements[0]);



    // Load the projection matrix from .mat file
    mxArray* matSelectedChannelInd = CMatlabLibrary::loadMatVariable(calibrationFileName, "selectedChannelInd");
    assert(NULL != matSelectedChannelInd);

    // Verify that the size is ok
    assert(m_noOfSelectedChannels == mxGetM(matSelectedChannelInd));
    assert(1 == static_cast<int>(mxGetN(matSelectedChannelInd)));

    dataElements = static_cast<double*>(mxGetData(matSelectedChannelInd));
    m_selectedChannelInd = new size_t[m_noOfSelectedChannels];
    for (size_t channelIndex = 0; channelIndex < m_noOfSelectedChannels; channelIndex++)
    {
        m_selectedChannelInd[channelIndex] = static_cast<size_t>(dataElements[channelIndex]);
    }


    if (m_noOfCarChannels != 0)
    {
        // Load the projection matrix from .mat file
        mxArray* matCarChannelIndex = CMatlabLibrary::loadMatVariable(calibrationFileName, "carChannelInd");
        assert(NULL != matCarChannelIndex);

        // Verify that the size is ok
        assert(m_noOfCarChannels == mxGetM(matCarChannelIndex));
        assert(1 == static_cast<int>(mxGetN(matCarChannelIndex)));

        dataElements = static_cast<double*>(mxGetData(matCarChannelIndex));
        m_carChannelInd = new size_t[m_noOfCarChannels];
        for (size_t channelIndex = 0; channelIndex < m_noOfCarChannels; channelIndex++)
        {
            m_carChannelInd[channelIndex] = static_cast<int>(dataElements[channelIndex]);
        }
    }

    setCarNorm(referenceChannel);
}

CCarAlgorithm::~CCarAlgorithm()
{
	if (m_carChannelInd != 0)
	{
		delete[] m_carChannelInd;
		m_carChannelInd = 0;
	}

	if (m_selectedChannelInd != 0)
	{
		delete[] m_selectedChannelInd;
		m_selectedChannelInd = 0;
	}
}

//void CCarAlgorithm::process(double* result, INT16* input)
//{
//	INT16* inputData = input;
//	double average = 0.0;
//	for (size_t i = 0; i < m_noOfCarChannels; ++i)
//	{
//		average += double(inputData[m_carChannelInd[i]]);
//	}
//	average/=m_noOfCarChannels;
//
//	size_t countSelect = 0;
//	for (size_t ch = 0; ch < m_totNumChannels; ch++)
//	{
//		if (ch == m_selectedChannelInd[countSelect])
//		{
//			result[ch] = double(inputData[ch]) - average;
//			countSelect++;
//		} 
//		else
//		{
//			result[ch] = double(inputData[ch]);
//		}
//	}
//}

void CCarAlgorithm::process(double* result, double* input)
{
	double average = 0.0;
	for (size_t i = 0; i < m_noOfCarChannels; ++i)
	{
		average += input[m_carChannelInd[i]];
	}
	average /= m_carNorm;

    memcpy(result, input, m_totNumChannels * sizeof(double));

	size_t countSelect = 0;
	for (size_t channelIndex = 0; channelIndex < m_noOfSelectedChannels; ++channelIndex)
	{
        result[m_selectedChannelInd[channelIndex]] -= average;
	}
}

void CCarAlgorithm::processArrayInPlace(const size_t noOfSamples, double** dataBuffer)
{
	double average = 0.0;
	for (size_t si = 0; si < noOfSamples; ++si)
	{
		average = 0.0;
		for (size_t i = 0; i < m_noOfCarChannels; ++i)
		{
			average += dataBuffer[m_carChannelInd[i]][si];
		}
		average /= m_carNorm;

		for (size_t i = 0; i < m_noOfSelectedChannels; ++i)
		{
			dataBuffer[m_selectedChannelInd[i]][si] -= average;
		}
	}
}

void CCarAlgorithm::setCarNorm(const size_t referenceChannel)
{
    bool isReferenceCarMember = false;
    for (size_t channelInd = 0; channelInd < m_noOfCarChannels; ++channelInd)
    {
        if (m_carChannelInd[channelInd] == referenceChannel)
        {
            isReferenceCarMember = true;
        }
    }

    if (isReferenceCarMember)
    {
        m_carNorm = double(m_noOfCarChannels);
    }
    else
    {
        m_carNorm = double(m_noOfCarChannels + 1);
    }
}
