#ifndef MLDACLASSIFIER_H
#define MLDACLASSIFIER_H

#include <string>
#include <stdio.h>

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Implementation of the multiclass linear discriminant analysis decoder
* @author Tom
*
* Implementation detail: The module is used to process each incoming feature vector
* into "actions" when the expectation for classes other than the first one crosses
* the threshold. If the expectation for none of those classes crosses the threshold
* the outcome is 0 (the code for the first "do nothing" class).
* Expectation values are computed by combining two internal models for two different
* sets if input features.
*/
class DLL_EXPORT CMldaClassifier
{
public:
    /**
    * Constructor.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the decoder
    */
    CMldaClassifier(std::string calibrationFileName);

	// [NOT SUPPORTED ANY MORE]
	///**
	//* Constructor.
	//*
	//* @param modelFileName        Text file that holds the parameters needed to initialize the decoder
	//* @param systemTimeStepSec    The time step of the processing pipeline in seconds
	//*/
	//CMldaClassifier(std::string modelFileName
	//			  , const double systemTimeStepSec);

	/**
	* Constructor.
	*
	* @param noOfClass			Number of classes in the decoder
	* @param noOfFeatures1		Number of signal features corresponding to the internal model1 used for decoding
	* @param noOfFeatures2		Number of signal features corresponding to the internal model1 used for decoding
    * @param expectationThreshold todo
    * @param classMeans1 todo
	* @param classMeans2 todo
    * @param inverseCovMatrix1 todo
	* @param inverseCovMatrix2 todo
	*/
	CMldaClassifier(const size_t noOfClass
				  , const size_t noOfFeatures1
				  , const size_t noOfFeatures2
				  , const double expectationThreshold
				  , const CDoubleMatrix classMeans1
				  , const CDoubleMatrix classMeans2
				  , const CDoubleMatrix inverseCovMatrix1
				  , const CDoubleMatrix inverseCovMatrix2);
	/**
	* Destructor.
	*/
	virtual ~CMldaClassifier();

	/**
	* Process function - used to calculate the decoding output from the signal features.
	* The output is the "action" related to one of the classes.
	*
	* @param currentFeature1		The features that enter the decoder to be processed by the internal model1
	* @param currentFeature2		The features that enter the decoder to be processed by the internal model2
	*/
	size_t process(const CDoubleVector * const currentFeature1, const CDoubleVector * const currentFeature2);

	/**
	* Function that returns the number of classes.
	*/
	size_t getNoOfClass();

    /**
    * Function that returns the number of features corresponding to the internal model1.
    */
    size_t getNoOfFeatures1();

	/**
	* Function that returns the number of features corresponding to the internal model2.
	*/
	size_t getNoOfFeatures2();

private:
    /**
    * Function that calculates the log probabilities based on the internal model1.
    *
    * @param classInput		The signal features minus the class mean
    */
    double calcCovInvProd1(CDoubleVector* classInput);

	/**
	* Function that calculates the log probabilities based on the internal model2.
	*
	* @param classInput		The signal features minus the class mean
	*/
	double calcCovInvProd2(CDoubleVector* classInput);

    /**
    * Function that calculates the expectation.
    *
    * @param featureVector1		The vector of signal features corresponding to the internal model1 (input)
	* @param featureVector2		The vector of signal features corresponding to the internal model2 (input)
    * @param expectation		The vector of class expectations (output)
    */
    void calcExpectation(CDoubleVector* expectation, const CDoubleVector* const featureVector1, const CDoubleVector* const featureVector2);

private:
    /// Helper variable used to calculate the matrix * vector products using CDoubleVector constrcuts
    CDoubleVector* m_helpProduct1;

	/// Helper variable used to calculate the matrix * vector products using CDoubleVector constrcuts
	CDoubleVector* m_helpProduct2;

    /// Helper variable used to calculate the matrix * vector products
	double* m_tmpProb;

    /// Helper variable used to calculate the matrix * vector products using CDoubleVector constrcuts
    CDoubleVector* m_helpDistVect1;

	/// Helper variable used to calculate the matrix * vector products using CDoubleVector constrcuts
	CDoubleVector* m_helpDistVect2;

	// [UNUSED]
	///// Helper variable used to calculate the matrix * vector products
	//double* m_tmpDistVect;

	/// Helper variable used to calculate the matrix * vector products
	double* m_tmpLogProb;

	/// Variable that holds the expectation values
    CDoubleVector* m_expectation;

    /// Variable that holds the inverse of the cholesky decomposition of the covariance matrix corresponding to the internal model1
    CDoubleMatrix* m_choleskyInvese1;

	/// Variable that holds the inverse of the cholesky decomposition of the covariance matrix corresponding to the internal model2
	CDoubleMatrix* m_choleskyInvese2;

	/// Variable that holds the class means of the internal model1
	double** m_classMeans1;

	/// Variable that holds the class means of the internal model2
	double** m_classMeans2;

	/// Number of features corresponding to the internal model1
	size_t m_noOfFeatures1;

	/// Number of features corresponding to the internal model2
	size_t m_noOfFeatures2;

	/// Number of classes
	size_t m_noOfClass;

	/// Number of classes
	size_t m_noOfTaps;

	/// Probability expectation threshold
	double m_expectationThreshold;

	/// File used to save the expectation values
	FILE* m_fileExpectation;

	/// Flag used to determine whether to log the expectation values or not
	static const bool m_logExpectationValues = true;
};

#endif // MLDACLASSIFIER_H