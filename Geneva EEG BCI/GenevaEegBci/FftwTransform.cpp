#include "FftwTransform.h"

#include <cmath>
#include <assert.h>
#include <stdexcept>
#include <sstream>

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif


using namespace std;

CFftwTransform::CFftwTransform(const size_t& timeWindowSize)
: m_timeWindowSize(timeWindowSize)
, m_spectrumSize(size_t(floor(timeWindowSize / 2.0) + 1.0))
, m_fftwplan           (0)
, m_fftwOutput         (0)
, m_fftwInput          (0)
{
    if(m_timeWindowSize < 1)
    {
        throw runtime_error("CFftwTransform: timeWindowSize must be > 1.");
    }

    // init fftw
    m_fftwInput = new double[m_timeWindowSize];
    memset(m_fftwInput, 0, m_timeWindowSize * sizeof(double));

    m_fftwOutput = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * m_spectrumSize);

    m_fftwplan = fftw_plan_dft_r2c_1d(int(m_timeWindowSize)
        , m_fftwInput
        , m_fftwOutput
        , FFTW_MEASURE);
}

CFftwTransform::~CFftwTransform()
{
    if(m_fftwplan != 0)
    {
        fftw_destroy_plan(m_fftwplan);
        m_fftwplan = 0;
    }

    if (m_fftwOutput != 0)
    {
        fftw_free(m_fftwOutput);
        m_fftwOutput = 0;        
    }

    if (m_fftwInput != 0)
    {
        delete[] m_fftwInput;
        m_fftwInput = 0;
    }
}

size_t CFftwTransform::getTimeWindowSize() const
{
    return m_timeWindowSize;
}

size_t CFftwTransform::getSpectrumSize() const
{
    return m_spectrumSize;
}

void CFftwTransform::doFourierTransform(CDoubleVector& realSpectrum , CDoubleVector& imaginarySpectrum , const CDoubleVector& realTimeSeries)
{
	if(realTimeSeries.getSize() != m_timeWindowSize)
	{
		ostringstream message;
		message << "CFftwTransform: can't do Fourier transform, parameter realTimeSeries has size ";
		message << realTimeSeries.getSize() << ", but expected " << m_timeWindowSize << ".";
		throw runtime_error(message.str());
	}
	if(imaginarySpectrum.getSize() != m_spectrumSize)
	{
		ostringstream message;
		message << "CFftwTransform: can't do Fourier transform, parameter imaginarySpectrum has size ";
		message << imaginarySpectrum.getSize() << ", but expected " << m_spectrumSize << ".";
		throw runtime_error(message.str());
	}

	if(realSpectrum.getSize() != m_spectrumSize)
	{
		ostringstream message;
		message << "CFftwTransform: can't do Fourier transform, parameter realSpectrum has size ";
		message << realSpectrum.getSize() << ", but expected " << m_spectrumSize << ".";
		throw runtime_error(message.str());
	}

	memcpy(m_fftwInput, realTimeSeries.getValues(), m_timeWindowSize * sizeof(double));

	// do Fourier transformation
	fftw_execute(m_fftwplan); 

	double* const realValues = realSpectrum.getValues();
	double* const imagValues = imaginarySpectrum.getValues();
	for(size_t index = 0; index < m_spectrumSize; ++index)
	{
		realValues[index] = m_fftwOutput[index][0];
		imagValues[index] = m_fftwOutput[index][1];
	}
}

/*
void CFftwTransform::doFourierTransform(CDoubleVector& realSpectrum 
                                       , CDoubleVector& imaginarySpectrum
                                       , const CDoubleVector& realTimeSeries)
{
    if(realTimeSeries.getSize() != m_timeWindowSize)
    {
        ostringstream message;
        message << "CFftwTransform: can't do Fourier transform, parameter realTimeSeries has size ";
        message << realTimeSeries.getSize() << ", but expected " << m_timeWindowSize << ".";
        throw runtime_error(message.str());
    }
    if(imaginarySpectrum.getSize() != m_spectrumSize)
    {
        ostringstream message;
        message << "CFftwTransform: can't do Fourier transform, parameter imaginarySpectrum has size ";
        message << imaginarySpectrum.getSize() << ", but expected " << m_spectrumSize << ".";
        throw runtime_error(message.str());
    }

    if(realSpectrum.getSize() != m_spectrumSize)
    {
        ostringstream message;
        message << "CFftwTransform: can't do Fourier transform, parameter realSpectrum has size ";
        message << realSpectrum.getSize() << ", but expected " << m_spectrumSize << ".";
        throw runtime_error(message.str());
    }

    memcpy(m_fftwInput, realTimeSeries.getValues(), m_timeWindowSize * sizeof(double));

    // do Fourier transformation
    fftw_execute(m_fftwplan); 

    double* const realValues = realSpectrum.getValues();
    double* const imagValues = imaginarySpectrum.getValues();
    for(size_t index = 0; index < m_spectrumSize; ++index)
    {
        realValues[index] = m_fftwOutput[index][0];
        imagValues[index] = m_fftwOutput[index][1];
    }
}
*/