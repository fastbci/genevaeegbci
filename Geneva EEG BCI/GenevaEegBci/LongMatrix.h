#ifndef LONGMATRIX_H
#define LONGMATRIX_H

/**
@brief encapsulate a Matrix of double
@ingroup utils
@author joerg

PLEASE DOCUMENT
*/
class CLongMatrix
{
public:
    /**
    Initializing the matrix with null.

    @param rowSize row Size, number of columns
    @param columnSize column size, number of rows
    */
    CLongMatrix(const size_t& rowSize, const size_t& columnSize);

    /**
    Initializing the matrix with a constant.

    @param rowSize row Size, number of columns
    @param columnSize column size, number of rows
    @param initValue initial Value for the matrix
    */
    CLongMatrix(const size_t& rowSize, const size_t& columnSize,
        const long& initValue);

    /**
    Initializing the matrix with a vector given by an pointer, the vector must have
    (rowSize * columnSize) valid elements. The matrix is filled column by column.

    @param rowSize row Size, number of columns
    @param columnSize column size, number of rows
    @param values vector of values for initializing
    */
    CLongMatrix(const size_t& rowSize, const size_t& columnSize,
        const long* const values);

    /**
    Copy constructor

    @param other matrix to copy
    */
    CLongMatrix(const CLongMatrix& other);

    /**
    Destructor
    */
    virtual ~CLongMatrix();

public:
    /**
    Get size of the Columns, number of rows
    */
    size_t getColumnSize() const;

    /**
    Get size of the rows, the number of columns
    */
    size_t getRowSize() const;

    /**
    Returns a Pointer,
    that points on the first element of the matrix.
    Order of the Elements is: first column, from top to bottom,
    then the second(top to bottom), and so on.
    The pointer is owned by the matrix.
    */
    long* const getValues() const;

    /**
    Returns a pointer to a column of this matrix.
    The pointer is owned by the matrix.

    @param columnIndex index of the column to get
    @return pointer to the column
    */
    long* const getColumnAt(size_t columnIndex) const;

    /**
    Assignment operator
    */
    CLongMatrix& operator=(const CLongMatrix& other);

    /**
    @brief todo
    @param toInsert todo
    @param columnOffset todo
    @param rowOffset todo
    */
    void insertAt(const CLongMatrix& toInsert, const size_t columnOffset,
        const size_t rowOffset);

    /**
    Subtracts a other matrix from this matrix (this[i] = this[i] - other[i])
    @param other matrix to subtract 
    */
    void subtract(const CLongMatrix& other);

    /**
    Adds a other matrix to this matrix (this[i] = this[i] + other[i])
    @param other matrix to add
    */
    void add(const CLongMatrix& other);

    /**
    Adds a matrix with a factor to this matrix (this[i] = this[i] + (factor * other[i]))
    @param factor the factor
    @param other matrix to add
    */
    void addWeighted(const long& factor, const CLongMatrix& other);

    /**
    Multiply factor to all values of the matrix
    @param factor value witch is multiplied to all values of the matrix
    */
    void mult(const long& factor);

    /**
    Provide the transposition of the matrix
    @return transposed matrix
    */
    CLongMatrix getTransposed() const;

    long& at(const size_t& row, const size_t& column) const;
    long& operator [] (size_t position);


private:
    const size_t m_columnSize;
    const size_t m_rowSize;
    long* m_values;
};

#endif // LONGMATRIX_H