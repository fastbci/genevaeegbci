#ifndef DATABUFFER_H
#define DATABUFFER_H

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

class DLL_EXPORT CDataBuffer
{
public:

	enum TimeOrder {
		LAST_IN_AT_BEGGINING /// channel[0] holds most recent value
		, LAST_IN_AT_END /// channel[capacity - 1] holds most recent value
	};
	/**
	* @brief Constructor.
	*
	* @param numberOfChannels Specifies many channels each amplifier frame has
	* @param capacity         Specifies how many previous time steps are kept in the buffer
    * @param timeOrder        Data organization
	*/
	CDataBuffer(const size_t& numberOfChannels, const size_t& capacity, const TimeOrder& timeOrder);

	/// destructor
	virtual ~CDataBuffer();

public:
	size_t getNumberOfChannels() const;

	size_t getCapacity() const;

	double* getChannel(const size_t& channelNumber);

	void getBuffer(double** bufferPointer);

	TimeOrder getTimeOrder() const;

	void appendOneFrame(const double* const signal);

	void append(const size_t& noOfSamples, const double* const* const signal);

    void flushBuffer();

private:
	/// not implement
	//CDataBuffer& operator=(const CDataBuffer& other);
	
private:
    size_t            m_numberOfChannels;
    size_t            m_capacity;
    size_t            m_index;
    double**          m_bufferedSignal;
    const TimeOrder   m_timeOrder;
};

#endif // DATABUFFER_H