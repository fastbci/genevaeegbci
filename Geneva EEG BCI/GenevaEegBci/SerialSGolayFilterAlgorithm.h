#ifndef SERIALSGOLAYFILTERALGORITHM_H
#define SERIALSGOLAYFILTERALGORITHM_H

#include <vector>

#ifndef DATABUFFER_H
#include "DataBuffer.h"
#endif

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

using namespace std;

/**
* @brief Savitzky-Golay filter algorithm implementation using point product
* @author Tom
*
* Implementation detail: The module stores the input data in a buffer and
* generates a low-passfilterd signal at a fixed frequency.
*/
class DLL_EXPORT CSerialSGolayFilterAlgorithm
{

public:
    /**
    * Constructor.
    *
    * @param leftWidth        How many time steps to the left should be used for smoothing
    * @param rightWidth       How many time steps to the right should be used for smoothing
    * @param polynomialOrder  order of the polynomial used for smoothing
    * @param windowSize       length of the time series that has to be smoothed,, must be odd.
    * @param stepSize         Frequency at which to calculate the filtered signal
    * @param numberOfChannels number of channels
    */
    CSerialSGolayFilterAlgorithm(const size_t& leftWidth
                               , const size_t& rightWidth
                               , const size_t& polynomialOrder
                               , const size_t& windowSize
                               , const size_t& stepSize
                               , const size_t& numberOfChannels);
    CSerialSGolayFilterAlgorithm(std::string modelFileName);

    /// destructor
    virtual ~CSerialSGolayFilterAlgorithm();

    /**
    * For every windowSize-th step, the process function outputs a low-pass filtered output.
    *
    * @param output        vetor that contains the filtered output
    * @param inputFrame    input that contains a signal sample
    */
    void process(std::vector<CDoubleVector*>& output, CDoubleVector* inputFrame);

    /**
    * Add the input frame to the buffer.
    *
    * @param inputFrame			input that contains a signal sample
    */
    const bool addToBuffer(CDoubleVector* inputFrame);

    /**
    * Helper function. Calculates the Savitzky-Golay coefficients.
    *
    * @param leftWidth        How many time steps to the left should be used for smoothing
    * @param rightWidth       How many time steps to the right should be used for smoothing
    * @param polynomialOrder  order of the polynomial used for smoothing
    */
    static CDoubleVector getCoefficients(const size_t& leftWidth, const size_t& rightWidth, const size_t& polynomialOrder);

    /**
    * Initialize or reset the algorithm by filling up its buffer.
    *
    * @param initBuffer        pointer to an array of signal samples needed to initialize the buffer
    */
    void reset(CDoubleVector** initBuffer);

    /**
    * Flush the internal buffer of the class.
    */
    void flushBuffer();

    /**
    * Returns the number of channels
    */
    size_t getNumberOfChannels() { return m_numberOfChannels; };

    /**
    * Returns the window size
    */
    size_t getWindowSize() { return m_windowSize; };

private:
    /// polynomial order
    size_t m_polynomialOrder;

    /// all m_windowSize steps a smoothing is performed
    size_t m_windowSize;

    /// all m_windowSize steps a smoothing is performed
    size_t m_stepSize;

    /// buffer for the values of the frames that have not been smoothed yet
    CDataBuffer* m_buffer;

    /// counts how much frames have arrived since last smoothing operation
    size_t m_currentStep;

    /// number of channels.
    size_t m_numberOfChannels;

    /// filter coefficients.
    CDoubleVector* m_filterCoefficients;

    /// is the buffer filled with data
    bool  m_isBufferFilled;

    /// number of signal samples in the buffer
    size_t  m_numberOfBufferedSamples;
};

#endif // SERIALSGOLAYFILTERALGORITHM_H
