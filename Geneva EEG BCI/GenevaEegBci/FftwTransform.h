#ifndef FFTWTRANSFORM_H
#define FFTWTRANSFORM_H

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#include <fftw/fftw3.h>


/**
* @brief Implementation with Fastest Fourier Transformation of the West library.
* @ingroup utils
* @author joerg
*/
class CFftwTransform
{
public:
	/**
	PLEASE DOCUMENT
	*/
    CFftwTransform(const size_t& timeWindowSize);
    virtual ~CFftwTransform();

public:
	/**
	PLEASE DOCUMENT
	*/
    size_t getTimeWindowSize() const;
    
	/**
	PLEASE DOCUMENT
	*/
    size_t getSpectrumSize() const;
	/**
	PLEASE DOCUMENT
	*/
    void doFourierTransform(CDoubleVector& realSpectrum
        , CDoubleVector& imaginarySpectrum
        , const CDoubleVector& realTimeSeries);

private:
    /// not implemented
    CFftwTransform& operator=(const CFftwTransform& other);

private:
    const size_t m_timeWindowSize;
    const size_t m_spectrumSize;
    /// FFTW plan used, describing the Fourier transformation done to calculate the amplitude spectra.
    fftw_plan m_fftwplan;

    /// contains the output for the fftw. Kept as member due to performance.
    fftw_complex* m_fftwOutput;

    /// contains the input for the fftw. Kept as member due to performance.
    double* m_fftwInput;
};
#endif // FFTWTRANSFORM_H
