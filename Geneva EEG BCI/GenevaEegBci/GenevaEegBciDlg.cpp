// GenevaEegBciDlg.cpp : implementation file
//

/***************************************************************************

// From the TERMINAL:
// curl -v --data-urlencode -d "http://128.179.164.231:9090/therapy/state=off?";
*/
/***************************************************************************/

// Main files include
#include "stdafx.h"
#include "GenevaEegBci.h"
#include "GenevaEegBciDlg.h"
#include <stdlib.h>

// Eego amplifier
#include "EegData.h"

// Standard libs
#include <locale>
#include <numeric>
#include <codecvt>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma comment(lib, "winmm.lib") 

#using <System.dll>

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

#ifndef MATLABLIBRARY_H
#include "MatlabLibrary.h"
#endif

#ifndef SIZETVECTOR_H
#include "SizeTVector.h"
#endif

#include "DataBuffer.h"
#include "EegData.h"
#include "SerialRationalTransferFunctionFilter.h"
#include "ChannelInterpolationAlgorithm.h"
#include "EogCorrectionAlgorithm.h"
#include "CarAlgorithm.h"

#include "RealImaginaryStftAlgorithm.h"
#include "DenoisingAlgorithm.h"
#include "FrequencyBandSelector.h"
#include "SpectralSourceReconstruction.h"
#include "SpectralAdaptiveFeatureNormalization.h"

#include "SerialSGolayFilterAlgorithm.h"
#include "CorticalSourceReconstruction.h"
#include "AdaptiveFeatureNormalizationAlgorithm.h"

#include "FeatureTimeSelector.h"
#include "MldaClassifier.h"
#include "ConsolidateDecisionAlgorithm.h"


using namespace System;
using namespace std;
using namespace std::chrono;
using namespace System::Speech::Synthesis;

#define WM_CLOCKWISE        (WM_USER + 100)
#define WM_ANTICLOCKWISE    (WM_USER + 101)
#define WM_SELECT           (WM_USER + 102)
#define WM_READY            (WM_USER + 103)
#define WM_THREAD_COMPLETE  (WM_USER + 104)

#define CALIBRATION_CUE_START_DELAY 4000    // 4s
#define CALIBRATION_CUE_INTERVAL    1       // between calibration cues
#define CALIBRATION_CUE_END_DELAY   5000    // Delay at the end, before UI reset
#define CALIBRATION_INIT_DELAY      20000   // 20s initialization delay before calibration start
#define EOG_REST_STATE_DURATION     190000  // EOG Resting State calibration duration

// CAboutDlg dialog used for App About
class CAboutDlg : public CDialog
{
public:
    CAboutDlg();

    // Dialog Data
#ifdef AFX_DESIGN_TIME
    enum { IDD = IDD_ABOUTBOX };
#endif

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


/**
* @brief Main UI class constructor
*/
CGenevaEegBciDlg::CGenevaEegBciDlg(CWnd* pParent /*=NULL*/)
: CDialog(CGenevaEegBciDlg::IDD, pParent)
, stopAcquisition(CreateEvent(0,TRUE,FALSE,0))
, acquisitionStopped(CreateEvent(0, TRUE, TRUE, 0))
, stopCalib(CreateEvent(0, TRUE, FALSE, 0))
, calibStopped(CreateEvent(0, TRUE, TRUE, 0))
, stopRestStateEogCalib(CreateEvent(0, TRUE, FALSE, 0))
, restStateEogCalibStopped(CreateEvent(0, TRUE, TRUE, 0))
, m_acquisitionControlFlag(false)
, m_calibControlFlag(false)
, m_restStateCalibControlFlag(false)
, m_eogCalibControlFlag(false)
, m_spellerKey(A)
, m_spellerMode(LETTER)
, m_bmpId(IDB_BITMAP2)
, m_keysToUse(A)
, m_keyIndex(0)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDI_THUNDER);
    m_pToolTip = NULL;

    // Create and open Dialog log file
    time_t t = time(0);
    struct tm * now = localtime(&t);

    string buffer(80, '\0');
    strftime(&buffer[0], buffer.size(), "%F_%H-%M-%S.txt", now);

    m_fileDlg = ofstream("..\\Log\\logDlg_" + buffer, std::ios::out);
}

/**
* @brief Destructor
*/
CGenevaEegBciDlg::~CGenevaEegBciDlg()
{
    delete m_pToolTip;

    CloseHandle(stopAcquisition);
    CloseHandle(acquisitionStopped);
    CloseHandle(stopCalib);
    CloseHandle(calibStopped);
    CloseHandle(stopRestStateEogCalib);
    CloseHandle(restStateEogCalibStopped);
}

/**
* @brief Dialog specific method to pre translate messages
*/
BOOL CGenevaEegBciDlg::PreTranslateMessage(MSG* pMsg)
{
    if (NULL != m_pToolTip)
    {
        m_pToolTip->RelayEvent(pMsg);
    }

    if (!m_acquisitionControlFlag && !m_calibControlFlag && !m_restStateCalibControlFlag)
    {
        if (pMsg->message == WM_KEYDOWN)
        {
            if (pMsg->wParam == VK_LEFT)
            {
                OnAnticlockwise();
                return TRUE;
            }
            else if (pMsg->wParam == VK_RIGHT)
            {
                OnClockwise();
                return TRUE;
            }
            else if (pMsg->wParam == VK_DOWN)
            {
                OnSelect();
                return TRUE;
            }
            else if (pMsg->wParam == VK_UP)
            {
                // Reset UI to Grey scale UI
                ResetUi(false, true, IDB_BITMAP1);
                return TRUE;
            }
        }
    }

    return CDialog::PreTranslateMessage(pMsg);
}

/**
* @brief Exchange and validate the dialog data
*/
void CGenevaEegBciDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);

    DDX_Control(pDX, IDC_ACQUISITION_START, m_editStartAcquisitionControl);
    DDX_Control(pDX, IDC_CALIBRATION, m_editStartCalibration);
    DDX_Control(pDX, IDC_REST_STATE_CALIB, m_editStartRestStateCalib);
    DDX_Control(pDX, IDC_EOG_CALIB, m_editStartEogCalib);
    DDX_Control(pDX, IDC_STATIC_BMP, m_StaticBmp);
    DDX_Control(pDX, IDC_STATIC1, m_Static1);
    DDX_Control(pDX, IDC_STATIC2, m_Static2);
    DDX_Control(pDX, IDC_STATIC3, m_Static3);
    DDX_Control(pDX, IDC_STATIC4, m_Static4);
    DDX_Control(pDX, IDC_STATIC5, m_Static5);
    DDX_Control(pDX, IDC_STATIC6, m_Static6);

    DDX_Control(pDX, IDC_STATIC7, m_Static7);
    DDX_Control(pDX, IDC_STATIC8, m_Static8);
    DDX_Control(pDX, IDC_STATIC9, m_Static9);
    DDX_Control(pDX, IDC_STATIC10, m_Static10);
    DDX_Control(pDX, IDC_STATIC11, m_Static11);
    DDX_Control(pDX, IDC_STATIC12, m_Static12);
    DDX_Control(pDX, IDC_STATIC13, m_Static13);
    DDX_Control(pDX, IDC_STATIC14, m_Static14);
    DDX_Control(pDX, IDC_STATIC15, m_Static15);
    DDX_Control(pDX, IDC_STATIC16, m_Static16);
    DDX_Control(pDX, IDC_STATIC17, m_Static17);
    DDX_Control(pDX, IDC_STATIC18, m_Static18);

    DDX_Control(pDX, IDC_STATIC_KEY_ENTERED, m_StaticSpellerCharacters);
    DDX_Control(pDX, IDC_STATIC_RESULT, m_StaticSpellerResult);
    DDX_Control(pDX, IDC_STATIC_TEXT_TO_SPELL, m_StaticTextToSpell);
}

// GUI Message map
BEGIN_MESSAGE_MAP(CGenevaEegBciDlg, CDialog)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()

    ON_BN_CLICKED(IDOK, &CGenevaEegBciDlg::OnBnClickedOk)
    
    ON_BN_CLICKED(IDC_ACQUISITION_START, &CGenevaEegBciDlg::OnBnClickedStartAcquisitionControl)
    ON_BN_CLICKED(IDC_CALIBRATION, &CGenevaEegBciDlg::OnBnClickedCalib)

    ON_MESSAGE_VOID(WM_CLOCKWISE, OnClockwise)
    ON_MESSAGE_VOID(WM_ANTICLOCKWISE, OnAnticlockwise)
    ON_MESSAGE_VOID(WM_SELECT, OnSelect)
    ON_MESSAGE_VOID(WM_READY, OnAcquisitionReady)

    ON_MESSAGE_VOID(WM_THREAD_COMPLETE, OnThreadComplete)
    ON_BN_CLICKED(IDC_REST_STATE_CALIB, &CGenevaEegBciDlg::OnBnClickedRestStateCalib)
    ON_BN_CLICKED(IDC_EOG_CALIB, &CGenevaEegBciDlg::OnBnClickedEogCalib)
END_MESSAGE_MAP()


// CGenevaEegBciDlg message handlers

/**
* @brief Operations at app start
*/
BOOL CGenevaEegBciDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);            // Set big icon
    SetIcon(m_hIcon, FALSE);        // Set small icon

    // Set GUI font
    memset(&lf, 0, sizeof(LOGFONT));  
    lf.lfHeight = 25;                      // request a 12-pixel-height font
    lf.lfWidth = 10;
    lf.lfWeight = FW_NORMAL;
    lf.lfItalic = FALSE;
    lf.lfUnderline = FALSE;
    lf.lfStrikeOut = FALSE;
    lf.lfCharSet = OEM_CHARSET;
    lf.lfOutPrecision = OUT_TT_PRECIS;
    lf.lfQuality = ANTIALIASED_QUALITY;
    lf.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
    _tcsncpy_s(lf.lfFaceName, LF_FACESIZE,_T("Arial"), 60);
    VERIFY(font.CreateFontIndirect(&lf));  // create the font

    // Set letter labels font
    memset(&lfLetterKeyboardFont, 0, sizeof(lfLetterKeyboardFont));

    lfLetterKeyboardFont.lfHeight = 55;                    // 55-pixel-height
    lfLetterKeyboardFont.lfWeight = FW_BOLD;               // Bold
    VERIFY(m_spellerLetterKeyboardFont.CreateFontIndirect(&lfLetterKeyboardFont));

    // Populate an array with the different CStatic labels ...
    m_TransparentStaticArray.push_back(&m_Static1);
    m_TransparentStaticArray.push_back(&m_Static2);
    m_TransparentStaticArray.push_back(&m_Static3);
    m_TransparentStaticArray.push_back(&m_Static4);
    m_TransparentStaticArray.push_back(&m_Static5);
    m_TransparentStaticArray.push_back(&m_Static6);

    m_TransparentStaticArray.push_back(&m_Static7);
    m_TransparentStaticArray.push_back(&m_Static8);
    m_TransparentStaticArray.push_back(&m_Static9);
    m_TransparentStaticArray.push_back(&m_Static10);
    m_TransparentStaticArray.push_back(&m_Static11);
    m_TransparentStaticArray.push_back(&m_Static12);
    m_TransparentStaticArray.push_back(&m_Static13);
    m_TransparentStaticArray.push_back(&m_Static14);
    m_TransparentStaticArray.push_back(&m_Static15);
    m_TransparentStaticArray.push_back(&m_Static16);
    m_TransparentStaticArray.push_back(&m_Static17);
    m_TransparentStaticArray.push_back(&m_Static18);

    // .. and set the font
    for (auto it = m_TransparentStaticArray.begin(); it != m_TransparentStaticArray.end(); it++)
    {
        (*it)->SetFont(&m_spellerLetterKeyboardFont);
    }

    // Set phrases labels font
    memset(&lfPhrasesKeyboardFont, 0, sizeof(lfPhrasesKeyboardFont));

    lfPhrasesKeyboardFont.lfHeight = 35;                    // 35-pixel-height
    lfPhrasesKeyboardFont.lfWeight = FW_BOLD;               // Bold
    VERIFY(m_spellerPhrasesKeyboardFont.CreateFontIndirect(&lfPhrasesKeyboardFont));

    // Set menu labels font
    memset(&lfMenuFont, 0, sizeof(lfMenuFont));

    lfMenuFont.lfHeight = 40;                    // 40-pixel-height
    lfMenuFont.lfWeight = FW_NORMAL;
    VERIFY(m_spellerMenuFont.CreateFontIndirect(&lfMenuFont)); // create the font

    m_Static7.SetFont(&m_spellerMenuFont);
    m_Static8.SetFont(&m_spellerMenuFont);
    m_Static9.SetFont(&m_spellerMenuFont);
    m_Static10.SetFont(&m_spellerMenuFont);
    m_Static11.SetFont(&m_spellerMenuFont);
    m_Static12.SetFont(&m_spellerMenuFont);
    m_Static13.SetFont(&m_spellerMenuFont);
    m_Static14.SetFont(&m_spellerMenuFont);
    m_Static15.SetFont(&m_spellerMenuFont);
    m_Static16.SetFont(&m_spellerMenuFont);
    m_Static17.SetFont(&m_spellerMenuFont);
    m_Static18.SetFont(&m_spellerMenuFont);

    // Set result labels font
    memset(&lfResultFont, 0, sizeof(lfResultFont));

    lfResultFont.lfHeight = 30;                    // 30-pixel-height
    lfResultFont.lfWeight = FW_BOLD;               // Bold
    VERIFY(m_spellerResultFont.CreateFontIndirect(&lfResultFont)); // create the font

    m_StaticSpellerCharacters.SetFont(&m_spellerResultFont);
    m_StaticSpellerResult.SetFont(&m_spellerResultFont);
    m_StaticTextToSpell.SetFont(&m_spellerResultFont);

    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        CString strAboutMenu;
        strAboutMenu.LoadString(IDS_ABOUTBOX);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    // Set up the tooltip
    m_pToolTip = new CToolTipCtrl;
    if(!m_pToolTip->Create(this))
    {
        TRACE("Unable To create ToolTip\n");
        return TRUE;
    }

    // Configure language if available, else default language is English
    string filename = "..\\GenevaEegBci\\res\\config_language.txt";

    ifstream language_config_file = ifstream(filename.c_str());

    if (language_config_file.is_open())
    {
        getline(language_config_file, m_language);

        transform(m_language.begin(), m_language.end(), m_language.begin(), ::tolower);

        if (m_language == "en")
        {
            SetThreadUILanguage(MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT));
        }
        else if (m_language == "fr")
        {
            SetThreadUILanguage(MAKELANGID(LANG_FRENCH, SUBLANG_DEFAULT));
        }
        else if (m_language == "de")
        {
            SetThreadUILanguage(MAKELANGID(LANG_GERMAN, SUBLANG_DEFAULT));
        }
    }
    else
    {
        SetThreadUILanguage(MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT));
        m_language = "en";
    }

    // Init strings from String Table
    InitStrings();

    // Load Dictionary
    LoadDictionary();

    // Draw UI
    FormatSpellerLabels();
    FillSpellerLabels();
    m_StaticSpellerCharacters.SetWindowTextW(_T(""));
    m_StaticSpellerResult.SetWindowTextW(_T(""));
    m_StaticTextToSpell.SetWindowTextW(_T(""));

    // Manage process and UI thread priority
    SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
    SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);

    m_fileDlg << "Dialog Init\n";
    m_fileDlg << "Speller mode\tSpeller key\tBitmap ID\tKey Index\tSpeller characters\tSpeller results\tFunction\tKey1\tKey2\tKey3\tKey4\tKey5\tKey6\tMenu1-1\tMenu1-2\tMenu1-3\tMenu1-4\tMenu1-5\tMenu1-6\tMenu2-1\tMenu2-2\tMenu2-3\tMenu2-4\tMenu2-5\tMenu2-6\n" << m_spellerMode << "\t" << m_spellerKey << "\t" << m_bmpId << "\t" << m_keyIndex << std::endl;

    return TRUE;  // return TRUE  unless you set the focus to a control
}

/**
* @brief System functions
*/
void CGenevaEegBciDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialog::OnSysCommand(nID, lParam);
    }
}

/**
* @brief Dialog specific method executed when UI is painted
*/
void CGenevaEegBciDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialog::OnPaint();
    }
}

/**
* @brief Method called to obtain the cursor to display while the user drags
* the minimized window.
*/
HCURSOR CGenevaEegBciDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

//kill window
void CGenevaEegBciDlg::OnOK(void)
{  
    CWnd* pWnd = GetFocus();
    if(GetDlgItem(IDOK) == pWnd)
    {
        CDialog::OnOK();
        return;
    }
    NextDlgCtrl(); //will move focus to next control in tab order
}

/**
* @brief Method called before to close window
*/
void CGenevaEegBciDlg::OnBnClickedOk()
{ 
    //kill window
    OnOK();
}

/**
* @brief Start or stop the acquisition thread when calib button is pressed
*/
void CGenevaEegBciDlg::OnBnClickedStartAcquisitionControl()
{
    if (m_acquisitionControlFlag == false) // acquisition off. Start acquisition
    {     
        ResetEvent(stopAcquisition);
        ResetEvent(acquisitionStopped);

        m_acquisitionThread = AfxBeginThread(&CGenevaEegBciDlg::processThreadAcquisition, this, THREAD_PRIORITY_TIME_CRITICAL, 0, CREATE_SUSPENDED, 0);
        m_acquisitionThread->ResumeThread();

        m_acquisitionControlFlag = true;
        m_editStartAcquisitionControl.SetWindowTextW(_T("Stop"));
        m_StaticSpellerCharacters.SetWindowTextW(_T("Wait..."));

        // UI update flag
        static bool isUiModified = false;

        // Set the text to spell
        SetTextToSpell();

        // Add start acquisiton time
        time_t t = time(0);
        struct tm * now = localtime(&t);

        char buffer[80];
        strftime(buffer, sizeof(buffer), "%F_%H-%M-%S", now);

        string date_time(buffer);

        m_fileDlg << "Start acquisition " << date_time << endl;
    }
    else    // acquisition on. Stop acquisition
    {
        SetEvent(stopAcquisition);
        WaitForSingleObject(acquisitionStopped, INFINITE);

        m_acquisitionThread = 0;

        m_editStartAcquisitionControl.SetWindowTextW(_T("Start"));

        // Add stop acquisition time
        time_t t = time(0);
        struct tm * now = localtime(&t);

        char buffer[80];
        strftime(buffer, sizeof(buffer), "%F_%H-%M-%S", now);

        string date_time(buffer);

        m_fileDlg << "Stop acquisition " << date_time << endl;
    }
}

/**
* @brief Code to be executed to perform the acquisition and processing of EEG data
*/
void CGenevaEegBciDlg::acquisitionControl(LPVOID parameter)
{
    // EEG data class
    string channelSelector_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateChannelSelector.mat";
    CEegData eegData(channelSelector_calibrationFile);

    vector<size_t> decoderDecision;
    vector<size_t> decoderConsolidatedDecision;
    vector<long long> loopDuration;
    vector<long long> samplesCount;
    static bool isUiModified = false; // UI update flag

    bool is_init = eegData.initAcquisition();

    // Exit if any problem with amplifiers
    if (!is_init)
    {
        // Stop thread and update UI accordingly
        SetEvent(acquisitionStopped);

        PostMessage(WM_THREAD_COMPLETE, 0, 0);

        m_fileDlg << "Amplifier init failed" << endl;

        return;
    }

    // High-pass eeg filter to remove recording drifts
    string eegSerialRationalTransferFunctionFilter_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateSerialRationalTransferFunctionFilterEeg.mat";
    CSerialRationalTransferFunctionFilter eegSerialRationalTransferFunctionFilter(eegSerialRationalTransferFunctionFilter_calibrationFile);

    // High-pass eog filter to remove recording drifts
    string eogSerialRationalTransferFunctionFilter_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateSerialRationalTransferFunctionFilterEog.mat";
    CSerialRationalTransferFunctionFilter eogSerialRationalTransferFunctionFilter(eogSerialRationalTransferFunctionFilter_calibrationFile);

    // Channel interpolation
    string channelInterpolationAlgorithm_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateChannelInterpolationAlgorithm.mat";
    CChannelInterpolationAlgorithm channelInterpolationAlgorithm(channelInterpolationAlgorithm_calibrationFile);

    // EOG correction
    string eogCorrectionAlgorithm_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateEogCorrectionAlgorithm.mat";
    CEogCorrectionAlgorithm eogCorrectionAlgorithm(eogCorrectionAlgorithm_calibrationFile);

    // CAR algorithm
    string carAlgorithm_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateCarAlgorithm.mat";
    CCarAlgorithm carAlgorithm(carAlgorithm_calibrationFile, eegData.getReferenceChannelIndex());

    // Real & imaginary stft algorithm
    string realImaginaryStftAlgorithm_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateRealImaginaryStftAlgorithm.mat";
    CRealImaginaryStftAlgorithm realImaginaryStftAlgorithm(realImaginaryStftAlgorithm_calibrationFile);

    // Denoising algorithm
    string denoisingAlgorithm_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateDenoisingAlgorithm.mat";
    CDenoisingAlgorithm denoisingAlgorithm(denoisingAlgorithm_calibrationFile);

    // Real frequency band selector
    string realFrequencyBandSelector_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateRealFrequencyBandSelector.mat";
    CFrequencyBandSelector realFrequencyBandSelector(realFrequencyBandSelector_calibrationFile);

    // Imginary frequency band selector
    string imaginaryFrequencyBandSelector_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateImaginaryFrequencyBandSelector.mat";
    CFrequencyBandSelector imaginaryFrequencyBandSelector(imaginaryFrequencyBandSelector_calibrationFile);

    // Real spectral source reconstruction
    string realSpectralSourceReconstruction_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateRealSpectralSourceReconstruction.mat";
    CSpectralSourceReconstruction realSpectralSourceReconstruction(realSpectralSourceReconstruction_calibrationFile);

    // Imaginary spectral source reconstruction
    string imaginarySpectralSourceReconstruction_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateImaginarySpectralSourceReconstruction.mat";
    CSpectralSourceReconstruction imaginarySpectralSourceReconstruction(realSpectralSourceReconstruction_calibrationFile);

    // Spectral adaptive feature normalization
    string spectralAdaptiveFeatureNormalization_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateRealSpectralAdaptiveFeatureNormalization.mat";
    CSpectralAdaptiveFeatureNormalization spectralAdaptiveFeatureNormalization(spectralAdaptiveFeatureNormalization_calibrationFile);

    // Spectral feature time selector
    string spectralFeatureTimeSelector_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateSpectralFeatureTimeSelector.mat";
    CFeatureTimeSelector spectralFeatureTimeSelector(spectralFeatureTimeSelector_calibrationFile);

    // Savitzky-Golay filter
    string serialSGolayFilterAlgorithm_calibrationFile = "..\\SystemTestData\\integrationTestData_SerialSGolayFilterAlgorithm.mat";
    CSerialSGolayFilterAlgorithm serialSGolayFilterAlgorithm(serialSGolayFilterAlgorithm_calibrationFile);

    // Cortical source reconstruction
    string corticalSourceReconstruction_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateCorticalSourceReconstruction.mat";
    CCorticalSourceReconstruction corticalSourceReconstruction(corticalSourceReconstruction_calibrationFile);

    // Adaptive feature normalization
    string adaptiveFeatureNormalizationAlgorithm_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateAdaptiveFeatureNormalizationAlgorithm.mat";
    CAdaptiveFeatureNormalizationAlgorithm adaptiveFeatureNormalizationAlgorithm(adaptiveFeatureNormalizationAlgorithm_calibrationFile);

    // Feature time selector
    string featureTimeSelector_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateFeatureTimeSelector.mat";
    CFeatureTimeSelector featureTimeSelector(featureTimeSelector_calibrationFile);

    // Decoder
    string mldaClassifier_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateMldaClassifier.mat";
    CMldaClassifier mldaClassifier(mldaClassifier_calibrationFile);

    // Decision consolidator
    string consolidateDecisionAlgorithm_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateConsolidateDecisionAlgorithm.mat";
    CConsolidateDecisionAlgorithm consolidateDecisionAlgorithm(consolidateDecisionAlgorithm_calibrationFile);


    ChannelConfig channelConfig = eegData.GetChannelConfig();

    const size_t numberOfEegChannels = channelConfig.eegChannelsWithRef.size();
    const size_t numberOfEogChannels = 2; // always two channels: eogBipHor/eogBipVert

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Process the signals

    vector<CDoubleVector*> acquiredData;

    CDoubleVector* eegSignals = new CDoubleVector(numberOfEegChannels, 0.0);
    CDoubleVector* eogSignals = new CDoubleVector(numberOfEogChannels, 0.0);
    CDoubleVector* filteredEegSignals = new CDoubleVector(numberOfEegChannels, 0.0);
    CDoubleVector* filteredEogSignals = new CDoubleVector(numberOfEogChannels, 0.0);
    CDoubleVector* interpolatedSignal = new CDoubleVector(numberOfEegChannels, 0.0);
    CDoubleVector* eogCorrectedSignal = new CDoubleVector(numberOfEegChannels, 0.0);
    vector<CDoubleMatrix*> helpStftReal;
    vector<CDoubleMatrix*> helpStftImaginary;
    CDoubleMatrix* realDenoisedSpectralMatrix = new CDoubleMatrix(realFrequencyBandSelector.getNumberOfSpectralBins(), realFrequencyBandSelector.getNumberOfChannels(), 0.0);
    CDoubleMatrix* imaginaryDenoisedSpectralMatrix = new CDoubleMatrix(imaginaryFrequencyBandSelector.getNumberOfSpectralBins(), imaginaryFrequencyBandSelector.getNumberOfChannels(), 0.0);
    CDoubleMatrix* helpRealSpectralMatrix = new CDoubleMatrix(realFrequencyBandSelector.getNumberOfSelectedBins(), realFrequencyBandSelector.getNumberOfChannels(), 0.0);
    CDoubleMatrix* helpImaginarySpectralMatrix = new CDoubleMatrix(imaginaryFrequencyBandSelector.getNumberOfSelectedBins(), imaginaryFrequencyBandSelector.getNumberOfChannels(), 0.0);
    CDoubleMatrix* realSpectralCorticalActivity = new CDoubleMatrix(realSpectralSourceReconstruction.getNumberOfSpectralBins(), realSpectralSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleMatrix* imaginarySpectralCorticalActivity = new CDoubleMatrix(imaginarySpectralSourceReconstruction.getNumberOfSpectralBins(), imaginarySpectralSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleMatrix* spectralAmplitude = new CDoubleMatrix(realSpectralSourceReconstruction.getNumberOfSpectralBins(), realSpectralSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleMatrix* normalizedSpectralAmplitude = new CDoubleMatrix(realSpectralSourceReconstruction.getNumberOfSpectralBins(), realSpectralSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleVector* meanSpectralAmplitude = new CDoubleVector(realSpectralSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    vector<CDoubleVector*> filteredEEG;
    CDoubleVector* corticalActivity = new CDoubleVector(corticalSourceReconstruction.getNumberOfCorticalSources(), 0.0);
    CDoubleVector* normalizedCorticalActivity = new CDoubleVector(corticalSourceReconstruction.getNumberOfCorticalSources(), 0.0);
	CDoubleVector* timeFeatureVector = new CDoubleVector(corticalSourceReconstruction.getNumberOfCorticalSources() * featureTimeSelector.getNumberOfTaps(), 0.0);
	CDoubleVector* spectralFeatureVector = new CDoubleVector(corticalSourceReconstruction.getNumberOfCorticalSources() * spectralFeatureTimeSelector.getNumberOfTaps(), 0.0);

    size_t decodingResult;
    bool isReadyRealImaginaryStftAlgorithm = false;
    bool isReadySerialSGolayFilterAlgorithm = false;

    auto acq_start = high_resolution_clock::now();

    size_t samples_available_count;
     
    while (WAIT_TIMEOUT == WaitForSingleObject(stopAcquisition, 0))
    {
        auto temp = high_resolution_clock::now();
        auto delay = duration_cast<microseconds>(temp - acq_start).count();

        loopDuration.push_back(delay);

        acq_start = temp;

        eegData.acquireAndStoreNewSamples(acquiredData);
        samples_available_count = acquiredData.size();

        samplesCount.push_back(samples_available_count);

        if (!samples_available_count)
        {
            continue;
        }

        auto acq_end = high_resolution_clock::now();

        for (size_t testIndex = 0; testIndex < samples_available_count; ++testIndex)
        {
            // Channel selector
            eegData.channelSelector(acquiredData.at(testIndex)->getValues(), eegSignals->getValues(), eogSignals->getValues());
            delete acquiredData.at(testIndex);
            acquiredData.at(testIndex) = 0;

            // High-pass filtering
            if (!eegSerialRationalTransferFunctionFilter.process(filteredEegSignals, eegSignals) | !eogSerialRationalTransferFunctionFilter.process(filteredEogSignals, eogSignals))
            {
                continue;
            }

            // Channel interpolation
            channelInterpolationAlgorithm.process(interpolatedSignal, filteredEegSignals);

            // Eog correction
            eogCorrectionAlgorithm.process(eogCorrectedSignal, interpolatedSignal, filteredEogSignals);

            // Threshold artifact correction
            eegData.artefactThreshold(eogCorrectedSignal->getValues(), numberOfEegChannels);

            // Common average referencing
            carAlgorithm.process(eegSignals->getValues(), eogCorrectedSignal->getValues());

            if (isReadyRealImaginaryStftAlgorithm && isReadySerialSGolayFilterAlgorithm)
            {
                // Spectral decomposition
                realImaginaryStftAlgorithm.process(helpStftReal, helpStftImaginary, eegSignals);

                // Savitzky-Golay filtering
                serialSGolayFilterAlgorithm.process(filteredEEG, eegSignals);
            }
            else
            {
                isReadyRealImaginaryStftAlgorithm = realImaginaryStftAlgorithm.addToBuffer(eegSignals);

                isReadySerialSGolayFilterAlgorithm = serialSGolayFilterAlgorithm.addToBuffer(eegSignals);
            }
        }

        acquiredData.clear();

        assert(helpStftReal.size() == filteredEEG.size());
        for (size_t outputIndex = 0; outputIndex < helpStftReal.size(); outputIndex++)
        {
            // Spectral denoising
            denoisingAlgorithm.process(realDenoisedSpectralMatrix, imaginaryDenoisedSpectralMatrix, helpStftReal.at(outputIndex), helpStftImaginary.at(outputIndex));
            delete helpStftReal.at(outputIndex);
            helpStftReal.at(outputIndex) = 0;
            delete helpStftImaginary.at(outputIndex);
            helpStftImaginary.at(outputIndex) = 0;

            // Frequency band selector for real spectrum
            realFrequencyBandSelector.process(helpRealSpectralMatrix, realDenoisedSpectralMatrix);

            // Frequency band selector for imaginary spectrum
            realFrequencyBandSelector.process(helpImaginarySpectralMatrix, imaginaryDenoisedSpectralMatrix);

            // Spectral source reconstruction for the real spectrum
            realSpectralSourceReconstruction.process(realSpectralCorticalActivity, helpRealSpectralMatrix);

            // Spectral source reconstruction for the imaginary spectrum
            imaginarySpectralSourceReconstruction.process(imaginarySpectralCorticalActivity, helpImaginarySpectralMatrix);

            // Calculate the spectral amplitude of cortical activity
            CMathLibrary::elementWiseAbsoluteValue(spectralAmplitude, realSpectralCorticalActivity[0], imaginarySpectralCorticalActivity[0]);

            // Perform the spectral adaptive feature normalization
            spectralAdaptiveFeatureNormalization.process(normalizedSpectralAmplitude, spectralAmplitude, eegData.GetIsArtefactDetected());

            // Calculate the mean over all the selected spectral bins
            CMathLibrary::matrixMeanAcrossFirstDimension(meanSpectralAmplitude, normalizedSpectralAmplitude[0]);

            // Cortical source reconstruction
            corticalSourceReconstruction.process(corticalActivity, filteredEEG.at(outputIndex));
            delete filteredEEG.at(outputIndex);
            filteredEEG.at(outputIndex) = 0;

            // Adaptive feature normalization
            adaptiveFeatureNormalizationAlgorithm.process(normalizedCorticalActivity[0], corticalActivity[0], eegData.GetIsArtefactDetected());

            if (!adaptiveFeatureNormalizationAlgorithm.getIsInitialized() || !spectralAdaptiveFeatureNormalization.getIsInitialized())
            {
                continue;
            }

            // Add spectral and MRCP features to time selector buffers
            featureTimeSelector.addToBuffer(normalizedCorticalActivity);
            spectralFeatureTimeSelector.addToBuffer(meanSpectralAmplitude);
        }

        helpStftReal.clear();
        helpStftImaginary.clear();
        filteredEEG.clear();

        if (featureTimeSelector.getIsBufferFilled() && spectralFeatureTimeSelector.getIsBufferFilled())
        {
            // Feature time selector
            featureTimeSelector.generateFeatureVector(timeFeatureVector);
            spectralFeatureTimeSelector.generateFeatureVector(spectralFeatureVector);

            // Decoder
            decodingResult = mldaClassifier.process(timeFeatureVector, spectralFeatureVector);
        }
        else
        {
            decodingResult = 0;
        }

        size_t consolidatedDecision = consolidateDecisionAlgorithm.process(decodingResult, eegData.GetIsArtefactDetected(), static_cast<size_t>(samples_available_count));

        decoderDecision.push_back(decodingResult);
        decoderConsolidatedDecision.push_back(consolidatedDecision);
        
        // Wait on adaptive feature normalization to be initialized
        if (adaptiveFeatureNormalizationAlgorithm.getIsInitialized() && spectralAdaptiveFeatureNormalization.getIsInitialized())
        {
            if (!isUiModified)
            {
                PostMessage(WM_READY, 0, 0);
                isUiModified = true;
            }

            applyDecision(consolidatedDecision);
        }
    }

    // Create filename
    time_t t = time(0);
    struct tm * now = localtime(&t);

    string buffer(80, '\0');
    strftime(&buffer[0], buffer.size(), "%F_%H-%M-%S.mat", now);

    const size_t noOfDecisions = decoderDecision.size();
    double* storageBuffer = new double[noOfDecisions];
    for (size_t testIndex = 0; testIndex < noOfDecisions; ++testIndex)
    {
        storageBuffer[testIndex] = decoderDecision.at(testIndex);
    }

    CMatlabLibrary::saveMatDoubleMatrixData("..\\SystemTestData\\ProcedureLog_testResults" + buffer, "decoderDecision", storageBuffer, 1, noOfDecisions);

    const size_t noOfConsolidatedDecisions = decoderConsolidatedDecision.size();
    storageBuffer = new double[noOfConsolidatedDecisions];
    for (size_t testIndex = 0; testIndex < noOfConsolidatedDecisions; ++testIndex)
    {
        storageBuffer[testIndex] = decoderConsolidatedDecision.at(testIndex);
    }

    CMatlabLibrary::saveMatDoubleMatrixData("..\\SystemTestData\\ProcedureLog_testResults" + buffer, "decoderConsolidatedDecision", storageBuffer, 1, noOfConsolidatedDecisions);

    const size_t noOfLoopTiming = loopDuration.size();
    storageBuffer = new double[noOfLoopTiming];
    for (size_t testIndex = 0; testIndex < noOfLoopTiming; ++testIndex)
    {
        storageBuffer[testIndex] = loopDuration.at(testIndex);
    }

    CMatlabLibrary::saveMatDoubleMatrixData("..\\SystemTestData\\ProcedureLog_testResults" + buffer, "loopTiming", storageBuffer, 1, noOfLoopTiming);

    const size_t noOfSamplesCount = samplesCount.size();
    storageBuffer = new double[noOfSamplesCount];
    for (size_t testIndex = 0; testIndex < noOfSamplesCount; ++testIndex)
    {
        storageBuffer[testIndex] = samplesCount.at(testIndex);
    }

    CMatlabLibrary::saveMatDoubleMatrixData("..\\SystemTestData\\ProcedureLog_testResults" + buffer, "samplesCount", storageBuffer, 1, noOfSamplesCount);


    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Cleanup

    delete eegSignals;
    delete eogSignals;
    delete filteredEegSignals;
    delete filteredEogSignals;
    delete interpolatedSignal;
    delete eogCorrectedSignal;
    delete helpRealSpectralMatrix;
    delete helpImaginarySpectralMatrix;
    delete realDenoisedSpectralMatrix;
    delete imaginaryDenoisedSpectralMatrix;
    delete realSpectralCorticalActivity;
    delete imaginarySpectralCorticalActivity;
    delete spectralAmplitude;
    delete normalizedSpectralAmplitude;
    delete meanSpectralAmplitude;
    delete corticalActivity;
    delete normalizedCorticalActivity;
    delete timeFeatureVector;
    delete spectralFeatureVector;

    // Reset UI update flag
    isUiModified = false;

    // Stop thread and update UI accordingly
    PostMessage(WM_THREAD_COMPLETE, 0, 0);

    SetEvent(acquisitionStopped);
}

/**
* @brief Start or stop the calib thread when calib button is pressed
*/
void CGenevaEegBciDlg::OnBnClickedCalib()
{
    if (m_calibControlFlag == false) // calib off. Start calib
    {
        ResetEvent(stopCalib);
        ResetEvent(calibStopped);

        m_calibThread = AfxBeginThread(&CGenevaEegBciDlg::calibThread, this, THREAD_PRIORITY_TIME_CRITICAL, 0, CREATE_SUSPENDED, 0);
        m_calibThread->ResumeThread();

        m_editStartCalibration.SetWindowTextW(_T("Stop"));

        CString text;
        text.LoadString(IDS_PLEASE_WAIT_AND_RELAX);
        m_StaticSpellerCharacters.SetWindowTextW(text);

        m_calibControlFlag = true;

        m_fileDlg << "Start calib" << endl;
    }
    else    // calib on. Stop calib
    {
        SetEvent(stopCalib);
        WaitForSingleObject(calibStopped, INFINITE);

        m_calibThread = 0;

        m_fileDlg << "Stop calib" << endl;
    }
}

/**
* @brief Reset the UI and its state machine
*/
void CGenevaEegBciDlg::ResetUi(bool keepTextToSpell, bool redraw, unsigned int bmpId)
{
    m_spellerKey = A;
    m_spellerMode = LETTER;
    m_bmpId = bmpId;
    m_keysToUse = A;

    m_spellerCharacters.clear();
    m_spellerResult.clear();
    m_spellerCodedKeys.clear();

    m_keyIndex = 0;

    // Set that the next entered word is not a one letter one and set One Letter index accordingly
    m_isWordOneLetter.clear();
    m_isWordOneLetter.push_back(false);
    m_indexOneLetter.clear();
    m_indexOneLetter.push_back(0);

    m_StaticSpellerCharacters.SetWindowTextW(_T(""));
    m_StaticSpellerResult.SetWindowTextW(_T(""));

    if (!keepTextToSpell)
    {
        m_StaticTextToSpell.SetWindowTextW(_T(""));
    }
    
    if (redraw)
    {
        // Update and refresh UI
        FormatSpellerLabels();
        FillSpellerLabels();
        RedrawBitmap(m_bmpId);
        RedrawSpellerLabels();
    }
}

/**
* @brief Code to be executed to perform the calibration of the keyboard
*/
void CGenevaEegBciDlg::calib()
{
   // EEG data class
    string channelSelector_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateChannelSelector.mat";
    CEegData eegData(channelSelector_calibrationFile);
    
    // Get the script based on current language
    string filename = "..\\GenevaEegBci\\res\\script_" + m_language + ".txt";
 
    ifstream script_file = ifstream(filename.c_str());
   
    // use default script
    if (!script_file.is_open())
    {
        filename = "..\\GenevaEegBci\\res\\script.txt";

        script_file = ifstream(filename.c_str());
    }
    
    string script_string;
    static bool isUiModified = false; // UI update flag

    getline(script_file, script_string);

    string::iterator it = script_string.begin();

    bool is_init = eegData.initAcquisition();

    // Exit if any problem with amplifiers
    if (!is_init)
    {
        // Stop thread and update UI accordingly
        PostMessage(WM_THREAD_COMPLETE, 0, 0);

        m_editStartCalibration.SetWindowTextW(_T("Calibration"));

        m_fileDlg << "Amplifier init failed" << endl;

        return;
    }

    auto calib_start_timer = steady_clock::now();
    auto calib_init_start_timer = steady_clock::now();

    // Wait at the end of the calibration
    chrono::time_point<steady_clock> calib_to_stop;
    long long delay_before_end = 0;

    bool IsCalibrationComplete = false;

    while (WAIT_TIMEOUT == WaitForSingleObject(stopCalib, 0))
    {
        auto calib_current_timer = steady_clock::now();

        auto calib_init_current_timer = steady_clock::now();
        auto init_delay = duration_cast<milliseconds>(calib_init_current_timer - calib_init_start_timer).count();

        // Let a 20s delay before launching keyboard movements
        if (init_delay > CALIBRATION_INIT_DELAY)
        {
            long long random_delay;

            if (!isUiModified)
            {
                PostMessage(WM_READY, 0, 0);
                isUiModified = true;
                calib_start_timer = calib_current_timer;
                random_delay = rand() % CALIBRATION_CUE_INTERVAL + CALIBRATION_CUE_START_DELAY;
            }

            auto delay = duration_cast<milliseconds>(calib_current_timer - calib_start_timer).count();
            
            // Change UI every random time between CALIBRATION_CUE_START and CALIBRATION_CUE_START + CALIBRATION_CUE_INTERVAL (in ms)
            if (delay > random_delay && !IsCalibrationComplete)
            {
                calib_start_timer = calib_current_timer;
                random_delay = rand() % CALIBRATION_CUE_INTERVAL + CALIBRATION_CUE_START_DELAY;

                switch (*it)
                {
                case 'S':
                case 's':
                    PostMessage(WM_SELECT, 0, 0);
                    SetEvent(eegData.select);
                    break;

                case 'A':
                case 'a':
                    PostMessage(WM_ANTICLOCKWISE, 0, 0);
                    SetEvent(eegData.anticlockwise);
                    break;

                case 'C':
                case 'c':
                    PostMessage(WM_CLOCKWISE, 0, 0);
                    SetEvent(eegData.clockwise);
                    break;

                default:
                    break;
                }

                ++it;
            }
        }

        vector<CDoubleVector*> acquiredData;
        eegData.acquireAndStoreNewSamples(acquiredData);

        for (size_t testIndex = 0; testIndex < acquiredData.size(); ++testIndex)
        {
            delete acquiredData.at(testIndex);
            acquiredData.at(testIndex) = 0;
        }

        // Wait before reseting the UI
        if (   (it == script_string.end()) 
            && !IsCalibrationComplete )
        {
            IsCalibrationComplete = true;
            delay_before_end = CALIBRATION_CUE_END_DELAY;
            calib_to_stop = steady_clock::now();
        }

        auto calib_end = steady_clock::now();
        auto end_delay = duration_cast<milliseconds>(calib_end - calib_to_stop).count();

        if (   (0 != delay_before_end)
            && (end_delay > delay_before_end) )
        {
            break;
        }
    }

    script_file.clear();
    script_file.seekg(0, ios::beg);

    // Reset UI update flag
    isUiModified = false;
    
    // Stop thread and update UI accordingly
    PostMessage(WM_THREAD_COMPLETE, 0, 0);

    SetEvent(calibStopped);
}

/**
* @brief Start or stop the calib thread when calib button is pressed
*/
void CGenevaEegBciDlg::OnBnClickedRestStateCalib()
{
    if (m_restStateCalibControlFlag == false) // calib off. Start calib
    {
        ResetEvent(stopRestStateEogCalib);
        ResetEvent(restStateEogCalibStopped);

        m_restStateEogCalibThread = AfxBeginThread(&CGenevaEegBciDlg::restStateEogCalibThread, this, THREAD_PRIORITY_TIME_CRITICAL, 0, CREATE_SUSPENDED, 0);
        m_restStateEogCalibThread->ResumeThread();

        m_editStartRestStateCalib.SetWindowTextW(_T("Stop"));

        CString text;
        text.LoadString(IDS_PLEASE_RELAX);
        m_StaticSpellerCharacters.SetWindowTextW(text);

        m_restStateCalibControlFlag = true;

        m_fileDlg << "Start resting state calibration" << endl;
    }
    else    // calib on. Stop calib
    {
        SetEvent(stopRestStateEogCalib);
        WaitForSingleObject(restStateEogCalibStopped, INFINITE);

        m_restStateEogCalibThread = 0;

        m_fileDlg << "Stop resting state calibration" << endl;
    }
}

/**
* @brief Start or stop the calib EOG thread when calib EOG button is pressed
*/
void CGenevaEegBciDlg::OnBnClickedEogCalib()
{
    if (m_eogCalibControlFlag == false) // calib off. Start calib
    {
        ResetEvent(stopRestStateEogCalib);
        ResetEvent(restStateEogCalibStopped);

        m_restStateEogCalibThread = AfxBeginThread(&CGenevaEegBciDlg::restStateEogCalibThread, this, THREAD_PRIORITY_TIME_CRITICAL, 0, CREATE_SUSPENDED, 0);
        m_restStateEogCalibThread->ResumeThread();

        m_editStartEogCalib.SetWindowTextW(_T("Stop"));

        CString text;
        text.LoadString(IDS_PLEASE_MOVE_YOUR_EYES);
        m_StaticSpellerCharacters.SetWindowTextW(text);

        m_eogCalibControlFlag = true;

        m_fileDlg << "Start EOG calibration" << endl;
    }
    else    // calib on. Stop calib
    {
        SetEvent(stopRestStateEogCalib);
        WaitForSingleObject(restStateEogCalibStopped, INFINITE);

        m_restStateEogCalibThread = 0;

        m_fileDlg << "Stop EOG calibration" << endl;
    }
}

/**
* @brief Code to be executed to perform the resting state and EOG calibration recording
*/
void CGenevaEegBciDlg::restStateEogCalib()
{
    // EEG data class
    string channelSelector_calibrationFile = "..\\SystemTestData\\integrationTestData_calibrateChannelSelector.mat";
    CEegData eegData(channelSelector_calibrationFile);

    bool is_init = eegData.initAcquisition();

    // Exit if any problem with amplifiers
    if (!is_init)
    {
        // Stop thread and update UI accordingly
        PostMessage(WM_THREAD_COMPLETE, 0, 0);

        m_fileDlg << "Amplifier init failed" << endl;

        return;
    }

    auto calib_start = steady_clock::now();

    while (WAIT_TIMEOUT == WaitForSingleObject(stopRestStateEogCalib, 0))
    {
        auto calib_running = steady_clock::now();
        auto delay = duration_cast<milliseconds>(calib_running - calib_start).count();

        vector<CDoubleVector*> acquiredData;
        eegData.acquireAndStoreNewSamples(acquiredData);

        for (size_t testIndex = 0; testIndex < acquiredData.size(); ++testIndex)
        {
            delete acquiredData.at(testIndex);
            acquiredData.at(testIndex) = 0;
        }

        // Resting State / Eog calib lasts 3 minutes
        if (delay > EOG_REST_STATE_DURATION)
        {
            break;
        }
    }

    // Stop thread and update UI accordingly
    PostMessage(WM_THREAD_COMPLETE, 0, 0);

    SetEvent(restStateEogCalibStopped);
}

/**
* @brief Standard deviation calculation
*/
const double CGenevaEegBciDlg::standardDeviation(double* distances, int numSamples, double mu)
{
    double E=0;

    for(int i=0;i<numSamples;i++)
    {
        E+=(distances[i] - mu)*(distances[i] - mu);
    }

    double std_val = sqrt(1.0/double(numSamples)*E);

    return std_val;
}

/**
* @brief Get well formated string for log files
*/
const string CGenevaEegBciDlg::getWellFormatedDate(void)
{
    time_t rawtime;
    time(&rawtime);
    struct tm * timePtr = localtime(&rawtime);

    string date = asctime(timePtr);

    // clean (symbols ":", " ")
    string str2remove;

    str2remove = ":";
    while (date.find(str2remove) != string::npos)
    {
        date.replace(date.find(str2remove), str2remove.length(), "");
    }

    str2remove = " ";

    while (date.find(str2remove) != string::npos)
    {
        date.replace(date.find(str2remove), str2remove.length(), "");
    }
    date = date.substr(0, 15);

    return date;
}

/**
* @brief Specific thread for the acquisition and processing of EEG
*/
UINT CGenevaEegBciDlg::processThreadAcquisition(LPVOID parameter)
{
    CGenevaEegBciDlg* me = (CGenevaEegBciDlg *)parameter;

    me->acquisitionControl(parameter);
    return 0;
}

/**
* @brief Specific thread to perform the calib of the circular keyboard
*/
UINT CGenevaEegBciDlg::calibThread(LPVOID parameter)
{
    CGenevaEegBciDlg* me = (CGenevaEegBciDlg *)parameter;

    me->calib();
    return 0;
}

/**
* @brief Specific thread to perform the recording of resting state and EOG calibration
*/
UINT CGenevaEegBciDlg::restStateEogCalibThread(LPVOID parameter)
{
    CGenevaEegBciDlg* me = (CGenevaEegBciDlg *)parameter;

    me->restStateEogCalib();
    return 0;
}

/**
* @brief Print function
*/
void CGenevaEegBciDlg::Output(const WCHAR* szFormat, ...)
{
    WCHAR szBuff[1000];
    va_list arg;
    va_start(arg, szFormat);
    _vsnwprintf_s(szBuff, sizeof(szBuff), szFormat, arg);
    va_end(arg);

    OutputDebugString(szBuff);
}

/**
* @brief Update the UI after a select key press
*/
void CGenevaEegBciDlg::OnSelect()
{
    SpeechSynthesizer^ synth = gcnew SpeechSynthesizer();

    // Show selected key in green
    ShowSelectedKey();

    CString cCurrentCharStr;
    CString cCurrentResultStr;
    CString text;

    m_StaticSpellerCharacters.GetWindowTextW(cCurrentCharStr);
    m_StaticSpellerResult.GetWindowTextW(cCurrentResultStr);

    CString cCurrentKeyString;
    if (m_spellerKey <= C)
    {
        m_TransparentStaticArray.at(m_spellerKey)->GetWindowTextW(cCurrentKeyString);
    }
    else
    {
        // Remove the SEL key
        m_TransparentStaticArray.at(m_spellerKey-1)->GetWindowTextW(cCurrentKeyString);
    }

    // If delete/cancel is selected
    if (BCK == m_spellerKey)
    {
        CancelLastAction();
    }
    else
    {
        switch (m_spellerMode)
        {
            case LETTER:
                if (SEL == m_spellerKey)
                {
                    // predict or menu
                    if (   !cCurrentCharStr.IsEmpty() 
                        && (m_predictedWords.size() > 0) )
                    {
                        // predict
                        m_spellerMode = PRED1;
                        m_bmpId = IDB_BITMAP_SELECT_PREDICT2;
                    }
                    else if (   !cCurrentCharStr.IsEmpty()
                             && (0 == m_predictedWords.size()) )
                    {
                        // Do nothing
                    }
                    else
                    {
                        // Go to Menu1
                        m_spellerMode = MENU1;
                        //m_bmpId = IDB_BITMAP_SELECT_PREDICT2; // to uncomment if Menu2 is needed
                    }
                }
                else
                {
                    Spell();
                }
                break;

            case DIGIT1:
                if (SEL == m_spellerKey)
                {
                    // Go to Digit2
                    m_spellerMode = DIGIT2;
                    m_bmpId = IDB_BITMAP_SELECT_PREDICT1;
                }
                else
                {
                    Spell();
                }
                break;

            case DIGIT2:
                if (SEL == m_spellerKey)
                {
                    // Go back to Digit1
                    m_spellerMode = DIGIT1;
                    m_bmpId = IDB_BITMAP_SELECT_PREDICT2;
                }
                else if (E == m_spellerKey)
                {
                    // Go to calc
                    m_spellerMode = CALC;
                }
                else
                {
                    Spell();
                }
                break;

            case CALC:
                if (SEL == m_spellerKey)
                {
                    // Return by default to Digit1
                }
                else
                {
                    Spell();
                }

                // All keys back to Digit1
                m_spellerMode = DIGIT1;
                break;

            case PUNCT:
                if (SEL == m_spellerKey)
                {
                    // Return by default to Letter
                }
                else
                {
                    Spell();
                }

                // Go back to Letter
                m_spellerMode = LETTER;
                break;

            case MENU1:
                switch (m_spellerKey)
                {
                    case A: // Phrases
                        // Go to Phrases1
                        m_spellerMode = PHRASES1;
                        break;

                    case B: // Speak
                        if (!cCurrentResultStr.IsEmpty())
                        {
                            String ^string_to_speak = gcnew String(cCurrentResultStr);
                            
                            synth->Speak(string_to_speak);

                            delete string_to_speak;
                        }
                        break;

                    case C: // Shift
                        if (!cCurrentResultStr.IsEmpty())
                        {
                            locale loc; 

                            if (islower(m_spellerResult.back()[0], loc))
                            {
                                m_spellerResult.back()[0] = toupper(m_spellerResult.back()[0], loc);
                            }
                            else
                            {
                                if (1 < m_spellerResult.back().size())
                                {
                                    if (islower(m_spellerResult.back()[1], loc))
                                    {
                                        transform(m_spellerResult.back().begin(), m_spellerResult.back().end(), m_spellerResult.back().begin(), ::toupper);
                                    }
                                    else
                                    {
                                        transform(m_spellerResult.back().begin(), m_spellerResult.back().end(), m_spellerResult.back().begin(), ::tolower);
                                    }
                                }
                                else
                                {
                                    transform(m_spellerResult.back().begin(), m_spellerResult.back().end(), m_spellerResult.back().begin(), ::tolower);
                                }
                            }

                            cCurrentResultStr = accumulate(m_spellerResult.begin(), m_spellerResult.end(), string("")).c_str();
                            m_StaticSpellerResult.SetWindowTextW(cCurrentResultStr);
                        }
                        break;

                    // To use if Menu2 is needed
                    //case SEL: // Menu2
                    //    if (cCurrentCharStr.IsEmpty())
                    //    {
                    //        // Go to Menu2
                    //        m_spellerMode = MENU2;
                    //        m_bmpId = IDB_BITMAP_SELECT_PREDICT1;
                    //    }
                    //    break;

                    case F: // 123
                        // Go to Digit1
                        m_spellerMode = DIGIT1;
                        break;

                    case E: // .,?
                        // Go to Punct
                        m_spellerMode = PUNCT;
                        break;

                    case D:
                        // todo implement cancel feature
                        // ResetUi(false);
                        break;
                }
                break;

            case MENU2:
                // Not used currently
                switch (m_spellerKey)
                {
                    case SEL: // Menu1
                        // Go back to Menu1
                        m_spellerMode = MENU1;
                        m_bmpId = IDB_BITMAP_SELECT_PREDICT2;
                        break;

                    default:
                        break;

                }
                break;

            case PRED1:
                text.LoadString(IDS_ONE_LETTER);

                if (SEL == m_spellerKey)
                {
                    if (m_predictedWords.size() > predKeys / 2)
                    {
                        // Go to Pred2
                        m_spellerMode = PRED2;
                        m_bmpId = IDB_BITMAP_SELECT_PREDICT1;
                    }
                }
                else if (cCurrentKeyString == text) // 1 Letter mode selected
                {
                    // Switch to ONE_LETTER mode and init the string index
                    m_spellerMode = ONE_LETTER;

                    // This word is using the one letter feature
                    m_isWordOneLetter.back() = true;

                    // Reset keyboard to first key to avoid empty key selection
                    m_bmpId = IDB_BITMAP2;
                    m_spellerKey = A;
                }
                else
                {
                    Spell();

                    // Go back to Letter
                    m_spellerMode = LETTER;
                }
                break;

            case PRED2:
                text.LoadString(IDS_ONE_LETTER);

                if (SEL == m_spellerKey)
                {
                    // Go back to Pred1
                    m_spellerMode = PRED1;
                    m_bmpId = IDB_BITMAP_SELECT_PREDICT2;
                }
                else if (cCurrentKeyString == text) // 1 Letter mode selected
                {
                    // Switch to ONE_LETTER mode and init the string index
                    m_spellerMode = ONE_LETTER;

                    // This word is using the one letter feature
                    m_isWordOneLetter.back() = true;

                    // Reset keyboard to first key to avoid empty key selection
                    m_bmpId = IDB_BITMAP2;
                    m_spellerKey = A;
                }
                else
                {
                    Spell();

                    // Go back to Letter
                    m_spellerMode = LETTER;
                }
                break;

            case PHRASES1:
                if (SEL == m_spellerKey)
                {
                    // Go to Phrases2
                    m_spellerMode = PHRASES2;
                    m_bmpId = IDB_BITMAP_SELECT_PREDICT1;
                }
                else
                {
                    Spell();
                }
                break;

            case PHRASES2:
                if (SEL == m_spellerKey)
                {
                    // Go back to Phrases1
                    m_spellerMode = PHRASES1;
                    m_bmpId = IDB_BITMAP_SELECT_PREDICT2;
                }
                else
                {
                    Spell();
                }
                break;

            case ONE_LETTER:
                if (SEL == m_spellerKey)
                {
                    // Do nothing
                }
                else
                {
                    Spell();

                    // Reset keyboard to first key to avoid empty key selection
                    m_bmpId = IDB_BITMAP2;
                    m_spellerKey = A;

                    // If no more letter to enter
                    if (m_spellerCodedKeys.back().size() <= m_indexOneLetter.back())
                    {
                        // Clear entered keys
                        m_StaticSpellerCharacters.SetWindowTextW(L"");
                        
                        // Go back to Letter
                        m_spellerMode = LETTER;
                    }
                }
                break;
        }
    }

    // Update and refresh UI
    FormatSpellerLabels();
    FillSpellerLabels();
    RedrawBitmap(m_bmpId);
    RedrawSpellerLabels();

    // Log
    m_StaticSpellerCharacters.GetWindowTextW(cCurrentCharStr);
    m_StaticSpellerResult.GetWindowTextW(cCurrentResultStr);

    m_fileDlg << m_spellerMode << "\t" << m_spellerKey << "\t" << m_bmpId << "\t" << m_keyIndex << "\t" << CT2A(cCurrentCharStr) << "\t" << CT2A(cCurrentResultStr) << "\tOnBnClickedSelect";

    for (auto i : m_label)
    {
        string localStr = CT2A(i);

        m_fileDlg << "\t" << localStr;
    }

    m_fileDlg << endl;
}

/**
* @brief Cancel last action performed
*/
void CGenevaEegBciDlg::CancelLastAction()
{
    CString cCurrentCharStr;
    CString cCurrentResultStr;

    m_StaticSpellerCharacters.GetWindowTextW(cCurrentCharStr);
    m_StaticSpellerResult.GetWindowTextW(cCurrentResultStr);

    if (   (LETTER == m_spellerMode)
        || (ONE_LETTER == m_spellerMode) )
    {
        // Remove entered keys
        if (!cCurrentCharStr.IsEmpty())
        {
            if (!m_isWordOneLetter.back())
            {
                m_spellerCharacters.back().pop_back();

                if (m_spellerCharacters.back().empty())
                {
                    // Remove last word structures
                    m_spellerCharacters.pop_back();
                    m_spellerCodedKeys.pop_back();

                    // Manage One Letter feature members accordingly
                    m_isWordOneLetter.pop_back();
                    m_indexOneLetter.pop_back();

                    m_StaticSpellerCharacters.SetWindowTextW(L"");

                    // Go back to Letter
                    m_spellerMode = LETTER;
                }
                else
                {
                    cCurrentCharStr = accumulate(m_spellerCharacters.back().begin(), m_spellerCharacters.back().end(), string("")).c_str();
                    m_StaticSpellerCharacters.SetWindowTextW(cCurrentCharStr);

                    m_spellerCodedKeys.back() = m_spellerCodedKeys.back().substr(0, m_spellerCodedKeys.back().size() - 1);
                    CodeToWordPred(m_spellerCodedKeys.back());
                }
            }
            else
            {
                if (   !m_spellerResult.empty()
                    && (m_spellerResult.size() == m_spellerCharacters.size()) )
                {
                    if (!m_spellerResult.back().empty())
                    {
                        // Remove last letter of the result
                        m_spellerResult.back() = m_spellerResult.back().substr(0, m_spellerResult.back().size() - 1);

                        // Update result accordingly
                        cCurrentResultStr = accumulate(m_spellerResult.begin(), m_spellerResult.end(), string("")).c_str();
                        m_StaticSpellerResult.SetWindowTextW(cCurrentResultStr);

                        m_indexOneLetter.back()--;

                        if (m_spellerResult.back().empty())
                        {
                            m_spellerResult.pop_back();
                        }
                    }
                }
                else
                {
                    m_isWordOneLetter.back() = false;

                    // Perform prediction to update Pred mode labels
                    CodeToWordPred(m_spellerCodedKeys.back());

                    // Display the corresponding Pred mode page
                    if (m_predictedWords.size() > predKeys / 2)
                    {
                        // Go to Pred2
                        m_spellerMode = PRED2;
                    }
                    else
                    {
                        // Go to Pred1
                        m_spellerMode = PRED1;
                    }
                }
            }
        }
        else if (!cCurrentResultStr.IsEmpty())
        {          
            // If possible, reset the last entered keys
            if (!m_spellerCharacters.empty())
            {
                if (!m_spellerCharacters.back().empty())
                {
                    cCurrentCharStr = accumulate(m_spellerCharacters.back().begin(), m_spellerCharacters.back().end(), string("")).c_str();
                    m_StaticSpellerCharacters.SetWindowTextW(cCurrentCharStr);

                    if (!m_isWordOneLetter.back())
                    {
                        m_spellerResult.pop_back();

                        if (m_spellerCodedKeys.back() != "")
                        {
                            // Predict word based on coded keys entered
                            CodeToWordPred(m_spellerCodedKeys.back());
                        }
                        else
                        {
                            // Remove last word structures
                            m_spellerCharacters.pop_back();
                            m_spellerCodedKeys.pop_back();

                            // Manage One Letter feature members accordingly
                            m_isWordOneLetter.pop_back();
                            m_indexOneLetter.pop_back();
                        }
                    }
                    else
                    {
                        if (ONE_LETTER != m_spellerMode)
                        {
                            // Switch to ONE_LETTER mode and init the string index
                            m_spellerMode = ONE_LETTER;
                            m_indexOneLetter.back() = m_spellerCodedKeys.back().size() - 1;

                            // Remove last letter of the result and the added space
                            m_spellerResult.back() = m_spellerResult.back().substr(0, m_spellerResult.back().size() - 2);
                        }
                        else 
                        {
                            m_indexOneLetter.back()--;
                        }
                    }
                }
            }
            else
            {
                // Remove last predicted word
                m_spellerResult.pop_back();
                m_spellerCodedKeys.pop_back();
                m_isWordOneLetter.pop_back();
                m_indexOneLetter.pop_back();
            }

            // Update result accordingly
            cCurrentResultStr = accumulate(m_spellerResult.begin(), m_spellerResult.end(), string("")).c_str();
            m_StaticSpellerResult.SetWindowTextW(cCurrentResultStr);
        }
    }
    else if (CALC == m_spellerMode)
    {
        m_spellerMode = DIGIT1;
    }
    else
    {
        if (DIGIT1 == m_spellerMode && cCurrentResultStr != "")
        {
            cCurrentResultStr += " ";
            m_StaticSpellerResult.SetWindowTextW(cCurrentResultStr);
        }

        // Go back to Letter
        m_spellerMode = LETTER;
    }
}

/**
* @brief Update the UI after a processing thread is complete
*/
void CGenevaEegBciDlg::OnAcquisitionReady()
{
    // Reset UI but keep the text to spell
    ResetUi(true);
}

/**
* @brief Set text to spell
*/
void CGenevaEegBciDlg::SetTextToSpell()
{
    // Get the script based on current language
    string filename = "..\\GenevaEegBci\\res\\script_" + m_language + ".txt";

    ifstream script_file = ifstream(filename.c_str());

    // use default script
    if (!script_file.is_open())
    {
        filename = "..\\GenevaEegBci\\res\\script.txt";

        script_file = ifstream(filename.c_str());
    }

    // Set Text to spell
    vector<string> to_spell_string;
    static unsigned int to_spell_index = 1;

    for (string each; getline(script_file, each); to_spell_string.push_back(each));

    wstring u16_conv = wstring_convert<codecvt_utf8_utf16<wchar_t>, wchar_t>{}.from_bytes(to_spell_string[to_spell_index].c_str());

    CString temp_cstring(u16_conv.c_str());

    // Update the text to spell index for next online session
    to_spell_index++;
    if (to_spell_index >= to_spell_string.size())
    {
        to_spell_index = 1;
    }

    m_StaticTextToSpell.SetWindowTextW(temp_cstring);
}

/**
* @brief Update the UI after a processing thread is complete
*/
void CGenevaEegBciDlg::OnThreadComplete()
{
    // Reset UI to Grey scale UI
    ResetUi(false, true, IDB_BITMAP1);

    if (m_acquisitionControlFlag)
    {
        m_acquisitionControlFlag = false;
        m_editStartAcquisitionControl.SetWindowTextW(_T("Start"));
    }

    if (m_calibControlFlag)
    {
        m_calibControlFlag = false;
        m_editStartCalibration.SetWindowTextW(_T("Calibration"));
    }

    if (m_restStateCalibControlFlag)
    {
        m_restStateCalibControlFlag = false;
        m_editStartRestStateCalib.SetWindowTextW(_T("Resting State"));
    }

    if (m_eogCalibControlFlag)
    {
        m_eogCalibControlFlag = false;
        m_editStartEogCalib.SetWindowTextW(_T("EOG"));
    }
}

/**
* @brief Update the UI after a select key press
*/
void CGenevaEegBciDlg::ShowSelectedKey()
{
    unsigned int selectedBmpId = spellerKeySelecteBmpMap[m_bmpId];

    RedrawBitmap(selectedBmpId);
    RedrawSpellerLabels(false);

    auto transition_start = steady_clock::now();
    auto transition_end = steady_clock::now();

    while (duration_cast<milliseconds>(transition_end - transition_start).count() < 200)
    {
        transition_end = steady_clock::now();
    }
}

/**
* @brief Update the bitmap after a clockwise key press
*/
void CGenevaEegBciDlg::OnClockwise()
{
    // Adapt keyboard layout based on mode
    AdaptKeyboards();

    // Determine the next key
    m_keyIndex = (m_keyIndex + 1) % m_keysVector.size();
    m_spellerKey = static_cast<spellerKey>(m_keysVector.at(m_keyIndex));

    // Determine the bitmap to display
    if (   ((m_spellerMode == DIGIT1) || (m_spellerMode == PRED1) || (m_spellerMode == PHRASES1))
        && (SEL == m_spellerKey) )
    {
        // In mode Digit1 or Pred1 or Phrases1, specific bitmap to display
        m_bmpId = IDB_BITMAP_SELECT_PREDICT2;
    }
    else
    {
        // Use the unordered map to determine the bitmap to display
        m_bmpId = spellerKeyBmpMap[m_spellerKey];
    }

    // Update UI
    RedrawBitmap(m_bmpId);
    RedrawSpellerLabels(false);

    // Log
    CString cCurrentCharStr;
    CString cCurrentResultStr;

    m_StaticSpellerCharacters.GetWindowTextW(cCurrentCharStr);
    m_StaticSpellerResult.GetWindowTextW(cCurrentResultStr);

    m_fileDlg << m_spellerMode << "\t" << m_spellerKey << "\t" << m_bmpId << "\t" << m_keyIndex << "\t" << CT2A(cCurrentCharStr) << "\t" << CT2A(cCurrentResultStr) << "\tOnBnClickedClockwise";

    for (auto i : m_label)
    {
        string localStr = CT2A(i);

        m_fileDlg << "\t" << localStr;
    }

    m_fileDlg << endl;
}

/**
* @brief Update the bitmap after a counter clockwise key press
*/
void CGenevaEegBciDlg::OnAnticlockwise()
{
    // Adapt keyboard layout based on mode
    AdaptKeyboards();

    // Determine the next key
    m_keyIndex = m_keyIndex - 1;

    // Sanity check
    if (0 > m_keyIndex)
    {
        m_keyIndex = m_keysVector.size() - 1;
    }

    m_spellerKey = static_cast<spellerKey>(m_keysVector.at(m_keyIndex));

    // Determine the bitmap to display
    if (   ((m_spellerMode == DIGIT1) || (m_spellerMode == PRED1) || (m_spellerMode == PHRASES1))
        && (SEL == m_spellerKey) )
    {
        // In mode Digit1 or Pred1 or Phrases1, specific bitmap to display
        m_bmpId = IDB_BITMAP_SELECT_PREDICT2;
    }
    else
    {
        // Use the unordered map to determine the bitmap to display
        m_bmpId = spellerKeyBmpMap[m_spellerKey];
    }

    // Update UI
    RedrawBitmap(m_bmpId);
    RedrawSpellerLabels(false);

    // Log
    CString cCurrentCharStr;
    CString cCurrentResultStr;

    m_StaticSpellerCharacters.GetWindowTextW(cCurrentCharStr);
    m_StaticSpellerResult.GetWindowTextW(cCurrentResultStr);

    m_fileDlg << m_spellerMode << "\t" << m_spellerKey << "\t" << m_bmpId << "\t" << m_keyIndex << "\t" << CT2A(cCurrentCharStr) << "\t" << CT2A(cCurrentResultStr) << "\tOnBnClickedCounterclockwise";
    
    for (auto i : m_label)
    {
        string localStr = CT2A(i);

        m_fileDlg << "\t" << localStr;
    }

    m_fileDlg << endl;
}

/**
* @brief Update the keys vector
*/
void CGenevaEegBciDlg::AdaptKeyboards()
{
    // Clear the keys vector
    m_keysVector.clear();

    // Compute the keys to be displayed vector
    if (   (m_spellerMode == PRED1)
        && (m_predictedWords.size() <= predKeys / 2) )
    {
        merge(m_keyboardKeys.begin(), m_keyboardKeys.begin() + m_predictedWords.size(), m_menuKeys.begin(), m_menuKeys.end(), back_inserter(m_keysVector));
    }
    else if (   (m_spellerMode == PRED2)
             && (m_predictedWords.size() <= predKeys) )
    {
        merge(m_keyboardKeys.begin(), m_keyboardKeys.begin() + (m_predictedWords.size() - predKeys / 2), m_menuKeys.begin(), m_menuKeys.end(), back_inserter(m_keysVector));
    }
    else if (   (m_spellerMode == ONE_LETTER)
             && (m_oneLetterKeysValue[m_keysToUse].size() <= predKeys / 2))
    {
        merge(m_keyboardKeys.begin(), m_keyboardKeys.begin() + m_oneLetterKeysValue[m_keysToUse].size(), m_menuKeys.begin(), m_menuKeys.end(), back_inserter(m_keysVector));
    }
    else
    {
        merge(m_keyboardKeys.begin(), m_keyboardKeys.end(), m_menuKeys.begin(), m_menuKeys.end(), back_inserter(m_keysVector));
    }

    m_keyIndex = distance(m_keysVector.begin(), find(m_keysVector.begin(), m_keysVector.end(), m_spellerKey));
}

/**
* @brief Initialize the string from string table resource
*/
void CGenevaEegBciDlg::InitStrings()
{
    CString tmp_str;

    // Letter mode keys value
    for (unsigned int i = IDS_ABCD; i <= IDS_MENU2_6; i++)
    {
        tmp_str.LoadString(i);
        m_letterKeysValue.push_back(tmp_str);
    }

    m_letterPredKeysValue = m_letterKeysValue;

    // Prediction mode keys value
    m_pred1KeysValue = m_letterKeysValue;
    m_pred2KeysValue = m_letterKeysValue;

    // Menu 1 mode keys value
    for (unsigned int i = IDS_PHRASES; i <= IDS_CLEAR; i++)
    {
        tmp_str.LoadString(i);

        m_menu1KeysValue.push_back(tmp_str);
    }
    for (unsigned int i = IDS_MENU1_1; i <= IDS_MENU2_6; i++)
    {
        tmp_str.LoadString(i);

        m_menu1KeysValue.push_back(tmp_str);
    }

    // Menu 2 mode keys value
    for (unsigned int i = IDS_ONE_LETTER; i <= IDS_SETTINGS; i++)
    {
        tmp_str.LoadString(i);

        m_menu2KeysValue.push_back(tmp_str);
    }
    for (unsigned int i = IDS_MENU1_1; i <= IDS_MENU2_6; i++)
    {
        tmp_str.LoadString(i);

        m_menu2KeysValue.push_back(tmp_str);
    }

    // Phrases 1 mode keys value
    for (unsigned int i = IDS_REPEAT; i <= IDS_MENU_PHRASES2_6; i++)
    {
        tmp_str.LoadString(i);

        m_phrases1KeysValue.push_back(tmp_str);
    }

    // Phrases 2 mode keys value
    for (unsigned int i = IDS_I_DONT_KNOW; i <= IDS_BAD; i++)
    {
        tmp_str.LoadString(i);

        m_phrases2KeysValue.push_back(tmp_str);
    }
    for (unsigned int i = IDS_MENU_PHRASES1_1; i <= IDS_MENU_PHRASES2_6; i++)
    {
        tmp_str.LoadString(i);

        m_phrases2KeysValue.push_back(tmp_str);
    }

    // Punct mode menu update
    for (unsigned int i = IDS_MENU1_1; i <= IDS_MENU2_6; i++)
    {
        tmp_str.LoadString(i);

        m_punctKeysValue.push_back(tmp_str);
    }
}

/**
* @brief Redraw the bitmap of the current state
*/
void CGenevaEegBciDlg::RedrawBitmap(unsigned int bmpId)
{
    CBitmap bmp;

    bmp.LoadBitmap(bmpId);
    hBmp = (HBITMAP)bmp;
    m_StaticBmp.SetBitmap(hBmp);
}

/**
* @brief Redraw the speller labels
*/
void CGenevaEegBciDlg::RedrawSpellerLabels(bool redrawResult)
{
    for (auto it = m_TransparentStaticArray.begin(); it != m_TransparentStaticArray.end(); it++)
    {
        (*it)->RedrawWindow();
    }
    
    if (redrawResult)
    {
        m_StaticSpellerCharacters.RedrawWindow();
        m_StaticSpellerResult.RedrawWindow();
    }
}

/**
* @brief Redraw the speller labels
*/
void CGenevaEegBciDlg::FillSpellerLabels()
{
    static CString cCurrentStr;

    m_StaticSpellerCharacters.GetWindowTextW(cCurrentStr);

    switch (m_spellerMode)
    {
        case LETTER:
            m_label = m_letterKeysValue;

            // Show prediction if Coded values
            if (!cCurrentStr.IsEmpty())
            {
                m_label = m_letterPredKeysValue;
            }
            break;

        case DIGIT1:
            m_label = m_digit1KeysValue;
            break;

        case DIGIT2:
            m_label = m_digit2KeysValue;
            break;

        case CALC:
            m_label = m_calcKeysValue;
            break;

        case PUNCT:
            m_label = m_punctKeysValue;
            break;

        case MENU1:
            m_label = m_menu1KeysValue;
            break;

        case MENU2:
            m_label = m_menu2KeysValue;
            break;

        case PRED1:
            m_label = m_pred1KeysValue;
            break;

        case PRED2:
            m_label = m_pred2KeysValue;
            break;

        case PHRASES1:
            m_label = m_phrases1KeysValue;
            break;

        case PHRASES2:
            m_label = m_phrases2KeysValue;
            break;

        case ONE_LETTER:
            for (auto it = m_TransparentStaticArray.begin(); it != m_TransparentStaticArray.end(); it++)
            {
                unsigned int index = (it - m_TransparentStaticArray.begin());

                m_keysToUse = stringSpellerKeyMap[m_spellerCodedKeys.back()[m_indexOneLetter.back()]];

                if (index < m_oneLetterKeysValue[m_keysToUse].size())
                {
                    m_label.at(index) = m_oneLetterKeysValue[m_keysToUse][index];
                }
                else
                {
                    m_label.at(index) = L"";
                }
            }
            break;

        default:
            break;
    }

    for (auto it = m_TransparentStaticArray.begin(); it != m_TransparentStaticArray.end(); it++)
    {
        (*it)->SetWindowTextW(*(m_label.begin() + (it - m_TransparentStaticArray.begin())));
    }
}

/**
* @brief Set the font of the speller labels
*/
void CGenevaEegBciDlg::FormatSpellerLabels()
{
    // Set Keyboard labels font
    if (LETTER == m_spellerMode)
    {
        // With a member variable associated to the static control
        for (auto it = m_TransparentStaticArray.begin(); it != m_TransparentStaticArray.begin() + (NUM_SPELLER_KEY - 2); it++)
        {
            (*it)->SetFont(&m_spellerLetterKeyboardFont);
        }
    }
    else
    {
        // With a member variable associated to the static control
        for (auto it = m_TransparentStaticArray.begin(); it != m_TransparentStaticArray.begin() + (NUM_SPELLER_KEY - 2); it++)
        {
            (*it)->SetFont(&m_spellerPhrasesKeyboardFont);
        }
    }
}

/**
* @brief Update the entered keys and the prediction result based on the selected key
*/
void CGenevaEegBciDlg::Spell()
{
    CString cSelectedKeyStr;
    CString cCodedStr;
    CString cCurrentCharStr;
    CString cCurrentResultStr;
    string conversionStr;

    // Get displayed strings for result static and entered keys static
    m_StaticSpellerCharacters.GetWindowTextW(cCurrentCharStr);
    m_StaticSpellerResult.GetWindowTextW(cCurrentResultStr);

    // Encode the hit key
    if (m_spellerKey <= C)
    {
        m_TransparentStaticArray.at(m_spellerKey)->GetWindowTextW(cSelectedKeyStr);
        cCodedStr = m_codedValue.at(m_spellerKey);
    }
    else
    {
        // Remove the SEL key
        m_TransparentStaticArray.at(m_spellerKey-1)->GetWindowTextW(cSelectedKeyStr);
        cCodedStr = m_codedValue.at(m_spellerKey-1);
    }

    string selectedKeyStdStr = CT2A(cSelectedKeyStr);
    string currentResultStdStr = CT2A(cCurrentResultStr);

    // Add separators
    switch (m_spellerMode)
    {
        case LETTER:
            cSelectedKeyStr += "----- ";
            break;

        case DIGIT1:
        case DIGIT2:
        case CALC:
            m_spellerCodedKeys.push_back("");
            m_spellerCharacters.push_back({ "" });

            // Set that the entered word is not a one letter one
            m_isWordOneLetter.push_back(false);
            m_indexOneLetter.push_back(0);
            break;

        case PUNCT:
            if (!currentResultStdStr.empty() && m_spellerResult.empty())
            {
                if (currentResultStdStr.back() == ' ')
                {
                    cCurrentResultStr = currentResultStdStr.substr(0, currentResultStdStr.size() - 1).c_str();
                    m_spellerResult.back() = m_spellerResult.back().substr(0, m_spellerResult.back().size() - 1);
                }
            }

            cSelectedKeyStr += " ";

            m_spellerCodedKeys.push_back("");
            m_spellerCharacters.push_back({ "" });

            // Set that the entered word is not a one letter one
            m_isWordOneLetter.push_back(false);
            m_indexOneLetter.push_back(0);
            break;

        case PRED1:
        case PRED2:
            if (selectedKeyStdStr.back() != '\'')
            {
                cSelectedKeyStr += " ";
            }

            // Clear entered keys
            m_StaticSpellerCharacters.SetWindowTextW(L"");
            break;

        case PHRASES1:
        case PHRASES2:
            cSelectedKeyStr += " ";

            m_spellerCodedKeys.push_back("");
            m_spellerCharacters.push_back({""});

            // Set that the entered word is not a one letter one
            m_isWordOneLetter.push_back(false);
            m_indexOneLetter.push_back(0);
            break;

        case ONE_LETTER:
            m_indexOneLetter.back()++;
            
            cSelectedKeyStr.MakeLower();

            if (m_spellerCodedKeys.back().size() <= m_indexOneLetter.back())
            {
                cSelectedKeyStr += " ";
            }
            break;

        default:
            break;
    }
    
    // Update the UI based on the current mode
    if (LETTER == m_spellerMode)
    {    
        // Update the entered keys
        cCurrentCharStr += cSelectedKeyStr;
        conversionStr = CT2A(cSelectedKeyStr);

        vector<string> conversionVectorStr;
        string transitionStr;

        if (   m_spellerCharacters.empty()
            || ( m_spellerCharacters.size() == m_spellerResult.size() ) )
        {
            // First letter ever entered or first letter of a new word entered
            conversionVectorStr.push_back(conversionStr);
            m_spellerCharacters.push_back(conversionVectorStr);
            transitionStr = CT2A(cCodedStr);
            m_spellerCodedKeys.push_back(transitionStr);

            // Set that the entered word is not a one letter one
            m_isWordOneLetter.push_back(false);
            m_indexOneLetter.push_back(0);
        }
        else
        {
            m_spellerCharacters.back().push_back(conversionStr);
            m_spellerCodedKeys.back() += CT2A(cCodedStr);
        }

        m_StaticSpellerCharacters.SetWindowTextW(cCurrentCharStr);

        // Update the prediction result
        CodeToWordPred(m_spellerCodedKeys.back());
    }
    else
    {
        cCurrentResultStr += cSelectedKeyStr;
        conversionStr = CT2A(cSelectedKeyStr);
        
        if (   m_spellerResult.empty() 
            || ( (m_spellerCharacters.size() > m_spellerResult.size()) ) )
        {
            m_spellerResult.push_back(conversionStr);
        }
        else
        {
            m_spellerResult.back() += conversionStr;
        }

        m_StaticSpellerResult.SetWindowTextW(cCurrentResultStr);
    }
}

/**
* @brief Update the entered keys and the prediction result based on the selected key
*/
void CGenevaEegBciDlg::LoadDictionary()
{
    const char *varName = "DC";
    const char *fieldName = "C";
    const char *fieldName2 = "D";

    MATFile *mfPtr;     /* MAT-file pointer */
    mxArray *varPtr;    /* mxArray pointer */
    mxArray *fdPtr;     /* field pointer for dictionary */
    mxArray *fcPtr;     /* field pointer for coded dictionary */
    mxArray *edPtr;     /* element pointer for dictionary */
    mxArray *ecPtr;     /* element pointer for coded dictionary */

    mwSize nElements;   /* number of elements in array */

    mwSize ncCells;     /* number of cells in coded dictionary array */
    mwSize ndCells;     /* number of cells in dictionary array */
    mwIndex cIdx;       /* cell index */

    // Get the dictionary based on current language
    string filename = "..\\GenevaEegBci\\res\\DC_" + m_language + ".mat";

    // Open .mat file
     mfPtr = matOpen(filename.c_str(), "r");
    if (mfPtr == NULL)
    {
        m_fileDlg << "Error opening file: " << filename.c_str() << "\n";

        // Open default dictionary
        filename = "..\\GenevaEegBci\\res\\DC.mat";

        mfPtr = matOpen(filename.c_str(), "r");
    }

    // Get DC variable
    varPtr = matGetVariable(mfPtr, varName);
    if (varPtr == NULL)
    {
        m_fileDlg << "mxArray not found: " << varName << "\n";
    }

    // Get coded dictionary strings
    nElements = (mwSize)mxGetNumberOfElements(varPtr);

    fcPtr = mxGetField(varPtr, 0, fieldName);

    ncCells = (mwSize)mxGetNumberOfElements(fcPtr);

    for (cIdx = 0; cIdx < ncCells; cIdx++)
    {
        ecPtr = mxGetCell(fcPtr, cIdx);
        m_codedDictionary.push_back(mxArrayToString(ecPtr));
        mxDestroyArray(ecPtr);
    }

    // Get dictionary strings
    fdPtr = mxGetField(varPtr, 0, fieldName2);

    ndCells = (mwSize)mxGetNumberOfElements(fdPtr);

    for (cIdx = 0; cIdx < ndCells; cIdx++)
    {
        edPtr = mxGetCell(fdPtr, cIdx);
        char* temp = mxArrayToUTF8String(edPtr);

        wstring u16_conv = wstring_convert<codecvt_utf8_utf16<wchar_t>, wchar_t>{}.from_bytes(temp);

        m_dictionary.push_back(u16_conv);
        mxDestroyArray(edPtr);
    }

    // Close the .mat file
    if (matClose(mfPtr) != 0)
    {
        m_fileDlg << "Error closing file: " << filename.c_str() << "\n";
    }
}

/**
* @brief Based on the code input, update the predicted words vector
*/
void CGenevaEegBciDlg::CodeToWordPred(const string code)
{
    int count = 0;
    vector<wstring> matching_string_vector;
    vector<wstring> starting_string_vector;
    
    //     'abcd' 'efgh' 'ijkl' 'mnop' 'qrstu' 'vwxyz'
    //        1      2      3      4      5       6
    //        A      B      C      D      E       F

    m_predictedWords.clear();

    for (auto it = m_codedDictionary.begin(); (it != m_codedDictionary.end()) && (count < predKeys - 1); it++)
    {
        // Search for strict match between code and coded dictionary
        if (code.compare(*it) == 0)
        {
            count++;
            matching_string_vector.push_back(*(m_dictionary.begin() + (it - m_codedDictionary.begin())));
        }
    }

    for (auto it = m_codedDictionary.begin(); (it != m_codedDictionary.end()) && (count < predKeys - 1); it++)
    {
        // Search for string starting by code
        bool start_with = startsWith(*it, code);
        if (start_with)
        {
            // Count only if starting but not matching
            if (find(matching_string_vector.begin(), matching_string_vector.end(), *(m_dictionary.begin() + (it - m_codedDictionary.begin()))) == matching_string_vector.end())
            {
                count++;
                starting_string_vector.push_back(*(m_dictionary.begin() + (it - m_codedDictionary.begin())));
            }
        }
    }

    // Concatenate both vectors to get perfect match first and then words starting by code
    m_predictedWords = matching_string_vector;
    m_predictedWords.insert(m_predictedWords.end(), starting_string_vector.begin(), starting_string_vector.end());

    // Always add 1 Letter mode as the last prediction
    CString tmp_str;
    tmp_str.LoadString(IDS_ONE_LETTER);
    wstring one_letter(tmp_str);
    m_predictedWords.push_back(one_letter);

    // Reset the Letter mode menu labels
    for (auto it = m_letterPredKeysValue.begin() + (NUM_SPELLER_KEY - 2); it != m_letterPredKeysValue.end(); it++)
    {
        *it = "";
    }


    // Reset the Pred1 mode labels
    for (auto it = m_pred1KeysValue.begin(); it != m_pred1KeysValue.end(); it++)
    {
        *it = "";
    }

    // Reset the Pred2 mode labels
    for (auto it = m_pred2KeysValue.begin(); it != m_pred2KeysValue.end(); it++)
    {
        *it = "";
    }

    // Fill the Letter and Pred1 / Pred2 mode menu labels based on the predicted words
    unsigned int counter = 0;
    for (auto i : m_predictedWords)
    {
        CString temp = i.c_str();

        // Fill Letter mode keys and menus
        m_letterPredKeysValue.at((NUM_SPELLER_KEY - 2) + counter) = temp;
        m_pred1KeysValue.at((NUM_SPELLER_KEY - 2) + counter) = temp;
        m_pred2KeysValue.at((NUM_SPELLER_KEY - 2) + counter) = temp;

        counter++;
    }

    // Fill the Pred1 mode keys labels based on the predicted words
    for (auto it = m_pred1KeysValue.begin(); it != m_pred1KeysValue.begin() + (NUM_SPELLER_KEY - 2); it++)
    {
        unsigned int index = it - m_pred1KeysValue.begin();

        if (index < m_predictedWords.size())
        {
            *it = m_predictedWords.at(index).c_str();
        }
        else
        {
            *it = L"";
        }
    }

    // Fill the Pred2 mode keys labels based on the predicted words
    for (auto it = m_pred2KeysValue.begin(); it != m_pred2KeysValue.begin() + (NUM_SPELLER_KEY - 2); it++)
    {
        unsigned int index = (predKeys / 2) + (it - m_pred2KeysValue.begin());

        if (index < m_predictedWords.size())
        {
            *it = m_predictedWords.at(index).c_str();
        }
        else
        {
            *it = L"";
        }
    }
}

/**
* @brief Check if the string 'mainStr' starts with given string 'toMatch'
*/
const bool CGenevaEegBciDlg::startsWith(const string mainStr, const string toMatch)
{
    // std::string::find returns 0 if toMatch is found at starting
    if (mainStr.find(toMatch) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* @brief Update UI based on decision made by the EegData class
*/
void CGenevaEegBciDlg::applyDecision(size_t decoderDecision)
{   
    switch (decoderDecision)
    {
        case SELECT:
            PostMessage(WM_SELECT, 0, 0);
            break;

        case CLOCKWISE:
            PostMessage(WM_CLOCKWISE, 0, 0);
            break;

        case ANTICLOCKWISE:
            PostMessage(WM_ANTICLOCKWISE, 0, 0);
            break;

        case NONE:
        default:
            break;
    }
}
