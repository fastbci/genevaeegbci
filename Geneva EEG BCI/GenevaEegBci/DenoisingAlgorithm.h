#ifndef DENOISINGALGORITHM_H
#define DENOISINGALGORITHM_H

#include <string>

class CDoubleMatrix;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Denoising of spectral amplitudes
* @author Tom
*/
class DLL_EXPORT CDenoisingAlgorithm
{
public:
    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the denoising algorithm
    */
    CDenoisingAlgorithm(std::string calibrationFileName);

    /**
    * Destructor.
    */
    ~CDenoisingAlgorithm();

    /**
    * Process function - used to denoise the real and imaginary spectrum.
    *
    * @param realSpectrumOutput		    This contains the real part of the denoised spectrum
    * @param imaginarySpectrumOutput	This contains the imaginary part of the denoised spectrum
    * @param realSpectrumImput		    The real part of the original spectrum
    * @param imaginarySpectrumImput		The imaginary part of the original spectrum
    */
    void process(CDoubleMatrix* const realSpectrumOutput, CDoubleMatrix* const imaginarySpectrumOutput, const CDoubleMatrix * const realSpectrumImput, const CDoubleMatrix * const imaginarySpectrumImput);

    /**
    * Function that returns the number of channels.
    */
    const size_t getNumberOfChannels();

    /**
    * Function that returns the number of spectral bins.
    */
    const size_t getNumberOfSpectralBins();

private:
    /// Median amplitude for each channel and bin
    CDoubleMatrix* m_medianAmplitudes;

    /// Transposed PSCA matrix
    CDoubleMatrix* m_pscaProjectionMatrixTranspose;

    /// Helper matrix that stores the results of calculations
    CDoubleMatrix* m_helperRho;

    /// Helper matrix that stores the results of calculations
    CDoubleMatrix* m_helperTheta;

    /// Helper matrix that stores the results of calculations
    CDoubleMatrix* m_helperCalc;

    /// Number of signal channels
    size_t m_numberOfChannels;

    /// Number of spectral bins
    size_t m_numberOfSpectralBins;
};

#endif // DENOISINGALGORITHM_H