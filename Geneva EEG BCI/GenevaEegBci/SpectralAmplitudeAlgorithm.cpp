#include "stdafx.h"
#include <stdexcept>
#include <assert.h>
#include "SpectralAmplitudeAlgorithm.h"

#ifndef FFTWTRANSFORM_H
#include "FftwTransform.h"
#endif

#ifndef FFTWREVERSETRANSFORM_H
#include "FftwReverseTransform.h"
#endif

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

using namespace std;

CSpectralAmplitudeAlgorithm::CSpectralAmplitudeAlgorithm(const WindowType& windowType
														, const size_t& fourierWindowLength
														, const size_t& stepSize
														, const size_t& numberOfChannels
														, const size_t& samplingFrequency
														, const CDoubleMatrix& normFactors)
: m_fourierWindowLength(fourierWindowLength)
, m_windowCoefficients(fourierWindowLength)
, m_stepSize(stepSize)
, m_buffer(numberOfChannels, fourierWindowLength, CDataBuffer::LAST_IN_AT_END)
, m_currentStep(0)
, m_numberOfBins(size_t(floor(fourierWindowLength / 2.0) + 1.0))
, m_fourierTransform(new CFftwTransform(fourierWindowLength))
, m_fourierInput(fourierWindowLength)
, m_fourierRealOutput(size_t(floor(fourierWindowLength / 2.0) + 1.0))
, m_fourierImagOutput(size_t(floor(fourierWindowLength / 2.0) + 1.0))
, m_samplingFrequency(samplingFrequency)
, m_normFactors(0)
, m_numberOfChannels(numberOfChannels)
{
	// Verify that the spectral normalization matrix has the correct size
	if (normFactors.getRowSize() != m_numberOfBins || normFactors.getColumnSize() != m_numberOfChannels)
	{
		throw runtime_error("CSpectralFilterAlgorithm: normFactors have wrong size.");
	}

	// Copy the normalization matrix
	const double factor = m_fourierWindowLength * 0.5;
	m_normFactors = new double*[numberOfChannels];
	for (size_t c = 0; c < m_numberOfChannels; ++c)
	{
		m_normFactors[c] = new double[m_numberOfBins];
		for (size_t bin = 0; bin < m_numberOfBins; ++bin)
		{
			m_normFactors[c][bin] = normFactors.at(bin, c) / factor;
		}
	}

	// Verify that the length of the Fourtier transform window is larger than 1
	if (m_fourierWindowLength < 1)
	{
		throw runtime_error("CSpectralFilterAlgorithm: Fourier window length must be > 1.");
	}
	assert(m_windowCoefficients.getSize() == m_fourierWindowLength);

	// Verify that the step size is larger than 0
	if (m_stepSize == 0)
	{
		throw runtime_error("Parameter stepSize was 0. (must be >0)");
	}

	// Verify other input parameters
	assert(m_currentStep == 0);
	assert(m_numberOfBins > 0);
	assert(m_fourierInput.getSize() == m_fourierWindowLength);
	assert(m_fourierRealOutput.getSize() == m_numberOfBins);
	assert(m_fourierImagOutput.getSize() == m_numberOfBins);

	// Calculate the anti-aliasing window
	double a0 = 0.0;
	double a1 = 0.0;
	double a2 = 0.0;

	switch (windowType)
	{
	case NONE:
		a0 = 1.0;
		a1 = 0.0;
		a2 = 0.0;
		break;
	case HAMMING:
		a0 = 0.54;
		a1 = -0.46;
		a2 = 0.0;
		break;
	case HANN:
		a0 = 0.5;
		a1 = -0.5;
		a2 = 0.0;
		break;
	case BLACKMAN:
		a0 = 0.42;
		a1 = -0.5;
		a2 = 0.08;
		break;
	default:
		throw runtime_error("CSpectralFilterAlgorithm: Unknown window type.");
	}

	const double myPi = CMathLibrary::pi();
	double coefSqureSum = 0;
	for (size_t index = 0; index < m_fourierWindowLength; ++index)
	{
		m_windowCoefficients[index] =
			a0
			+ a1 * cos(2.0 * myPi * index / (m_fourierWindowLength - 1))
			+ a2 * cos(4.0 * myPi * index / (m_fourierWindowLength - 1));
		coefSqureSum += m_windowCoefficients[index] * m_windowCoefficients[index];
	}

	for (size_t index = 0; index < m_fourierWindowLength; ++index)
	{
		m_windowCoefficients[index] /= sqrt(coefSqureSum);
	}
}


CSpectralAmplitudeAlgorithm::~CSpectralAmplitudeAlgorithm()
{
	if (m_fourierTransform != 0)
	{
		delete m_fourierTransform;
		m_fourierTransform = 0;
	}

	for (size_t c = 0; c < m_numberOfChannels; ++c)
	{
		delete[] m_normFactors[c];
		m_normFactors[c] = 0;
	}
	delete[] m_normFactors;
	m_normFactors = 0;
}

void CSpectralAmplitudeAlgorithm::process(std::vector<CDoubleMatrix*>& output, CDoubleVector* inputFrame)
{
	assert(inputFrame->getSize() == m_numberOfChannels);
	m_buffer.appendOneFrame(inputFrame->getValues()); // frame doesn't need to be deleted since buffer owns it now.
	++m_currentStep;

	if (m_currentStep == m_stepSize)
	{
		m_currentStep = 0;
		CDoubleMatrix* spectralAmplitudes = new CDoubleMatrix(m_numberOfChannels, m_numberOfBins);

		// prepare input to Fourier transformation
		const double* const coeffs = m_windowCoefficients.getValues();
		double* const fourierInputValues = m_fourierInput.getValues();
		for (size_t channel = 0; channel < m_numberOfChannels; ++channel)
		{
			const double* const channelData = m_buffer.getChannel(channel);
			for (size_t index = 0; index < m_fourierWindowLength; ++index)
			{
				fourierInputValues[m_fourierWindowLength - index - 1] = coeffs[index] * channelData[index];
			}

			// do Fourier transformation
			m_fourierTransform->doFourierTransform(m_fourierRealOutput, m_fourierImagOutput, m_fourierInput);

			double* const bins = spectralAmplitudes->getColumnAt(channel);
			const double* const realSpectrum = m_fourierRealOutput.getValues();
			const double* const imagSpectrum = m_fourierImagOutput.getValues();
			for (size_t binIndex = 0; binIndex < m_numberOfBins; ++binIndex)
			{
				bins[binIndex] += sqrt(realSpectrum[binIndex] * realSpectrum[binIndex]
					+ imagSpectrum[binIndex] * imagSpectrum[binIndex]) * m_normFactors[channel][binIndex];
			}
		}
		output.push_back(spectralAmplitudes);
	}
}

void CSpectralAmplitudeAlgorithm::reset(CDoubleVector* initBuffer)
{
	m_currentStep = 0;

	for (size_t sampleIndex = 0; sampleIndex < m_fourierWindowLength; ++sampleIndex)
	{
		m_buffer.appendOneFrame(initBuffer[sampleIndex].getValues()); // frame doesn't need to be deleted since buffer owns it now.
	}
}