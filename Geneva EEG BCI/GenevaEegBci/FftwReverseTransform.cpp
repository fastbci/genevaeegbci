#include "FftwReverseTransform.h"

#include <cmath>
#include <stdexcept>
#include <sstream>
#include <iostream>

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif


using namespace std;

CFftwReverseTransform::CFftwReverseTransform(const size_t& timeWindowSize)
: m_timeWindowSize(timeWindowSize)
, m_spectrumSize(size_t(floor(timeWindowSize / 2.0) + 1.0))
{
    if(m_timeWindowSize < 1)
    {
        throw runtime_error("CFftwReverseTransform: timeWindowSize must be > 1.");
    }

    // init fftw

    m_fftwOutput = new double[m_timeWindowSize];//(fftw_complex*) fftw_malloc(sizeof(fftw_complex) * m_timeWindowSize);       
    m_fftwInput = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * m_spectrumSize);       

    m_fftwplan = fftw_plan_dft_c2r_1d(int(m_timeWindowSize)
        , m_fftwInput 
        , m_fftwOutput        
        //, FFTW_BACKWARD // implicitly always backwards
        , FFTW_MEASURE);
}

CFftwReverseTransform::~CFftwReverseTransform()
{
    if(m_fftwplan != 0)
    {
        fftw_destroy_plan(m_fftwplan);
        m_fftwplan = 0;
    }

    if (m_fftwOutput != 0)
    {
        //fftw_free(m_fftwOutput);
        delete[] m_fftwOutput;
        m_fftwOutput = 0;        
    }

    if (m_fftwInput != 0)
    {
        fftw_free(m_fftwInput);
        m_fftwInput = 0;
    }
}

size_t CFftwReverseTransform::getTimeWindowSize() const
{
    return m_timeWindowSize;
}

size_t CFftwReverseTransform::getSpectrumSize() const
{
    return m_spectrumSize;
}

void CFftwReverseTransform::doInverseFourierTransform(CDoubleVector& realTimeSeries, const CDoubleVector& realSpectrum, const CDoubleVector& imaginarySpectrum)
{
    if(realTimeSeries.getSize() != m_timeWindowSize)
    {
        ostringstream message;
        message << "CFftwReverseTransform: can't do Fourier transform, parameter realTimeSeries has size ";
        message << realTimeSeries.getSize() << ", but expected " << m_timeWindowSize << ".";
        throw runtime_error(message.str());
    }
    if(imaginarySpectrum.getSize() != m_spectrumSize)
    {
        ostringstream message;
        message << "CFftwReverseTransform: can't do Fourier transform, parameter imaginarySpectrum has size ";
        message << imaginarySpectrum.getSize() << ", but expected " << m_spectrumSize << ".";
        throw runtime_error(message.str());
    }

    if(realSpectrum.getSize() != m_spectrumSize)
    {
        ostringstream message;
        message << "CFftwReverseTransform: can't do Fourier transform, parameter realSpectrum has size ";
        message << realSpectrum.getSize() << ", but expected " << m_spectrumSize << ".";
        throw runtime_error(message.str());
    }   

    double* const realValues = realSpectrum.getValues();
    double* const imagValues = imaginarySpectrum.getValues();

    //m_fftwInput[0][0] = realValues[0];
    //m_fftwInput[0][1] = imagValues[0];
    for(size_t index = 0; index < m_spectrumSize; ++index)
    {
        //cout << "index = " << index << " upper " << m_timeWindowSize - index << endl;
        //if(!(m_timeWindowSize - index < m_timeWindowSize))
        //{
        //    int x; x++;
        //}

        m_fftwInput[index][0] = realValues[index];
        m_fftwInput[index][1] = imagValues[index];

  /*      m_fftwInput[m_timeWindowSize - index][0] = realValues[index];
        m_fftwInput[m_timeWindowSize - index][1] = imagValues[index];*/
    }

    // do reverse transformation
    fftw_execute(m_fftwplan);

    // write result into realTimeSeries    
    //memcpy(realTimeSeries.getValues(), m_fftwOutput, m_timeWindowSize * sizeof(double));
    
    double* const timeValues = realTimeSeries.getValues();
    for(size_t index = 0; index < m_timeWindowSize; ++index)
    {
        timeValues[index] = m_fftwOutput[index] / m_timeWindowSize;
    }

}