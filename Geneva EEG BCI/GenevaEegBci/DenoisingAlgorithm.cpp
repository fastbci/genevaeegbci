#include "DenoisingAlgorithm.h"
#include <assert.h>

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

#ifndef MATLABLIBRARY_H
#include "../GenevaEegBci/MatlabLibrary.h"
#endif

using namespace std;


CDenoisingAlgorithm::CDenoisingAlgorithm(string calibrationFileName)
: m_medianAmplitudes(0)
, m_pscaProjectionMatrixTranspose(0)
, m_numberOfChannels(0)
, m_numberOfSpectralBins(0)
, m_helperRho(0)
, m_helperTheta(0)
, m_helperCalc(0)
{
    // Load numberOfSignals from .mat file
    mxArray* matNumberOfChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfChannels");
    assert(NULL != matNumberOfChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfChannels)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfChannels));
    m_numberOfChannels = static_cast<size_t>(dataElements[0]);


    // Load numberOfCorticalSources from .mat file
    mxArray* matNumberOfSpectralBins = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfSpectralBins");
    assert(NULL != matNumberOfSpectralBins);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfSpectralBins)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfSpectralBins)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfSpectralBins));
    m_numberOfSpectralBins = static_cast<size_t>(dataElements[0]);


    // Load the ??? matrix from .mat file
    mxArray* matMedianAmplitudes = CMatlabLibrary::loadMatVariable(calibrationFileName, "medianAmplitudes");
    assert(NULL != matMedianAmplitudes);

    // Verify that the size is ok
    assert(m_numberOfSpectralBins == mxGetM(matMedianAmplitudes));
    assert(m_numberOfChannels == mxGetN(matMedianAmplitudes));

    dataElements = static_cast<double*>(mxGetData(matMedianAmplitudes));
    m_medianAmplitudes = new CDoubleMatrix(m_numberOfSpectralBins, m_numberOfChannels, dataElements);


    // Load the ??? matrix from .mat file
    mxArray* matPscaProjectionMatrixTranspose = CMatlabLibrary::loadMatVariable(calibrationFileName, "pscaProjectionMatrixTranspose");
    assert(NULL != matPscaProjectionMatrixTranspose);

    // Verify that the size is ok
    assert(m_numberOfSpectralBins == mxGetM(matPscaProjectionMatrixTranspose));
    assert(m_numberOfSpectralBins == mxGetN(matPscaProjectionMatrixTranspose));

    dataElements = static_cast<double*>(mxGetData(matPscaProjectionMatrixTranspose));
    m_pscaProjectionMatrixTranspose = new CDoubleMatrix(m_numberOfSpectralBins, m_numberOfSpectralBins, dataElements);

    // Initilize the helper variables
    double initValue = 0;
    m_helperRho = new CDoubleMatrix(m_numberOfSpectralBins, m_numberOfChannels, initValue);
    m_helperTheta = new CDoubleMatrix(m_numberOfSpectralBins, m_numberOfChannels, initValue);
    m_helperCalc = new CDoubleMatrix(m_numberOfSpectralBins, m_numberOfChannels, initValue);
}


CDenoisingAlgorithm::~CDenoisingAlgorithm()
{
    if (m_medianAmplitudes != 0)
    {
        delete m_medianAmplitudes;
        m_medianAmplitudes = 0;
    }

    if (m_pscaProjectionMatrixTranspose != 0)
    {
        delete m_pscaProjectionMatrixTranspose;
        m_pscaProjectionMatrixTranspose = 0;
    }

    if (m_helperRho != 0)
    {
        delete m_helperRho;
        m_helperRho = 0;
    }

    if (m_helperTheta != 0)
    {
        delete m_helperTheta;
        m_helperTheta = 0;
    }

    if (m_helperCalc != 0)
    {
        delete m_helperCalc;
        m_helperCalc = 0;
    }
}

void CDenoisingAlgorithm::process(CDoubleMatrix* const realSpectrumOutput, CDoubleMatrix* const imaginarySpectrumOutput, const CDoubleMatrix * const realSpectrumImput, const CDoubleMatrix * const imaginarySpectrumImput)
{
    CMathLibrary::cartesianToPolar2D(m_helperTheta, m_helperRho, realSpectrumImput[0], imaginarySpectrumImput[0]);
    CMathLibrary::elementWiseDivideMatrixMatrix(m_helperCalc, m_helperRho[0], m_medianAmplitudes[0]);
    CMathLibrary::multMatrixMatrix(m_helperRho, m_pscaProjectionMatrixTranspose[0], m_helperCalc[0]);
    CMathLibrary::elementWiseMultMatrixMatrix(m_helperCalc, m_helperRho[0], m_medianAmplitudes[0]);
    CMathLibrary::polarToCartesian2D(realSpectrumOutput, imaginarySpectrumOutput, m_helperTheta[0], m_helperCalc[0]);
}

const size_t CDenoisingAlgorithm::getNumberOfChannels()
{
    return m_numberOfChannels;
}

const size_t CDenoisingAlgorithm::getNumberOfSpectralBins()
{
    return m_numberOfSpectralBins;
}