#include "SizeTVector.h"

#include <memory.h>
#include <assert.h>


CSizeTVector::CSizeTVector(const size_t& size)
	: m_size(size)
	, m_values(0)
{
	if (size == 0)
	{
		throw std::invalid_argument("argument size of CSizeTVector have to be not zero");
	}
	else
	{
		m_values = new size_t[size];
		memset(m_values, 0, m_size * sizeof(size_t));
	}
}

CSizeTVector::CSizeTVector(const size_t& size, const size_t& initValue)
	: m_size(size)
	, m_values(0)
{
	if (size == 0)
	{
		throw std::invalid_argument("argument size of CSizeTVector have to be not zero");
	}
	else
	{
		m_values = new size_t[size];
		for (size_t index = 0; index < m_size; ++index)
		{
			m_values[index] = initValue;
		}
	}
}

CSizeTVector::CSizeTVector(const size_t& size, const size_t* const values)
	: m_size(size)
	, m_values(0)
{
	if (size == 0)
	{
		throw std::invalid_argument("argument \"size\" of CSizeTVector have to be bigger than zero");
	}
	else
	{
		m_values = new size_t[size];
		memcpy(m_values, values, size * sizeof(size_t));
	}
}

CSizeTVector::CSizeTVector(const CSizeTVector& other)
	: m_size(other.m_size)
	, m_values(new size_t[m_size])
{
	// not nice but fast
	memcpy(m_values, other.getValues(), m_size * sizeof(size_t));
}

CSizeTVector::~CSizeTVector()
{
	if (m_values != 0)
	{
		delete[] m_values;
		m_values = 0;
	}
}

size_t CSizeTVector::getSize() const
{
	return m_size;
}

size_t* const CSizeTVector::getValues() const
{
	return m_values;
}

void CSizeTVector::mult(const size_t& factor)
{
	for (size_t row = 0; row < m_size; ++row)
	{
		m_values[row] *= factor;
	}
}

void CSizeTVector::add(const CSizeTVector& other)
{
	assert(m_size == other.getSize());

	const size_t* const otherValues = other.getValues();
	for (size_t row = 0; row < m_size; ++row)
	{
		m_values[row] += otherValues[row];
	}
}

void CSizeTVector::addWeighted(const size_t& factor, const CSizeTVector& other)
{
	assert(m_size == other.getSize());

	const size_t* const otherValues = other.getValues();
	for (size_t row = 0; row < m_size; ++row)
	{
		m_values[row] += factor * otherValues[row];
	}
}

void CSizeTVector::subtract(const CSizeTVector& other)
{
	assert(m_size == other.getSize());

	const size_t* const otherValues = other.getValues();
	for (size_t row = 0; row < m_size; ++row)
	{
		if (otherValues[row] > m_values[row])
		{
			m_values[row] = 0;
		}
		else
		{
			m_values[row] -= otherValues[row];
		}
	}
}

CSizeTVector& CSizeTVector::operator=(const CSizeTVector& other)
{
	assert(other.getSize() == m_size);
	// not nice but fast
	memcpy(m_values, other.getValues(), m_size * sizeof(size_t));
	return *this;
}

size_t& CSizeTVector::operator[](const size_t& position)
{
	assert(position < m_size);
	return m_values[position];
}

const size_t& CSizeTVector::operator[](const size_t& position) const
{
	assert(position < m_size);
	return m_values[position];
}

bool CSizeTVector::operator==(const CSizeTVector& other)
{
	if (other.m_size != m_size)
	{
		return false;
	}
	for (size_t index = 0; index < m_size; ++index)
	{
		if (other.m_values[index] != m_values[index])
		{
			return false;
		}
	}
	return true;
}

size_t CSizeTVector::getMax() const
{
	size_t maxValue = m_values[0];
	for (size_t index = 0; index < m_size; ++index)
	{
		if (m_values[index]>maxValue)
		{
			maxValue = m_values[index];
		}
	}

	return maxValue;
}

void CSizeTVector::findMinMax(size_t& minValue, size_t& maxValue)
{
	minValue = m_values[0];
	maxValue = m_values[0];
	for (size_t index = 0; index < m_size; ++index)
	{
		if (m_values[index]>maxValue)
		{
			maxValue = m_values[index];
		}
		else if (m_values[index]<minValue)
		{
			minValue = m_values[index];
		}
	}
}
