#include "SerialSGolayFilterAlgorithm.h"

#include <math.h>
#include <sstream>
#include <assert.h>
#include <iostream>
#include <stdexcept>
#include <assert.h>

#include "../GenevaEegBci/MatlabLibrary.h"

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif


using namespace std;

CSerialSGolayFilterAlgorithm::CSerialSGolayFilterAlgorithm(const size_t& leftWidth
                                                         , const size_t& rightWidth
                                                         , const size_t& polynomialOrder
                                                         , const size_t& windowSize
                                                         , const size_t& stepSize
                                                         , const size_t& numberOfChannels)
: m_polynomialOrder(polynomialOrder)
, m_windowSize(windowSize)
, m_stepSize(stepSize)
, m_currentStep(0)
, m_numberOfChannels(numberOfChannels)
, m_isBufferFilled(false)
, m_numberOfBufferedSamples(0)
{
    // Verify that the window size corresponds to the initialization inputs
    if (m_windowSize < (leftWidth + rightWidth + 1))
    {
        throw runtime_error("windowSize must be >= leftWidth + rightWidth + 1");
    }

    // Verify that the window size is odd
    if ((leftWidth + rightWidth) % 2 != 0)
    {
        throw runtime_error("leftWidth + rightWidth must be even");
    }

    m_filterCoefficients = new CDoubleVector(m_windowSize,getCoefficients(leftWidth, rightWidth, polynomialOrder).getValues());
    
    m_buffer = new CDataBuffer(numberOfChannels, windowSize, CDataBuffer::LAST_IN_AT_END);
}

CSerialSGolayFilterAlgorithm::CSerialSGolayFilterAlgorithm(std::string modelFileName)
: m_polynomialOrder(0)
, m_windowSize(0)
, m_stepSize(0)
, m_currentStep(0)
, m_numberOfChannels(0)
, m_isBufferFilled(false)
, m_numberOfBufferedSamples(0)
{
    // Load numberOfFeatures from .mat file
    mxArray* matLeftWidth = CMatlabLibrary::loadMatVariable(modelFileName, "leftWidth");
    assert(NULL != matLeftWidth);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetM(matLeftWidth)));
    assert(1 == static_cast<int>(mxGetN(matLeftWidth)));

    double* dataElements = static_cast<double*>(mxGetData(matLeftWidth));
    const size_t leftWidth = static_cast<size_t>(dataElements[0]);


    // Load recursiveLength from .mat file
    mxArray* matRightWidth = CMatlabLibrary::loadMatVariable(modelFileName, "rightWidth");
    assert(NULL != matRightWidth);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matRightWidth)));
    assert(1 == static_cast<int>(mxGetM(matRightWidth)));

    dataElements = static_cast<double*>(mxGetData(matRightWidth));
    const size_t rightWidth = static_cast<size_t>(dataElements[0]);


    // Load numberOfChannels from .mat file
    mxArray* matPolynomialOrder = CMatlabLibrary::loadMatVariable(modelFileName, "polynomialOrder");
    assert(NULL != matPolynomialOrder);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matPolynomialOrder)));
    assert(1 == static_cast<int>(mxGetM(matPolynomialOrder)));

    dataElements = static_cast<double*>(mxGetData(matPolynomialOrder));
    m_polynomialOrder = static_cast<size_t>(dataElements[0]);


    // Load numberOfChannels from .mat file
    mxArray* matStepSize = CMatlabLibrary::loadMatVariable(modelFileName, "stepSize");
    assert(NULL != matStepSize);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matStepSize)));
    assert(1 == static_cast<int>(mxGetM(matStepSize)));

    dataElements = static_cast<double*>(mxGetData(matStepSize));
    m_stepSize = static_cast<size_t>(dataElements[0]);


    // Load numberOfChannels from .mat file
    mxArray* matNumberOfChannels = CMatlabLibrary::loadMatVariable(modelFileName, "numberOfChannels");
    assert(NULL != matNumberOfChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfChannels)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfChannels));
    m_numberOfChannels = static_cast<size_t>(dataElements[0]);

    m_windowSize = leftWidth + rightWidth + 1;

    m_buffer = new CDataBuffer(m_numberOfChannels, m_windowSize, CDataBuffer::LAST_IN_AT_END);

    m_currentStep = 0;

    m_filterCoefficients = new CDoubleVector(m_windowSize, getCoefficients(leftWidth, rightWidth, m_polynomialOrder).getValues());
}

CSerialSGolayFilterAlgorithm::~CSerialSGolayFilterAlgorithm()
{
    if (m_filterCoefficients != 0)
    {
        delete m_filterCoefficients;
        m_filterCoefficients = 0;
    }

    if (m_buffer != 0)
    {
        delete m_buffer;
        m_buffer = 0;
    }
}

void CSerialSGolayFilterAlgorithm::process(std::vector<CDoubleVector*>& output, CDoubleVector* inputFrame)
{
    if (m_isBufferFilled)
    {
        assert(inputFrame->getSize() == m_numberOfChannels);
        m_buffer->appendOneFrame(inputFrame->getValues()); // frame doesn't need to be deleted since buffer owns it now.
        ++m_currentStep;
    }
    else
    {
        if (addToBuffer(inputFrame))
        {
            m_currentStep = m_stepSize;
        }
        else
        {
            return;
        }
    }

    double* const filtCoef = m_filterCoefficients->getValues();

    if (m_currentStep == m_stepSize)
    {
        m_currentStep = 0;

        CDoubleVector* filterResult = new CDoubleVector(m_numberOfChannels);
        double* const filtRez = filterResult->getValues();
        for (size_t channel = 0; channel < m_numberOfChannels; ++channel)
        {
            filtRez[channel] = 0;
            const double* const channelData = m_buffer->getChannel(channel);
            for (size_t timeInd = 0; timeInd < m_windowSize; ++timeInd)
            {
                filtRez[channel] += channelData[timeInd] * filtCoef[timeInd];
            }
        }

        output.push_back(filterResult);
    }
}

const bool CSerialSGolayFilterAlgorithm::addToBuffer(CDoubleVector* inputFrame)
{
    assert(inputFrame->getSize() == m_numberOfChannels);
    m_buffer->appendOneFrame(inputFrame->getValues()); // frame doesn't need to be deleted since buffer owns it now.
    ++m_numberOfBufferedSamples;

    if (m_numberOfBufferedSamples == m_windowSize)
    {
        m_isBufferFilled = true;
    }

    return m_isBufferFilled;
}

CDoubleVector CSerialSGolayFilterAlgorithm::getCoefficients(const size_t& leftWidth, const size_t& rightWidth, const size_t& polynomialOrder)
{
    const size_t frameSize = leftWidth + rightWidth + 1;
    if (!(frameSize % 2 == 1))
    {
        throw runtime_error("leftWidth + rightWidth + 1 must be odd!");
    }
    if (!(frameSize > polynomialOrder))
    {
        throw runtime_error("polynomial order must be smaller than leftWidth + rightWidth + 1!");
    }

    CDoubleMatrix vandermonde(frameSize, polynomialOrder + 1, 1.0);
    for (size_t col = 1; col < vandermonde.getColumnSize(); ++col)
    {
        double val = -double(leftWidth);
        for (size_t row = 0; row < vandermonde.getRowSize(); ++row, ++val)
        {
            vandermonde.at(row, col) = pow(val, double(col));
        }
    }

    // calculate inv(vand^T * vand) * vand^T
    CDoubleMatrix toInvert(polynomialOrder + 1, polynomialOrder + 1);
    CDoubleMatrix inverted(polynomialOrder + 1, polynomialOrder + 1);
    CDoubleMatrix coeffs(polynomialOrder + 1, frameSize);
    CDoubleMatrix vandermondeTransposed = vandermonde.getTransposed();
    CMathLibrary::multMatrixMatrix(&toInvert, vandermondeTransposed, vandermonde);
    CMathLibrary::invertMatrix(inverted, toInvert);
    CMathLibrary::multMatrixMatrix(&coeffs, inverted, vandermondeTransposed);
    CDoubleVector result(frameSize);
    for (size_t col = 0; col < coeffs.getColumnSize(); ++col)
    {
        result[col] = coeffs.at(0, col);
        //////////////////////////////////////////////
        // Why only use first raw ?  for smoothing only, don't need derivative
        //////////////////////////////////////////////////////
    }
    return result;
}

void CSerialSGolayFilterAlgorithm::reset(CDoubleVector** initBuffer)
{
    m_currentStep = 0;
    for (size_t sampleIndex = 0; sampleIndex < m_windowSize; ++sampleIndex)
    {
        m_buffer->appendOneFrame(initBuffer[sampleIndex]->getValues()); // frame doesn't need to be deleted since buffer owns it now.
    }

    m_isBufferFilled = true;
    m_numberOfBufferedSamples = m_windowSize;
}

void CSerialSGolayFilterAlgorithm::flushBuffer()
{
    m_buffer->flushBuffer();
    m_isBufferFilled = false;
    m_numberOfBufferedSamples = 0;
    m_currentStep = 0;
}
