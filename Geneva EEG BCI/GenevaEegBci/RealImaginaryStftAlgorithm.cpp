#include "stdafx.h"
#include <stdexcept>
#include <assert.h>
#include "RealImaginaryStftAlgorithm.h"

#ifndef MATLABLIBRARY_H
#include "../GenevaEegBci/MatlabLibrary.h"
#endif

#ifndef FFTWTRANSFORM_H
#include "FftwTransform.h"
#endif

#ifndef FFTWREVERSETRANSFORM_H
#include "FftwReverseTransform.h"
#endif

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

using namespace std;


CRealImaginaryStftAlgorithm::CRealImaginaryStftAlgorithm(const WindowType& windowType
													   , const size_t& fourierWindowLength
													   , const size_t& stepSize
													   , const size_t& numberOfChannels
													   , const size_t& samplingFrequency)
: m_fourierWindowLength(fourierWindowLength)
, m_windowCoefficients(new CDoubleVector(fourierWindowLength))
, m_stepSize(stepSize)
, m_buffer(new CDataBuffer(numberOfChannels, fourierWindowLength, CDataBuffer::LAST_IN_AT_END))
, m_currentStep(0)
, m_numberOfBins(size_t(floor(fourierWindowLength / 2.0) + 1.0))
, m_fourierTransform(new CFftwTransform(fourierWindowLength))
, m_fourierInput(new CDoubleVector(fourierWindowLength))
, m_fourierRealOutput(new CDoubleVector(size_t(floor(fourierWindowLength / 2.0) + 1.0)))
, m_fourierImagOutput(new CDoubleVector(size_t(floor(fourierWindowLength / 2.0) + 1.0)))
, m_samplingFrequency(samplingFrequency)
, m_numberOfChannels(numberOfChannels)
, m_numberOfBufferedSamples(0)
, m_isBufferFilled(false)
{
    initialize(windowType);
}



CRealImaginaryStftAlgorithm::CRealImaginaryStftAlgorithm(string calibrationFileName)
: m_fourierWindowLength(0)
, m_windowCoefficients(0)
, m_stepSize(0)
, m_buffer(0)
, m_currentStep(0)
, m_numberOfBins(0)
, m_fourierTransform(0)
, m_fourierInput(0)
, m_fourierRealOutput(0)
, m_fourierImagOutput(0)
, m_samplingFrequency(0)
, m_numberOfChannels(0)
, m_numberOfBufferedSamples(0)
, m_isBufferFilled(false)
{
    // Load windowType from .mat file
    mxArray* matWindowType = CMatlabLibrary::loadMatVariable(calibrationFileName, "windowType");
    assert(NULL != matWindowType);

    // Verify that the size is ok
    size_t numStrings = mxGetM(matWindowType);
    size_t numCharacters = mxGetN(matWindowType);

    char* charWindowType = new char[numCharacters + 1];
    mxGetString(matWindowType, charWindowType, numCharacters + 1);
    string stringWindowType(charWindowType, numCharacters);

    CRealImaginaryStftAlgorithm::WindowType windowType;

    if (stringWindowType == "hamming")
    {
        windowType = CRealImaginaryStftAlgorithm::HAMMING;
    }
    else if (stringWindowType == "blackman")
    {
        windowType = CRealImaginaryStftAlgorithm::BLACKMAN;
    }
    else if (stringWindowType == "hann")
    {
        windowType = CRealImaginaryStftAlgorithm::HANN;
    }
    else
    {
        windowType = CRealImaginaryStftAlgorithm::NONE;
    }

    // Load fourierWindowLength from .mat file
    mxArray *matFourierWindowLength = CMatlabLibrary::loadMatVariable(calibrationFileName, "fourierWindowLength");
    assert(NULL != matFourierWindowLength);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matFourierWindowLength)));
    assert(1 == static_cast<int>(mxGetM(matFourierWindowLength)));

    double* dataElements = static_cast<double*>(mxGetData(matFourierWindowLength));
    m_fourierWindowLength = static_cast<size_t>(dataElements[0]);


    // Load stepSize from .mat file
    mxArray *matStepSize = CMatlabLibrary::loadMatVariable(calibrationFileName, "stepSize");
    assert(NULL != matStepSize);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matStepSize)));
    assert(1 == static_cast<int>(mxGetM(matStepSize)));

    dataElements = static_cast<double*>(mxGetData(matStepSize));
    m_stepSize = static_cast<size_t>(dataElements[0]);


    // Load numberOfChannels from .mat file
    mxArray *matNumberOfChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfChannels");
    assert(NULL != matNumberOfChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfChannels)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfChannels));
    m_numberOfChannels = static_cast<size_t>(dataElements[0]);


    // Load samplingFrequency from .mat file
    mxArray *matSamplingFrequency = CMatlabLibrary::loadMatVariable(calibrationFileName, "samplingFrequency");
    assert(NULL != matSamplingFrequency);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matSamplingFrequency)));
    assert(1 == static_cast<int>(mxGetM(matSamplingFrequency)));

    dataElements = static_cast<double*>(mxGetData(matSamplingFrequency));
    m_samplingFrequency = static_cast<size_t>(dataElements[0]);

    m_windowCoefficients = new CDoubleVector(m_fourierWindowLength);
    m_buffer = new CDataBuffer(m_numberOfChannels, m_fourierWindowLength, CDataBuffer::LAST_IN_AT_END);
    m_fourierTransform = new CFftwTransform(m_fourierWindowLength);
    m_numberOfBins = size_t(floor(m_fourierWindowLength / 2.0) + 1.0);
    m_fourierInput = new CDoubleVector(m_fourierWindowLength);
    m_fourierRealOutput = new CDoubleVector(m_numberOfBins);
    m_fourierImagOutput = new CDoubleVector(m_numberOfBins);

    initialize(windowType);
}


CRealImaginaryStftAlgorithm::~CRealImaginaryStftAlgorithm()
{
    if (m_windowCoefficients != 0)
    {
        delete m_windowCoefficients;
        m_windowCoefficients = 0;
    }

    if (m_buffer != 0)
    {
        delete m_buffer;
        m_buffer = 0;
    }
    
    if (m_fourierTransform != 0)
	{
		delete m_fourierTransform;
		m_fourierTransform = 0;
	}

    if (m_fourierInput != 0)
    {
        delete m_fourierInput;
        m_fourierInput = 0;
    }

    if (m_fourierRealOutput != 0)
    {
        delete m_fourierRealOutput;
        m_fourierRealOutput = 0;
    }

    if (m_fourierImagOutput != 0)
    {
        delete m_fourierImagOutput;
        m_fourierImagOutput = 0;
    }
}

void CRealImaginaryStftAlgorithm::process(std::vector<CDoubleMatrix*>& outputReal
										, std::vector<CDoubleMatrix*>& outputImaginary
										, CDoubleVector* inputFrame)
{
    if (m_isBufferFilled)
    { 
	    assert(inputFrame->getSize() == m_numberOfChannels);
	    m_buffer->appendOneFrame(inputFrame->getValues()); // frame doesn't need to be deleted since buffer owns it now.
	    ++m_currentStep;
    }
    else
    {
        if (addToBuffer(inputFrame))
        { 
            m_currentStep = m_stepSize;
        }
        else
        {
            return;
        }
    }

	if (m_currentStep == m_stepSize)
	{
		m_currentStep = 0;
		CDoubleMatrix* realSpectralAmplitudes = new CDoubleMatrix(m_numberOfBins,m_numberOfChannels);
		CDoubleMatrix* imaginarySpectralAmplitudes = new CDoubleMatrix(m_numberOfBins,m_numberOfChannels);

		// prepare input to Fourier transformation
		const double* const coeffs = m_windowCoefficients->getValues();
		double* const fourierInputValues = m_fourierInput->getValues();
		for (size_t channel = 0; channel < m_numberOfChannels; ++channel)
		{
			const double* const channelData = m_buffer->getChannel(channel);
			for (size_t index = 0; index < m_fourierWindowLength; ++index)
			{
				fourierInputValues[index] = coeffs[index] * channelData[index];
			}

			// do Fourier transformation
			m_fourierTransform->doFourierTransform(m_fourierRealOutput[0], m_fourierImagOutput[0], m_fourierInput[0]);

			memcpy(realSpectralAmplitudes->getColumnAt(channel), m_fourierRealOutput->getValues(), m_numberOfBins * sizeof(double));
			memcpy(imaginarySpectralAmplitudes->getColumnAt(channel), m_fourierImagOutput->getValues(), m_numberOfBins * sizeof(double));
		}
		outputReal.push_back(realSpectralAmplitudes);
		outputImaginary.push_back(imaginarySpectralAmplitudes);
	}
}

const bool CRealImaginaryStftAlgorithm::addToBuffer(CDoubleVector* inputFrame)
{
    assert(inputFrame->getSize() == m_numberOfChannels);
    m_buffer->appendOneFrame(inputFrame->getValues()); // frame doesn't need to be deleted since buffer owns it now.
    ++m_numberOfBufferedSamples;

    if (m_numberOfBufferedSamples == m_fourierWindowLength)
    {
        m_isBufferFilled = true;
    }

    return m_isBufferFilled;
}

void CRealImaginaryStftAlgorithm::reset(CDoubleVector** initBuffer)
{
	m_currentStep = 0;

	for (size_t sampleIndex = 0; sampleIndex < m_fourierWindowLength; ++sampleIndex)
	{
		m_buffer->appendOneFrame(initBuffer[sampleIndex]->getValues()); // frame doesn't need to be deleted since buffer owns it now.
	}

    m_numberOfBufferedSamples = m_fourierWindowLength;
    m_isBufferFilled = true;
}

void CRealImaginaryStftAlgorithm::flushBuffer()
{
    m_buffer ->flushBuffer();
    m_isBufferFilled = false;
    m_numberOfBufferedSamples = 0;
    m_currentStep = 0;
}


size_t CRealImaginaryStftAlgorithm::getNumberOfBins()
{
	return m_numberOfBins;
}

void CRealImaginaryStftAlgorithm::initialize(CRealImaginaryStftAlgorithm::WindowType windowType)
{
    // Verify that the length of the Fourtier transform window is larger than 1
    if (m_fourierWindowLength < 1)
    {
        throw runtime_error("CSpectralFilterAlgorithm: Fourier window length must be > 1.");
    }
    assert(m_windowCoefficients->getSize() == m_fourierWindowLength);

    // Verify that the step size is larger than 0
    if (m_stepSize == 0)
    {
        throw runtime_error("Parameter stepSize was 0. (must be >0)");
    }

    // Verify other input parameters
    assert(m_currentStep == 0);
    assert(m_numberOfBins > 0);
    assert(m_fourierInput->getSize() == m_fourierWindowLength);
    assert(m_fourierRealOutput->getSize() == m_numberOfBins);
    assert(m_fourierImagOutput->getSize() == m_numberOfBins);

    // Calculate the anti-aliasing window
    double a0 = 0.0;
    double a1 = 0.0;
    double a2 = 0.0;

    switch (windowType)
    {
    case NONE:
        a0 = 1.0;
        a1 = 0.0;
        a2 = 0.0;
        break;
    case HAMMING:
        a0 = 0.54;
        a1 = -0.46;
        a2 = 0.0;
        break;
    case HANN:
        a0 = 0.5;
        a1 = -0.5;
        a2 = 0.0;
        break;
    case BLACKMAN:
        a0 = 0.42;
        a1 = -0.5;
        a2 = 0.08;
        break;
    default:
        throw runtime_error("CRealImaginaryStftAlgorithm: Unknown window type.");
    }

    const double myPi = CMathLibrary::pi();
    double coefSqureSum = 0;
    double* const coeffs = m_windowCoefficients->getValues();
    for (size_t index = 0; index < m_fourierWindowLength; ++index)
    {
        coeffs[index] = a0
                      + a1 * cos(2.0 * myPi * index / (m_fourierWindowLength - 1))
                      + a2 * cos(4.0 * myPi * index / (m_fourierWindowLength - 1));
        coefSqureSum += coeffs[index] * coeffs[index];
    }

    for (size_t index = 0; index < m_fourierWindowLength; ++index)
    {
        coeffs[index] /= sqrt(coefSqureSum);
    }
}