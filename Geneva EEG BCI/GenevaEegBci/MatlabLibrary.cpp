#include "stdafx.h"
#include "MatlabLibrary.h"

using namespace std;

/*= ==============================================================================================*/
mxArray* CMatlabLibrary::loadMatData(string fileName)
{
	string varName = "data";

	cout << "Loading file: " << fileName << "\n";
	MATFile *matFile = matOpen(fileName.c_str(), "r");
	if (matFile == NULL)
	{
		printf("ERROR: Error opening file %s\n", fileName.c_str());
		return NULL;
	}

	// Read data
	mxArray *matData = matGetVariable(matFile, varName.c_str());
	if (matData == NULL)
	{
		printf("ERROR: MATLAB array called '%s' not found in file %s\n", varName.c_str(), fileName.c_str());
	}

	// Close .mat file
	if (matClose(matFile) != 0)
	{
		printf("ERROR: Error closing file %s\n", fileName.c_str());

		if (matData != NULL)
		{
			mxDestroyArray(matData);
			matData = NULL;
		}
	}

	return matData;
}

/*===============================================================================================*/
mxArray* CMatlabLibrary::loadMatVariable(string fileName, string variableName)
{
	cout << "Loading file: " << fileName << "\n";
	MATFile *matFile = matOpen(fileName.c_str(), "r");
	if (matFile == NULL)
	{
		printf("ERROR: Error opening file %s\n", fileName.c_str());
		return NULL;
	}

	// Read data
	mxArray *matData = matGetVariable(matFile, variableName.c_str());
	if (matData == NULL)
	{
		printf("ERROR: MATLAB array called '%s' not found in file %s\n", variableName.c_str(), fileName.c_str());
	}

	// Close .mat file
	if (matClose(matFile) != 0)
	{
		printf("ERROR: Error closing file %s\n", fileName.c_str());

		if (matData != NULL)
		{
			mxDestroyArray(matData);
			matData = NULL;
		}
	}

	return matData;
}

/*===============================================================================================*/
void CMatlabLibrary::saveMatDoubleMatrixData(string fileName, string dataName, double* data, size_t numSamples, size_t numChannels)
{
	// Update the output .mat file. If the file does not exist, create it.
	MATFile *matOutputFile = matOpen(fileName.c_str(), "u");
	if (matOutputFile == NULL)
	{
		matOutputFile = matOpen(fileName.c_str(), "w");
		if (matOutputFile == NULL)
		{
			printf("ERROR: Error opening file %s\n", fileName.c_str());
			return;
		}
	}

	// Store double matrix data
	mxArray *matData = mxCreateDoubleMatrix(numSamples, numChannels, mxREAL);
	double* dataElements = static_cast<double*>(mxGetData(matData));
	memcpy(dataElements, data, numChannels * numSamples * sizeof(double));
	if (matPutVariable(matOutputFile, dataName.c_str(), matData) != 0)
	{
		printf("ERROR: Writing variable %s file %s\n", dataName.c_str(), fileName.c_str());
	}
	mxDestroyArray(matData);
	matData = NULL;
	dataElements = NULL;

	// Close file
	if (matClose(matOutputFile) != 0)
	{
		printf("ERROR: Error closing file %s\n", fileName.c_str());
		return;
	}

	return;
}


/*===============================================================================================*/
void CMatlabLibrary::saveMatDouble3dData(string fileName, string dataName, double* data, size_t dim1Len, size_t dim2Len, size_t dim3Len)
{
	// Update the output .mat file. If the file does not exist, create it.
	MATFile *matOutputFile = matOpen(fileName.c_str(), "u");
	if (matOutputFile == NULL)
	{
		matOutputFile = matOpen(fileName.c_str(), "w");
		if (matOutputFile == NULL)
		{
			printf("ERROR: Error opening file %s\n", fileName.c_str());
			return;
		}
	}

	// Store double 3d data
	size_t dimLen[3];
	dimLen[0] = dim1Len;
	dimLen[1] = dim2Len;
	dimLen[2] = dim3Len;

	mxArray *matData = mxCreateNumericArray(3, dimLen, mxDOUBLE_CLASS, mxREAL);
	double* dataElements = static_cast<double*>(mxGetData(matData));
	memcpy(dataElements, data, dim1Len * dim2Len * dim3Len * sizeof(double));
	if (matPutVariable(matOutputFile, dataName.c_str(), matData) != 0)
	{
		printf("ERROR: Writing variable %s file %s\n", dataName.c_str(), fileName.c_str());
	}
	mxDestroyArray(matData);
	matData = NULL;
	dataElements = NULL;

	// Close file
	if (matClose(matOutputFile) != 0)
	{
		printf("ERROR: Error closing file %s\n", fileName.c_str());
		return;
	}

	return;
}

/*===============================================================================================*/
void CMatlabLibrary::saveMatEegEogData(string fileName, double *eegBuffer, double *eogBuffer, size_t numSamples, size_t numEegChannels, size_t numEogChannels)
{
	// Create output .mat file
	MATFile *matOutputFile = matOpen(fileName.c_str(), "w");
	if (matOutputFile == NULL)
	{
		printf("ERROR: Error opening file %s\n", fileName.c_str());
		return;
	}

	// Store EEG data
	string varName = "eegData";
	mxArray *eegData = mxCreateDoubleMatrix(numSamples, numEegChannels, mxREAL);
	double* eegDataElements = static_cast<double*>(mxGetData(eegData));
	memcpy(eegDataElements, eegBuffer, numEegChannels * numSamples * sizeof(double));
	if (matPutVariable(matOutputFile, varName.c_str(), eegData) != 0)
	{
		printf("ERROR: Writing variable %s file %s\n", varName.c_str(), fileName.c_str());
	}
	mxDestroyArray(eegData);
	eegData = NULL;
	eegDataElements = NULL;

	// Store EOG data
	varName = "eogData";
	mxArray *eogData = mxCreateDoubleMatrix(numSamples, numEogChannels, mxREAL);
	double* eogDataElements = static_cast<double*>(mxGetData(eogData));
	memcpy(eogDataElements, eogBuffer, numEogChannels * numSamples * sizeof(double));
	if (matPutVariable(matOutputFile, varName.c_str(), eogData) != 0)
	{
		printf("ERROR: Writing variable %s file %s\n", varName.c_str(), fileName.c_str());
	}
	mxDestroyArray(eogData);
	eogData = NULL;
	eogDataElements = NULL;

	// Close file
	if (matClose(matOutputFile) != 0)
	{
		printf("ERROR: Error closing file %s\n", fileName.c_str());
		return;
	}

	return;
}