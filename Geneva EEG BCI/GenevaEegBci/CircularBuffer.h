#include <algorithm> // for std::min
#pragma once

template <class T>
class CircularBuffer
{
public:
	CircularBuffer(size_t signals, size_t dimension);
	~CircularBuffer();

	size_t size() { return size_; }
	size_t capacity() { return capacity_; }
	// Write data in (double) buffer
	void write(T *data, size_t bytes, size_t signal);
	void writeDouble(T *data, size_t bytes, size_t signal);
	// Read data and return the requested signal
	T** readDoublePointer();
	T* readPointer(size_t signal);
	T readValue(size_t signal, size_t value);

private:
	size_t capacity_;
	size_t signals_;
	size_t sizeT_;
	size_t shiftDouble_;
	size_t* end_index_;
	size_t* size_;
	// Data
	T** data_;
	T** dataDouble_;
	// Debug
	int counterTest;
};


template <class T>
CircularBuffer<T>::CircularBuffer(size_t signals, size_t dimension)
{
	capacity_ = sizeof(T)*dimension;
	shiftDouble_ = dimension;
	signals_ = signals;
	sizeT_ = sizeof(T);

	int dimensionDouble_ = 2*dimension;
	int capacityDouble_ = 2*capacity_;

	end_index_ = new size_t[signals];
	size_ = new size_t[signals];
	data_ = new T*[signals];
	dataDouble_ = new T*[signals];

	for (size_t i = 0; i<signals; i++)
	{
		data_[i] = new T[dimension];
		memset(data_[i], 0, capacity_);

		dataDouble_[i] = new T[dimensionDouble_];
		memset(dataDouble_[i], 0, capacityDouble_);

		end_index_[i] = 0;
		size_[i] = 0;
	}

	// Debug
	counterTest = 0;
}

template <class T>
CircularBuffer<T>::~CircularBuffer()
{
	for (size_t i = 0; i<signals_; i++)
	{
		delete[] data_[i];
		delete[] dataDouble_[i];
	}
	delete[] data_;
	delete[] dataDouble_;

	delete end_index_;
	delete size_;
}

// Write in buffer
template <class T>
void CircularBuffer<T>::write(T *data, size_t bytes, size_t signal)
{
	// Return if there are no data
	if (bytes != 0)
	{
		size_t capacity = capacity_;
		size_t bytes_to_write = min(bytes, capacity - size_[signal]);

		// Write in a single step
		if (bytes_to_write == bytes)
		{
			memcpy(data_[signal] + end_index_[signal], data, bytes_to_write);

			// Increase occupied size
			end_index_[signal] += bytes_to_write/sizeT_;
			size_[signal] += bytes_to_write;

			if (end_index_[signal] == capacity/sizeT_)
			{
				end_index_[signal] = 0;
				size_[signal] = 0;
			}
		}
		// Write in two steps
		else
		{
			size_t size_1 = bytes_to_write;
			memcpy(data_[signal] + end_index_[signal], data, size_1);
			size_t size_2 = bytes - size_1;
			memcpy(data_[signal], data + size_1/sizeT_, size_2);
			
			// Increase occupied size 
			end_index_[signal] = size_2/sizeT_;
			size_[signal] = size_2;
		}

		// Debug
		//if(signal == 0)
		//	counterTest++;
		//if(counterTest == 2)
		//	int breakPoint = 1;
	}
}

// Write in double buffer
template <class T>
void CircularBuffer<T>::writeDouble(T *data, size_t bytes, size_t signal)
{
	// Return if there are no data
	if (bytes != 0)
	{
		size_t capacity = capacity_;
		size_t bytes_to_write = min(bytes, capacity - size_[signal]);

		// Write in a single step
		if (bytes_to_write == bytes)
		{
			memcpy(dataDouble_[signal] + end_index_[signal], data, bytes_to_write);
			memcpy(dataDouble_[signal] + shiftDouble_ + end_index_[signal], data, bytes_to_write);

			// Increase occupied size
			end_index_[signal] += bytes_to_write/sizeT_;
			size_[signal] += bytes_to_write;

			if (end_index_[signal] == capacity/sizeT_)
			{
				end_index_[signal] = 0;
				size_[signal] = 0;
			}
		}
		// Write in two steps
		else
		{
			size_t size_1 = bytes_to_write;
			memcpy(dataDouble_[signal] + end_index_[signal], data, size_1);
			memcpy(dataDouble_[signal] + shiftDouble_ + end_index_[signal], data, size_1);
			size_t size_2 = bytes - size_1;
			memcpy(dataDouble_[signal], data + size_1/sizeT_, size_2);
			memcpy(dataDouble_[signal] + shiftDouble_ , data + size_1/sizeT_, size_2);
			
			// Increase occupied size 
			end_index_[signal] = size_2/sizeT_;
			size_[signal] = size_2;
		}

		// Debug
		//if(signal == 0)
		//	counterTest++;
		//if(counterTest == 2)
		//	int breakPoint = 1;
	}
}

template <class T>
T** CircularBuffer<T>::readDoublePointer()
{
	return data_;
}

template <class T>
T* CircularBuffer<T>::readPointer(size_t signal)
{
	return data_[signal];
}

template <class T>
T CircularBuffer<T>::readValue(size_t signal, size_t value)
{
	return data_[signal][value];
}