// GenevaEegBciDlg.h : header file
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"

// Standard libs
#include <stdio.h>
#include <queue>
#include <afxmt.h>
#include <array>
#include <unordered_map>
#include <string>
#include <fstream>

// Filtering
#include "DspFilters\Butterworth.h"
#include "DspFilters\Dsp.h"

#include "TransparentStatic.h"

#define _DEBUG_ 1
#define _TIME_DELAY_BEBUG_ 1

/// UI keys structure
typedef enum _spellerKey {
    A   = 0,
    B   = 1,
    C   = 2,
    SEL = 3,
    F   = 4,
    E   = 5,
    D   = 6,
    BCK = 7,
    NUM_SPELLER_KEY = 8
} spellerKey;

/// UI modes structure
typedef enum _spellerMode {
    LETTER = 0,
    DIGIT1 = 1,
    DIGIT2 = 2,
    CALC   = 3,
    PUNCT  = 4,
    MENU1  = 5,
    MENU2  = 6,
    PRED1  = 7,
    PRED2  = 8,
    PHRASES1 = 9,
    PHRASES2 = 10,
    ONE_LETTER = 11
} spellerMode;

/**
* @brief CGenevaEegBciDlg dialog class
* @author Simon
*/
class CGenevaEegBciDlg : public CDialog
{
// Construction
public:
    CGenevaEegBciDlg(CWnd* pParent = NULL);    ///< standard constructor
    ~CGenevaEegBciDlg();    ///< destructor

// Dialog Data
    enum { IDD = IDD_GENEVAEEGBCI_DIALOG };

    static UINT processThreadAcquisition(LPVOID parameter);
    static UINT calibThread(LPVOID parameter);
    static UINT restStateEogCalibThread(LPVOID parameter);
    
    void OnOK(void);    ///< overloaded OnOK to prevent 'return' from triggering OnOK (closing app)

    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedStartAcquisitionControl();
    afx_msg void OnSelect();
    afx_msg void OnClockwise();
    afx_msg void OnAnticlockwise();
    afx_msg void OnBnClickedCalib();
    afx_msg void OnAcquisitionReady();
    afx_msg void OnThreadComplete();
    afx_msg void OnBnClickedRestStateCalib();
    afx_msg void OnBnClickedEogCalib();

    void CancelLastAction();
    void SetTextToSpell();
    void ShowSelectedKey();

    // Variable for window display
    CFont font;
    CFont m_spellerLetterKeyboardFont;
    CFont m_spellerPhrasesKeyboardFont;
    CFont m_spellerMenuFont;
    CFont m_spellerResultFont;
    LOGFONT lf;
    LOGFONT lfLetterKeyboardFont;
    LOGFONT lfPhrasesKeyboardFont;
    LOGFONT lfMenuFont;
    LOGFONT lfResultFont;

    /// Bitmap handle
    HBITMAP hBmp;

    // Buttons
    CButton m_Close; //button to close
    CButton m_editStartAcquisitionControl;
    CButton m_editStartCalibration;
    CButton m_editStartRestStateCalib;
    CButton m_editStartEogCalib;

    CStatic m_StaticBmp;
    CTransparentStatic m_Static1; // A
    CTransparentStatic m_Static2; // B
    CTransparentStatic m_Static3; // C
    CTransparentStatic m_Static4; // F
    CTransparentStatic m_Static5; // E
    CTransparentStatic m_Static6; // D

    CTransparentStatic m_Static7;  // Menu1/Pred1
    CTransparentStatic m_Static8;  // Menu2/Pred2
    CTransparentStatic m_Static9;  // Menu1/Pred1
    CTransparentStatic m_Static10; // Menu2/Pred2
    CTransparentStatic m_Static11; // Menu1/Pred1
    CTransparentStatic m_Static12; // Menu2/Pred2
    CTransparentStatic m_Static13; // Menu1/Pred1
    CTransparentStatic m_Static14; // Menu2/Pred2
    CTransparentStatic m_Static15; // Menu1/Pred1
    CTransparentStatic m_Static16; // Menu2/Pred2
    CTransparentStatic m_Static17; // Menu1/Pred1
    CTransparentStatic m_Static18; // Menu2/Pred2

    CStatic m_StaticSpellerCharacters;
    CStatic m_StaticSpellerResult;
    CStatic m_StaticTextToSpell;

    std::vector<CTransparentStatic*> m_TransparentStaticArray;

    // Print in output
    static void Output(const WCHAR* szFormat, ...);

    // Reset UI and its state machine
    void ResetUi(bool keepTextToSpell = false, bool redraw = true, unsigned int bmpId = IDB_BITMAP2);

protected:
    // Generated message map functions
    virtual BOOL OnInitDialog();
    afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();

    virtual BOOL PreTranslateMessage(MSG* pMsg);
    virtual void DoDataExchange(CDataExchange* pDX);    ///< DDX/DDV support

    HICON m_hIcon;
    CToolTipCtrl* m_pToolTip;


private:
    const double standardDeviation(double*, int, double);
    const std::string getWellFormatedDate();
    void InitStrings();
    void RedrawBitmap(unsigned int bmpId);
    void RedrawSpellerLabels(bool redrawResult = true);
    void FillSpellerLabels();
    void FormatSpellerLabels();
    void Spell();
    void AdaptKeyboards();
        
    // Stimulation functions
    void acquisitionControl(LPVOID parameter);
    void calib();
    void restStateEogCalib();

    DECLARE_MESSAGE_MAP()
    
    // Threads
    CWinThread* m_acquisitionThread;
    CWinThread* m_calibThread;
    CWinThread* m_restStateEogCalibThread;

    // Threads handles
    HANDLE stopAcquisition;
    HANDLE acquisitionStopped;
    HANDLE stopCalib;
    HANDLE calibStopped;
    HANDLE stopRestStateEogCalib;
    HANDLE restStateEogCalibStopped;

    bool m_acquisitionControlFlag;
    bool m_calibControlFlag;
    bool m_restStateCalibControlFlag;
    bool m_eogCalibControlFlag;

    spellerKey m_spellerKey;
    spellerMode m_spellerMode;
    unsigned int m_bmpId;

    // Dictionary variables
    static const unsigned int predKeys = 12;
    std::vector<std::wstring> m_dictionary;
    std::vector<std::string> m_codedDictionary;
    std::vector<std::wstring> m_predictedWords;
    std::vector <unsigned int> m_indexOneLetter;

    std::vector<CString> m_label;
    std::vector<CString> m_codedValue = { L"A", L"B", L"C", L"F", L"E", L"D" };
    std::vector<CString> m_letterKeysValue;
    std::vector<CString> m_letterPredKeysValue;
    std::vector<CString> m_digit1KeysValue = { L"1", L"2", L"3", L"6", L"5", L"4", L"1", L"2", L"3", L"4", L"5", L"6", L"7", L"8", L"9", L"0", L"+-*/.", L"=" };
    std::vector<CString> m_digit2KeysValue = { L"7", L"8", L"9", L"=", L"+-*/.", L"0", L"1", L"2", L"3", L"4", L"5", L"6", L"7", L"8", L"9", L"0", L"+-*/.", L"=" };
    std::vector<CString> m_calcKeysValue = { L"*", L"-", L"+", L"/", L".", L"C", L"1", L"2", L"3", L"4", L"5", L"6", L"7", L"8", L"9", L"0", L"+-*/.", L"=" };
    std::vector<CString> m_punctKeysValue = { L".", L",", L"?", L"@", L"'", L"!" };
    std::vector<CString> m_menu1KeysValue;
    std::vector<CString> m_menu2KeysValue;
    std::vector<CString> m_pred1KeysValue;
    std::vector<CString> m_pred2KeysValue;
    std::vector<CString> m_phrases1KeysValue;
    std::vector<CString> m_phrases2KeysValue;
    
    std::vector<std::vector<CString>> m_oneLetterKeysValue = { { L"A", L"B", L"C", L"D"},
                                                               { L"E", L"F", L"G", L"H"},
                                                               { L"I", L"J", L"K", L"L" },
                                                               { L"" },
                                                               { L"V", L"W", L"X", L"Y", L"Z" },
                                                               { L"R", L"S", L"T", L"U" },
                                                               { L"M", L"N", L"O", L"P", L"Q" } };
    
    std::vector<spellerKey> m_keyboardKeys = { A, B, C, F, E, D };
    std::vector<spellerKey> m_menuKeys = { SEL, BCK };
    std::vector<int> m_keysVector;

    std::unordered_map<spellerKey, unsigned int> spellerKeyBmpMap = { {A, IDB_BITMAP2},{B, IDB_BITMAP3},{C, IDB_BITMAP4}, {F, IDB_BITMAP5}, {E, IDB_BITMAP6}, {D, IDB_BITMAP7}, {SEL, IDB_BITMAP_SELECT_PREDICT1}, {BCK, IDB_BITMAP_BACKSPACE} };
    std::unordered_map<unsigned int, unsigned int> spellerKeySelecteBmpMap = { {IDB_BITMAP2, IDB_BITMAP20},{IDB_BITMAP3, IDB_BITMAP30},{IDB_BITMAP4, IDB_BITMAP40}, {IDB_BITMAP5, IDB_BITMAP50}, {IDB_BITMAP6, IDB_BITMAP60}, {IDB_BITMAP7, IDB_BITMAP70}, {IDB_BITMAP_SELECT_PREDICT1, IDB_BITMAP_SELECT_PREDICT1_SEL}, {IDB_BITMAP_SELECT_PREDICT2, IDB_BITMAP_SELECT_PREDICT2_SEL}, {IDB_BITMAP_BACKSPACE, IDB_BITMAP_BACKSPACE_SEL} };
    
    std::unordered_map<char, spellerKey> stringSpellerKeyMap = { {'A', A},{'B', B},{'C', C}, {'F', F}, {'E', E}, {'D', D} };

    spellerKey m_keysToUse;

    std::vector<std::vector<std::string>> m_spellerCharacters;
    std::vector<std::string> m_spellerResult;
    std::vector <std::string> m_spellerCodedKeys;
    int m_keyIndex;             // needs to be an int
    std::vector<bool> m_isWordOneLetter;

    std::string m_language;

    // Dictionary methods
    void LoadDictionary();
    void CodeToWordPred(std::string code);
    const bool startsWith(std::string mainStr, std::string toMatch);

    // UI update
    void applyDecision(size_t decoderDecision);

    // The class is not copyable because of the output stream
    CGenevaEegBciDlg(const CGenevaEegBciDlg&);
    CGenevaEegBciDlg& operator=(const CGenevaEegBciDlg&);

    // Output stream for the log file
    std::ofstream m_fileDlg;
};
