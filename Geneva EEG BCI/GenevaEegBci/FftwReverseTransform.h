#ifndef FFTWREVERSETRANSFORM_H
#define FFTWREVERSETRANSFORM_H

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#include <fftw/fftw3.h>

/**
* @brief Reverse Fourier transformation implemented with Fastest Fourier Transfromation of the West library.
* @ingroup utils
*
* @author Joerg
*/
class CFftwReverseTransform
{
public:
    CFftwReverseTransform(const size_t& timeWindowSize);
    virtual ~CFftwReverseTransform();

public:
	/**
	PLEASE DOCUMENT
	*/
    size_t getTimeWindowSize() const;

	/**
	PLEASE DOCUMENT
	*/
    size_t getSpectrumSize() const;

	/**
	PLEASE DOCUMENT
	*/
    void doInverseFourierTransform(CDoubleVector& realTimeSeries
        , const CDoubleVector& realSpectrum
        , const CDoubleVector& imaginarySpectrum);

private:
    /// not implemented
    CFftwReverseTransform& operator=(const CFftwReverseTransform& other);

private:
    const size_t m_timeWindowSize;
    const size_t m_spectrumSize;
    /// FFTW plan used, describing the Fourier transformation done to calculate the amplitude spectra.
    fftw_plan m_fftwplan;

    /// contains the input for the fftw. Kept as member due to performance.
    fftw_complex* m_fftwInput;

    /// contains the output for the fftw. Kept as member due to performance.
    double* m_fftwOutput;
};
#endif // FFTWREVERSETRANSFORM_H
