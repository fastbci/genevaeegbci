#pragma once

#include "stdafx.h"
#include <windows.h>
#include <fstream>
#include <memory>
#include <algorithm>
#include <chrono>
#include "DoubleVector.h"
#include <mat.h>                    // MAT-File API Library

// EEGO FUNCTIONS
#include <eemagine/sdk/factory.h>   // SDK header

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/// Channel configuration structure
struct ChannelConfig
{
    std::vector<int> eegChannelsWithRef;
    std::vector<size_t> eegChannelsWithRefIndex;
    std::vector<int> eogBipHor;
    std::vector<int> eogBipVert;
    size_t refChannel;
    size_t refChannelIndex;
};

/// Threshold configuration structure
const struct ThresholdConfig
{
    double threshold;
	size_t refractoryPeriod;
};

/// Decoder decision structure
typedef enum _decision 
{
    NONE = 0,
    ANTICLOCKWISE = 1,
    CLOCKWISE = 2,
    SELECT = 3,
} decision;

/**
* @brief Class used for acquiring and storing EEG data from Eego Amplifiers
* @author Simon
*/
class DLL_EXPORT CEegData
{
public:
    CEegData(std::string calibrationFileName);
    ~CEegData();

    /** Accessors */
    const UINT64 GetSampleCount(void) const {return m_sampleCount;};
    void SetSampleCount(UINT64 sample_count) {m_sampleCount = sample_count;};

    const UINT16 GetSamplingRate(void) const {return m_samplingRate;};
    void SetSamplingRate(UINT16 sampling_rate) {m_samplingRate = sampling_rate;};

    const double GetRefRange(void) const {return m_refRange;};
    void SetRefRange(double ref_range) {m_refRange = ref_range;};

    const double GetBipolarRange(void) const {return m_bipolarRange;};
    void SetBipolarRange(double bipolar_range) {m_bipolarRange = bipolar_range;};

    const UINT16 GetDataWindowDuration(void) const {return m_dataWindowDuration;};
    void SetDataWindowDuration (UINT16 data_window_duration) {m_dataWindowDuration = data_window_duration;};

    const bool GetIsArtefactDetected(void) const {return m_isArtefactDetected;};
    void SetIsArtefactDetected(bool is_artefact_detected) {m_isArtefactDetected = is_artefact_detected;};

	const size_t GetArtefactRefractoryCounter(void) const { return m_artefactRefractoryCounter; };
	void SetArtefactRefractoryCounter(size_t artefact_refractory_counter) { m_artefactRefractoryCounter = artefact_refractory_counter; };

    const ChannelConfig GetChannelConfig(void) const { return m_channelConfig; };

    const ThresholdConfig GetThresholdConfig(void) const { return m_thresholdConfig; };

    const bool initAcquisition();
    void acquireAndStoreNewSamples(std::vector<CDoubleVector*>& output);

    void channelSelector(const double *dataBuffer, double *eegBuffer, double *eogBuffer);
    const bool artefactThreshold(const double* eegBuffer, const size_t numChannels);

    const size_t getReferenceChannelIndex(void) const {return m_channelConfig.refChannelIndex;};

    HANDLE clockwise;
    HANDLE anticlockwise;
    HANDLE select;

private:
    //  Eego amplifier
    eemagine::sdk::factory m_fact; /// Eego factory
    std::unique_ptr<eemagine::sdk::amplifier> m_amp; /// Eego amplifier
    std::unique_ptr<eemagine::sdk::stream> m_eegStream; /// Eego stream
    UINT64 m_sampleCount;
    UINT16 m_samplingRate;
    double m_refRange;
    double m_bipolarRange;
    UINT16 m_dataWindowDuration; 

    // Constant expressions
    static constexpr UINT16 DEFAULT_SAMPLING_RATE       = 1024;
    static constexpr double DEFAULT_REF_RANGE           = 1.0;
    static constexpr double DEFAULT_BIPOLAR_RANGE       = 4.0;
    static constexpr UINT16 DEFAULT_DATA_WINDOW_DURATION = 1000;

    ChannelConfig m_channelConfig;
    
    ThresholdConfig m_thresholdConfig;

    bool m_isArtefactDetected;
	size_t m_artefactRefractoryCounter;

    decision m_decision;
    
    // The class is not copyable because of the output stream
    CEegData(const CEegData&);
    CEegData& operator=(const CEegData&);

    std::ofstream m_fileEegData; /// Output stream for eeg data storage
};

