#ifndef MATHLIBRARY_H
#define MATHLIBRARY_H

class CDoubleMatrix;
class CDoubleVector;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief contains static mathematic functions 
* @ingroup utils 
* @author WHO
*
* This class contains static functions for matrix multiplications. It is a wrapper
* for the underlying BLAS and LAPACK functions.
*/
struct DLL_EXPORT CMathLibrary
{
private:
    /// not implemented
    CMathLibrary();

    /// destructor
    ~CMathLibrary();

public:
    /**
    * Given a vector V1 and a vector V2, this function computes r = V1 . V2.
    *
    * @return the result vector
    * @param vectorOne  the vector V1
    * @param vectorTwo  the vector V2
    */
    static double multVectorDotVector(const CDoubleVector& vectorOne, const CDoubleVector& vectorTwo);

    /**
    * Given a symmetric matrix M and a vector v, this function computes r = M . v.
    *
    * @param result the result vector r is written in here.
    * @param matrix the matrix M
    * @param vec the vector v
    */
    static void multSymmetricMatrixVector(CDoubleVector* const result, const CDoubleMatrix& matrix, const CDoubleVector& vec);

    /**
    * Given a lower triangular matrix M and a vector v, this function computes r = M . v.
    *
    * @param result the result vector r is written in here.
    * @param matrix the lower triangular matrix M
    * @param vec the vector v
    */
    static void multLowerTriangularMatrixVector(CDoubleVector* const result, const CDoubleMatrix& matrix, const CDoubleVector& vec);

    /**
    * Computes the Cholesky factorization of a symmetric positive definite matrix in place.
    *
    * @param matrix the symmetric positive definite matrix
    */
    static void choleskyFactorization(const CDoubleMatrix& matrix);

    /**
    * Given a matrix M  and a vector v, this function computes r = M . v.
    *
    * @param result the result vector r is written in here.
    * @param matrix the matrix M
    * @param vec the vector v
    */
    static void multMatrixVector(CDoubleVector* const result, const CDoubleMatrix& matrix, const CDoubleVector& vec);

    /**
    * I'll decument this later, I promise (<-- Tomislav ;)
    */
    static void multAddMatrixVector(CDoubleVector* const result, const CDoubleMatrix& matrix, const CDoubleVector& vec, const CDoubleVector& vecAdd);

    /**
    * Take two vectors that define a set of points in 2D in cartesian coordiantes x and y and tranfer them into polar coordinates theta and rho
    *
    * @param theta the resultiong vector theta is written in here.
    * @param rho the resultiong vector rho is written in here.
    * @param x the x-axis coordinates of points
    * @param y the y-axis coordinates of points
    */
    static void cartesianToPolar2D(CDoubleVector* const theta, CDoubleVector* const rho, const CDoubleVector& x, const CDoubleVector& y);

    /**
    * Take two matrices that define a matrix of points in 2D in cartesian coordiantes x and y and tranfer them into polar coordinates theta and rho
    *
    * @param theta the resultiong matrix theta is written in here.
    * @param rho the resultiong matrix rho is written in here.
    * @param x the x-axis coordinates of points
    * @param y the y-axis coordinates of points
    */
    static void cartesianToPolar2D(CDoubleMatrix* const theta, CDoubleMatrix* const rho, const CDoubleMatrix& x, const CDoubleMatrix& y);

    /**
    * Take two vectors that define a set of points in 2D in polar coordiantes theta and rho and tranfer them into Cartesian coordinates x and y 
    *
    * @param x the resulting x-axis coordinates are written here
    * @param y the resulting y-axis coordinates are written here
    * @param theta the angular polar coordinates of points.
    * @param rho the radial polar coordinates of points.
    */
    static void polarToCartesian2D(CDoubleVector* const x, CDoubleVector* const y, const CDoubleVector& theta, const CDoubleVector& rho);

    /**
    * Take two matrices that define a matrix of points in 2D in polar coordiantes theta and rho and tranfer them into Cartesian coordinates x and y
    *
    * @param x the resulting x-axis coordinates are written here
    * @param y the resulting y-axis coordinates are written here
    * @param theta the angular polar coordinates of points.
    * @param rho the radial polar coordinates of points.
    */
    static void polarToCartesian2D(CDoubleMatrix* const x, CDoubleMatrix* const y, const CDoubleMatrix& theta, const CDoubleMatrix& rho);

    /**
    * Given the vectors v_1, v_2 and a factor alpha
    * , this function computes M = alpha . (v_1 . v_2^T) + M.
    * The result also serves as an accumulator.
    *
    * @param result the resulting matrix of dimension dim(v_1) . dim(v_2) is written in here.
    * @param factor \alpha
    * @param vec1 v_1
    * @param vec2 v_2
    */
    static void multVectorVector(CDoubleMatrix* const result, const double& factor, const CDoubleVector& vec1, const CDoubleVector& vec2);

    /**
    * I'll document this later, I promise (<-- Tomislav ;)
    *
    * @param leftMatrix The left matrix used for multiplication.
    * @param rightMatrix The right matrix used for multiplication.
    * @param result The result is written here.
    */
    static void multMatrixMatrix(CDoubleMatrix* const result, const CDoubleMatrix& leftMatrix, const CDoubleMatrix& rightMatrix);


    /**
    * Element-wise matrix multiplication between two matrices of the same size.
    *
    * @param result The result is written here.
    * @param leftMatrix The left matrix used for multiplication.
    * @param rightMatrix The right matrix used for multiplication.
    */
    static void elementWiseMultMatrixMatrix(CDoubleMatrix* const result, const CDoubleMatrix& leftMatrix, const CDoubleMatrix& rightMatrix);


    /**
    * Element-wise matrix division between two matrices of the same size.
    *
    * @param result The result is written here.
    * @param leftMatrix The left matrix used for division.
    * @param rightMatrix The right matrix used for multiplication.
    */
    static void elementWiseDivideMatrixMatrix(CDoubleMatrix* const result, const CDoubleMatrix& leftMatrix, const CDoubleMatrix& rightMatrix);


    /**
    * Element-wise matrix absolute value from real and imaginary sub-matrices.
    *
    * @param result             The result is written here.
    * @param firstComponent     The matrix containing the first component values.
    * @param secondComponent    The matrix containing the second component values.
    */
    static void elementWiseAbsoluteValue(CDoubleMatrix* const result, const CDoubleMatrix& firstComponent, const CDoubleMatrix& secondComponent);


    /**
    * Mean of the matrix accross the first dimension.
    *
    * @param result             The result is written here.
    * @param inputMatrix        The matrix containing the values to be averaged.
    */
    static void matrixMeanAcrossFirstDimension(CDoubleVector* const result, const CDoubleMatrix& inputMatrix);


    /**
    * Calculates a mean value of an array of values. 
    *
    * @param  vec  Array of values from which the mean should be calculated.
    * @return mean of vec
    */
    static double mean(const CDoubleVector& vec);
    
    /**
    * Inverts a matrix: R = M^-1.
    *
    * @param inverted the inverted matrix.
    * @param toInvert the matrix to invert.
    */
    static void invertMatrix(CDoubleMatrix& inverted, const CDoubleMatrix& toInvert);

    /**
    * Inverts a lower diagonal matrix: R = M^-1.
    *
    * @param matrix the matrix M to invert.
    */
    static void invertLowerTriangularMatrixInPlace(CDoubleMatrix& matrix);

    /// @return value of pi: 3.14159265358979323846
    static double pi();

private:
    /// not implemented
    CMathLibrary& operator=(const CMathLibrary& other); 
};

#endif // MATHLIBRARY_H