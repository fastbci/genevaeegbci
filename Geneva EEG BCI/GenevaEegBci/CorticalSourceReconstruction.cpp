#include "CorticalSourceReconstruction.h"
#include <assert.h>

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

#ifndef MATLABLIBRARY_H
#include "../GenevaEegBci/MatlabLibrary.h"
#endif

using namespace std;

CCorticalSourceReconstruction::CCorticalSourceReconstruction(string calibrationFileName)
: m_projectionMatrix(0)
, m_numberOfSignals(0)
, m_numberOfCorticalSources(0)
{
    // Load numberOfSignals from .mat file
    mxArray* matNumberOfSignals = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfSignals");
    assert(NULL != matNumberOfSignals);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfSignals)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfSignals)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfSignals));
    m_numberOfSignals = static_cast<size_t>(dataElements[0]);


    // Load numberOfCorticalSources from .mat file
    mxArray* matNumberOfCorticalSources = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfCorticalSources");
    assert(NULL != matNumberOfCorticalSources);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfCorticalSources)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfCorticalSources)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfCorticalSources));
    m_numberOfCorticalSources = static_cast<size_t>(dataElements[0]);


    // Load the projection matrix from .mat file
    mxArray* matProjectionMatrix = CMatlabLibrary::loadMatVariable(calibrationFileName, "projectionMatrix");
    assert(NULL != matProjectionMatrix);

    // Verify that the size is ok
    assert(m_numberOfCorticalSources == mxGetM(matProjectionMatrix));
    assert(m_numberOfSignals == mxGetN(matProjectionMatrix));

    dataElements = static_cast<double*>(mxGetData(matProjectionMatrix));
    m_projectionMatrix = new CDoubleMatrix(m_numberOfCorticalSources, m_numberOfSignals, dataElements);
}


CCorticalSourceReconstruction::~CCorticalSourceReconstruction()
{
    delete m_projectionMatrix;
    m_projectionMatrix = 0;
}

void CCorticalSourceReconstruction::process(CDoubleVector* const corticalSourceActivity, const CDoubleVector * const signalVector)
{
    assert(signalVector->getSize() == m_numberOfSignals);
    assert(corticalSourceActivity->getSize() == m_numberOfCorticalSources);

    CMathLibrary::multMatrixVector(corticalSourceActivity, m_projectionMatrix[0], signalVector[0]);
}

size_t CCorticalSourceReconstruction::getNumberOfSignals()
{
    return m_numberOfSignals;
}

size_t CCorticalSourceReconstruction::getNumberOfCorticalSources()
{
    return m_numberOfCorticalSources;
}