#include "SpectralSourceReconstruction.h"
#include <assert.h>

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

#ifndef MATLABLIBRARY_H
#include "MatlabLibrary.h"
#endif

using namespace std;

CSpectralSourceReconstruction::CSpectralSourceReconstruction(string calibrationFileName)
: m_projectionMatrix(0)
, m_numberOfSpectralBins(0)
, m_numberOfChannels(0)
, m_numberOfCorticalSources(0)
{
    // Load numberOfSpectralBins from .mat file
    mxArray* matNumberOfSpectralBins = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfSpectralBins");
    assert(NULL != matNumberOfSpectralBins);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfSpectralBins)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfSpectralBins)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfSpectralBins));
    m_numberOfSpectralBins = static_cast<size_t>(dataElements[0]);


    // Load numberOfSignals from .mat file
    mxArray* matNumberOfChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfChannels");
    assert(NULL != matNumberOfChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfChannels)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfChannels));
    m_numberOfChannels = static_cast<size_t>(dataElements[0]);


    // Load numberOfCorticalSources from .mat file
    mxArray* matNumberOfCorticalSources = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfCorticalSources");
    assert(NULL != matNumberOfCorticalSources);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfCorticalSources)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfCorticalSources)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfCorticalSources));
    m_numberOfCorticalSources = static_cast<size_t>(dataElements[0]);


    // Load the projection matrix from .mat file
    mxArray* matProjectionMatrix = CMatlabLibrary::loadMatVariable(calibrationFileName, "projectionMatrix");
    assert(NULL != matProjectionMatrix);

    // Verify that the size is ok
    assert(m_numberOfChannels == mxGetM(matProjectionMatrix));
    assert(m_numberOfCorticalSources == mxGetN(matProjectionMatrix));

    dataElements = static_cast<double*>(mxGetData(matProjectionMatrix));
    m_projectionMatrix = new CDoubleMatrix(m_numberOfChannels, m_numberOfCorticalSources, dataElements);
}


CSpectralSourceReconstruction::~CSpectralSourceReconstruction()
{
    if (m_projectionMatrix == 0)
    {
        delete m_projectionMatrix;
        m_projectionMatrix = 0;
    }
}

void CSpectralSourceReconstruction::process(CDoubleMatrix* const spectralCorticalActivity, const CDoubleMatrix * const spectralNeuralActivity)
{
    assert(spectralNeuralActivity->getRowSize() == m_numberOfSpectralBins);
    assert(spectralNeuralActivity->getColumnSize() == m_numberOfChannels);
    assert(spectralCorticalActivity->getRowSize() == m_numberOfSpectralBins);
    assert(spectralCorticalActivity->getColumnSize() == m_numberOfCorticalSources);

    CMathLibrary::multMatrixMatrix(spectralCorticalActivity, spectralNeuralActivity[0], m_projectionMatrix[0]);
}

size_t CSpectralSourceReconstruction::getNumberOfSpectralBins()
{
    return m_numberOfSpectralBins;
}

size_t CSpectralSourceReconstruction::getNumberOfChannels()
{
    return m_numberOfChannels;
}

size_t CSpectralSourceReconstruction::getNumberOfCorticalSources()
{
    return m_numberOfCorticalSources;
}
