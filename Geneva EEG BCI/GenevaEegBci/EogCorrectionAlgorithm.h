#ifndef EOGCORRECTIONALGORITHM_H
#define EOGCORRECTIONALGORITHM_H

#include <string>

class CDoubleMatrix;
class CDoubleVector;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Correct the EEG signal by substracting the EOG correlates
* @author Tom
*
* Implementation detail: The module is used to process each incoming neural signal vector
* into a corrected vector.
*/
class DLL_EXPORT CEogCorrectionAlgorithm
{
public:
    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the EOG correction algorithm
    */
    CEogCorrectionAlgorithm(std::string calibrationFileName);

    /**
    * Destructor.
    */
    ~CEogCorrectionAlgorithm();

    /**
    * Process function - used to remove the EOG correlates from the EEG activity.
    *
    * @param correctedSignal		The corrected neural signal
    * @param neuralSignal		    The neural signals (e.g. EEG activity)
    * @param eogSignal		        The signals that dirrectly records the noise (e.g. EOG activity)
    */
    void process(CDoubleVector* const correctedSignal, const CDoubleVector * const neuralSignal, const CDoubleVector * const eogSignal);

    /**
    * Function that returns the number of neural channels.
    */
    size_t getNumberOfNeuralChannels();

    /**
    * Function that returns the number of eog channels.
    */
    size_t getNumberOfEogChannels();

private:
    /// Matrix used to infer the EOG correlates in the neural signals
    CDoubleMatrix* m_regressionMatrix;

    /// Vector used for the help with the calculation
    CDoubleVector* m_helperVector;

    /// Number of neural signals
    size_t m_numberOfNeuralChannels;

    /// Number of neural signals
    size_t m_numberOfEogChannels;
};

#endif // EOGCORRECTIONALGORITHM_H

