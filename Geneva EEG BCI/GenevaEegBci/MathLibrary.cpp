#include "MathLibrary.h"

#include <assert.h>
#include <memory.h>
#include <algorithm>

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#include "mkl.h"

CMathLibrary::~CMathLibrary()
{
}

double CMathLibrary::multVectorDotVector(const CDoubleVector& vectorOne, const CDoubleVector& vectorTwo)
{
    assert(vectorOne.getSize() > 0);
    assert(vectorOne.getSize() == vectorTwo.getSize());

    size_t increment = 1;
    size_t vectorLength = vectorOne.getSize();

    return cblas_ddot(vectorLength
                  , vectorOne.getValues(), increment
                  , vectorTwo.getValues(), increment);
}

void CMathLibrary::multMatrixVector(CDoubleVector* const result, const CDoubleMatrix& matrix, const CDoubleVector& vec)
{
    assert(result->getSize()      == matrix.getRowSize());
    assert(vec.getSize()          == matrix.getColumnSize());
    assert(matrix.getColumnSize() != 0);
    assert(matrix.getRowSize()    != 0);

    //This is not thread safe!!
    size_t incr    = 1;
    double one     = 1.0;
    double zero    = 0.0;
    size_t cols    = matrix.getColumnSize();
    size_t rows    = matrix.getRowSize();

    cblas_dgemv(CblasColMajor, CblasNoTrans
        , rows, cols
        , one, matrix.getValues(), rows
        , vec.getValues(), incr 
        , zero, result->getValues(), incr);
}

void CMathLibrary::multSymmetricMatrixVector(CDoubleVector* const result, const CDoubleMatrix& matrix, const CDoubleVector& vec)
{
    assert(result->getSize() == matrix.getRowSize());
    assert(vec.getSize() == matrix.getColumnSize());
    assert(matrix.getColumnSize() != 0);
    assert(matrix.getRowSize() != 0);

    const char uplo = 'L';
    const int rows = matrix.getRowSize();
    const int cols = matrix.getColumnSize();
    const double alpha = 1.0;
    const int lda = rows;
    const int incx = 1;
    const double beta = 0.0;
    const int incy = 1;

    dsymv(&uplo, &rows, &alpha, matrix.getValues(), &lda, vec.getValues(), &incx, &beta, result->getValues(), &incy);
}

void CMathLibrary::multLowerTriangularMatrixVector(CDoubleVector* const result, const CDoubleMatrix& matrix, const CDoubleVector& vec)
{
    assert(matrix.getRowSize() == matrix.getColumnSize());
    assert(result->getSize() == matrix.getRowSize());
    assert(vec.getSize() == matrix.getColumnSize());
    assert(matrix.getColumnSize() != 0);
    assert(matrix.getRowSize() != 0);

    const char uplo = 'L';
    const char trans = 'N';
    const char diag = 'N';
    const int rows = matrix.getRowSize();
    const int lda = rows;
    const int incx = 1;

    memcpy(result->getValues(), vec.getValues(), rows * sizeof(double));
    dtrmv(&uplo, &trans, &diag, &rows, matrix.getValues(), &lda, result->getValues(), &incx);
}

void CMathLibrary::choleskyFactorization(const CDoubleMatrix& matrix)
{
    assert(matrix.getColumnSize() != 0);
    assert(matrix.getRowSize() != 0);
    assert(matrix.getRowSize() == matrix.getColumnSize());

    const char uplo = 'L';
    const int rows = matrix.getRowSize();
    const double alpha = 1.0;
    const int lda = rows;
    int info = 0;

    dpotrf(&uplo, &rows, matrix.getValues(), &lda, &info);
}

void CMathLibrary::multAddMatrixVector(CDoubleVector* const result, const CDoubleMatrix& matrix, const CDoubleVector& vec, const CDoubleVector& vecAdd)
{
    assert(result->getSize()      == matrix.getRowSize());
    assert(vec.getSize()          == matrix.getColumnSize());
	assert(matrix.getRowSize()    == vecAdd.getSize());
    assert(matrix.getColumnSize() != 0);
    assert(matrix.getRowSize()    != 0);

    size_t incr    = 1;
    double one     = 1.0;
    size_t cols    = matrix.getColumnSize();
    size_t rows    = matrix.getRowSize();

	memcpy(result->getValues(),vecAdd.getValues(),rows*sizeof(double));

    cblas_dgemv(CblasColMajor, CblasNoTrans
        , rows, cols
        , one, matrix.getValues(), rows
        , vec.getValues(), incr 
        , one, result->getValues(), incr);
}


void CMathLibrary::multVectorVector(CDoubleMatrix* const result, const double& factor, const CDoubleVector& vec1, const CDoubleVector& vec2)
{
    // case vec1 != vec2
	size_t incr  = 1;
    size_t size1 = vec1.getSize();
    size_t size2 = vec2.getSize();
    cblas_dger(CblasColMajor, size1, size2, (double)factor
        , vec1.getValues(), incr
        , vec2.getValues(), incr
        , result->getValues(), size1);
}

void CMathLibrary::multMatrixMatrix(CDoubleMatrix* const result, const CDoubleMatrix& leftMatrix, const CDoubleMatrix& rightMatrix)
{
    assert(result->getRowSize()			== leftMatrix.getRowSize());
    assert(result->getColumnSize()		== rightMatrix.getColumnSize());
	assert(leftMatrix.getColumnSize()   == rightMatrix.getRowSize());
    assert(leftMatrix.getRowSize()		!= 0);
    assert(leftMatrix.getColumnSize()	!= 0);
	assert(rightMatrix.getColumnSize()	!= 0);

    double one     = 1.0;
    double zero    = 0.0;
    size_t colsL    = leftMatrix.getColumnSize();
    size_t rowsL    = leftMatrix.getRowSize();
	size_t colsR    = rightMatrix.getColumnSize();
	size_t rowsR    = rightMatrix.getRowSize();

    cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, rowsL, colsR, colsL
		, one, leftMatrix.getValues(), rowsL
		, rightMatrix.getValues(), rowsR
		, zero, result->getValues(), rowsL);
}

void CMathLibrary::elementWiseMultMatrixMatrix(CDoubleMatrix* const result, const CDoubleMatrix& leftMatrix, const CDoubleMatrix& rightMatrix)
{
    assert(leftMatrix.getRowSize() == rightMatrix.getRowSize());
    assert(leftMatrix.getColumnSize() == rightMatrix.getColumnSize());
    assert(leftMatrix.getRowSize() == result->getRowSize());
    assert(leftMatrix.getColumnSize() == result->getColumnSize());

    double* leftValues = leftMatrix.getValues();
    double* rightValues = rightMatrix.getValues();
    double* resultValues = result->getValues();
    size_t noOfPoints = leftMatrix.getRowSize() * leftMatrix.getColumnSize();
    for (size_t pointInd = 0; pointInd < noOfPoints; ++pointInd)
    {
        resultValues[pointInd] = leftValues[pointInd] * rightValues[pointInd];
    }
}

void CMathLibrary::elementWiseDivideMatrixMatrix(CDoubleMatrix* const result, const CDoubleMatrix& leftMatrix, const CDoubleMatrix& rightMatrix)
{
    assert(leftMatrix.getRowSize() == rightMatrix.getRowSize());
    assert(leftMatrix.getColumnSize() == rightMatrix.getColumnSize());
    assert(leftMatrix.getRowSize() == result->getRowSize());
    assert(leftMatrix.getColumnSize() == result->getColumnSize());

    double* leftValues = leftMatrix.getValues();
    double* rightValues = rightMatrix.getValues();
    double* resultValues = result->getValues();
    size_t noOfPoints = leftMatrix.getRowSize() * leftMatrix.getColumnSize();
    for (size_t pointInd = 0; pointInd < noOfPoints; ++pointInd)
    {
        resultValues[pointInd] = leftValues[pointInd] / rightValues[pointInd];
    }
}

double CMathLibrary::mean(const CDoubleVector& vec)
{
    double sum = 0.0;
    for(size_t index = 0; index < vec.getSize(); ++index)
    {
        sum += vec[index];
    }
    return sum / vec.getSize();
}
double CMathLibrary::pi()
{
    return 3.14159265358979323846;
}

void CMathLibrary::invertLowerTriangularMatrixInPlace(CDoubleMatrix& matrix)
{
    assert(matrix.getRowSize() == matrix.getColumnSize());

    const char uplo = 'L';
    const char diag = 'N';
    const int noOfRows = matrix.getRowSize();
    const int lda = noOfRows;
    int info = 0;

    dtrtri(&uplo, &diag, &noOfRows, matrix.getValues(), &lda, &info);
}

void CMathLibrary::invertMatrix(CDoubleMatrix& inverted, const CDoubleMatrix& toInvert)
{
    assert(inverted.getRowSize()    == toInvert.getRowSize());
    assert(inverted.getColumnSize() == toInvert.getColumnSize());
    assert(inverted.getRowSize()    == inverted.getColumnSize());

    const int noOfRows       = toInvert.getRowSize();
    const int noOfColumns    = toInvert.getColumnSize();
    const int lda            = noOfRows;
    const int sizeOfDiagonal = std::min(noOfRows, noOfColumns);
    const int lwork = 5 * sizeOfDiagonal;
    int info;
    int* ipiv = new int[sizeOfDiagonal];
    double* work = new double[lwork];

    memcpy(inverted.getValues(), toInvert.getValues(), noOfRows * noOfColumns * sizeof(double));

    dgetrf(&noOfRows, &noOfColumns, inverted.getValues(), &lda, ipiv, &info);
    dgetri(&sizeOfDiagonal, inverted.getValues(), &lda, ipiv, work, &lwork, &info);

    delete[] work;
    delete[] ipiv;
}

void CMathLibrary::cartesianToPolar2D(CDoubleVector* const theta, CDoubleVector* const rho, const CDoubleVector& x, const CDoubleVector& y)
{
    size_t noOfPoints = x.getSize();
    assert(y.getSize() == noOfPoints);
    assert(theta->getSize() == noOfPoints);
    assert(rho->getSize() == noOfPoints);

    double* xValues = x.getValues();
    double* yValues = y.getValues();
    double* thetaValues = theta->getValues();
    double* rhoValues = rho->getValues();
    for (size_t pointInd = 0; pointInd < noOfPoints; ++pointInd)
    {
        thetaValues[pointInd] = atan2(yValues[pointInd], xValues[pointInd]);
        rhoValues[pointInd] = sqrt(yValues[pointInd] * yValues[pointInd] + xValues[pointInd] * xValues[pointInd]);
    }
}

void CMathLibrary::cartesianToPolar2D(CDoubleMatrix* const theta, CDoubleMatrix* const rho, const CDoubleMatrix& x, const CDoubleMatrix& y)
{
    size_t noOfColumns = x.getColumnSize();
    assert(y.getColumnSize() == noOfColumns);
    assert(theta->getColumnSize() == noOfColumns);
    assert(rho->getColumnSize() == noOfColumns);

    size_t noOfRows = x.getRowSize();
    assert(y.getRowSize() == noOfRows);
    assert(theta->getRowSize() == noOfRows);
    assert(rho->getRowSize() == noOfRows);

    double* xValues = x.getValues();
    double* yValues = y.getValues();
    double* thetaValues = theta->getValues();
    double* rhoValues = rho->getValues();
    size_t noOfPoints = noOfRows * noOfColumns;
    for (size_t pointInd = 0; pointInd < noOfPoints; ++pointInd)
    {
        thetaValues[pointInd] = atan2(yValues[pointInd], xValues[pointInd]);
        rhoValues[pointInd] = sqrt(yValues[pointInd] * yValues[pointInd] + xValues[pointInd] * xValues[pointInd]);
    }
}

void CMathLibrary::polarToCartesian2D(CDoubleVector* const x, CDoubleVector* const y, const CDoubleVector& theta, const CDoubleVector& rho)
{
    size_t noOfPoints = theta.getSize();
    assert(rho.getSize() == noOfPoints);
    assert(x->getSize() == noOfPoints);
    assert(y->getSize() == noOfPoints);

    double* thetaValues = theta.getValues();
    double* rhoValues = rho.getValues();
    double* xValues = x->getValues();
    double* yValues = y->getValues();
    for (size_t pointInd = 0; pointInd < noOfPoints; ++pointInd)
    {
        xValues[pointInd] = rhoValues[pointInd] * cos(thetaValues[pointInd]);
        yValues[pointInd] = rhoValues[pointInd] * sin(thetaValues[pointInd]);
    }
}

void CMathLibrary::polarToCartesian2D(CDoubleMatrix* const x, CDoubleMatrix* const y, const CDoubleMatrix& theta, const CDoubleMatrix& rho)
{
    size_t noOfColumns = theta.getColumnSize();
    assert(rho.getColumnSize() == noOfColumns);
    assert(x->getColumnSize() == noOfColumns);
    assert(y->getColumnSize() == noOfColumns);

    size_t noOfRows = theta.getRowSize();
    assert(rho.getRowSize() == noOfRows);
    assert(x->getRowSize() == noOfRows);
    assert(y->getRowSize() == noOfRows);

    double* thetaValues = theta.getValues();
    double* rhoValues = rho.getValues();
    double* xValues = x->getValues();
    double* yValues = y->getValues();
    size_t noOfPoints = noOfRows * noOfColumns;
    for (size_t pointInd = 0; pointInd < noOfPoints; ++pointInd)
    {
        xValues[pointInd] = rhoValues[pointInd] * cos(thetaValues[pointInd]);
        yValues[pointInd] = rhoValues[pointInd] * sin(thetaValues[pointInd]);
    }

}

void CMathLibrary::elementWiseAbsoluteValue(CDoubleMatrix* const result, const CDoubleMatrix& firstComponent, const CDoubleMatrix& secondComponent)
{
    const size_t noOfRows = firstComponent.getRowSize();
    assert(secondComponent.getRowSize() == noOfRows);
    assert(result->getRowSize() == noOfRows);

    const size_t noOfColumns = firstComponent.getColumnSize();
    assert(secondComponent.getColumnSize() == noOfColumns);
    assert(result->getColumnSize() == noOfColumns);

    const size_t noOfPoints = noOfRows * noOfColumns;

    double* xValues = firstComponent.getValues();
    double* yValues = secondComponent.getValues();
    double* resultValues = result->getValues();
    for (size_t pointInd = 0; pointInd < noOfPoints; ++pointInd)
    {
        resultValues[pointInd] = sqrt(yValues[pointInd] * yValues[pointInd] + xValues[pointInd] * xValues[pointInd]);
    }
}

void CMathLibrary::matrixMeanAcrossFirstDimension(CDoubleVector* const result, const CDoubleMatrix& inputMatrix)
{
    const size_t noOfRows = inputMatrix.getRowSize();
    const size_t noOfColumns = inputMatrix.getColumnSize();
    assert(noOfColumns == result->getSize());

    double* const resultValues = result->getValues();
    memset(resultValues, 0, noOfColumns * sizeof(double));

    for (size_t columnInd = 0; columnInd < noOfColumns; ++columnInd)
    {
        double* const matValues = inputMatrix.getColumnAt(columnInd);
        for (size_t rowInd = 0; rowInd < noOfRows; ++rowInd)
        {
            resultValues[columnInd] += matValues[rowInd];
        }
        resultValues[columnInd] /= noOfRows;
    }
}
