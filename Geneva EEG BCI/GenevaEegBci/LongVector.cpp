#include "LongVector.h"

#include <memory.h>
#include <assert.h>
#include <stdexcept>


CLongVector::CLongVector(const size_t& size)
: m_size(size)
, m_values(0)
{
    if (size == 0)
    {
        throw std::invalid_argument("argument size of CLongVector have to be not zero");
    }
    else
    {
        m_values = new long[size];
        memset(m_values, 0, m_size * sizeof(long));
    }
}

CLongVector::CLongVector(const size_t& size, const long& initValue)
: m_size(size)
, m_values(0)
{
    if (size == 0)
    {
        throw std::invalid_argument("argument size of CLongVector have to be not zero");
    }
    else
    {
        m_values = new long[size];
        for(size_t index = 0; index < m_size; ++index)
        {
            m_values[index] = initValue;
        }		
    }    
}

CLongVector::CLongVector(const size_t& size, const long* const values)
: m_size(size)
, m_values(0)
{
    if (size == 0)
    {
        throw std::invalid_argument("argument \"size\" of CLongVector have to be bigger than zero");
    }
    else
    {
        m_values = new long[size];
        memcpy(m_values, values, size * sizeof(long));
    }    
}

CLongVector::CLongVector(const CLongVector& other)
: m_size(other.m_size)
, m_values(new long[m_size])
{
    // not nice but fast
    memcpy(m_values, other.getValues(), m_size * sizeof(long));
}

CLongVector::~CLongVector()
{
    if (m_values!=0)
    {
        delete [] m_values;
        m_values = 0;
    }
}

size_t CLongVector::getSize() const
{
    return m_size;
}

long* const CLongVector::getValues() const
{
    return m_values;
}

void CLongVector::mult(const long& factor)
{
    for(size_t row = 0; row < m_size; ++row)
    {
        m_values[row] *= factor;
    }
}

void CLongVector::add(const CLongVector& other)
{
    assert(m_size == other.getSize());

    const long* const otherValues = other.getValues();
    for(size_t row = 0; row < m_size; ++row)
    {
        m_values[row] += otherValues[row];
    }
}

void CLongVector::addWeighted(const long& factor, const CLongVector& other)
{
    assert(m_size == other.getSize());

    const long* const otherValues = other.getValues();
    for(size_t row = 0; row < m_size; ++row)
    {
        m_values[row] += factor * otherValues[row];
    }
}

void CLongVector::subtract(const CLongVector& other)
{
    assert(m_size == other.getSize());

    const long* const otherValues = other.getValues();
    for(size_t row = 0; row < m_size; ++row)
    {
        m_values[row] -= otherValues[row];
    }
}

CLongVector& CLongVector::operator=(const CLongVector& other)
{
    assert(other.getSize() == m_size);
    // not nice but fast
    memcpy(m_values, other.getValues(), m_size * sizeof(long));
    return *this;
}

long& CLongVector::operator[](const size_t& position)
{
    assert(position < m_size);
    return m_values[position];
}

const long& CLongVector::operator[](const size_t& position) const
{
    assert(position < m_size);
    return m_values[position];
}

bool CLongVector::operator==( const CLongVector& other )
{
    if(other.m_size != m_size)
    {
        return false;
    }
    for(size_t index = 0; index < m_size ; ++index)
    {
        if(other.m_values[index] != m_values[index])
        {
            return false;
        }
    }
    return true;
}