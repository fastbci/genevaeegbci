#include "stdafx.h"
#include "Blackrock.h"
#include <string>
#include <assert.h>


CBlackrock::CBlackrock(void)
: m_blkrckInstance(0)
, m_emgBufferSize(20)
, m_numEmgChannels(cbNUM_ANAIN_CHANS)
, m_emgSampleRate(2000)
, m_spikeBufferSize(100)
, m_neuroBufferSize(600)
, m_neuronalSampleRate(30000)
, m_numNeuroChannels(96)
{
	listAnalogChannels.clear();
}


CBlackrock::~CBlackrock(void)
{
}

void CBlackrock::setup_connection_blackrock()
{
	cbSdkResult resTest2;
	resTest2 = cbSdkClose(m_blkrckInstance); //ADDED FOR DEBUGGING

	// GET VERSION
	cbSdkVersion ver;
	cbSdkResult res = cbSdkGetVersion(m_blkrckInstance, &ver);

	// OPEN CONNECTION
	cbSdkConnection con = cbSdkConnection();
	cbSdkConnectionType conType = CBSDKCONNECTION_DEFAULT;
	// cbSdkInstrumentType instType;
	resTest2 = cbSdkOpen(m_blkrckInstance, conType, con);

	cbSdkInstrumentType instType = CBSDKINSTRUMENT_COUNT;
	resTest2 = cbSdkGetType(m_blkrckInstance, &conType, &instType);

	// CHECK and STORE NAMES OF CHANNEL BEING READ
	char label[cbNUM_ANALOG_CHANS][17];
	UINT16 channelSDK = 0;
	for (UINT16 ch=0; ch<cbNUM_ANALOG_CHANS; ch++)
	{
		channelSDK = ch + 1;
		resTest2 = cbSdkGetChannelLabel(m_blkrckInstance, channelSDK, NULL, m_label[ch]); // Get channel label
		// Fill in analog channels vector
		if(ch>=cbNUM_FE_CHANS)
		{
			listAnalogChannels.push_back(m_label[ch]);
		}
	}
}

void CBlackrock::flushBuffer(void)
{
	cbSdkResult resTest;
	bool bFlushBuffer = true;
	resTest = cbSdkGetTrialData(m_blkrckInstance, bFlushBuffer, NULL, NULL, NULL, NULL);
	if (resTest != CBSDKRESULT_SUCCESS )
	{
		throw std::runtime_error("CBlackrock::flushBuffer"); 
	}
}

int CBlackrock::connection_init_EMG_secondVersion() // BLACKROCK
{
	cbSdkResult resTest2;

	// SET CONFIG SETUP 
	UINT16 uBegChan   = 1;
	UINT32 uBegMask   = 0;
	UINT32 uBegVal    = 0;
	UINT16 uEndChan   = 10;
	UINT32 uEndMask   = 0;
	UINT32 uEndVal    = 0;
	bool   bDouble    = false;
	bool   bAbsolute  = false;
	UINT32 uWaveforms = 0;
	UINT32 uConts     = cbSdk_CONTINUOUS_DATA_SAMPLES;
	UINT32 uEvents    = 0;
	UINT32 uComments  = 0;
	UINT32 uTrackings = 0;
	UINT32 bWithinTrial = false;

	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance
								 , &bWithinTrial
								 , &uBegChan
								 , &uBegMask
								 , &uBegVal
								 , &uEndChan
								 , &uEndMask
								 , &uEndVal
								 , &bDouble
								 , &uWaveforms
								 , &uConts
								 , &uEvents
								 , &uComments
								 , &uTrackings);


	uEvents = 0;
	UINT32 bActive = 1; // 0 leave buffer intact, 1 clear the buffer
	uBegChan = 0;
	uEndChan = 0;
	uBegVal = 0;
	uBegMask = 0; 
	uEndMask = 0;
	uEndVal = 0;
	resTest2 = cbSdkSetTrialConfig(m_blkrckInstance
								 , bActive
								 , uBegChan
								 , uBegMask
								 , uBegVal
								 , uEndChan
								 , uEndMask
								 , uEndVal
								 , bDouble
								 , uWaveforms
								 , uConts
								 , uEvents
								 , uComments
								 , uTrackings
								 , bAbsolute);
	if( resTest2 != CBSDKRESULT_SUCCESS )
	{
		bool debug2 = true;
	}

	// SEND COMMENT
	UINT8 charset = 1;
	std::string commentMessage = "EMG init";
	for (int i=0; i< 1; i++)
	{
		resTest2 = cbSdkSetComment( m_blkrckInstance, 50000, charset, commentMessage.c_str());
	}

	//DEACTIVATING THE NEURAL DATA - first 128 channels
	UINT16 nchan = 128;
	UINT32 bActive2 = 0; //activate channel 1, deactivating 0
	for (UINT16 ch = 1; ch <= nchan; ch++)  //i must start from 1 because dchannel =0, bactive= 0 means deactivate everything
	{
		resTest2 = cbSdkSetChannelMask( m_blkrckInstance, ch, bActive2 );
	}

	return 0;
}

int CBlackrock::connection_init_EMG(void)
{
	cbSdkResult resTest2;

	// Get configuration
	UINT16 uBegChan   = 0;
	UINT32 uBegMask   = 0;
	UINT32 uBegVal    = 0;
	UINT16 uEndChan   = 0;
	UINT32 uEndMask   = 0;
	UINT32 uEndVal    = 0;
	bool   bDouble    = false;
	bool   bAbsolute  = false;
	UINT32 uWaveforms = 0;
	UINT32 uConts     = 0;
	UINT32 uEvents    = 0;
	UINT32 uComments  = 0;
	UINT32 uTrackings = 0;
	UINT32 bWithinTrial = false;

	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance
									, &bWithinTrial
									, &uBegChan
									, &uBegMask
									, &uBegVal
									, &uEndChan
									, &uEndMask
									, &uEndVal
									, &bDouble
									, &uWaveforms
									, &uConts
									, &uEvents
									, &uComments
									, &uTrackings);

	// Set configuration
	uConts = cbSdk_CONTINUOUS_DATA_SAMPLES;
	uEvents = 0;
	UINT32 bActive = 1; // 0 leave buffer intact, 1 clear the buffer
	uBegVal = 0;
	uEndVal = 0;
	uBegMask = 0;
	uEndMask = 0;

	uBegChan   = 1;
	uEndChan   = cbNUM_ANALOG_CHANS;

	resTest2 = cbSdkSetTrialConfig(m_blkrckInstance
									, bActive
									, uBegChan
									, uBegMask
									, uBegVal
									, uEndChan
									, uEndMask
									, uEndVal
									, bDouble
									, uWaveforms
									, uConts
									, uEvents
									, uComments
									, uTrackings
									, bAbsolute); // Configure a data collection trial

	if( resTest2 != CBSDKRESULT_SUCCESS )
	{
		bool debug2 = true;
	}

	// Send comment of EMG Start
	UINT8 charset = 1;
	std::string commentMessage = "EMG START";
	resTest2 = cbSdkSetComment( m_blkrckInstance, 50000, charset, commentMessage.c_str());

	// Activate all the channels
	for (UINT16 ch = 1; ch <= cbNUM_ANALOG_CHANS; ch++)
	{
		resTest2 = cbSdkSetChannelMask(m_blkrckInstance, ch, bActive);
	}
	// Deactivate first 96 channels (Spikes)
	UINT32 bDeactive = 0;
	for (UINT16 ch = 1; ch <= m_numNeuroChannels; ch++)
	{
		resTest2 = cbSdkSetChannelMask(m_blkrckInstance, ch, bDeactive);
	}
	// Get number of enabled analog channels
	do{
		resTest2 = cbSdkInitTrialData(m_blkrckInstance, bActive, NULL, &m_trialCont, NULL, NULL);
	}while(m_trialCont.count == 0);

	return (int)m_trialCont.count;
}

int CBlackrock::connection_init_neuro(void)
{
	cbSdkResult resTest2;

	// SET CONFIG SETUP 
	UINT16 uBegChan   = 1;
	UINT32 uBegMask   = 0;
	UINT32 uBegVal    = 0;
	UINT16 uEndChan   = 128;
	UINT32 uEndMask   = 0;
	UINT32 uEndVal    = 0;
	bool   bDouble    = false;
	bool   bAbsolute  = false;
	UINT32 uWaveforms = 0;
	UINT32 uConts     = 0;
	UINT32 uEvents    = 0;
	UINT32 uComments  = 0;
	UINT32 uTrackings = 0;
	UINT32 bWithinTrial = false;

	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance
		, &bWithinTrial
		, &uBegChan
		, &uBegMask
		, &uBegVal
		, &uEndChan
		, &uEndMask
		, &uEndVal
		, &bDouble
		, &uWaveforms
		, &uConts
		, &uEvents
		, &uComments
		, &uTrackings);


	// SEND COMMENT
	UINT8 charset = 1;
	std::string commentMessage = "Neuro init";
	resTest2 = cbSdkSetComment(m_blkrckInstance, 50000, charset, commentMessage.c_str());

	//DEACTIVATING ALL THE CHANNELS
	UINT32 bActive2 = 0; //activate channel 1, deactivating 0
	UINT16 dchannel = 0;
	resTest2 = cbSdkSetChannelMask(m_blkrckInstance, dchannel, bActive2);

	//ACTIVATING ONLY THE CHANNELS TO BE USED
	bActive2 = 1; //activate channel: 1, deactivating 0
	for (UINT16 ch = 1; ch <= m_numNeuroChannels; ch++)  //i must start from 1 because dchannel =0, bactive= 0 means deactivate everything
	{
		resTest2 = cbSdkSetChannelMask(m_blkrckInstance, ch, bActive2);
	}

    cbPKT_CHANINFO chan_info;
    cbSdkResult r = cbSdkGetChannelConfig(0, 1, &chan_info);

    // chan_info.smpgroup = 5;

    // r = cbSdkSetChannelConfig(0, 1, &chan_info);


    resTest2 = cbSdkGetTrialConfig(m_blkrckInstance
        , &bWithinTrial
        , &uBegChan
        , &uBegMask
        , &uBegVal
        , &uEndChan
        , &uEndMask
        , &uEndVal
        , &bDouble
        , &uWaveforms
        , &uConts
        , &uEvents
        , &uComments
        , &uTrackings);


    uEvents = 0;
    uConts = cbSdk_CONTINUOUS_DATA_SAMPLES;
    // uConts = 4096;
    UINT32 bActive = 1; // 0 leave buffer intact, 1 clear the buffer
    uBegChan = 0;
    uEndChan = 0;
    uBegVal = 0;
    uEndVal = 0;
    uBegMask = 0;
    uEndMask = 0;
    resTest2 = cbSdkSetTrialConfig(m_blkrckInstance
                                , bActive
                                , uBegChan
                                , uBegMask
                                , uBegVal
                                , uEndChan
                                , uEndMask
                                , uEndVal
                                , bDouble
                                , uWaveforms
                                , uConts
                                , uEvents
                                , uComments
                                , uTrackings
                                , bAbsolute); // Configure a data collection trial

    if (resTest2 != CBSDKRESULT_SUCCESS)
    {
        bool debug2 = true;
    }

    resTest2 = cbSdkGetTrialConfig(m_blkrckInstance
        , &bWithinTrial
        , &uBegChan
        , &uBegMask
        , &uBegVal
        , &uEndChan
        , &uEndMask
        , &uEndVal
        , &bDouble
        , &uWaveforms
        , &uConts
        , &uEvents
        , &uComments
        , &uTrackings);


	return 0;
}

int CBlackrock::connection_init_neuro_double(void)
{
	cbSdkResult resTest2;

	// SET CONFIG SETUP 
	UINT16 uBegChan = 1;
	UINT32 uBegMask = 0;
	UINT32 uBegVal = 0;
	UINT16 uEndChan = 128;
	UINT32 uEndMask = 0;
	UINT32 uEndVal = 0;
	bool   bDouble = false;
	bool   bAbsolute = false;
	UINT32 uWaveforms = 0;
	UINT32 uConts = 0;
	UINT32 uEvents = 0;
	UINT32 uComments = 0;
	UINT32 uTrackings = 0;
	UINT32 bWithinTrial = false;

	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance
		, &bWithinTrial
		, &uBegChan
		, &uBegMask
		, &uBegVal
		, &uEndChan
		, &uEndMask
		, &uEndVal
		, &bDouble
		, &uWaveforms
		, &uConts
		, &uEvents
		, &uComments
		, &uTrackings);

	uEvents = 0;
	uConts = cbSdk_CONTINUOUS_DATA_SAMPLES;
	// uConts = 4096;
	UINT32 bActive = 1; // 0 leave buffer intact, 1 clear the buffer
	uBegChan = 0;
	uEndChan = 0;
	uBegVal = 0;
	uEndVal = 0;
	uBegMask = 0;
	uEndMask = 0;
	bDouble = true;
	resTest2 = cbSdkSetTrialConfig(m_blkrckInstance
		, bActive
		, uBegChan
		, uBegMask
		, uBegVal
		, uEndChan
		, uEndMask
		, uEndVal
		, bDouble
		, uWaveforms
		, uConts
		, uEvents
		, uComments
		, uTrackings
		, bAbsolute); // Configure a data collection trial

	if (resTest2 != CBSDKRESULT_SUCCESS)
	{
		bool debug2 = true;
	}

	// SEND COMMENT
	UINT8 charset = 1;
	std::string commentMessage = "Neuro init";
	resTest2 = cbSdkSetComment(m_blkrckInstance, 50000, charset, commentMessage.c_str());

	//DEACTIVATING ALL THE CHANNELS
	UINT32 bActive2 = 0; //activate channel 1, deactivating 0
	UINT16 dchannel = 0;
	resTest2 = cbSdkSetChannelMask(m_blkrckInstance, dchannel, bActive2);

	//ACTIVATING ONLY THE CHANNELS TO BE USED
	bActive2 = 1; //activate channel: 1, deactivating 0
	for (UINT16 ch = 1; ch <= m_numNeuroChannels; ch++)  //i must start from 1 because dchannel =0, bactive= 0 means deactivate everything
	{
		resTest2 = cbSdkSetChannelMask(m_blkrckInstance, ch, bActive2);
	}

	return 0;
}

// mask_start and mask_stop is the 2 variables that set the channels to be activated
int CBlackrock::connection_init_spikes() // BLACKROCK
{
	cbSdkResult resTest2;

	// SET CONFIG SETUP 
	UINT16 uBegChan   = 1;
	UINT32 uBegMask   = 0;
	UINT32 uBegVal    = 0;
	UINT16 uEndChan   = 128;
	UINT32 uEndMask   = 0;
	UINT32 uEndVal    = 0;
	bool   bDouble    = false;
	bool   bAbsolute  = false;
	UINT32 uWaveforms = 0;
	UINT32 uConts     = 0;
	UINT32 uEvents    = 0;
	UINT32 uComments  = 0;
	UINT32 uTrackings = 0;
	UINT32 bWithinTrial = false;

	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance
								 , &bWithinTrial
								 , &uBegChan
								 , &uBegMask
								 , &uBegVal
								 , &uEndChan
								 , &uEndMask
								 , &uEndVal
								 , &bDouble
								 , &uWaveforms
								 , &uConts
								 , &uEvents
								 , &uComments
								 , &uTrackings);

	// uEvents = cbSdk_EVENT_DATA_SAMPLES;
	uConts = 0;
	UINT32 bActive = 1; // 0 leave buffer intact, 1 clear the buffer
	uBegChan = 0;
	uEndChan = 0;
	uBegVal = 0;
	uEndVal = 0;
	uBegMask = 0;
	uEndMask = 0;
	resTest2 = cbSdkSetTrialConfig(m_blkrckInstance
								 , bActive
								 , uBegChan
								 , uBegMask
								 , uBegVal
								 , uEndChan
								 , uEndMask
								 , uEndVal
								 , bDouble
								 , uWaveforms
								 , uConts
								 , uEvents
								 , uComments
								 , uTrackings
								 , bAbsolute); // Configure a data collection trial

	if( resTest2 != CBSDKRESULT_SUCCESS )
	{
		bool debug2 = true;
	}

	// SEND COMMENT
	UINT32 rgba = 50000;
	UINT8 charset = 1;
	std::string commentMessage = "Spike init";
	resTest2 = cbSdkSetComment(m_blkrckInstance, rgba, charset, commentMessage.c_str());

	//DEACTIVATING ALL THE CHANNELS
	UINT32 bActive2 = 0; //activate channel 1, deactivating 0
	UINT16 dchannel = 0;
	resTest2 = cbSdkSetChannelMask(m_blkrckInstance, dchannel, bActive2);

	//ACTIVATING ONLY THE CHANNELS TO BE USED
	bActive2 = 1; //activate channel: 1, deactivating 0
	for (UINT16 ch = 1; ch <= m_numNeuroChannels; ch++)  //i must start from 1 because dchannel =0, bactive= 0 means deactivate everything
	{
		resTest2 = cbSdkSetChannelMask(m_blkrckInstance, ch, bActive2);
	}

	return 0;
}

// End BlackRock connection
void CBlackrock::connection_end()
{

	// SEND COMMENT
	cbSdkResult resTest2;
	UINT32 rgba = 128;
	UINT8 charset = 1;
	std::string commentMessage = "STOP";
	for (int i=0; i< 1; i++)
	{
		//resTest2 = cbSdkSetComment( nInstance, 8912896, charset ,pippo);
		resTest2 = cbSdkSetComment(m_blkrckInstance, 128, charset, commentMessage.c_str());
	}
	resTest2 = cbSdkClose(m_blkrckInstance);
}

//Function to get EMG raw data
void CBlackrock::get_EMG_old(const int CHANNEL2USE_m1
					   , const int CHANNEL2USE_m2
					   , const int* const CHANNELS2USE
					   , INT16* Signal_m1
					   , INT16* Signal_m2
					   , INT16** Signals)
{

	cbSdkResult resTest2;
	UINT32 bActive = 1; // 0 leave buffer intact, 1 clear the buffer
	// INIT TRIAL (EVERY TIME)
	resTest2 = cbSdkInitTrialData(m_blkrckInstance, bActive, NULL, &m_trialCont, NULL, NULL);

	// BUFFER FOR FASTER UPDATING - smaller size (20 samples)
	for (int chio=0; chio< m_trialCont.count; chio++)
	{
		m_trialCont.num_samples[chio] = m_emgBufferSize;
	}

	// GET CONFIG
	bool bTrialDouble = false;
	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &bTrialDouble);


	// ALLOCATE MEMORY
	int ch = 0;
	double* pi = NULL;

	if(bTrialDouble)
	{
		for(ch=0; ch<m_trialCont.count; ch++)
		{
			m_trialCont.samples[ch] = new double[m_trialCont.num_samples[ch]];
		}
	}
	else
	{
		for(ch=0; ch<m_trialCont.count; ch++)
		{
			m_trialCont.samples[ch] = new INT16[m_trialCont.num_samples[ch]];
		}
	}

	// GET DATA
	bool bFlushBuffer = true;
	UINT16 activeChannels = -1;
	UINT32 num_samples = -1;
	UINT32 time_rec = -1;
	resTest2 = cbSdkGetTrialData(m_blkrckInstance, bFlushBuffer, NULL, &m_trialCont, NULL, NULL);


	// WAIT UNTIL BUFFER UPDATED
	double waitTime = 0;
	if (m_trialCont.sample_rates[0] > 0)
	{
		waitTime = double(m_emgBufferSize)/m_trialCont.sample_rates[0]*1000-2; // remove 2ms to avoid accumulating delays
	}

	Sleep(waitTime);

	// STORE IN GLOBAL FUNCTION
	INT16* test = (INT16 *)m_trialCont.samples[CHANNEL2USE_m1];
	for (size_t j=0; j<m_trialCont.num_samples[0]; j++)
	{
		Signal_m1[j] = test[j];
	}

	INT16* test2 = (INT16 *)m_trialCont.samples[CHANNEL2USE_m2];
	for (size_t j=0; j<m_trialCont.num_samples[0]; j++)
	{
		Signal_m2[j] = test2[j];
	}

	for (size_t chan = 0; chan<m_numEmgChannels; chan++)
	{
		INT16* test = (INT16 *)m_trialCont.samples[CHANNELS2USE[chan]];
		for (size_t j=0; j<m_trialCont.num_samples[0]; j++)
		{
			Signals[chan][j] = test[j];
		}
	}
}

void CBlackrock::get_EMG_secondVersion(const int* const CHANNELS2USE, INT16** Signals)
{
	cbSdkResult resTest2;
	UINT32 bActive = 1;
	// INIT TRIAL (EVERY TIME)
	resTest2 = cbSdkInitTrialData(m_blkrckInstance, bActive, NULL, &m_trialCont, NULL, NULL);

	// BUFFER FOR FASTER UPDATING - smaller size (20 samples)
	for (int chio=0; chio< m_trialCont.count; chio++)
	{
		m_trialCont.num_samples[chio] = m_emgBufferSize;
	}

	// GET CONFIG
	bool bTrialDouble = false;
	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &bTrialDouble);


	// ALLOCATE MEMORY
	int ch = 0;
	double* pi = NULL;

	if(bTrialDouble)
	{
		for(ch=0; ch<m_trialCont.count; ch++)
		{
			m_trialCont.samples[ch] = new double[m_trialCont.num_samples[ch]];
		}
	}
	else
	{
		for(ch=0; ch<m_trialCont.count; ch++)
		{
			m_trialCont.samples[ch] = new INT16[m_trialCont.num_samples[ch]];
		}
	}

	// GET DATA
	bool bFlushBuffer = true;
	UINT16 activeChannels = -1;
	UINT32 num_samples = -1;
	UINT32 time_rec = -1;
	resTest2 = cbSdkGetTrialData(m_blkrckInstance, bFlushBuffer, NULL, &m_trialCont, NULL, NULL);


	// WAIT UNTIL BUFFER UPDATED
	double waitTime = 0;
	if (m_trialCont.sample_rates[0] > 0)
	{
		waitTime = double(m_emgBufferSize)/m_trialCont.sample_rates[0]*1000-2; // remove 2ms to avoid accumulating delays
	}

	Sleep(waitTime);

	// STORE IN GLOBAL FUNCTION
	for (size_t chan = 0; chan<m_numEmgChannels; chan++)
	{
		INT16* test = (INT16 *)m_trialCont.samples[CHANNELS2USE[chan]];
		for (size_t j=0; j<m_trialCont.num_samples[0]; j++)
		{
			Signals[chan][j] = test[j];
		}
	}
}

// Get EMG data
void CBlackrock::get_EMG(const size_t noChan2Use
							, const size_t* ChannelsToUse
							, INT16** EMGsignal
							, size_t& noSamples)
{
	cbSdkResult resTest2;

	// Get configuration
	bool bTrialDouble = false;
	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &bTrialDouble);
	// or use for visualization of the values
	//UINT16 uBegChan   = 0;
	//UINT32 uBegMask   = 0;
	//UINT32 uBegVal    = 0;
	//UINT16 uEndChan   = 0;
	//UINT32 uEndMask   = 0;
	//UINT32 uEndVal    = 0;
	//bool   bTrialDouble    = false;
	//bool   bAbsolute  = false;
	//UINT32 uWaveforms = 0;
	//UINT32 uConts     = 0;
	//UINT32 uEvents    = 0;
	//UINT32 uComments  = 0;
	//UINT32 uTrackings = 0;
	//UINT32 bWithinTrial = false;
	//resTest2 = cbSdkGetTrialConfig(m_blkrckInstance
	//								, &bWithinTrial
	//								, &uBegChan
	//								, &uBegMask
	//								, &uBegVal
	//								, &uEndChan
	//								, &uEndMask
	//								, &uEndVal
	//								, &bTrialDouble
	//								, &uWaveforms
	//								, &uConts
	//								, &uEvents
	//								, &uComments
	//								, &uTrackings);
	
	if (resTest2 != CBSDKRESULT_SUCCESS )
		throw std::runtime_error("cbSdkGetTrialConfig {Blackrock.cpp}"); 

	// Initialize trial (EVERY TIME)
	UINT32 bActive = 1;
	resTest2 = cbSdkInitTrialData(m_blkrckInstance, bActive, NULL, &m_trialCont, NULL, NULL);
	if (resTest2 != CBSDKRESULT_SUCCESS )
		throw std::runtime_error("cbSdkInitTrialData {Blackrock.cpp}"); 

	// Allocate memory
	int ch = 0;
	if(bTrialDouble)
	{
		for(ch=0; ch<m_trialCont.count; ch++)
		{
			m_trialCont.samples[ch] = new double[m_trialCont.num_samples[ch]];
		}
	}
	else
	{
		for(ch=0; ch<m_trialCont.count; ch++)
		{
			m_trialCont.samples[ch] = new INT16[m_trialCont.num_samples[ch]];
		}
	}

	// Get data
	bool bFlushBuffer = true;
	resTest2 = cbSdkGetTrialData(m_blkrckInstance, bFlushBuffer, NULL, &m_trialCont, NULL, NULL);

	// Store in global function
	for (size_t chan = 0; chan<noChan2Use; chan++)
	{
		EMGsignal[chan] = (INT16 *)m_trialCont.samples[ChannelsToUse[chan]];
	}
	noSamples = m_trialCont.num_samples[ChannelsToUse[0]];
}

void CBlackrock::get_neuro(const size_t noChan2Use
							, const size_t* const CHANNELS2USE
							, INT16** Signals
							, size_t& noSamples
							, INT16** dataHolder
							, size_t& totNoChannels)
{
	cbSdkResult resTest2;

	/*
	// SET CONFIG SETUP 
	UINT16 uBegChan   = 0;
	UINT32 uBegMask   = 0;
	UINT32 uBegVal    = 0;
	UINT16 uEndChan   = 0;
	UINT32 uEndMask   = 0;
	UINT32 uEndVal    = 0;
	bool   bDouble    = false;
	bool   bAbsolute  = false;
	UINT32 uWaveforms = 0;
	UINT32 uConts     = 0;
	UINT32 uEvents    = 0;
	UINT32 uComments  = 0;
	UINT32 uTrackings = 0;
	UINT32 bWithinTrial = false;

	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance
		, &bWithinTrial
		, &uBegChan
		, &uBegMask
		, &uBegVal
		, &uEndChan
		, &uEndMask
		, &uEndVal
		, &bDouble
		, &uWaveforms
		, &uConts
		, &uEvents
		, &uComments
		, &uTrackings);


	if (resTest2 != CBSDKRESULT_SUCCESS )
	{
		throw std::runtime_error("cbSdkGetTrialConfig"); 
	}
		*/

	/*
	cbSdkTrialCont trialcont;
	memset(&trialcont, 0, sizeof(trialcont));
	resTest2 = cbSdkInitTrialData(m_blkrckInstance, NULL, &trialcont, NULL, NULL);
	*/

	// GET CONFIG
	bool bTrialDouble = false;
	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &bTrialDouble);

	if (resTest2 != CBSDKRESULT_SUCCESS )
	{
		throw std::runtime_error("cbSdkGetTrialConfig"); 
	}

	// INIT TRIAL (EVERY TIME)
	UINT32 bActive = 1;
	resTest2 = cbSdkInitTrialData(m_blkrckInstance, bActive, NULL, &m_trialCont, NULL, NULL);

	if (resTest2 != CBSDKRESULT_SUCCESS )
	{
		throw std::runtime_error("cbSdkInitTrialData"); 
	}

	// ALLOCATE MEMORY
	int ch = 0;
	double* pi = NULL;
	dataHolder = (INT16**)m_trialCont.samples;
	totNoChannels = m_trialCont.count;

	if(bTrialDouble)
	{
		for(ch=0; ch<m_trialCont.count; ch++)
		{
			m_trialCont.samples[ch] = new double[m_trialCont.num_samples[ch]];
		}
	}
	else
	{
		for(ch=0; ch<m_trialCont.count; ch++)
		{
			m_trialCont.samples[ch] = new INT16[m_trialCont.num_samples[ch]];
		}
	}

	// GET DATA
	bool bFlushBuffer = true;
	resTest2 = cbSdkGetTrialData(m_blkrckInstance, bFlushBuffer, NULL, &m_trialCont, NULL, NULL);

	// STORE IN GLOBAL FUNCTION
	for (size_t chan = 0; chan<noChan2Use; chan++)
	{
		Signals[chan] = (INT16 *)m_trialCont.samples[CHANNELS2USE[chan]];
	}
	noSamples = m_trialCont.num_samples[CHANNELS2USE[0]];
}

void CBlackrock::get_neuro_downsample(const size_t noChan2Use
									, const size_t* const CHANNELS2USE
									, double** dataBuffer
									, const size_t bufferSize
									, const size_t downsampleRate
									, size_t& timeCounter
									, size_t& outCount)
{
	cbSdkResult resTest2;

	// GET CONFIG
	bool bTrialDouble = false;
	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &bTrialDouble);

	if (resTest2 != CBSDKRESULT_SUCCESS)
	{
		throw std::runtime_error("cbSdkGetTrialConfig");
	}

	// INIT TRIAL (EVERY TIME)
	UINT32 bActive = 1;
	resTest2 = cbSdkInitTrialData(m_blkrckInstance, bActive, NULL, &m_trialCont, NULL, NULL);

	if (resTest2 != CBSDKRESULT_SUCCESS)
	{
		throw std::runtime_error("cbSdkInitTrialData");
	}

	// ALLOCATE MEMORY
	int ch = 0;
	if (bTrialDouble)
	{
		for (ch = 0; ch<m_trialCont.count; ch++)
		{
			m_trialCont.samples[ch] = new double[m_trialCont.num_samples[ch]];
		}
	}
	else
	{
		for (ch = 0; ch<m_trialCont.count; ch++)
		{
			m_trialCont.samples[ch] = new INT16[m_trialCont.num_samples[ch]];
		}
	}

	// GET DATA
	bool bFlushBuffer = true;
	resTest2 = cbSdkGetTrialData(m_blkrckInstance, bFlushBuffer, NULL, &m_trialCont, NULL, NULL);

	outCount = 0;
	while (timeCounter < m_trialCont.num_samples[CHANNELS2USE[0]] && outCount < bufferSize)
	{
		for (size_t chan = 0; chan<noChan2Use; chan++)
		{
			if (bTrialDouble)
			{
				double* p = (double *)m_trialCont.samples[CHANNELS2USE[chan]];
				dataBuffer[chan][outCount] = (double)p[timeCounter];
			}
			else
			{
				INT16* p = (INT16 *)m_trialCont.samples[CHANNELS2USE[chan]];
				dataBuffer[chan][outCount] = (double)p[timeCounter];
			}
		}
		timeCounter += downsampleRate;
		outCount++;
	}
	timeCounter -= min(m_trialCont.num_samples[CHANNELS2USE[0]], bufferSize);

	for (ch = 0; ch<m_trialCont.count; ch++)
	{
		delete[] m_trialCont.samples[ch];
	}
}

void CBlackrock::get_neuro_double(const size_t noChan2Use
								, const size_t* const CHANNELS2USE
								, double** Signals
								, size_t& noSamples)
{
	cbSdkResult resTest2;

	// GET CONFIG
	bool bTrialDouble = false;
	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &bTrialDouble);

	if (resTest2 != CBSDKRESULT_SUCCESS)
	{
		throw std::runtime_error("cbSdkGetTrialConfig");
	}

	// INIT TRIAL (EVERY TIME)
	UINT32 bActive = 1;
	resTest2 = cbSdkInitTrialData(m_blkrckInstance, bActive, NULL, &m_trialCont, NULL, NULL);

	if (resTest2 != CBSDKRESULT_SUCCESS)
	{
		throw std::runtime_error("cbSdkInitTrialData");
	}

	// ALLOCATE MEMORY
	int ch = 0;
	double* pi = NULL;

	assert(bTrialDouble);
	for (ch = 0; ch<m_trialCont.count; ch++)
	{
		m_trialCont.samples[ch] = new double[m_trialCont.num_samples[ch]];
	}

	// GET DATA
	bool bFlushBuffer = true;
	resTest2 = cbSdkGetTrialData(m_blkrckInstance, bFlushBuffer, NULL, &m_trialCont, NULL, NULL);

	// STORE IN GLOBAL FUNCTION
	for (size_t chan = 0; chan<noChan2Use; chan++)
	{
		Signals[chan] = (double*)m_trialCont.samples[CHANNELS2USE[chan]];
	}
	noSamples = m_trialCont.num_samples[CHANNELS2USE[0]];
}

//function to get SPIKING data...}}}}}}
void CBlackrock::get_spikes(UINT32*** spikes_32
						  , double*** spikes
						  , double* Raster)
{
	cbSdkResult resTest2;
	// INIT TRIAL (EVERY TIME)
	UINT32 bActive = 1;
	resTest2 = cbSdkInitTrialData(m_blkrckInstance,bActive, &m_trialEvent,NULL, NULL, NULL);

	
	// GET CONFIG
	bool bTrialDouble = false;
	resTest2 = cbSdkGetTrialConfig(m_blkrckInstance, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &bTrialDouble);
	for (UINT32 channel = 0; channel < m_trialEvent.count; channel++)
	{
		UINT16 ch = m_trialEvent.chan[channel]; // Actual channel number

		// Fill timestamps for non-empty channels
		for(UINT u = 0; u <= cbMAXUNITS; u++)
		{
			// m_trialEvent.timestamps[channel][u] = NULL;
			UINT32 num_samples = m_trialEvent.num_samples[channel][u];
			if (num_samples > 0)
			{
				if (bTrialDouble)
				{
					m_trialEvent.timestamps[channel][u] = new double[m_trialEvent.num_samples[channel][u]];
				}
				else
				{
					m_trialEvent.timestamps[channel][u] = new UINT32[m_trialEvent.num_samples[channel][u]];
				}
			}
			else 
			{
				m_trialEvent.timestamps[channel][u] = NULL;
			}
		}

		// Fill values for non-empty digital or serial channels
		if (ch == cbNUM_ANALOG_CHANS + cbNUM_ANALOGOUT_CHANS + cbNUM_DIGIN_CHANS || ch == cbMAXCHANS)
		{
			UINT32 num_samples = m_trialEvent.num_samples[channel][0];
			// m_trialEvent.waveforms[channel] = NULL;
			if (num_samples > 0)
			{
				if (bTrialDouble)
				{
					m_trialEvent.waveforms[channel] = new double[m_trialEvent.num_samples[channel][0]];
				}
				else
				{
					m_trialEvent.waveforms[channel] = new UINT16[m_trialEvent.num_samples[channel][0]];
				}
			}
			else
			{
				m_trialEvent.waveforms[channel] = NULL;
			}
		}
		else
		{
			m_trialEvent.waveforms[channel] = NULL;
		}

	}
	//END MEMORY ALLOCATION

	// GET DATA
	bool bFlushBuffer = true;
	UINT16 activeChannels = -1;
	UINT32 num_samples = -1;
	UINT32 time_rec = -1;
	resTest2 = cbSdkGetTrialData(m_blkrckInstance, bFlushBuffer, &m_trialEvent, NULL, NULL, NULL);


	// STORE IN GLOBAL FUNCTION (ALL SIGNALS)
	for (UINT32 channel = 0; channel < m_trialEvent.count; channel++)
	{
		UINT16 ch = m_trialEvent.chan[channel]; // Actual channel number
		// Fill timestamps for non-empty channels
		for(UINT u = 0; u <= cbMAXUNITS; u++)
		{   
			UINT32 num_samples = m_trialEvent.num_samples[channel][u];
			if (num_samples > 0)
			{   
				if (bTrialDouble)
				{
					memcpy(spikes[ch-1][u], m_trialEvent.timestamps[channel][u], num_samples * sizeof(double));
				}
				else
				{
					memcpy(spikes_32[ch-1][u], m_trialEvent.timestamps[channel][u], num_samples * sizeof(UINT32));
					Raster[ch-1] += num_samples;
				}
			}
		} 
	}

	// DELETE THE MEMORY
	for (UINT32 channel = 0; channel < m_trialEvent.count; channel++)
	{
		for(UINT u = 0; u <= cbMAXUNITS; u++)
		{
			if (m_trialEvent.timestamps[channel][u] != NULL)
			{
				delete[] m_trialEvent.timestamps[channel][u];
				m_trialEvent.timestamps[channel][u] = NULL;
			}
		}

		if (m_trialEvent.waveforms[channel] != NULL)
		{
			delete[] m_trialEvent.waveforms[channel];
			m_trialEvent.waveforms[channel] = NULL;
		}
	}	
}

UINT32 CBlackrock::getTime(void)
{
	UINT32 tmpTime;
	cbSdkResult cbRes;
	cbRes = cbSdkGetTime(m_blkrckInstance, &tmpTime);
	return tmpTime;
}

size_t CBlackrock::getSampleRate(void)
{
	return m_neuronalSampleRate;
}

void CBlackrock::setComment(UINT32 cbColor, std::string cbMessage)
{
	cbSdkResult cbRes;
	cbRes = cbSdkSetComment(m_blkrckInstance, cbColor, m_charset, cbMessage.c_str());
}

// Get analog data
std::vector<std::string> CBlackrock::getAnalogData()
{
	return listAnalogChannels;
}