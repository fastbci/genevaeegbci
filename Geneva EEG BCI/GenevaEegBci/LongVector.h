#ifndef LONGVECTOR_H
#define LONGVECTOR_H

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
@brief class witch encapsulates a Vector of long values.
@ingroup utils
*/
class DLL_EXPORT CLongVector
{
public:
    //Constructor
    /**
    @param size the size of the vector
    */
    CLongVector(const size_t& size);
    /**
    @param size the size of the vector
    @param initValue the value with all cell of the vector are initialized
    */
    CLongVector(const size_t& size, const long& initValue);
    /**
    size have to be the the %size of values 
    @param size the size of the vector
    @param values a long pointer with the values to copy in the encapsulated long vector
    */
    CLongVector(const size_t& size, const long* const values);
    /**
    Copy constructor
    @param other vector to copy
    */
    CLongVector(const CLongVector& other);

    //Destructor
    virtual ~CLongVector();


public:
    /**
    @return length of the vector
    */
    size_t getSize() const;

    /**
    @return pointer to the encapsulated vector
    */
    long* const getValues() const;

    /**
    multiply factor to all values stored in the vector
    @param factor value witch is multiplied to all values of the vector
    */
    void mult(const long& factor);

    /**
    adds a vector to this vector (this[i] = this[i] + other[i])
    @param other vector to add
    */
    void add(const CLongVector& other);

    /**
    adds a vector with a factor to this vector (this[i] = this[i] + (factor * other[i]))
    @param factor the factor
    @param other vector to add
    */
    void addWeighted(const long& factor, const CLongVector& other);

    /**
    subtract a other vector from this vector (this[i] = this[i] - other[i])
    @param other vector to subtract 
    */
    void subtract(const CLongVector& other);

    /**
    counting of rows starts with row 0.
    */ 
    long& operator[](const size_t& position);

    /**
    * assignment operator
    */
    const long& operator[](const size_t& position) const;

    /**
    * assignment operator
    */
    CLongVector& operator=(const CLongVector& other);

    /**
    * Tests for equality. True if vectors have the same size and component wise
    * the same values.
    *
    * @param other the other vector
    * @return true if equal
    */
    bool operator==(const CLongVector& other); 

private:
    const size_t m_size;
    long* m_values;
};

#endif // DOUBLEVECTOR_H