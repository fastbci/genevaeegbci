#ifndef SPECTRALADAPTIVEFEATURENORMALIZATION_H
#define SPECTRALADAPTIVEFEATURENORMALIZATION_H

#include <string>

class CDoubleVector;
class CDoubleMatrix;
class CAdaptiveFeatureNormalizationAlgorithm;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Performs adaptive feature normalization of spectral matrices
* @author Tom
*
* Implementation detail: This module is used to process each incoming spectral matrix
* into an output matrix of the same size.
*/
class DLL_EXPORT CSpectralAdaptiveFeatureNormalization
{
public:
    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the spectral source reconstruction
    */
    CSpectralAdaptiveFeatureNormalization(std::string calibrationFileName);

    /**
    * Destructor.
    */
    ~CSpectralAdaptiveFeatureNormalization();

    /**
    * Process function - used to perform the adaptive feature normalization.
    *
    * @param outputMatrix       The normalized features
    * @param inputMatrix		The un-normalized features
    */
    void process(CDoubleMatrix* const outputMatrix, const CDoubleMatrix * const inputMatrix, const bool artifactPresence);

    /**
    * Resets the algorithm.
    *
    * @param means          The matrix containing the new initialization means
    * @param variances      The matrix containing the new initialization variances
    */
    void reset(const CDoubleMatrix& means, const CDoubleMatrix& variances);

    /**
    * Function that returns the number of neural signals.
    */
    size_t getNumberOfSpectralBins();

    /**
    * Function that returns the number of neural signals.
    */
    size_t getNumberOfChannels();

	/**
	* Returns the initialization flag
	*/
	bool getIsInitialized();

private:
    /// safe value to prevent division with 0
    const double c_safeNormalizationValue = 1e-12;

    /// recursive length that controls the memory of the process
    size_t m_recursiveLength;

	/// initialization length to compute initial estimates on
	size_t m_initLength;

    /// number of spectral bins
    size_t m_numberOfSpectralBins;

    /// number of channels
    size_t m_numberOfChannels;

    /// Adaptive feature normalization algorithm
    CAdaptiveFeatureNormalizationAlgorithm* m_adaptiveFeatureNormalizationAlgorithm;

    /// Helper algorithm that holds the input
    CDoubleVector* m_helpInput;

    /// Helper algorithm that holds the output
    CDoubleVector* m_helpOutput;
};

#endif // SPECTRALADAPTIVEFEATURENORMALIZATION_H

