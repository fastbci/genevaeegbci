#ifndef FREQUENCYBANDSELECTOR_H
#define FREQUENCYBANDSELECTOR_H

#include <string>

class CDoubleMatrix;
class CSizeTVector;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Sub-selects a band of frequencies.
* @author Tom
*/
class DLL_EXPORT CFrequencyBandSelector
{
public:
    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the frequency band selector
    */
    CFrequencyBandSelector(std::string calibrationFileName);

    /**
    * Destructor.
    */
    ~CFrequencyBandSelector();

    /**
    * Process function - used to sub select the frequency bins.
    *
    * @param spectrumOutput		This contains the sub-selected spectrum
    * @param spectrumImput		The complete spectrum
    */
    void process(CDoubleMatrix* const spectrumOutput, const CDoubleMatrix* const spectrumImput);

    /**
    * Function that returns the number of channels.
    */
    const size_t getNumberOfChannels();

    /**
    * Function that returns the number of spectral bins.
    */
    const size_t getNumberOfSpectralBins();

    /**
    * Function that returns the number of sub-selected spectral bins.
    */
    const size_t getNumberOfSelectedBins();

private:
    /// Number of signal channels
    size_t m_numberOfChannels;

    /// Number of spectral bins
    size_t m_numberOfSpectralBins;

    /// Number of spectral bins
    size_t m_numberOfSelectedBins;

    /// Number of spectral bins
    CSizeTVector* m_selectedBins;
};

#endif // FREQUENCYBANDSELECTOR_H
