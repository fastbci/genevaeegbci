#include "SpectralAdaptiveFeatureNormalization.h"
#include <assert.h>

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

#ifndef MATLABLIBRARY_H
#include "MatlabLibrary.h"
#endif

#ifndef ADAPTIVEFEATURENORMALIZATIONALGORITHM_H
#include "AdaptiveFeatureNormalizationAlgorithm.h"
#endif

using namespace std;


CSpectralAdaptiveFeatureNormalization::CSpectralAdaptiveFeatureNormalization(string calibrationFileName)
{
    // Load numberOfSpectralBins from .mat file
    mxArray* matNumberOfSpectralBins = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfSpectralBins");
    assert(NULL != matNumberOfSpectralBins);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfSpectralBins)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfSpectralBins)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfSpectralBins));
    m_numberOfSpectralBins = static_cast<size_t>(dataElements[0]);


    // Load numberOfSignals from .mat file
    mxArray* matNumberOfChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfChannels");
    assert(NULL != matNumberOfChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfChannels)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfChannels));
    m_numberOfChannels = static_cast<size_t>(dataElements[0]);


    // Load recursiveLength from .mat file
    mxArray* matRecursiveLength = CMatlabLibrary::loadMatVariable(calibrationFileName, "recursiveLength");
    assert(NULL != matRecursiveLength);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matRecursiveLength)));
    assert(1 == static_cast<int>(mxGetM(matRecursiveLength)));

    dataElements = static_cast<double*>(mxGetData(matRecursiveLength));
    m_recursiveLength = static_cast<size_t>(dataElements[0]);


	// Load initLength from .mat file
	mxArray* matInitLength = CMatlabLibrary::loadMatVariable(calibrationFileName, "initLength");
	assert(NULL != matInitLength);

	// Verify that the size is ok
	assert(1 == static_cast<int>(mxGetN(matInitLength)));
	assert(1 == static_cast<int>(mxGetM(matInitLength)));

	dataElements = static_cast<double*>(mxGetData(matInitLength));
	m_initLength = static_cast<size_t>(dataElements[0]);


    //// Load the matrix containing the initialization means from .mat file
    //mxArray* matInitMeans = CMatlabLibrary::loadMatVariable(calibrationFileName, "initMeans");
    //assert(NULL != matInitMeans);

    //// Verify that the size is ok
    //assert(m_numberOfSpectralBins == mxGetM(matInitMeans));
    //assert(m_numberOfChannels == mxGetN(matInitMeans));

    //dataElements = static_cast<double*>(mxGetData(matInitMeans));
    //CDoubleVector initMeans(m_numberOfSpectralBins * m_numberOfChannels, dataElements);


    //// Load the matrix containing the initialization variances from .mat file
    //mxArray* matInitValiances = CMatlabLibrary::loadMatVariable(calibrationFileName, "initVariances");
    //assert(NULL != matInitValiances);

    //// Verify that the size is ok
    //assert(m_numberOfSpectralBins == mxGetM(matInitValiances));
    //assert(m_numberOfChannels == mxGetN(matInitValiances));

    //dataElements = static_cast<double*>(mxGetData(matInitValiances));
    //CDoubleVector initVariances(m_numberOfSpectralBins * m_numberOfChannels, dataElements);


    // Initialize the adaptive feature normalization
    m_adaptiveFeatureNormalizationAlgorithm = new CAdaptiveFeatureNormalizationAlgorithm(m_numberOfSpectralBins * m_numberOfChannels, m_initLength, m_recursiveLength);

    // Initialize the helper variables
    m_helpInput = new CDoubleVector(m_numberOfSpectralBins * m_numberOfChannels, 0.0);
    m_helpOutput = new CDoubleVector(m_numberOfSpectralBins * m_numberOfChannels, 0.0);
}


CSpectralAdaptiveFeatureNormalization::~CSpectralAdaptiveFeatureNormalization()
{
    if (m_adaptiveFeatureNormalizationAlgorithm != 0)
    {
        delete m_adaptiveFeatureNormalizationAlgorithm;
        m_adaptiveFeatureNormalizationAlgorithm = 0;
    }

    if (m_helpInput != 0)
    {
        delete m_helpInput;
        m_helpInput = 0;
    }

    if (m_helpOutput != 0)
    {
        delete m_helpOutput;
        m_helpOutput = 0;
    }
}


void CSpectralAdaptiveFeatureNormalization::process(CDoubleMatrix* const outputMatrix, const CDoubleMatrix * const inputMatrix, const bool artifactPresence)
{
    assert(inputMatrix->getRowSize() == outputMatrix->getRowSize());
    assert(inputMatrix->getColumnSize() == outputMatrix->getColumnSize());
    assert(inputMatrix->getRowSize() * inputMatrix->getColumnSize() == m_helpInput->getSize());

    memcpy(m_helpInput->getValues(), inputMatrix->getValues(), m_helpInput->getSize() * sizeof(double));
    m_adaptiveFeatureNormalizationAlgorithm->process(m_helpOutput[0], m_helpInput[0], artifactPresence);
    memcpy(outputMatrix->getValues(), m_helpOutput->getValues(), m_helpOutput->getSize() * sizeof(double));
}


void CSpectralAdaptiveFeatureNormalization::reset(const CDoubleMatrix& means, const CDoubleMatrix& variances)
{
    assert(means.getRowSize() == variances.getRowSize());
    assert(means.getColumnSize() == variances.getColumnSize());
    assert(means.getRowSize() * means.getColumnSize() == m_helpInput->getSize());

    memcpy(m_helpInput->getValues(), means.getValues(), m_helpInput->getSize() * sizeof(double));
    memcpy(m_helpOutput->getValues(), variances.getValues(), m_helpOutput->getSize() * sizeof(double));
    m_adaptiveFeatureNormalizationAlgorithm->reset(m_helpInput[0], m_helpOutput[0]);
}


size_t CSpectralAdaptiveFeatureNormalization::getNumberOfSpectralBins()
{
    return m_numberOfSpectralBins;
}


size_t CSpectralAdaptiveFeatureNormalization::getNumberOfChannels()
{
    return m_numberOfChannels;
}


bool CSpectralAdaptiveFeatureNormalization::getIsInitialized()
{
	if (m_adaptiveFeatureNormalizationAlgorithm != 0)
	{
		return m_adaptiveFeatureNormalizationAlgorithm->getIsInitialized();
	}
	else
	{
		false;
	}
}
