#pragma once

/**
* @brief UI class to get transparent static label
* @author Simon
*/
class CTransparentStatic : public CStatic
{
    DECLARE_DYNAMIC(CTransparentStatic)

public:
    CTransparentStatic();
    virtual ~CTransparentStatic();

protected:
    afx_msg LRESULT OnSetText(WPARAM,LPARAM);
    afx_msg HBRUSH CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/);
    DECLARE_MESSAGE_MAP()
};