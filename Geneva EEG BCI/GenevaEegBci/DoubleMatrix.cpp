#include "DoubleMatrix.h"

#include <memory.h>
#include <assert.h>
#include <fstream>


CDoubleMatrix::CDoubleMatrix(const size_t& rowSize, const size_t& columnSize)
	: m_columnSize(columnSize)
	, m_rowSize(rowSize)
	, m_values(0)
{   
	if (columnSize <= 0 || rowSize <= 0)
	{
		throw std::invalid_argument("arguments rowSize and columnSize of CDoubleMatrix() have to be bigger than zero");
	}
	else
	{
		m_values = new double[m_columnSize * m_rowSize];
		::memset(m_values, 0, m_columnSize * m_rowSize * sizeof(double));
	}
}

CDoubleMatrix::CDoubleMatrix(const size_t& rowSize, const size_t& columnSize, const double& initValue)
	: m_columnSize(columnSize)
	, m_rowSize(rowSize)
	, m_values(0)
{
	if (columnSize <= 0 || rowSize <= 0)
	{
		throw std::invalid_argument("arguments rowSize and columnSize of CDoubleMatrix() have to be bigger than zero");
	}
	else
	{
		m_values = new double[m_columnSize * m_rowSize];
		for(size_t index = 0; index < m_columnSize * m_rowSize; ++index)
		{
			m_values[index] = initValue;
		}
	}
}

CDoubleMatrix::CDoubleMatrix(const size_t& rowSize, const size_t& columnSize, const double* const values)
	: m_columnSize(columnSize)
	, m_rowSize(rowSize)
	, m_values(0)
{
	if (columnSize <= 0 || rowSize <= 0)
	{
		throw std::invalid_argument("arguments rowSize and columnSize of CDoubleMatrix() have to be bigger than zero");
	}
	else
	{
		m_values = new double[m_columnSize * m_rowSize];
		memcpy(m_values, values, m_columnSize * m_rowSize * sizeof(double));   
	}
}

CDoubleMatrix::CDoubleMatrix(const CDoubleMatrix& other)
	: m_columnSize(other.m_columnSize)
	, m_rowSize(other.m_rowSize)
	, m_values((m_columnSize != 0 && m_rowSize != 0)? new double[m_columnSize * m_rowSize] : 0)
{
	// not nice but fast
	memcpy(m_values, other.getValues(), m_columnSize * m_rowSize * sizeof(double));
}

CDoubleMatrix::~CDoubleMatrix()
{
	if (m_values!=0)
	{
		delete [] m_values;
		m_values = 0;
	}
}

size_t CDoubleMatrix::getColumnSize() const
{
	return m_columnSize;
}

size_t CDoubleMatrix::getRowSize() const
{
	return m_rowSize;
}

double* const CDoubleMatrix::getValues() const
{
	return m_values;
}

double* const CDoubleMatrix::getColumnAt(size_t columnIndex) const
{
	assert(columnIndex < m_columnSize);
	return m_values + columnIndex * m_rowSize;
}

CDoubleMatrix& CDoubleMatrix::operator=(const CDoubleMatrix& other)
{
	assert(other.getColumnSize() == m_columnSize);
	assert(other.getRowSize()    == m_rowSize);

	// not nice but fast
	memcpy(m_values, other.getValues(), m_columnSize * m_rowSize * sizeof(double));
	return *this;
}

void CDoubleMatrix::insertAt(const CDoubleMatrix& toInsert, const size_t columnOffset, const size_t rowOffset)
{
	// TODO optimize for speed
	assert(getRowSize() >= rowOffset + toInsert.getRowSize());
	assert(getColumnSize() >= columnOffset + toInsert.getColumnSize());

	for(size_t columnIndex = 0; columnIndex < toInsert.getColumnSize(); ++columnIndex)
	{
		double* destinationStart = getValues() + getRowSize() * (columnOffset + columnIndex) + rowOffset; 
		const double* sourceStart = toInsert.getValues() + columnIndex * toInsert.getRowSize();
		memcpy(destinationStart, sourceStart, toInsert.getRowSize() * sizeof(double));
	}
}

void CDoubleMatrix::subtract(const CDoubleMatrix& other)
{
	assert(other.getColumnSize() == m_columnSize);
	assert(other.getRowSize() == m_rowSize);

	const size_t length = m_rowSize * m_columnSize;
	const double* const otherValues = other.getValues();
	for(size_t index = 0; index < length; ++index)
	{
		m_values[index] -= otherValues[index];
	}
}

void CDoubleMatrix::add(const CDoubleMatrix& other)
{
	assert(other.getColumnSize() == m_columnSize);
	assert(other.getRowSize() == m_rowSize);

	const size_t length = m_rowSize * m_columnSize;
	const double* const otherValues = other.getValues();
	for(size_t index = 0; index < length; ++index)
	{
		m_values[index] += otherValues[index];
	}
}


void CDoubleMatrix::addWeighted(const double& factor, const CDoubleMatrix& other)
{
	assert(other.getColumnSize() == m_columnSize);
	assert(other.getRowSize() == m_rowSize);

	const size_t length = m_rowSize * m_columnSize;
	const double* const otherValues = other.getValues();
	for(size_t index = 0; index < length; ++index)
	{
		m_values[index] += factor * otherValues[index];
	}
}


void CDoubleMatrix::mult(const double& factor)
{
	const size_t length = m_rowSize * m_columnSize;

	for(size_t index = 0; index < length; ++index)
	{
		m_values[index] *= factor;
	}
}

double& CDoubleMatrix::at(const size_t& row, const size_t& column) const
{
	assert(column < m_columnSize);
	assert(row < m_rowSize);

	return m_values[column * m_rowSize + row];
}

double& CDoubleMatrix::operator [](size_t position)
{
	assert(position<=(m_rowSize+1)*(m_columnSize+1)-1);
	return (*(m_values+position));
}

CDoubleMatrix CDoubleMatrix::getTransposed() const
{
	CDoubleMatrix transposed(getColumnSize(), getRowSize());
	for(size_t col = 0; col < m_columnSize; ++col)
	{
		for(size_t row = 0; row < m_rowSize; ++row)
		{
			transposed.at(col, row) = at(row, col);
		}
	}
	return transposed;
}