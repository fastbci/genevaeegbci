#include "EegData.h"

#include <vector>
#include <iterator>
#include "DoubleMatrix.h"
#include "MatlabLibrary.h"

using namespace std;
using namespace eemagine::sdk;
using namespace std::chrono;

bool pred(const eemagine::sdk::amplifier *a, const eemagine::sdk::amplifier *b);

/**
* @brief Class constructor
*/
CEegData::CEegData(string calibrationFileName)
: clockwise(CreateEvent(0, TRUE, FALSE, 0))
, anticlockwise(CreateEvent(0, TRUE, FALSE, 0))
, select(CreateEvent(0, TRUE, FALSE, 0))
, m_sampleCount(0)
, m_samplingRate(DEFAULT_SAMPLING_RATE)
, m_refRange(DEFAULT_REF_RANGE)
, m_bipolarRange(DEFAULT_BIPOLAR_RANGE)
, m_dataWindowDuration(DEFAULT_DATA_WINDOW_DURATION)
, m_isArtefactDetected(false)
, m_artefactRefractoryCounter(0)
, m_decision(NONE)
{
    // Create and open EEG data log file
    const time_t t = time(0);
    struct tm * now = localtime(&t);

    string buffer(80, '\0');
    strftime(&buffer[0], buffer.size(), "%F_%H-%M-%S.bin", now);

    m_fileEegData = ofstream("..\\Log\\eegData_" + buffer, std::ios::out | std::ios::binary);


    // Load numberOfEegChannels from .mat file
    mxArray* matNumberOfEegChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfEegChannels");
    assert(NULL != matNumberOfEegChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfEegChannels)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfEegChannels));
    size_t number_of_eeg_channels = static_cast<size_t>(dataElements[0]);


    // Load numberOfEogChannels from .mat file
    mxArray* matNumberOfEogChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfEogChannels");
    assert(NULL != matNumberOfEogChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfEogChannels)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfEogChannels));
    size_t number_of_eog_channels = static_cast<size_t>(dataElements[0]); // Number of separate EOG channels per hor/vert EOG channel (if 1: bipolar channel assumed; if 2: bipolar channel will be derived)


    // Load eegChannelsWithRef from .mat file
    mxArray* matEegChannelWithRef = CMatlabLibrary::loadMatVariable(calibrationFileName, "eegChannelsWithRef");
    assert(NULL != matEegChannelWithRef);

    // Verify that the size is ok
    assert(1 == mxGetM(matEegChannelWithRef));
    assert(number_of_eeg_channels == static_cast<int>(mxGetN(matEegChannelWithRef)));

    dataElements = static_cast<double*>(mxGetData(matEegChannelWithRef));
    for (size_t channelIndex = 0; channelIndex < number_of_eeg_channels; channelIndex++)
    {
        m_channelConfig.eegChannelsWithRef.push_back(static_cast<int>(dataElements[channelIndex]));
    }


    // Load eegChannelsWithRefIndex from .mat file
    mxArray* matEegChannelWithRefIndex = CMatlabLibrary::loadMatVariable(calibrationFileName, "eegChannelsWithRefIndex");
    assert(NULL != matEegChannelWithRefIndex);

    // Verify that the size is ok
    assert(1 == mxGetM(matEegChannelWithRefIndex));
    assert(number_of_eeg_channels == static_cast<int>(mxGetN(matEegChannelWithRefIndex)));

    dataElements = static_cast<double*>(mxGetData(matEegChannelWithRefIndex));
    for (size_t channelIndex = 0; channelIndex < number_of_eeg_channels; channelIndex++)
    {
        m_channelConfig.eegChannelsWithRefIndex.push_back(static_cast<int>(dataElements[channelIndex]));
    }


    // Load eogBipHor from .mat file
    mxArray* matEogBipHor = CMatlabLibrary::loadMatVariable(calibrationFileName, "eogBipHor");
    assert(NULL != matEogBipHor);

    // Verify that the size is ok
    assert(1 == mxGetM(matEogBipHor));
    assert(number_of_eog_channels == static_cast<int>(mxGetN(matEogBipHor)));

    dataElements = static_cast<double*>(mxGetData(matEogBipHor));
    for (size_t channelIndex = 0; channelIndex < number_of_eog_channels; channelIndex++)
    {
        m_channelConfig.eogBipHor.push_back(static_cast<int>(dataElements[channelIndex]));
    }


    // Load eogBipVert from .mat file
    mxArray* matEogBipVert = CMatlabLibrary::loadMatVariable(calibrationFileName, "eogBipVert");
    assert(NULL != matEogBipVert);

    // Verify that the size is ok
    assert(1 == mxGetM(matEogBipVert));
    assert(number_of_eog_channels == static_cast<int>(mxGetN(matEogBipVert)));

    dataElements = static_cast<double*>(mxGetData(matEogBipVert));
    for (size_t channelIndex = 0; channelIndex < number_of_eog_channels; channelIndex++)
    {
        m_channelConfig.eogBipVert.push_back(static_cast<int>(dataElements[channelIndex]));
    }


    // Load refChannel from .mat file
    mxArray* matRefChannel = CMatlabLibrary::loadMatVariable(calibrationFileName, "refChannel");
    assert(NULL != matRefChannel);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matRefChannel)));
    assert(1 == static_cast<int>(mxGetM(matRefChannel)));

    dataElements = static_cast<double*>(mxGetData(matRefChannel));
    m_channelConfig.refChannel = static_cast<size_t>(dataElements[0]);


    // Load refChannelIndex from .mat file
    mxArray* matRefChannelIndex = CMatlabLibrary::loadMatVariable(calibrationFileName, "refChannelIndex");
    assert(NULL != matRefChannelIndex);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matRefChannelIndex)));
    assert(1 == static_cast<int>(mxGetM(matRefChannelIndex)));

    dataElements = static_cast<double*>(mxGetData(matRefChannelIndex));
    m_channelConfig.refChannelIndex = static_cast<size_t>(dataElements[0]);


    // Load artifactThreshold from .mat file
    mxArray* matArtifactThreshold = CMatlabLibrary::loadMatVariable(calibrationFileName, "artifactThreshold");
    assert(NULL != matArtifactThreshold);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matArtifactThreshold)));
    assert(1 == static_cast<int>(mxGetM(matArtifactThreshold)));

    dataElements = static_cast<double*>(mxGetData(matArtifactThreshold));
    m_thresholdConfig.threshold = static_cast<double>(dataElements[0]);

	// Load artifactRefractoryPeriod from .mat file
	mxArray* matArtifactRefractoryPeriod = CMatlabLibrary::loadMatVariable(calibrationFileName, "artifactRefractoryPeriod");
	assert(NULL != matArtifactRefractoryPeriod);

	// Verify that the size is ok
	assert(1 == static_cast<int>(mxGetN(matArtifactRefractoryPeriod)));
	assert(1 == static_cast<int>(mxGetM(matArtifactRefractoryPeriod)));

	dataElements = static_cast<double*>(mxGetData(matArtifactRefractoryPeriod));
	m_thresholdConfig.refractoryPeriod = static_cast<double>(dataElements[0]);
	
	m_artefactRefractoryCounter = m_thresholdConfig.refractoryPeriod;
}

/**
* @brief Class destructor
*/
CEegData::~CEegData()
{
}

/**
* @brief Predicate used to sort Eego amplifiers by ascending serial number
*/
bool pred(const amplifier *a, const amplifier *b)
{
    return a->getSerialNumber() < b->getSerialNumber();
}

/**
* @brief Use the Eego factory to build an amplifier and an EEG stream
*/
const bool CEegData::initAcquisition()
{
    try
    {
        vector<amplifier*> amplifiers = m_fact.getAmplifiers();

        sort(amplifiers.begin(), amplifiers.end(), pred);

        if (amplifiers.size() == 2)
        {
            m_amp.reset(m_fact.createCascadedAmplifier(amplifiers));
        }
        else
        {
            m_amp.reset(m_fact.getAmplifier());
        }

        m_eegStream.reset(m_amp->OpenEegStream(m_samplingRate, m_refRange, m_bipolarRange));

        return true;
    }
    catch(...)
    {
        return false;
    }
}

/**
* @brief Update data buffer with the last samples of the amplifier if available
*/
void CEegData::acquireAndStoreNewSamples(std::vector<CDoubleVector*>& output)
{
    //static unsigned int counter = 0;

    // Acquire amplifier data buffer
    buffer buffer = m_eegStream->getData();

    // Calculate for loop parameters
    const UINT16 allChannelCount = buffer.getChannelCount();
    const UINT16 allChannelAndDecisionCount = allChannelCount + 1;
    const UINT32 sampleCount = buffer.getSampleCount();

    // Exit if no sample
    if (0 == sampleCount)
    {
        Sleep(1);
        return;
    }

    // Local buffer to get samples
    double** send_buffer = new double*[sampleCount];
    //vector<double> data; // debug


    // Acquisition loop
    for (UINT32 s = 0; s < sampleCount; s++)
    {
        send_buffer[s] = new double[allChannelAndDecisionCount];
        for (UINT16 c = 0; c < allChannelCount; c++)
        {
            send_buffer[s][c] = buffer.getSample(c, s);
        }

        if (WAIT_OBJECT_0 == WaitForSingleObject(clockwise, 0))
        {
            m_decision = CLOCKWISE;
            ResetEvent(clockwise);
        }
        else if (WAIT_OBJECT_0 == WaitForSingleObject(anticlockwise, 0))
        {
            m_decision = ANTICLOCKWISE;
            ResetEvent(anticlockwise);
        }
        else if (WAIT_OBJECT_0 == WaitForSingleObject(select, 0))
        {
            m_decision = SELECT;
            ResetEvent(select);
        }
        else
        {
            m_decision = NONE;
            ResetEvent(clockwise);
            ResetEvent(anticlockwise);
            ResetEvent(select);
        }

        send_buffer[s][allChannelCount] = static_cast<double>(m_decision);

        CDoubleVector* dataVector = new CDoubleVector(allChannelCount, send_buffer[s]);
        output.push_back(dataVector);

        // Storage
        m_fileEegData.write(reinterpret_cast<const char *>(send_buffer[s]), allChannelAndDecisionCount * sizeof(double));

        // Free allocated memory to avoid memory leak
        delete[] send_buffer[s];
    }
    delete[] send_buffer;
}

/**
* @brief Select EEG channels and fill buffers 
*/
void CEegData::channelSelector(const double *dataBuffer, double *eegBuffer, double *eogBuffer)
{
    // Select EOG channels
    // Horizontal EOG
    if (m_channelConfig.eogBipHor.size() == 1)
    {
        eogBuffer[0] = dataBuffer[m_channelConfig.eogBipHor[0]]; // Bipolar channel already provided
    }
    else
    {
        eogBuffer[0] = dataBuffer[m_channelConfig.eogBipHor[1]] - dataBuffer[m_channelConfig.eogBipHor[0]]; // Derive bipolar channel
    }
    
    // Vertical EOG
    if (m_channelConfig.eogBipVert.size() == 1)
    {
        eogBuffer[1] = dataBuffer[m_channelConfig.eogBipVert[0]]; // Bipolar channel already provided
    }
    else
    {
        eogBuffer[1] = dataBuffer[m_channelConfig.eogBipVert[1]] - dataBuffer[m_channelConfig.eogBipVert[0]]; // Derive bipolar channel
    }

    // Select EEG channels
    size_t numChannels = m_channelConfig.eegChannelsWithRef.size();

    for (unsigned int iChannel = 0; iChannel < numChannels; iChannel++)
    {
        if (iChannel == m_channelConfig.refChannelIndex)
        {
            // If ref channel, force value to 0
            eegBuffer[iChannel] = 0;
        }
        else
        {
            eegBuffer[iChannel] = dataBuffer[m_channelConfig.eegChannelsWithRef[iChannel]];
        }
    }
}

/**
* @brief Check for artifacts based on configured threshold and keep value during refractory period
*/
const bool CEegData::artefactThreshold(const double *eegBuffer, const size_t numChannels)
{
    bool artifactPresence = false;

    // Parse EEG channels
    for (unsigned int iChannel = 0; (iChannel < numChannels) && (artifactPresence != true); iChannel++)
    {
        if (abs(eegBuffer[iChannel]) > m_thresholdConfig.threshold)
        {
			artifactPresence = true;
        }
    }

	if (artifactPresence)
	{
		m_artefactRefractoryCounter = 0;
		m_isArtefactDetected = true;
	}

	if (m_artefactRefractoryCounter <= m_thresholdConfig.refractoryPeriod)
	{
		m_artefactRefractoryCounter++;
	}
	else
	{
		m_isArtefactDetected = false;
	}

    return m_isArtefactDetected;
}
