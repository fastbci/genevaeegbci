// GenevaEegBci.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include <afxwinappex.h>

/**
* @brief Main class of Geneva EEG BCI solution
* @author Simon
*/
class CGenevaEegBciApp : public CWinAppEx
{
public:
	CGenevaEegBciApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CGenevaEegBciApp theApp;