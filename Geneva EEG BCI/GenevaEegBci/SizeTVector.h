#ifndef SIZETVECTOR_H
#define SIZETVECTOR_H

#include <iostream>

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* Class witch encapsulates a Vector of size_t values
*/
class DLL_EXPORT CSizeTVector
{
public:
	//Constructor
	/**
	@param size the size of the vector
	*/
	CSizeTVector(const size_t& size);
	/**
	@param size the size of the vector
	@param initValue the value with all cell of the vector are initialized
	*/
	CSizeTVector(const size_t& size, const size_t& initValue);
	/**
	size have to be the the %size of values
	@param size the size of the vector
	@param values a size_t pointer with the values to copy in the encapsulated size_t vector
	*/
	CSizeTVector(const size_t& size, const size_t* const values);
	/**
	this is the copy constructor
	@param other the vector to copy
	*/
	CSizeTVector(const CSizeTVector& other);

	//Destructor
	virtual ~CSizeTVector();


public:
	/**
	@return length of the vector
	*/
	size_t getSize() const;

	/**
	@return pointer to the encapsulated vector
	*/
	size_t* const getValues() const;

	/**
	multiply factor to all values stored in the vector
	@param factor value witch is multiplied to all values of the vector
	*/
	void mult(const size_t& factor);

	/**
	adds a vector to this vector (this[i] = this[i] + other[i])
	@param other vector to add
	*/
	void add(const CSizeTVector& other);

	/**
	adds a vector with a factor to this vector (this[i] = this[i] + (factor * other[i]))
	@param factor the factor
	@param other vector to add
	*/
	void addWeighted(const size_t& factor, const CSizeTVector& other);

	/**
	subtract a other vector from this vector (this[i] = this[i] - other[i])
	@param other vector to subtract
	*/
	void subtract(const CSizeTVector& other);

	/**
	find minimum and maximum values
	@param minValue minimum value in the vector
	@param maxValue maximum value in the vector
	*/
	void findMinMax(size_t& minValue, size_t& maxValue);

	/**
	return maximum value
	*/
	size_t getMax() const;

	/**
	counting of rows starts with row 0.
	*/
	size_t& operator[](const size_t& position);

	/**
	* assignment operator
	*/
	const size_t& operator[](const size_t& position) const;

	/**
	* assignment operator
	*/
	CSizeTVector& operator=(const CSizeTVector& other);

	/**
	* Tests for equality. True if vectors have the same size and component wise
	* the same values.
	*
	* @param other the other vector
	* @return true if equal
	*/
	bool operator==(const CSizeTVector& other);

private:
	const size_t m_size;
	size_t* m_values;
};

#endif // SIZETVECTOR_H

