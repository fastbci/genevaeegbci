#include "FrequencyBandSelector.h"

#include <assert.h>

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef SIZETVECTOR_H
#include "SizeTVector.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

#ifndef MATLABLIBRARY_H
#include "../GenevaEegBci/MatlabLibrary.h"
#endif

using namespace std;

CFrequencyBandSelector::CFrequencyBandSelector(string calibrationFileName)
: m_numberOfChannels(0)
, m_numberOfSpectralBins(0)
, m_numberOfSelectedBins(0)
, m_selectedBins(0)
{
    // Load numberOfSignals from .mat file
    mxArray* matNumberOfChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfChannels");
    assert(NULL != matNumberOfChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfChannels)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfChannels));
    m_numberOfChannels = static_cast<size_t>(dataElements[0]);


    // Load numberOfCorticalSources from .mat file
    mxArray* matNumberOfSpectralBins = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfSpectralBins");
    assert(NULL != matNumberOfSpectralBins);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfSpectralBins)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfSpectralBins)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfSpectralBins));
    m_numberOfSpectralBins = static_cast<size_t>(dataElements[0]);


    // Load numberOfCorticalSources from .mat file
    mxArray* matNumberOfSelectedBins = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfSelectedBins");
    assert(NULL != matNumberOfSelectedBins);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfSelectedBins)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfSelectedBins)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfSelectedBins));
    m_numberOfSelectedBins = static_cast<size_t>(dataElements[0]);
    assert(m_numberOfSelectedBins <= m_numberOfSpectralBins);


    // Load the ??? matrix from .mat file
    mxArray* matSelectedBins = CMatlabLibrary::loadMatVariable(calibrationFileName, "selectedBins");
    assert(NULL != matSelectedBins);

    // Verify that the size is ok
    assert(m_numberOfSelectedBins == mxGetM(matSelectedBins));
    assert(1 == static_cast<int>(mxGetN(matSelectedBins)));

    dataElements = static_cast<double*>(mxGetData(matSelectedBins));
    m_selectedBins = new CSizeTVector(m_numberOfSelectedBins);
    for (size_t binIndex = 0; binIndex < m_numberOfSelectedBins; ++binIndex)
    {
        m_selectedBins->getValues()[binIndex] = static_cast<size_t>(dataElements[binIndex]);
    }
}


CFrequencyBandSelector::~CFrequencyBandSelector()
{
    if (m_selectedBins != 0)
    {
        delete m_selectedBins;
        m_selectedBins = 0;
    }
}


void CFrequencyBandSelector::process(CDoubleMatrix* const spectrumOutput, const CDoubleMatrix* const spectrumImput)
{
    assert(spectrumImput->getRowSize() == m_numberOfSpectralBins);
    assert(spectrumImput->getColumnSize() == m_numberOfChannels);
    assert(spectrumOutput->getRowSize() == m_numberOfSelectedBins);
    assert(spectrumOutput->getColumnSize() == m_numberOfChannels);

    for (size_t binIndex = 0; binIndex < m_numberOfSelectedBins; ++binIndex)
    {
        for (size_t channelIndex = 0; channelIndex < m_numberOfChannels; ++channelIndex)
        {
            spectrumOutput->at(binIndex, channelIndex) = spectrumImput->at(m_selectedBins->getValues()[binIndex], channelIndex);
        }
    }
}


const size_t CFrequencyBandSelector::getNumberOfChannels()
{
    return m_numberOfChannels;
}


const size_t CFrequencyBandSelector::getNumberOfSpectralBins()
{
    return m_numberOfSpectralBins;
}


const size_t CFrequencyBandSelector::getNumberOfSelectedBins()
{
    return m_numberOfSelectedBins;
}