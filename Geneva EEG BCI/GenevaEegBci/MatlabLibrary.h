#ifndef MATLABLIBRARY_H
#define MATLABLIBRARY_H

#include <stdlib.h>
#include <iostream>
#include <mat.h>  // MAT-File API Library
#include <string>

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief contains functions to manimulate matlab files
* @author Tomislav Milekovic
*
* This structure contains static functions for manipulating Matlab files. It is a wrapper
* for the underlying mx functions.
*/
struct DLL_EXPORT CMatlabLibrary
{
private:
	/// not implemented
	CMatlabLibrary();

	/// destructor
	~CMatlabLibrary();

public:

	static mxArray* loadMatData(std::string);

	static mxArray* loadMatVariable(std::string fileName, std::string variableName);

	static void saveMatEegEogData(std::string, double *, double *, size_t, size_t, size_t);

	static void saveMatDouble3dData(std::string fileName, std::string dataName, double* data, size_t dim1Len, size_t dim2Len, size_t dim3Len);

	static void saveMatDoubleMatrixData(std::string fileName, std::string dataName, double* data, size_t numSamples, size_t numChannels);

private:
	/// not implemented
	CMatlabLibrary& operator=(const CMatlabLibrary& other);
};


#endif // MATLABLIBRARY_H