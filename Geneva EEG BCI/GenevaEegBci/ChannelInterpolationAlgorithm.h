#ifndef CHANNELINTERPOLATIONALGORITHM_H
#define CHANNELINTERPOLATIONALGORITHM_H

#include <string>

class CDoubleMatrix;
class CDoubleVector;
class CSizeTVector;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Interpolate the missing EEG channels
* @author Tom
*
* Implementation detail: The module is used to interpolate the missing EEG channels in each incoming neural signal vector.
*/
class DLL_EXPORT CChannelInterpolationAlgorithm
{
public:
    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the EOG correction algorithm
    */
    CChannelInterpolationAlgorithm(std::string calibrationFileName);

    /**
    * Destructor.
    */
    ~CChannelInterpolationAlgorithm();

    /**
    * Process function - used to interpolate the missing EEG channels.
    *
    * @param interpolatedSignal		The interpolated EEG signal
    * @param neuralSignal		    The neural signals (e.g. EEG activity)
    */
    void process(CDoubleVector* const interpolatedSignal, const CDoubleVector * const neuralSignal);

    /**
    * Function that returns the number of neural channels.
    */
    size_t getNumberOfNeuralChannels();

private:
    /// Matrix holding the interpolation weights
    CDoubleMatrix** m_interpolationWeights;

    /// The list of channels being interpolated in every interpolation step
    CSizeTVector** m_interpolationChannels;

    /// Number of electrodes used for interpolation
    CDoubleVector** m_interpolationNeighbours;

    /// Vectors used for the help with the calculation
    CDoubleVector** m_helperVector;

    /// The number of channels being interpolated in every interpolation step
    CSizeTVector* m_numberOfInterpolatedChannels;

    /// Number of neural signals
    size_t m_numberOfNeuralChannels;

    /// Number of times to run the interpolation algorithm
    size_t m_numberOfInterpolations;
};

#endif // CHANNELINTERPOLATIONALGORITHM_H

