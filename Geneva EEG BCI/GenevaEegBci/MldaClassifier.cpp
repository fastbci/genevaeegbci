#include "stdafx.h"
#include "MldaClassifier.h"
#include <memory.h>

#ifndef MATLABLIBRARY_H
#include "MatlabLibrary.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif


using namespace std;

CMldaClassifier::CMldaClassifier(string calibrationFileName)
: m_tmpProb(0)
//, m_tmpDistVect(0) // [UNUSED]
, m_classMeans1(0)
, m_classMeans2(0)
, m_choleskyInvese1(0)
, m_choleskyInvese2(0)
, m_tmpLogProb(0)
, m_fileExpectation(0)
, m_helpDistVect1(0)
, m_helpDistVect2(0)
, m_helpProduct1(0)
, m_helpProduct2(0)
{
    // Load numberOfFeatures from .mat file
    mxArray* matNoOfClass = CMatlabLibrary::loadMatVariable(calibrationFileName, "noOfClass");
    assert(NULL != matNoOfClass);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNoOfClass)));
    assert(1 == static_cast<int>(mxGetM(matNoOfClass)));

    double* dataElements = static_cast<double*>(mxGetData(matNoOfClass));
    m_noOfClass = static_cast<size_t>(dataElements[0]);


    // Load number of features from .mat file
    mxArray* matNoOfFeatures = CMatlabLibrary::loadMatVariable(calibrationFileName, "noOfFeatures");
    assert(NULL != matNoOfFeatures);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNoOfFeatures)));
    assert(1 == static_cast<int>(mxGetM(matNoOfFeatures)));

    dataElements = static_cast<double*>(mxGetData(matNoOfFeatures));
    m_noOfFeatures1 = static_cast<size_t>(dataElements[0]);


	// Load number of spectral features from .mat file
	mxArray* matNoOfSpectralFeatures = CMatlabLibrary::loadMatVariable(calibrationFileName, "noOfSpectralFeatures");
	assert(NULL != matNoOfSpectralFeatures);

	// Verify that the size is ok
	assert(1 == static_cast<int>(mxGetN(matNoOfSpectralFeatures)));
	assert(1 == static_cast<int>(mxGetM(matNoOfSpectralFeatures)));

	dataElements = static_cast<double*>(mxGetData(matNoOfSpectralFeatures));
	m_noOfFeatures2 = static_cast<size_t>(dataElements[0]);


    // Load expectation threshold from .mat file
    mxArray* matExpectationThreshold = CMatlabLibrary::loadMatVariable(calibrationFileName, "expectationThreshold");
    assert(NULL != matExpectationThreshold);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matExpectationThreshold)));
    assert(1 == static_cast<int>(mxGetM(matExpectationThreshold)));

    dataElements = static_cast<double*>(mxGetData(matExpectationThreshold));
    m_expectationThreshold = dataElements[0];


    // Load the initial feature means from .mat file
    mxArray* matClassMeans = CMatlabLibrary::loadMatVariable(calibrationFileName, "classMeans");
    assert(NULL != matClassMeans);

    // Store data in data Buffer
    assert(m_noOfFeatures1 == mxGetM(matClassMeans));
    assert(m_noOfClass == mxGetN(matClassMeans));

    dataElements = static_cast<double*>(mxGetData(matClassMeans));
    m_classMeans1 = new double*[m_noOfClass];
    // Place the class means for the last negative class to the index 0 and populate other 1:m_noOfClass-1 indices
    // with the means of the first m_noOfClass-1 means
    m_classMeans1[0] = new double[m_noOfFeatures1];
    memcpy(m_classMeans1[0], dataElements + (m_noOfClass - 1) * m_noOfFeatures1, m_noOfFeatures1 * sizeof(double));
    for (size_t cl = 1; cl<m_noOfClass; cl++)
    {
        m_classMeans1[cl] = new double[m_noOfFeatures1];
        memcpy(m_classMeans1[cl], dataElements + (cl - 1) * m_noOfFeatures1, m_noOfFeatures1 * sizeof(double));
    }


	// Load the initial spectral feature means from .mat file
	mxArray* matSpectralClassMeans = CMatlabLibrary::loadMatVariable(calibrationFileName, "spectralClassMeans");
	assert(NULL != matSpectralClassMeans);

	// Store data in data Buffer
	assert(m_noOfFeatures2 == mxGetM(matSpectralClassMeans));
	assert(m_noOfClass == mxGetN(matSpectralClassMeans));

	dataElements = static_cast<double*>(mxGetData(matSpectralClassMeans));
	m_classMeans2 = new double*[m_noOfClass];
	// Place the class means for the last negative class to the index 0 and populate other 1:m_noOfClass-1 indices
	// with the means of the first m_noOfClass-1 means
	m_classMeans2[0] = new double[m_noOfFeatures2];
	memcpy(m_classMeans2[0], dataElements + (m_noOfClass - 1) * m_noOfFeatures2, m_noOfFeatures2 * sizeof(double));
	for (size_t cl = 1; cl < m_noOfClass; cl++)
	{
		m_classMeans2[cl] = new double[m_noOfFeatures2];
		memcpy(m_classMeans2[cl], dataElements + (cl - 1) * m_noOfFeatures2, m_noOfFeatures2 * sizeof(double));
	}


    // Load the initial feature cov matrix from .mat file
    mxArray* matCovMatrix = CMatlabLibrary::loadMatVariable(calibrationFileName, "covMatrix");
    assert(NULL != matCovMatrix);

    // Store data in data Buffer
    assert(m_noOfFeatures1 == mxGetM(matCovMatrix));
    assert(m_noOfFeatures1 == mxGetN(matCovMatrix));

    dataElements = static_cast<double*>(mxGetData(matCovMatrix));

    // Calculate the inverse of the Cholesky factorization from the covariance matrix in place
    m_choleskyInvese1 = new CDoubleMatrix(m_noOfFeatures1, m_noOfFeatures1, dataElements);
    CMathLibrary::choleskyFactorization(m_choleskyInvese1[0]);
    CMathLibrary::invertLowerTriangularMatrixInPlace(m_choleskyInvese1[0]);


	// Load the initial spectral feature cov matrix from .mat file
	mxArray* matSpectralCovMatrix = CMatlabLibrary::loadMatVariable(calibrationFileName, "spectralCovMatrix");
	assert(NULL != matSpectralCovMatrix);

	// Store data in data Buffer
	assert(m_noOfFeatures2 == mxGetM(matSpectralCovMatrix));
	assert(m_noOfFeatures2 == mxGetN(matSpectralCovMatrix));

	dataElements = static_cast<double*>(mxGetData(matSpectralCovMatrix));

	// Calculate the inverse of the Cholesky factorization from the covariance matrix in place
	m_choleskyInvese2 = new CDoubleMatrix(m_noOfFeatures2, m_noOfFeatures2, dataElements);
	CMathLibrary::choleskyFactorization(m_choleskyInvese2[0]);
	CMathLibrary::invertLowerTriangularMatrixInPlace(m_choleskyInvese2[0]);


    m_tmpLogProb = new double[m_noOfClass];
    m_tmpProb = new double[m_noOfClass];
    // m_tmpDistVect = new double[m_noOfFeatures]; // [UNUSED]

    m_expectation = new CDoubleVector(m_noOfClass, 0.0);
    m_helpDistVect1 = new CDoubleVector(m_noOfFeatures1, 0.0);
	m_helpDistVect2 = new CDoubleVector(m_noOfFeatures2, 0.0);
    m_helpProduct1 = new CDoubleVector(m_noOfFeatures1, 0.0);
	m_helpProduct2 = new CDoubleVector(m_noOfFeatures2, 0.0);

    // Create and open EEG data log file
    const time_t t = time(0);
    struct tm * now = localtime(&t);

    string buffer(80, '\0');
    strftime(&buffer[0], buffer.size(), "..\\Log\\Expectation_%F_%H-%M-%S.txt", now);

    m_fileExpectation = fopen(buffer.c_str(), "w");
}

// [NOT SUPPORTED ANY MORE]
//CMldaClassifier::CMldaClassifier(string modelFileName, const double systemTimeStepSec)
//: m_tmpProb(0)
//, m_tmpDistVect(0)
//, m_classMeans(0)
//, m_choleskyInvese(0)
//, m_tmpLogProb(0)
//, m_fileExpectation(0)
//, m_helpDistVect(0)
//, m_helpProduct(0)
//{
//    double nclass;
//    double nfeat;
//    double expect_th;
//
//    // load file that contains the parameters
//    FILE* file = fopen(modelFileName.c_str(), "r");
//    fscanf(file, "%le", &nclass);
//    fscanf(file, "%le", &nfeat);
//    fscanf(file, "%le", &expect_th);
//
//    m_noOfClass = size_t(nclass);
//    m_noOfFeatures = size_t(nfeat);
//    m_expectationThreshold = expect_th;
//
//    m_tmpLogProb = new double[m_noOfClass];
//    m_expectation = new CDoubleVector(m_noOfClass, 0.0);
//
//    m_classMeans = new double*[m_noOfClass];
//    for (size_t cl = 0; cl < m_noOfClass; cl++)
//    {
//        m_classMeans[cl] = new double[m_noOfFeatures];
//        for (size_t ft = 0; ft < m_noOfFeatures; ft++)
//        {
//            fscanf(file, "%le", &m_classMeans[cl][ft]);
//        }
//    }
//
//    m_choleskyInvese = new CDoubleMatrix(m_noOfFeatures, m_noOfFeatures, 0.0);
//    double* covMatrixValues = m_choleskyInvese->getValues();
//    for (size_t i = 0; i < m_noOfFeatures; i++)
//    {
//        for (size_t j = 0; j < m_noOfFeatures; j++)
//        {
//            fscanf(file, "%le", &covMatrixValues[i * m_noOfFeatures + j]);
//        }
//    }
//    CMathLibrary::choleskyFactorization(m_choleskyInvese[0]);
//    CMathLibrary::invertLowerTriangularMatrixInPlace(m_choleskyInvese[0]);
//
//    fclose(file);
//
//    m_tmpProb = new double[m_noOfClass];
//    m_tmpDistVect = new double[m_noOfFeatures];
//
//    m_helpDistVect = new CDoubleVector(m_noOfFeatures, 0.0);
//    m_helpProduct = new CDoubleVector(m_noOfFeatures, 0.0);
//
//    // Create and open EEG data log file
//    const time_t t = time(0);
//    struct tm * now = localtime(&t);
//
//    string buffer(80, '\0');
//    strftime(&buffer[0], buffer.size(), "Expectation_%F_%H-%M-%S.txt", now);
//
//    m_fileExpectation = fopen(buffer.c_str(), "w");
//}

CMldaClassifier::CMldaClassifier(const size_t noOfClass
    , const size_t noOfFeatures1
	, const size_t noOfFeatures2
    , const double expectationThreshold
    , const CDoubleMatrix classMeans1
	, const CDoubleMatrix classMeans2
    , const CDoubleMatrix covMatrix1
	, const CDoubleMatrix covMatrix2)
    : m_noOfClass(noOfClass)
    , m_noOfFeatures1(noOfFeatures1)
	, m_noOfFeatures2(noOfFeatures2)
    , m_expectationThreshold(expectationThreshold)
    , m_tmpProb(0)
//    , m_tmpDistVect(0) // [UNUSED]
    , m_classMeans1(0)
	, m_classMeans2(0)
    , m_choleskyInvese1(new CDoubleMatrix(covMatrix1))
	, m_choleskyInvese2(new CDoubleMatrix(covMatrix2))
    , m_tmpLogProb(0)
    , m_fileExpectation(0)
    , m_helpDistVect1(0)
	, m_helpDistVect2(0)
    , m_helpProduct1(0)
	, m_helpProduct2(0)
{
    // Verify that the class means matrix has the correct number of rows == number of features
    if (classMeans1.getRowSize() != noOfFeatures1)
    {
        throw runtime_error("number of features in classMeans1 is not consistent");
    }

	// Verify that the class means matrix has the correct number of rows == number of features
	if (classMeans2.getRowSize() != noOfFeatures2)
	{
		throw runtime_error("number of features in classMeans2 is not consistent");
	}

    // Verify that the class means matrix has the correct number of columns == number of classes
    if (classMeans1.getColumnSize() != noOfClass)
    {
        throw runtime_error("number of classes in classMeans1 is not consistent");
    }

	// Verify that the class means matrix has the correct number of columns == number of classes
	if (classMeans2.getColumnSize() != noOfClass)
	{
		throw runtime_error("number of classes in classMeans2 is not consistent");
	}

    // Verify that the covariance matrix has the correct rows and columns == number of features
    if (covMatrix1.getRowSize() != noOfFeatures1 || covMatrix1.getColumnSize() != noOfFeatures1)
    {
        throw runtime_error("number of features in covMatrix1 is not consistent");
    }

	// Verify that the covariance matrix has the correct rows and columns == number of features
	if (covMatrix2.getRowSize() != noOfFeatures2 || covMatrix2.getColumnSize() != noOfFeatures2)
	{
		throw runtime_error("number of features in covMatrix2 is not consistent");
	}

    m_tmpLogProb = new double[m_noOfClass];
    m_expectation = new CDoubleVector(m_noOfClass, 0.0);

    m_classMeans1 = new double*[m_noOfClass];
    for (size_t cl = 0; cl < m_noOfClass; cl++)
    {
        m_classMeans1[cl] = new double[m_noOfFeatures1];
        memcpy(m_classMeans1[cl], classMeans1.getColumnAt(cl), m_noOfFeatures1 * sizeof(double));
    }

	m_classMeans2 = new double*[m_noOfClass];
	for (size_t cl = 0; cl < m_noOfClass; cl++)
	{
		m_classMeans2[cl] = new double[m_noOfFeatures2];
		memcpy(m_classMeans2[cl], classMeans2.getColumnAt(cl), m_noOfFeatures2 * sizeof(double));
	}

    CMathLibrary::choleskyFactorization(m_choleskyInvese1[0]);
    CMathLibrary::invertLowerTriangularMatrixInPlace(m_choleskyInvese1[0]);

	CMathLibrary::choleskyFactorization(m_choleskyInvese2[0]);
	CMathLibrary::invertLowerTriangularMatrixInPlace(m_choleskyInvese2[0]);

    m_tmpProb = new double[m_noOfClass];
    // m_tmpDistVect = new double[m_noOfFeatures]; // [UNUSED]

    m_helpDistVect1 = new CDoubleVector(m_noOfFeatures1, 0.0);
	m_helpDistVect2 = new CDoubleVector(m_noOfFeatures2, 0.0);
    m_helpProduct1 = new CDoubleVector(m_noOfFeatures1, 0.0);
	m_helpProduct2 = new CDoubleVector(m_noOfFeatures2, 0.0);

    // Create and open EEG data log file
    const time_t t = time(0);
    struct tm * now = localtime(&t);

    string buffer(80, '\0');
    strftime(&buffer[0], buffer.size(), "Expectation_%F_%H-%M-%S.txt", now);

    m_fileExpectation = fopen(buffer.c_str(), "w");
}

CMldaClassifier::~CMldaClassifier()
{
    if (m_tmpProb != 0)
    {
        delete[] m_tmpProb;
        m_tmpProb = 0;
    }

	// [UNUSED]
    //if (m_tmpDistVect != 0)
    //{
    //    delete[] m_tmpDistVect;
    //    m_tmpDistVect = 0;
    //}

    if (m_tmpLogProb != 0)
    {
        delete[] m_tmpLogProb;
        m_tmpLogProb = 0;
    }

    if (m_expectation != 0)
    {
        delete m_expectation;
        m_expectation = 0;
    }

    if (m_classMeans1 != 0)
    {
        for (size_t i = 0; i < m_noOfClass; i++)
        {
            delete[] m_classMeans1[i];
			m_classMeans1[i] = 0;
        }
        delete[] m_classMeans1;
		m_classMeans1 = 0;
    }

	if (m_classMeans2 != 0)
	{
		for (size_t i = 0; i < m_noOfClass; i++)
		{
			delete[] m_classMeans2[i];
			m_classMeans2[i] = 0;
		}
		delete[] m_classMeans2;
		m_classMeans2 = 0;
	}

    if (m_choleskyInvese1 != 0)
    {
        delete m_choleskyInvese1;
        m_choleskyInvese1 = 0;
    }

	if (m_choleskyInvese2 != 0)
	{
		delete m_choleskyInvese2;
		m_choleskyInvese2 = 0;
	}

    if (m_fileExpectation != 0)
    {
        fclose(m_fileExpectation);
        m_fileExpectation = 0;
    }

    if (m_helpDistVect1 != 0)
    {
        delete m_helpDistVect1;
        m_helpDistVect1 = 0;
    }

	if (m_helpDistVect2 != 0)
	{
		delete m_helpDistVect2;
		m_helpDistVect2 = 0;
	}

    if (m_helpProduct1 != 0)
    {
        delete m_helpProduct1;
        m_helpProduct1 = 0;
    }
	
	if (m_helpProduct2 != 0)
	{
		delete m_helpProduct2;
		m_helpProduct2 = 0;
	}
}

size_t CMldaClassifier::getNoOfFeatures1()
{
    return m_noOfFeatures1;
}

size_t CMldaClassifier::getNoOfFeatures2()
{
	return m_noOfFeatures2;
}

size_t CMldaClassifier::getNoOfClass()
{
    return m_noOfClass;
}

double CMldaClassifier::calcCovInvProd1(CDoubleVector* classInput)
{
    //Y = L^-1 * X
    CMathLibrary::multLowerTriangularMatrixVector(m_helpProduct1, m_choleskyInvese1[0], classInput[0]);

    //REZ = Y'*Y
    return CMathLibrary::multVectorDotVector(m_helpProduct1[0], m_helpProduct1[0]);
}

double CMldaClassifier::calcCovInvProd2(CDoubleVector* classInput)
{
	//Y = L^-1 * X
	CMathLibrary::multLowerTriangularMatrixVector(m_helpProduct2, m_choleskyInvese2[0], classInput[0]);

	//REZ = Y'*Y
	return CMathLibrary::multVectorDotVector(m_helpProduct2[0], m_helpProduct2[0]);
}

void CMldaClassifier::calcExpectation(CDoubleVector* expectation, const CDoubleVector* const featureVector1, const CDoubleVector* const featureVector2)
{
    double* helpDistVectValues1 = m_helpDistVect1->getValues();
	double* helpDistVectValues2 = m_helpDistVect2->getValues();
    double* featureVectorValues1 = featureVector1->getValues();
	double* featureVectorValues2 = featureVector2->getValues();
    double* expectationValues = expectation->getValues();
    for (size_t cl = 0; cl < m_noOfClass; cl++)
    {
		// Model1 distance
        for (size_t ft = 0; ft < m_noOfFeatures1; ft++)
        {
            helpDistVectValues1[ft] = featureVectorValues1[ft] - m_classMeans1[cl][ft];
        }

		// Model2 distance
		for (size_t ft = 0; ft < m_noOfFeatures2; ft++)
		{
			helpDistVectValues2[ft] = featureVectorValues2[ft] - m_classMeans2[cl][ft];
		}

		// Combine models
		m_tmpProb[cl] = -(calcCovInvProd1(m_helpDistVect1) + calcCovInvProd2(m_helpDistVect2)) / 2;
    }

    // INIT
    memset(expectationValues, 0, m_noOfClass * sizeof(double));

    // LOOP for calculation of expectations
    double sum;
    for (size_t cl = 0; cl < m_noOfClass; cl++)
    {
        sum = 0.0;
        for (size_t na = 0; na < m_noOfClass; na++)
        {
            m_tmpLogProb[na] = m_tmpProb[na] - m_tmpProb[cl];
            sum = sum + exp(m_tmpLogProb[na]);
        }

        expectationValues[cl] = 1.0 / sum;
    }
}


size_t CMldaClassifier::process(const CDoubleVector * const currentFeature1, const CDoubleVector * const currentFeature2)
{
    calcExpectation(m_expectation, currentFeature1, currentFeature2);
    double* expectationValues = m_expectation->getValues();

    if (m_logExpectationValues)
    {
        for (size_t cl = 0; cl < m_noOfClass; cl++)
        {
            fprintf(m_fileExpectation, "%f, ", expectationValues[cl]);
        }
        fprintf(m_fileExpectation, "\n");
    }

    /// Look if any of the probabilities of the relevant classes is above the threshold
    size_t outState = 0;
    for (size_t cl = 1; cl < m_noOfClass; cl++)
    {
        if (expectationValues[cl] > m_expectationThreshold)
        {
            outState = cl;
        }
    }

    return outState;
}