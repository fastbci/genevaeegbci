#include "ChannelInterpolationAlgorithm.h"
#include <assert.h>

#ifndef DOUBLEMATRIX_H
#include "DoubleMatrix.h"
#endif

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

#ifndef SIZETVECTOR_H
#include "SizeTVector.h"
#endif

#ifndef MATHLIBRARY_H
#include "MathLibrary.h"
#endif

#ifndef MATLABLIBRARY_H
#include "../GenevaEegBci/MatlabLibrary.h"
#endif

using namespace std;



CChannelInterpolationAlgorithm::CChannelInterpolationAlgorithm(string calibrationFileName)
: m_interpolationWeights(0)
, m_interpolationChannels(0)
, m_interpolationNeighbours(0)
, m_helperVector(0)
, m_numberOfInterpolatedChannels(0)
, m_numberOfNeuralChannels(0)
, m_numberOfInterpolations(0)
{
    // Load numberOfNeuralChannels from .mat file
    mxArray* matNumberOfNeuralChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfNeuralChannels");
    assert(NULL != matNumberOfNeuralChannels);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfNeuralChannels)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfNeuralChannels)));

    double* dataElements = static_cast<double*>(mxGetData(matNumberOfNeuralChannels));
    m_numberOfNeuralChannels = static_cast<size_t>(dataElements[0]);


    // Load numberOfInterpolations from .mat file
    mxArray* matNumberOfInterpolations = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfInterpolations");
    assert(NULL != matNumberOfInterpolations);

    // Verify that the size is ok
    assert(1 == static_cast<int>(mxGetN(matNumberOfInterpolations)));
    assert(1 == static_cast<int>(mxGetM(matNumberOfInterpolations)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfInterpolations));
    m_numberOfInterpolations = static_cast<size_t>(dataElements[0]);

    if (m_numberOfInterpolations == 0)
    {
        return;
    }

    // Load the numberOfInterpolatedChannels from .mat file
    mxArray* matNumberOfInterpolatedChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "numberOfInterpolatedChannels");
    assert(NULL != matNumberOfInterpolatedChannels);

    // Verify that the size is ok
    assert(3 > mxGetM(matNumberOfInterpolatedChannels));
    assert(1 == static_cast<int>(mxGetN(matNumberOfInterpolatedChannels)));

    dataElements = static_cast<double*>(mxGetData(matNumberOfInterpolatedChannels));
    m_numberOfInterpolatedChannels = new CSizeTVector(m_numberOfInterpolations);
    for (size_t interpolationIndex = 0; interpolationIndex < m_numberOfInterpolations; ++interpolationIndex)
    {
        m_numberOfInterpolatedChannels->getValues()[interpolationIndex] = static_cast<size_t>(dataElements[interpolationIndex]);
    }


    m_interpolationChannels = new CSizeTVector*[m_numberOfInterpolations];
    m_interpolationWeights = new CDoubleMatrix*[m_numberOfInterpolations];
    m_interpolationNeighbours = new CDoubleVector*[m_numberOfInterpolations];
    m_helperVector = new CDoubleVector*[m_numberOfInterpolations];


    mxArray* matInterpolationChannels = CMatlabLibrary::loadMatVariable(calibrationFileName, "interpolationChannels");
    assert(NULL != matInterpolationChannels);

    mxArray* matInterpolationWeights = CMatlabLibrary::loadMatVariable(calibrationFileName, "interpolationWeights");
    assert(NULL != matInterpolationWeights);

    mxArray* matInterpolationNeighbours = CMatlabLibrary::loadMatVariable(calibrationFileName, "interpolationNeighbours");
    assert(NULL != matInterpolationNeighbours);

    mxArray* channelCellPointer = 0;
    mxArray* weightCellPointer = 0;
    mxArray* neighbourCellPointer = 0;
    size_t noOfChannels = 0;
    for (size_t interpolationIndex = 0; interpolationIndex < m_numberOfInterpolations; ++interpolationIndex)
    {
        noOfChannels = m_numberOfInterpolatedChannels->getValues()[interpolationIndex];


        channelCellPointer = mxGetCell(matInterpolationChannels, interpolationIndex);
        assert(NULL != channelCellPointer);

        // Verify that the size is ok
        assert(noOfChannels == mxGetM(channelCellPointer));
        assert(1 == static_cast<int>(mxGetN(channelCellPointer)));

        dataElements = static_cast<double*>(mxGetData(channelCellPointer));
        m_interpolationChannels[interpolationIndex] = new CSizeTVector(noOfChannels);
        for (size_t channelIndex = 0; channelIndex < noOfChannels; ++channelIndex)
        {
            m_interpolationChannels[interpolationIndex]->getValues()[channelIndex] = static_cast<size_t>(dataElements[channelIndex]);
        }
        channelCellPointer = 0;


        neighbourCellPointer = mxGetCell(matInterpolationNeighbours, interpolationIndex);
        assert(NULL != neighbourCellPointer);

        // Verify that the size is ok
        assert(noOfChannels == mxGetM(neighbourCellPointer));
        assert(1 == static_cast<int>(mxGetN(neighbourCellPointer)));

        dataElements = static_cast<double*>(mxGetData(neighbourCellPointer));
        m_interpolationNeighbours[interpolationIndex] = new CDoubleVector(noOfChannels, dataElements);
        neighbourCellPointer = 0;


        // Load the projection matrix from .mat file
        weightCellPointer = mxGetCell(matInterpolationWeights, interpolationIndex);
        assert(NULL != weightCellPointer);

        // Verify that the size is ok
        assert(noOfChannels == mxGetM(weightCellPointer));
        assert(m_numberOfNeuralChannels == mxGetN(weightCellPointer));

        dataElements = static_cast<double*>(mxGetData(weightCellPointer));
        m_interpolationWeights[interpolationIndex] = new CDoubleMatrix(noOfChannels, m_numberOfNeuralChannels, dataElements);

        m_helperVector[interpolationIndex] = new CDoubleVector(noOfChannels, 0.0);
    }
}


CChannelInterpolationAlgorithm::~CChannelInterpolationAlgorithm()
{
    if (m_interpolationWeights == 0)
    {
        for (size_t interpolationIndex = 0; interpolationIndex < m_numberOfInterpolations; ++interpolationIndex)
        {
            delete m_interpolationWeights[interpolationIndex];
            m_interpolationWeights[interpolationIndex] = 0;
        }
        delete[] m_interpolationWeights;
        m_interpolationWeights = 0;
    }

    if (m_interpolationChannels == 0)
    {
        for (size_t interpolationIndex = 0; interpolationIndex < m_numberOfInterpolations; ++interpolationIndex)
        {
            delete m_interpolationChannels[interpolationIndex];
            m_interpolationChannels[interpolationIndex] = 0;
        }
        delete[] m_interpolationChannels;
        m_interpolationChannels = 0;
    }

    if (m_interpolationNeighbours == 0)
    {
        for (size_t interpolationIndex = 0; interpolationIndex < m_numberOfInterpolations; ++interpolationIndex)
        {
            delete m_interpolationNeighbours[interpolationIndex];
            m_interpolationNeighbours[interpolationIndex] = 0;
        }
        delete[] m_interpolationNeighbours;
        m_interpolationNeighbours = 0;
    }

    if (m_helperVector == 0)
    {
        for (size_t interpolationIndex = 0; interpolationIndex < m_numberOfInterpolations; ++interpolationIndex)
        {
            delete m_helperVector[interpolationIndex];
            m_helperVector[interpolationIndex] = 0;
        }
        delete[] m_helperVector;
        m_helperVector = 0;
    }

    if (m_numberOfInterpolatedChannels == 0)
    {
        delete m_numberOfInterpolatedChannels;
        m_numberOfInterpolatedChannels = 0;
    }
}


void CChannelInterpolationAlgorithm::process(CDoubleVector* const interpolatedSignal, const CDoubleVector * const neuralSignal)
{
    assert(neuralSignal->getSize() == m_numberOfNeuralChannels);
    assert(interpolatedSignal->getSize() == m_numberOfNeuralChannels);
    memcpy(interpolatedSignal->getValues(), neuralSignal->getValues(), m_numberOfNeuralChannels * sizeof(double));

    for (size_t interpolationIndex = 0; interpolationIndex < m_numberOfInterpolations; ++interpolationIndex)
    {
        CMathLibrary::multMatrixVector(m_helperVector[interpolationIndex], m_interpolationWeights[interpolationIndex][0], interpolatedSignal[0]);

        size_t noOfChannels = m_numberOfInterpolatedChannels->getValues()[interpolationIndex];
        for (size_t channelIndex = 0; channelIndex < noOfChannels; ++channelIndex)
        {
            interpolatedSignal->getValues()[m_interpolationChannels[interpolationIndex]->getValues()[channelIndex]] = m_helperVector[interpolationIndex]->getValues()[channelIndex] / m_interpolationNeighbours[interpolationIndex]->getValues()[channelIndex];
        }
    }
}


size_t CChannelInterpolationAlgorithm::getNumberOfNeuralChannels()
{
    return m_numberOfNeuralChannels;
}
