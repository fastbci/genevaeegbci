#ifndef SPECTRALMPLITUDEALGORITHM_H
#define SPECTRALMPLITUDEALGORITHM_H

#include <vector>
#include "DataBuffer.h"

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

class CDoubleMatrix;
class CFftwTransform;

/**
* @brief Algorithm that calculates spectral amplitudes of a time series
* @author Tom
*
* Implementation detail: The module stores the input data in a buffer and
* generates spectral amplitudes at a fixed frequency.
*/
class CSpectralAmplitudeAlgorithm
{

public:
	/// Type of the anti-aliasing window.
	enum WindowType { NONE, BLACKMAN, HANN, HAMMING };

public:
	/**
	* Constructor.
	*
	* @param windowType				Type of the anti-aliasing window used
	* @param fourierWindowLength    Length of the data window used to calculate the fourier transform
	* @param stepSize				Frequency at which to calculate the amplitude estimates
	* @param numberOfChannels       Number of signal channels
	* @param samplingFrequency		Sampling frequency of the input signal
	* @param normFactors			Matrix containing the normalization factors
	*/
	CSpectralAmplitudeAlgorithm(const WindowType& windowType
							  , const size_t& fourierWindowLength
							  , const size_t& stepSize
							  , const size_t& numberOfChannels
							  , const size_t& samplingFrequency
							  , const CDoubleMatrix& normFactors);

	/// Destructor
	virtual ~CSpectralAmplitudeAlgorithm();

public:
	/**
	* For every windowSize-th step, the process function outputs an estimate of the spectral amplitude.
	*
	* @param output        vetor that contains the spectral amplitudes
	* @param inputFrame    input that contains a signal sample
	*/
	void process(std::vector<CDoubleMatrix*>& output, CDoubleVector* inputFrame);

	/**
	* Initialize or reset the algorithm by filling up its buffer.
	*
	* @param initBuffer        pointer to an array of signal samples needed to initialize the buffer
	*/
	void reset(CDoubleVector* initBuffer);

private:
	/// Length of the FFT window.
	const size_t m_fourierWindowLength;

	/// Window coefficients used for windowing of FFT windows. 
	CDoubleVector m_windowCoefficients;

	/// Step to move the FFT window (within the cutting window)
	const size_t m_stepSize;

	/// buffers channel values between time steps
	CDataBuffer m_buffer;

	/// counts how much frames have arrived since last smoothing operation
	size_t m_currentStep;

	/// number of frequency bins across the spectrum
	const size_t m_numberOfBins;

	/// class that performs the FFT calculation
	CFftwTransform* m_fourierTransform;

	/// helper class that holds the input to the FFT calculation
	CDoubleVector m_fourierInput;

	/// helper class that holds the real output of the FFT calculation
	CDoubleVector m_fourierRealOutput;

	/// helper class that holds the imaginary output of the FFT calculation
	CDoubleVector m_fourierImagOutput;

	/// sampling frequency of the input signal
	const size_t  m_samplingFrequency;

	/// normalization for each frequency bin
	double**      m_normFactors;

	/// number of channels of the input signal
	const double  m_numberOfChannels;
};

#endif // SPECTRALMPLITUDEALGORITHM_H



