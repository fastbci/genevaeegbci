#ifndef DOUBLEVECTOR_H
#define DOUBLEVECTOR_H

#include <iostream>

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
@brief class witch encapsulates a Vector of double values.
*/
class DLL_EXPORT CDoubleVector
{
public:
    //Constructor
    /**
    @param size the size of the vector
    */
    CDoubleVector(const size_t& size);
    /**
    @param size the size of the vector
    @param initValue the value with all cell of the vector are initialized
    */
    CDoubleVector(const size_t& size, const double& initValue);
    /**
    size have to be the the %size of values 
    @param size the size of the vector
    @param values a double pointer with the values to copy in the encapsulated double vector
    */
    CDoubleVector(const size_t& size, const double* const values);
    /**
    Copy constructor
    @param other vector to copy
    */
    CDoubleVector(const CDoubleVector& other);

    //Destructor
    virtual ~CDoubleVector();


public:
    /**
    @return length of the vector
    */
    size_t getSize() const;
    
    /**
    @return pointer to the encapsulated vector
    */
    double* const getValues() const;
    
    /**
    multiply factor to all values stored in the vector
    @param factor value witch is multiplied to all values of the vector
    */
    void mult(const double& factor);

    /**
    adds a vector to this vector (this[i] = this[i] + other[i])
    @param other vector to add
    */
    void add(const CDoubleVector& other);

    /**
    adds a vector with a factor to this vector (this[i] = this[i] + (factor * other[i]))
    @param factor the factor
    @param other vector to add
    */
    void addWeighted(const double& factor, const CDoubleVector& other);

    /**
    subtract a other vector from this vector (this[i] = this[i] - other[i])
    @param other vector to subtract 
    */
    void subtract(const CDoubleVector& other);

	/**
	find minimum and maximum values
	@param minValue minimum value in the vector
	@param maxValue maximum value in the vector
	*/
	void findMinMax(double& minValue, double& maxValue); 
    
    /**
    counting of rows starts with row 0.
    */ 
	double& operator[](const size_t& position);
    
    /**
    * assignment operator
    */
    const double& operator[](const size_t& position) const;

    /**
    * assignment operator
    */
    CDoubleVector& operator=(const CDoubleVector& other);
    
    /**
    * Tests for equality. True if vectors have the same size and component wise
    * the same values.
    *
    * @param other the other vector
    * @return true if equal
    */
    bool operator==(const CDoubleVector& other); 

private:
    const size_t m_size;
    double* m_values;
};

#endif // DOUBLEVECTOR_H

