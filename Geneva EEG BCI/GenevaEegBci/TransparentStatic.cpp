#include "stdafx.h"
#include "TransparentStatic.h"

IMPLEMENT_DYNAMIC(CTransparentStatic, CStatic)

/**
* @brief Transparent static label constructor
*/
CTransparentStatic::CTransparentStatic()
{
}

/**
* @brief Transparent static label destructor
*/
CTransparentStatic::~CTransparentStatic()
{
}

BEGIN_MESSAGE_MAP(CTransparentStatic, CStatic)
    ON_MESSAGE(WM_SETTEXT,OnSetText)
    ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()

/**
* @brief Transparent static label message handlers
*/
LRESULT CTransparentStatic::OnSetText(WPARAM wParam,LPARAM lParam)
{
    LRESULT Result = Default();
    Invalidate();
    UpdateWindow();
    return Result;
}

/**
* @brief Transparent static label message handlers
*/
HBRUSH CTransparentStatic::CtlColor(CDC* pDC, UINT /*nCtlColor*/)
{
    pDC->SetBkMode(TRANSPARENT);
    return (HBRUSH)GetStockObject(NULL_BRUSH);
}