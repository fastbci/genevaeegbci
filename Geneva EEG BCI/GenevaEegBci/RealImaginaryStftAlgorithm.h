#ifndef REALIMAGINARYATFTALGORITHM_H
#define REALIMAGINARYATFTALGORITHM_H

#include <vector>
#include "DataBuffer.h"

#ifndef DOUBLEVECTOR_H
#include "DoubleVector.h"
#endif

class CDoubleMatrix;
class CFftwTransform;

#ifndef DLL_EXPORT
    #define DLL_EXPORT __declspec(dllexport)
#endif

/**
* @brief Algorithm that calculates spectral amplitudes of a time series
* @author Tom
*
* Implementation detail: The module stores the input data in a buffer and
* generates spectral amplitudes at a fixed frequency.
*/
class DLL_EXPORT CRealImaginaryStftAlgorithm
{

public:
	/// Type of the anti-aliasing window.
	enum WindowType { NONE, BLACKMAN, HANN, HAMMING };

public:
	/**
	* Constructor.
	*
	* @param windowType				Type of the anti-aliasing window used
	* @param fourierWindowLength    Length of the data window used to calculate the fourier transform
	* @param stepSize				Frequency at which to calculate the amplitude estimates
	* @param numberOfChannels       Number of signal channels
	* @param samplingFrequency		Sampling frequency of the input signal
	*/
	CRealImaginaryStftAlgorithm(const WindowType& windowType
		, const size_t& fourierWindowLength
		, const size_t& stepSize
		, const size_t& numberOfChannels
		, const size_t& samplingFrequency);

    /**
    * Constructor that uses a matlab file for initialization.
    *
    * @param calibrationFileName        Matlab file that holds the parameters needed to initialize the EOG correction algorithm
    */
    CRealImaginaryStftAlgorithm(std::string calibrationFileName);

	/// Destructor
	virtual ~CRealImaginaryStftAlgorithm();

public:
	/**
	* For every windowSize-th step, the process function outputs an estimate of the spectral amplitude.
	*
	* @param outputReal			vetor that contains the real part of the fourier transform
	* @param outputImaginary    vetor that contains the imaginary part of the fourier transform
	* @param inputFrame			input that contains a signal sample
	*/
	void process(std::vector<CDoubleMatrix*>& outputReal, std::vector<CDoubleMatrix*>& outputImaginary, CDoubleVector* inputFrame);

    /**
    * Add the input frame to the buffer.
    *
    * @param inputFrame			input that contains a signal sample
    */
    const bool addToBuffer(CDoubleVector* inputFrame);
    
    /**
	* Initialize or reset the algorithm by filling up its buffer.
	*
	* @param initBuffer        pointer to an array of signal samples needed to initialize the buffer
	*/
	void reset(CDoubleVector** initBuffer);

    /**
    * Flush the internal buffer of the class.
    */
    void flushBuffer();

	/// Returns the number of frequency bins
	size_t getNumberOfBins();


private:
    /// Initialize the internal parameters of the CRealImaginaryStftAlgorithm
    void initialize(CRealImaginaryStftAlgorithm::WindowType windowType);


private:
	/// Length of the FFT window.
	size_t m_fourierWindowLength;

	/// Window coefficients used for windowing of FFT windows. 
	CDoubleVector* m_windowCoefficients;

	/// Step to move the FFT window (within the cutting window)
	size_t m_stepSize;

	/// buffers channel values between time steps
	CDataBuffer* m_buffer;

	/// counts how much frames have arrived since last smoothing operation
	size_t m_currentStep;

	/// number of frequency bins across the spectrum
	size_t m_numberOfBins;

	/// class that performs the FFT calculation
	CFftwTransform* m_fourierTransform;

	/// helper class that holds the input to the FFT calculation
	CDoubleVector* m_fourierInput;

	/// helper class that holds the real output of the FFT calculation
	CDoubleVector* m_fourierRealOutput;

	/// helper class that holds the imaginary output of the FFT calculation
	CDoubleVector* m_fourierImagOutput;

	/// sampling frequency of the input signal
	size_t  m_samplingFrequency;

	/// number of channels of the input signal
	size_t  m_numberOfChannels;

    /// is the buffer filled with data
    bool  m_isBufferFilled;

    /// number of signal samples in the buffer
    size_t  m_numberOfBufferedSamples;
};

#endif // REALIMAGINARYATFTALGORITHM_H