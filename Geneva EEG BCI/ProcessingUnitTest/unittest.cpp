#include "stdafx.h"
#include "CppUnitTest.h"
#include <mat.h>  // MAT-File API Library
#include <thread>
#include <chrono>

#ifndef SIZETVECTOR_H
#include "../GenevaEegBci/SizeTVector.h"
#endif

#ifndef DOUBLEVECTOR_H
#include "../GenevaEegBci/DoubleVector.h"
#endif

#ifndef DOUBLEMATRIX_H
#include "../GenevaEegBci/DoubleMatrix.h"
#endif

#ifndef MATLABLIBRARY_H
#include "../GenevaEegBci/MatlabLibrary.h"
#endif

#include "../GenevaEegBci/CarAlgorithm.h"
#include "../GenevaEegBci/RealImaginaryStftAlgorithm.h"
#include "../GenevaEegBci/AdaptiveFeatureNormalizationAlgorithm.h"
#include "../GenevaEegBci/EegData.h"
#include "../GenevaEegBci/SerialSGolayFilterAlgorithm.h"
#include "../GenevaEegBci/FeatureTimeSelector.h"
#include "../GenevaEegBci/MldaClassifier.h"
#include "../GenevaEegBci/CorticalSourceReconstruction.h"
#include "../GenevaEegBci/DenoisingAlgorithm.h"
#include "../GenevaEegBci/FrequencyBandSelector.h"
#include "../GenevaEegBci/SpectralSourceReconstruction.h"
#include "../GenevaEegBci/EogCorrectionAlgorithm.h"
#include "../GenevaEegBci/ChannelInterpolationAlgorithm.h"
#include "../GenevaEegBci/SerialRationalTransferFunctionFilter.h"
#include "../GenevaEegBci/ConsolidateDecisionAlgorithm.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace ProcessingUnitTest
{        
    TEST_CLASS(UnitTestCarAlgorithm)
    {
    public:
        TEST_METHOD(UnitTestCarAlgorithmProcess)
        {
            // Input and output data data
            const size_t noChan2Use = 6;
            const size_t referenceChannel = 5;
            double input[noChan2Use] = { 1, 5, 1, 2, 1, 0 };
            double expected[noChan2Use] = { 0, 4, 0, 1, 1, 0 };
            double actual[noChan2Use];

            // Channels used for the Common Average Reference calculation
            const size_t noOfCarChannels = 3;
            size_t carChannels[noOfCarChannels] = { 0, 2, 3 };

            // Channels on which CAR is applied
            const size_t noOfSelectedChannels = 4;
            size_t selectedChannels[noOfSelectedChannels] = { 0, 1, 2, 3};

            // Object construction
            CCarAlgorithm carAlgorithm(noChan2Use, noOfCarChannels, noOfSelectedChannels, carChannels, selectedChannels, referenceChannel);

            // Call process method and check results
            carAlgorithm.process(actual, input);
            Assert::AreEqual(expected[0], actual[0]);
            Assert::AreEqual(expected[1], actual[1]);
            Assert::AreEqual(expected[2], actual[2]);
            Assert::AreEqual(expected[3], actual[3]);
            Logger::WriteMessage("Unit Test Car Algorithm with { 1, 1, 1 }\t--> Ok");

            // Input and output data data
            double input1[noChan2Use] = { 0.5, 0.5, 0.5, 1.0 };
            double expected1[noChan2Use] = { 0, 0, 0, 0.5 };
            double actual1[noChan2Use];

            // Call process method and check results
            carAlgorithm.process(actual1, input1);
            Assert::AreEqual(expected1[0], actual1[0]);
            Assert::AreEqual(expected1[1], actual1[1]);
            Assert::AreEqual(expected1[2], actual1[2]);
            Logger::WriteMessage("Unit Test Car Algorithm with { 0.5, 0.5, 0.5 }\t--> Ok");

            // Input and output data data
            double input2[noChan2Use] = { 0.5, 0, 2, 1.5 };
            double expected2[noChan2Use] = { -0.5, -1, 1, 0.5 };

            // Call process method and check results
            carAlgorithm.process(actual1, input2);
            Assert::AreEqual(expected2[0], actual1[0]);
            Assert::AreEqual(expected2[1], actual1[1]);
            Assert::AreEqual(expected2[2], actual1[2]);
            Logger::WriteMessage("Unit Test Car Algorithm with {0.5, 1.5, 1}\t--> Ok");
        }
    };

    TEST_CLASS(UnitTestFeatureTimeSelector)
    {
    public:

        TEST_METHOD(UnitTestFeatureTimeSelectorProcess)
        {

			// Tolerance
			const double testTolerance = 1.0e-8;

			// Input file
			string fileName = "..\\TestData\\testData_FeatureTimeSelector.mat";

			// Load numberOfFeatures from .mat file
			mxArray *matNumberOfFeatures = CMatlabLibrary::loadMatVariable(fileName, "numberOfFeatures");
			Assert::IsNotNull(matNumberOfFeatures);

			// Verify that the size is ok
			Assert::AreEqual(1, static_cast<int>(mxGetN(matNumberOfFeatures)));
			Assert::AreEqual(1, static_cast<int>(mxGetM(matNumberOfFeatures)));

			double* dataElements = static_cast<double*>(mxGetData(matNumberOfFeatures));
			const size_t numberOfFeatures = static_cast<size_t>(dataElements[0]);


			// Load recursiveLength from .mat file
			mxArray* matNumberOfTaps = CMatlabLibrary::loadMatVariable(fileName, "numberOfTaps");
			Assert::IsNotNull(matNumberOfTaps);

			// Verify that the size is ok
			Assert::AreEqual(1, static_cast<int>(mxGetN(matNumberOfTaps)));
			Assert::AreEqual(1, static_cast<int>(mxGetM(matNumberOfTaps)));

			dataElements = static_cast<double*>(mxGetData(matNumberOfTaps));
			const size_t numberOfTaps = static_cast<size_t>(dataElements[0]);


			// Load the initial feature means from .mat file
			mxArray* matFeatureTaps = CMatlabLibrary::loadMatVariable(fileName, "featureTaps");
			Assert::IsNotNull(matFeatureTaps);

			// Store data in data Buffer
			Assert::AreEqual(numberOfTaps, mxGetM(matFeatureTaps));
			Assert::AreEqual(1, static_cast<int>(mxGetN(matFeatureTaps)));

			dataElements = static_cast<double*>(mxGetData(matFeatureTaps));
			CSizeTVector* featureTaps = new CSizeTVector(numberOfTaps);
			for (size_t tapIndex = 0; tapIndex < numberOfTaps; ++tapIndex)
			{
				featureTaps->getValues()[tapIndex] = static_cast<size_t>(dataElements[tapIndex]);
			}


			// Create the clas that is being unitested
			CFeatureTimeSelector* featureTimeSelector = new CFeatureTimeSelector(featureTaps
																			   , numberOfFeatures);


			///////////////////////////////////////////////////////////////////////////////////////////////
			// Read the test data

			// Load test data from .mat file
			mxArray* matTestData = CMatlabLibrary::loadMatVariable(fileName, "data");
			Assert::IsNotNull(matTestData);

			size_t noOfDim = mxGetNumberOfDimensions(matTestData);
			const size_t* dimTestData = new size_t[noOfDim];
			dimTestData = mxGetDimensions(matTestData);

			Assert::AreEqual(3, static_cast<int>(noOfDim));
			Assert::AreEqual(numberOfFeatures, dimTestData[0]);
			const size_t numberOfSamples = dimTestData[1];
			const size_t numberOfTests = dimTestData[2];

			vector<vector<CDoubleVector*>> testData;

			double* dataTestData = static_cast<double*>(mxGetData(matTestData));
			for (size_t testIndex = 0; testIndex < numberOfTests; ++testIndex)
			{
				vector<CDoubleVector*> smallTestData;
				for (size_t sampleIndex = 0; sampleIndex < numberOfSamples; ++sampleIndex)
				{
					smallTestData.push_back(new CDoubleVector(numberOfFeatures, dataTestData + sampleIndex * numberOfFeatures + testIndex * numberOfSamples * numberOfFeatures));
				}
				testData.push_back(smallTestData);
			}



			///////////////////////////////////////////////////////////////////////////////////////////////
			// Process data

			CDoubleVector** outputStorage = new CDoubleVector*[numberOfTests];
			for (size_t testIndex = 0; testIndex < numberOfTests; ++testIndex)
			{
				outputStorage[testIndex] = new CDoubleVector(numberOfFeatures * numberOfTaps, 0.0);
                if (featureTimeSelector->addToBuffer(testData.at(testIndex)))
                {
				    featureTimeSelector->generateFeatureVector(outputStorage[testIndex]);
                }
			}



			///////////////////////////////////////////////////////////////////////////////////////////////
			// Compare the process data with the MATLAB version

			// Load the real spectrum data from the .mat file
			mxArray* matSelectData = CMatlabLibrary::loadMatVariable(fileName, "selectData");
			Assert::IsNotNull(matSelectData);

			Assert::AreEqual(numberOfFeatures * numberOfTaps, mxGetM(matSelectData));
			Assert::AreEqual(numberOfTests, mxGetN(matSelectData));

			dataElements = static_cast<double*>(mxGetData(matSelectData));
			double validValue;
			double procValue;
			double testValue;
			for (size_t testIndex = 0; testIndex < numberOfTests; ++testIndex)
			{
				for (size_t selectIndex = 0; selectIndex < numberOfFeatures * numberOfTaps; ++selectIndex)
				{
					validValue = dataElements[selectIndex + testIndex * numberOfFeatures * numberOfTaps];
					procValue = outputStorage[testIndex]->getValues()[selectIndex];
					if (validValue == 0 && procValue == 0)
					{
						Assert::AreEqual(validValue, procValue);
					}
					else
					{
						testValue = abs((validValue - procValue) / (validValue + procValue));
						Assert::AreEqual(0, testValue, testTolerance);
					}
				}
			}


			///////////////////////////////////////////////////////////////////////////////////////////////
			// Save results

			double* storageBuffer = new double[numberOfFeatures * numberOfTaps * numberOfTests];
			for (size_t testIndex = 0; testIndex < numberOfTests; ++testIndex)
			{
				memcpy(storageBuffer + numberOfFeatures * numberOfTaps * testIndex, outputStorage[testIndex]->getValues(), numberOfFeatures * numberOfTaps * sizeof(double));
				delete outputStorage[testIndex];
				outputStorage[testIndex] = 0;
			}

			CMatlabLibrary::saveMatDoubleMatrixData("..\\TestData\\FeatureTimeSelector_testResults.mat", "selectData", storageBuffer, numberOfFeatures * numberOfTaps, numberOfTests);

			delete featureTaps;
			featureTaps = 0;

			delete featureTimeSelector;
			featureTimeSelector = 0;

			//delete[] dimTestData;
			//dimTestData = 0;

			delete[] outputStorage;
			outputStorage = 0;

			delete[] storageBuffer;
			storageBuffer = 0;

			Logger::WriteMessage("Unit Test Feature Time Selector\t--> Ok");
		}

    };

    TEST_CLASS(UnitTestRealImaginaryStftAlgorithm)
    {
    public:
        TEST_METHOD(UnitTestRealImaginaryStftAlgorithmProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-8;

            // Input file
            string fileName = "..\\TestData\\testData_RealImaginaryStftAlgorithm.mat";

            // Load windowType from .mat file
            mxArray* matWindowType = CMatlabLibrary::loadMatVariable(fileName, "windowType");
            Assert::IsNotNull(matWindowType);

            // Verify that the size is ok
            size_t numStrings = mxGetM(matWindowType);
            size_t numCharacters = mxGetN(matWindowType);

            char* charWindowType = new char[numCharacters + 1];
            mxGetString(matWindowType, charWindowType, numCharacters + 1);
            string stringWindowType(charWindowType, numCharacters);

            CRealImaginaryStftAlgorithm::WindowType windowType;

            if (stringWindowType == "hamming")
            {
                windowType = CRealImaginaryStftAlgorithm::HAMMING;
            }
            else if (stringWindowType == "blackman")
            {
                windowType = CRealImaginaryStftAlgorithm::BLACKMAN;
            }
            else if (stringWindowType == "hann")
            {
                windowType = CRealImaginaryStftAlgorithm::HANN;
            }
            else
            {
                windowType = CRealImaginaryStftAlgorithm::NONE;
            }

            // Load fourierWindowLength from .mat file
            mxArray *matFourierWindowLength = CMatlabLibrary::loadMatVariable(fileName, "fourierWindowLength");
            Assert::IsNotNull(matFourierWindowLength);

            // Verify that the size is ok
            Assert::AreEqual(1, static_cast<int>(mxGetN(matFourierWindowLength)));
            Assert::AreEqual(1, static_cast<int>(mxGetM(matFourierWindowLength)));

            double* dataElements = static_cast<double*>(mxGetData(matFourierWindowLength));
            const size_t fourierWindowLength = static_cast<size_t>(dataElements[0]);


            // Load stepSize from .mat file
            mxArray *matStepSize = CMatlabLibrary::loadMatVariable(fileName, "stepSize");
            Assert::IsNotNull(matStepSize);

            // Verify that the size is ok
            Assert::AreEqual(1, static_cast<int>(mxGetN(matStepSize)));
            Assert::AreEqual(1, static_cast<int>(mxGetM(matStepSize)));

            dataElements = static_cast<double*>(mxGetData(matStepSize));
            const size_t stepSize = static_cast<size_t>(dataElements[0]);


            // Load numberOfChannels from .mat file
            mxArray *matNumberOfChannels = CMatlabLibrary::loadMatVariable(fileName, "numberOfChannels");
            Assert::IsNotNull(matNumberOfChannels);

            // Verify that the size is ok
            Assert::AreEqual(1, static_cast<int>(mxGetN(matNumberOfChannels)));
            Assert::AreEqual(1, static_cast<int>(mxGetM(matNumberOfChannels)));

            dataElements = static_cast<double*>(mxGetData(matNumberOfChannels));
            const size_t numberOfChannels = static_cast<size_t>(dataElements[0]);


            // Load samplingFrequency from .mat file
            mxArray *matSamplingFrequency = CMatlabLibrary::loadMatVariable(fileName, "samplingFrequency");
            Assert::IsNotNull(matSamplingFrequency);

            // Verify that the size is ok
            Assert::AreEqual(1, static_cast<int>(mxGetN(matSamplingFrequency)));
            Assert::AreEqual(1, static_cast<int>(mxGetM(matSamplingFrequency)));

            dataElements = static_cast<double*>(mxGetData(matSamplingFrequency));
            const size_t samplingFrequency = static_cast<size_t>(dataElements[0]);


            CRealImaginaryStftAlgorithm* realImaginaryStftAlgorithm = new CRealImaginaryStftAlgorithm(windowType
                , fourierWindowLength
                , stepSize
                , numberOfChannels
                , samplingFrequency);

            const size_t numberOfBins = realImaginaryStftAlgorithm->getNumberOfBins();

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Initialize the internal STFT buffer

            // Load initialization data from .mat file
            mxArray* initializationData = CMatlabLibrary::loadMatVariable(fileName, "initializationData");
            Assert::IsNotNull(initializationData);

            // Store data in data Buffer
            Assert::AreEqual(numberOfChannels, mxGetM(initializationData));
            Assert::AreEqual(fourierWindowLength, mxGetN(initializationData));

            double* initElements = static_cast<double*>(mxGetData(initializationData));
            CDoubleVector** initializationBuffer = new CDoubleVector*[fourierWindowLength];
            for (size_t initIndex = 0; initIndex < fourierWindowLength; ++initIndex)
            {
                initializationBuffer[initIndex] = new CDoubleVector(numberOfChannels, initElements + initIndex * numberOfChannels);
            }

            realImaginaryStftAlgorithm->reset(initializationBuffer);

            for (size_t initIndex = 0; initIndex < fourierWindowLength; ++initIndex)
            {
                delete initializationBuffer[initIndex];
                initializationBuffer[initIndex] = 0;
            }
            delete[] initializationBuffer;
            initializationBuffer = 0;


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load test data from .mat file
            mxArray* testData = CMatlabLibrary::loadMatVariable(fileName, "testData");
            Assert::IsNotNull(testData);

            // Store data in data Buffer
            Assert::AreEqual(numberOfChannels, mxGetM(testData));
            size_t numSamples = mxGetN(testData);

            double* testElements = static_cast<double*>(mxGetData(testData));
            CDoubleVector** testBuffer = new CDoubleVector*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBuffer[testIndex] = new CDoubleVector(numberOfChannels, testElements + testIndex * numberOfChannels);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            vector<CDoubleMatrix*> realOutputStorage;
            vector<CDoubleMatrix*> imaginaryOutputStorage;

            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                realImaginaryStftAlgorithm->process(realOutputStorage, imaginaryOutputStorage, testBuffer[sampleIndex]);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the process data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* validRealSpectrum = CMatlabLibrary::loadMatVariable(fileName, "realSpectrum");
            Assert::IsNotNull(validRealSpectrum);

            size_t noOfDim = mxGetNumberOfDimensions(validRealSpectrum);
            const size_t* dimValidRealSpectrum = new size_t[noOfDim];
            dimValidRealSpectrum = mxGetDimensions(validRealSpectrum);

            Assert::AreEqual(3, static_cast<int>(noOfDim));
            Assert::AreEqual(numberOfBins, dimValidRealSpectrum[0]);
            Assert::AreEqual(numberOfChannels, dimValidRealSpectrum[1]);
            Assert::AreEqual(realOutputStorage.size(), dimValidRealSpectrum[2]);

            double* dataValidRealSpectrum = static_cast<double*>(mxGetData(validRealSpectrum));
            double validValue;
            double procValue;
            double testValue;
            for (size_t procIndex = 0; procIndex < realOutputStorage.size(); ++procIndex)
            {
                for (size_t chIndex = 0; chIndex < numberOfChannels; ++chIndex)
                {
                    for (size_t binIndex = 0; binIndex < numberOfBins; ++binIndex)
                    {
                        validValue = dataValidRealSpectrum[binIndex + chIndex * numberOfBins + procIndex * numberOfChannels * numberOfBins];
                        procValue = realOutputStorage.at(procIndex)->at(binIndex, chIndex);
                        if (validValue == 0 && procValue == 0)
                        {
                            Assert::AreEqual(validValue, procValue);
                        }
                        else
                        {
                            testValue = abs((validValue - procValue) / (validValue + procValue));
                            Assert::AreEqual(0, testValue, testTolerance);
                        }
                    }
                }
            }

            // Load the imaginary spectrum data from the .mat file
            mxArray* validImaginarySpectrum = CMatlabLibrary::loadMatVariable(fileName, "imaginarySpectrum");
            Assert::IsNotNull(validImaginarySpectrum);

            noOfDim = mxGetNumberOfDimensions(validImaginarySpectrum);
            const size_t* dimValidImaginarySpectrum = new size_t[noOfDim];
            dimValidImaginarySpectrum = mxGetDimensions(validImaginarySpectrum);

            Assert::AreEqual(3, static_cast<int>(noOfDim));
            Assert::AreEqual(numberOfBins, dimValidImaginarySpectrum[0]);
            Assert::AreEqual(numberOfChannels, dimValidImaginarySpectrum[1]);
            Assert::AreEqual(imaginaryOutputStorage.size(), dimValidImaginarySpectrum[2]);

            double* dataValidImaginarySpectrum = static_cast<double*>(mxGetData(validImaginarySpectrum));
            for (size_t procIndex = 0; procIndex < imaginaryOutputStorage.size(); ++procIndex)
            {
                for (size_t chIndex = 0; chIndex < numberOfChannels; ++chIndex)
                {
                    for (size_t binIndex = 0; binIndex < numberOfBins; ++binIndex)
                    {
                        validValue = dataValidImaginarySpectrum[binIndex + chIndex * numberOfBins + procIndex * numberOfChannels * numberOfBins];
                        procValue = imaginaryOutputStorage.at(procIndex)->at(binIndex, chIndex);
                        if (validValue == 0 && procValue == 0)
                        {
                            Assert::AreEqual(validValue, procValue);
                        }
                        else
                        {
                            testValue = abs((validValue - procValue) / (validValue + procValue));
                            Assert::AreEqual(0, testValue, testTolerance);
                        }
                    }
                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            size_t numberOfOutput = realOutputStorage.size();
            Assert::AreEqual(numberOfOutput, imaginaryOutputStorage.size());
            double* realStorageBuffer = new double[numberOfBins * numberOfChannels * numberOfOutput];
            double* imaginaryStorageBuffer = new double[numberOfBins * numberOfChannels * numberOfOutput];
            for (size_t outIndex = 0; outIndex < numberOfOutput; ++outIndex)
            {
                memcpy(realStorageBuffer + numberOfBins * numberOfChannels * outIndex, realOutputStorage.at(outIndex)->getValues(), numberOfBins * numberOfChannels * sizeof(double));
                delete realOutputStorage.at(outIndex);
                realOutputStorage.at(outIndex) = 0;

                memcpy(imaginaryStorageBuffer + numberOfBins * numberOfChannels * outIndex, imaginaryOutputStorage.at(outIndex)->getValues(), numberOfBins * numberOfChannels * sizeof(double));
                delete imaginaryOutputStorage.at(outIndex);
                imaginaryOutputStorage.at(outIndex) = 0;
            }

            CMatlabLibrary::saveMatDouble3dData("..\\TestData\\RealImaginaryStftAlgorithm_testResults.mat", "realStectrum", realStorageBuffer, numberOfBins, numberOfChannels, numberOfOutput);
            CMatlabLibrary::saveMatDouble3dData("..\\TestData\\RealImaginaryStftAlgorithm_testResults.mat", "imaginaryStectrum", imaginaryStorageBuffer, numberOfBins, numberOfChannels, numberOfOutput);

            delete[] realStorageBuffer;
            realStorageBuffer = 0;

            delete[] imaginaryStorageBuffer;
            imaginaryStorageBuffer = 0;

            realOutputStorage.clear();
            imaginaryOutputStorage.clear();

            Logger::WriteMessage("Unit Test Real Imaginary Stft Algorithm\t--> Ok");
        }
    };

    TEST_CLASS(UnitTestMldaClassifier)
    {
    public:
        TEST_METHOD(UnitTestMldaClassifierProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-8;

            // Input file
            string fileName = "..\\TestData\\testData_MldaClassifier.mat";

            // Create the class that is being unitested
            CMldaClassifier* mldaClassifier = new CMldaClassifier(fileName);


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data (MRCP & STFT features)

            // Load test data from .mat file
            mxArray* matTestDataMrcp = CMatlabLibrary::loadMatVariable(fileName, "dataMrcp");
            Assert::IsNotNull(matTestDataMrcp);
			
			mxArray* matTestDataStft = CMatlabLibrary::loadMatVariable(fileName, "dataStft");
			Assert::IsNotNull(matTestDataStft);

            const size_t numberOfMrcpFeatures = mldaClassifier->getNoOfFeatures1();
			const size_t numberOfStftFeatures = mldaClassifier->getNoOfFeatures2();

            // Store data in data Buffer
            Assert::AreEqual(numberOfMrcpFeatures, mxGetM(matTestDataMrcp));
			Assert::AreEqual(numberOfStftFeatures, mxGetM(matTestDataStft));
			size_t numSamples = mxGetN(matTestDataMrcp);
			Assert::AreEqual(numSamples, mxGetN(matTestDataStft));

            double* dataElementsMrcp = static_cast<double*>(mxGetData(matTestDataMrcp));
			double* dataElementsStft = static_cast<double*>(mxGetData(matTestDataStft));
            CDoubleVector** testBufferMrcp = new CDoubleVector*[numSamples];
			CDoubleVector** testBufferStft = new CDoubleVector*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBufferMrcp[testIndex] = new CDoubleVector(numberOfMrcpFeatures, dataElementsMrcp + testIndex * numberOfMrcpFeatures);
				testBufferStft[testIndex] = new CDoubleVector(numberOfStftFeatures, dataElementsStft + testIndex * numberOfStftFeatures);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            size_t* outputStorage = new size_t[numSamples];
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                outputStorage[sampleIndex] = mldaClassifier->process(testBufferMrcp[sampleIndex], testBufferStft[sampleIndex]);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the process data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "outState");
            Assert::IsNotNull(matValidOutput);

            Assert::AreEqual(1, static_cast<int>(mxGetM(matValidOutput)));
            Assert::AreEqual(numSamples, mxGetN(matValidOutput));

			double* dataElements = static_cast<double*>(mxGetData(matValidOutput));
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                Assert::AreEqual(static_cast<size_t>(dataElements[sampleIndex]), outputStorage[sampleIndex]);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBuffer = new double[numSamples];
            for (size_t outIndex = 0; outIndex < numSamples; ++outIndex)
            {
                storageBuffer[outIndex] = static_cast<double>(outputStorage[outIndex]);
            }

            CMatlabLibrary::saveMatDoubleMatrixData("..\\TestData\\MldaClassifier_testResults.mat", "outState", storageBuffer, 1, numSamples);

            delete mldaClassifier;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete testBufferMrcp[testIndex];
                testBufferMrcp[testIndex] = 0;
				
				delete testBufferStft[testIndex];
				testBufferStft[testIndex] = 0;
			}
            delete[] testBufferMrcp;
            testBufferMrcp = 0;
			
			delete[] testBufferStft;
			testBufferStft = 0;

            delete[] outputStorage;
            outputStorage = 0;

            delete[] storageBuffer;
            storageBuffer = 0;

            Logger::WriteMessage("Unit Test mLDA Classifier\t--> Ok");
        }

    };

    TEST_CLASS(UnitTestAdaptiveFeatureNormalizationAlgorithm)
    {
    public:
        TEST_METHOD(UnitTestAdaptiveFeatureNormalizationAlgorithmProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-8;

            // Input file
            string fileName = "..\\TestData\\testData_AdaptiveFeatureNormalizationAlgorithm.mat";

            // Create the class that is being unitested
            CAdaptiveFeatureNormalizationAlgorithm* adaptiveFeatureNormalizationAlgorithm = new CAdaptiveFeatureNormalizationAlgorithm(fileName);

            // Load numberOfChannels from .mat file
            mxArray* matSafeNormalizationValue = CMatlabLibrary::loadMatVariable(fileName, "safeNormalizationValue");
			Assert::IsNotNull(matSafeNormalizationValue);

            // Verify that the size is ok
            Assert::AreEqual(1, static_cast<int>(mxGetN(matSafeNormalizationValue)));
            Assert::AreEqual(1, static_cast<int>(mxGetM(matSafeNormalizationValue)));

            double* dataElements = static_cast<double*>(mxGetData(matSafeNormalizationValue));
            const double safeNormalizationValue = static_cast<size_t>(dataElements[0]);

            const double testSafeNormalizationValue = adaptiveFeatureNormalizationAlgorithm->getSafeNormalizationValue();
            const double maxSafe = max(safeNormalizationValue, testSafeNormalizationValue);
            Assert::AreEqual(testSafeNormalizationValue, safeNormalizationValue, maxSafe);


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load test data from .mat file
            mxArray* matTestData = CMatlabLibrary::loadMatVariable(fileName, "data");
            Assert::IsNotNull(matTestData);

            size_t numberOfFeatures = adaptiveFeatureNormalizationAlgorithm->getNumberOfFeatures();

            // Store data in data Buffer
            Assert::AreEqual(numberOfFeatures, mxGetM(matTestData));
            size_t numSamples = mxGetN(matTestData);

            dataElements = static_cast<double*>(mxGetData(matTestData));
            CDoubleVector** testBuffer = new CDoubleVector*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBuffer[testIndex] = new CDoubleVector(numberOfFeatures, dataElements + testIndex * numberOfFeatures);
            }

			// Load artificat presence data from the .mat file
			mxArray* matArtifactPresence = CMatlabLibrary::loadMatVariable(fileName, "artifactPresence");
			Assert::IsNotNull(matArtifactPresence);

			// Store data in data Buffer
			Assert::AreEqual(1, static_cast<int>(mxGetM(matArtifactPresence)));
			Assert::AreEqual(numSamples, mxGetN(matArtifactPresence));

			dataElements = static_cast<double*>(mxGetData(matArtifactPresence));
			bool* artifactPresence = new bool[numSamples];
			for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
			{
				artifactPresence[sampleIndex] = static_cast<bool>(dataElements[sampleIndex]);
			}


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            CDoubleVector** outputStorage = new CDoubleVector*[numSamples];
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                outputStorage[sampleIndex] = new CDoubleVector(numberOfFeatures);
                adaptiveFeatureNormalizationAlgorithm->process(outputStorage[sampleIndex][0], testBuffer[sampleIndex][0], artifactPresence[sampleIndex]);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the process data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "normData");
            Assert::IsNotNull(matValidOutput);

            Assert::AreEqual(numberOfFeatures, mxGetM(matValidOutput));
            Assert::AreEqual(numSamples, mxGetN(matValidOutput));

            dataElements = static_cast<double*>(mxGetData(matValidOutput));
            double validValue;
            double procValue;
            double testValue;
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                for (size_t featureIndex = 0; featureIndex < numberOfFeatures; ++featureIndex)
                {
                    validValue = dataElements[featureIndex + sampleIndex * numberOfFeatures];
                    procValue = outputStorage[sampleIndex]->getValues()[featureIndex];
                    if (validValue == 0 && procValue == 0)
                    {
                        Assert::AreEqual(validValue, procValue);
                    }
                    else
                    {
                        testValue = abs((validValue - procValue) / (validValue + procValue));
                        Assert::AreEqual(0, testValue, testTolerance);
                    }
                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBuffer = new double[numberOfFeatures * numSamples];
            for (size_t outIndex = 0; outIndex < numSamples; ++outIndex)
            {
                memcpy(storageBuffer + numberOfFeatures * outIndex, outputStorage[outIndex]->getValues(), numberOfFeatures * sizeof(double));
                delete outputStorage[outIndex];
                outputStorage[outIndex] = 0;
            }

            CMatlabLibrary::saveMatDoubleMatrixData("..\\TestData\\AdaptiveFeatureNormalizationAlgorithm_testResults.mat", "normData", storageBuffer, numberOfFeatures, numSamples);

            delete[] outputStorage;
            outputStorage = 0;

            Logger::WriteMessage("Unit Test Adaptative Feature Normalization\t--> Ok");
        }

    };

    TEST_CLASS(UnitTestSerialSGolayFilterAlgorithm)
    {
    public:

        TEST_METHOD(UnitTestSerialSGolayFilterAlgorithmProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-8;

            // Input file
            string fileName = "..\\TestData\\testData_SerialSGolayFilterAlgorithm.mat";

            // Create the class that is being unitested
            CSerialSGolayFilterAlgorithm* serialSGolayFilterAlgorithm = new CSerialSGolayFilterAlgorithm(fileName);


            size_t numberOfChannels = serialSGolayFilterAlgorithm->getNumberOfChannels();
            size_t windowSize = serialSGolayFilterAlgorithm->getWindowSize();

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Initialize the internal buffer

            // Load initialization data from .mat file
            mxArray* matInitializationData = CMatlabLibrary::loadMatVariable(fileName, "initializationData");
            Assert::IsNotNull(matInitializationData);

            // Store data in data Buffer
            Assert::AreEqual(numberOfChannels, mxGetM(matInitializationData));
            Assert::AreEqual(windowSize, mxGetN(matInitializationData));

            double* initElements = static_cast<double*>(mxGetData(matInitializationData));
            CDoubleVector** initializationBuffer = new CDoubleVector*[windowSize];
            for (size_t initIndex = 0; initIndex < windowSize; ++initIndex)
            {
                initializationBuffer[initIndex] = new CDoubleVector(numberOfChannels, initElements + initIndex * numberOfChannels);
            }

            serialSGolayFilterAlgorithm->reset(initializationBuffer);

            for (size_t initIndex = 0; initIndex < windowSize; ++initIndex)
            {
                delete initializationBuffer[initIndex];
                initializationBuffer[initIndex] = 0;
            }
            delete[] initializationBuffer;
            initializationBuffer = 0;


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load test data from .mat file
            mxArray* matTestData = CMatlabLibrary::loadMatVariable(fileName, "testData");
            Assert::IsNotNull(matTestData);

            // Store data in data Buffer
            Assert::AreEqual(numberOfChannels, mxGetM(matTestData));
            size_t numSamples = mxGetN(matTestData);

            double* dataElements = static_cast<double*>(mxGetData(matTestData));
            CDoubleVector** testBuffer = new CDoubleVector*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBuffer[testIndex] = new CDoubleVector(numberOfChannels, dataElements + testIndex * numberOfChannels);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            vector<CDoubleVector*> outputStorage;
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                serialSGolayFilterAlgorithm->process(outputStorage, testBuffer[sampleIndex]);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the processed data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "rezData");
            Assert::IsNotNull(matValidOutput);

            Assert::AreEqual(numberOfChannels, mxGetM(matValidOutput));
            Assert::AreEqual(outputStorage.size(), mxGetN(matValidOutput));

            dataElements = static_cast<double*>(mxGetData(matValidOutput));
            double validValue;
            double procValue;
            double testValue;
            for (size_t procIndex = 0; procIndex < outputStorage.size(); ++procIndex)
            {
                for (size_t chIndex = 0; chIndex < numberOfChannels; ++chIndex)
                {
                    validValue = dataElements[chIndex + procIndex * numberOfChannels];
                    procValue = outputStorage.at(procIndex)->getValues()[chIndex];
                    if (validValue == 0 && procValue == 0)
                    {
                        Assert::AreEqual(validValue, procValue);
                    }
                    else
                    {
                        testValue = abs((validValue - procValue) / (validValue + procValue));
                        Assert::AreEqual(0, testValue, testTolerance);
                    }
                }
            }
            

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBuffer = new double[numberOfChannels * outputStorage.size()];
            for (size_t outIndex = 0; outIndex < outputStorage.size(); ++outIndex)
            {
                memcpy(storageBuffer + numberOfChannels * outIndex, outputStorage.at(outIndex)->getValues(), numberOfChannels * sizeof(double));
                delete outputStorage.at(outIndex);
                outputStorage.at(outIndex) = 0;
            }

            CMatlabLibrary::saveMatDoubleMatrixData("..\\TestData\\SerialSGolayFilterAlgorithm_testResults.mat", "filtData", storageBuffer, numberOfChannels, outputStorage.size());

            delete serialSGolayFilterAlgorithm;

            delete[] storageBuffer;
            storageBuffer = 0;

            outputStorage.clear();

            Logger::WriteMessage("Unit Test Serial Savitzky Golay Filter Algorithm\t--> Ok");
        }
    };

    /*
    TEST_CLASS(UnitTestSpectralAmplitudeAlgorithm)
    {
    public:

        TEST_METHOD(UnitTestProcess)
        {
            // TODO: Your test code here


            Logger::WriteMessage("Unit Test Spectral Amplitude Algorithm\t--> Ok");
        }

    };
    */

    TEST_CLASS(UnitTestCorticalSourceReconstruction)
    {
    public:

        TEST_METHOD(UnitTestCorticalSourceReconstructionProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-8;

            // Input file
            string fileName = "..\\TestData\\testData_CorticalSourceReconstruction.mat";

            // Create the class that is being unitested
            CCorticalSourceReconstruction* corticalSourceReconstruction = new CCorticalSourceReconstruction(fileName);


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load test data from .mat file
            mxArray* matTestData = CMatlabLibrary::loadMatVariable(fileName, "data");
            Assert::IsNotNull(matTestData);

            const size_t numberOfSignals = corticalSourceReconstruction->getNumberOfSignals();
            const size_t numberOfCorticalSources = corticalSourceReconstruction->getNumberOfCorticalSources();

            // Store data in data Buffer
            Assert::AreEqual(numberOfSignals, mxGetM(matTestData));
            size_t numSamples = mxGetN(matTestData);

            double* dataElements = static_cast<double*>(mxGetData(matTestData));
            CDoubleVector** testBuffer = new CDoubleVector*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBuffer[testIndex] = new CDoubleVector(numberOfSignals, dataElements + testIndex * numberOfSignals);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            CDoubleVector** outputStorage = new CDoubleVector*[numSamples];
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                outputStorage[sampleIndex] = new CDoubleVector(numberOfCorticalSources);
                corticalSourceReconstruction->process(outputStorage[sampleIndex],testBuffer[sampleIndex]);
            }



            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the process data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "sourceActivity");
            Assert::IsNotNull(matValidOutput);

            Assert::AreEqual(numberOfCorticalSources, mxGetM(matValidOutput));
            Assert::AreEqual(numSamples, mxGetN(matValidOutput));

            dataElements = static_cast<double*>(mxGetData(matValidOutput));
            double validValue;
            double procValue;
            double testValue;
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                for (size_t sourceIndex = 0; sourceIndex < numberOfCorticalSources; ++sourceIndex)
                {
                    validValue = dataElements[sourceIndex + sampleIndex * numberOfCorticalSources];
                    procValue = outputStorage[sampleIndex]->getValues()[sourceIndex];
                    if (validValue == 0 && procValue == 0)
                    {
                        Assert::AreEqual(validValue, procValue);
                    }
                    else
                    {
                        testValue = abs((validValue - procValue) / (validValue + procValue));
                        Assert::AreEqual(0, testValue, testTolerance);
                    }
                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBuffer = new double[numberOfCorticalSources * numSamples];
            for (size_t outIndex = 0; outIndex < numSamples; ++outIndex)
            {
                memcpy(storageBuffer + outIndex * numberOfCorticalSources, outputStorage[outIndex]->getValues(), numberOfCorticalSources * sizeof(double));
            }

            CMatlabLibrary::saveMatDoubleMatrixData("..\\TestData\\CorticalSourceReconstruction_testResults.mat", "outState", storageBuffer, numberOfCorticalSources, numSamples);

            delete corticalSourceReconstruction;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete testBuffer[testIndex];
                testBuffer[testIndex] = 0;
            }
            delete[] testBuffer;
            testBuffer = 0;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete outputStorage[testIndex];
                outputStorage[testIndex] = 0;
            }
            delete[] outputStorage;
            outputStorage = 0;

            delete[] storageBuffer;
            storageBuffer = 0;

            Logger::WriteMessage("Unit Test Cortical Source Reconstruction\t--> Ok");
        }

    };

    TEST_CLASS(UnitTestDenoisingAlgorithm)
    {
    public:

        TEST_METHOD(UnitTestDenoisingAlgorithmProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-8;

            // Input file
            string fileName = "..\\TestData\\testData_DenoisingAlgorithm.mat";

            // Create the class that is being unitested
            CDenoisingAlgorithm* denoisingAlgorithm = new CDenoisingAlgorithm(fileName);

            const size_t numberOfSpectralBins = denoisingAlgorithm->getNumberOfSpectralBins();
            const size_t numberOfChannels = denoisingAlgorithm->getNumberOfChannels();


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load real test data from .mat file
            mxArray* matTestDataReal = CMatlabLibrary::loadMatVariable(fileName, "data_real");
            Assert::IsNotNull(matTestDataReal);

            const size_t noOfDim = mxGetNumberOfDimensions(matTestDataReal);
            const size_t* dimTestDataReal = new size_t[noOfDim];
            dimTestDataReal = mxGetDimensions(matTestDataReal);

            Assert::AreEqual(3, static_cast<int>(noOfDim));
            Assert::AreEqual(numberOfSpectralBins, dimTestDataReal[0]);
            Assert::AreEqual(numberOfChannels, dimTestDataReal[1]);
            const size_t numSamples = dimTestDataReal[2];

            // Store real test data in data Buffer
            double* dataElements = static_cast<double*>(mxGetData(matTestDataReal));
            CDoubleMatrix** testBufferReal = new CDoubleMatrix*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBufferReal[testIndex] = new CDoubleMatrix(numberOfSpectralBins, numberOfChannels, dataElements + testIndex * numberOfSpectralBins * numberOfChannels);
            }


            // Load imaginary test data from .mat file
            mxArray* matTestDataImaginary = CMatlabLibrary::loadMatVariable(fileName, "data_imaginary");
            Assert::IsNotNull(matTestDataImaginary);

            Assert::AreEqual(noOfDim, static_cast<size_t>(mxGetNumberOfDimensions(matTestDataImaginary)));
            const size_t* dimTestDataImaginary = new size_t[noOfDim];
            dimTestDataImaginary = mxGetDimensions(matTestDataImaginary);

            Assert::AreEqual(numberOfSpectralBins, dimTestDataImaginary[0]);
            Assert::AreEqual(numberOfChannels, dimTestDataImaginary[1]);
            Assert::AreEqual(numSamples, static_cast<size_t>(dimTestDataImaginary[2]));

            // Store imaginary test data in data Buffer
            dataElements = static_cast<double*>(mxGetData(matTestDataImaginary));
            CDoubleMatrix** testBufferImaginary = new CDoubleMatrix*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBufferImaginary[testIndex] = new CDoubleMatrix(numberOfSpectralBins, numberOfChannels, dataElements + testIndex * numberOfSpectralBins * numberOfChannels);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            CDoubleMatrix** outputStorageReal = new CDoubleMatrix*[numSamples];
            CDoubleMatrix** outputStorageImaginary = new CDoubleMatrix*[numSamples];
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                outputStorageReal[sampleIndex] = new CDoubleMatrix(numberOfSpectralBins, numberOfChannels);
                outputStorageImaginary[sampleIndex] = new CDoubleMatrix(numberOfSpectralBins, numberOfChannels);
                denoisingAlgorithm->process(outputStorageReal[sampleIndex]
                                          , outputStorageImaginary[sampleIndex]
                                          , testBufferReal[sampleIndex]
                                          , testBufferImaginary[sampleIndex]);
            }



            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the process data with the MATLAB version

            // Load real test data from .mat file
            mxArray* matDenoisedDataReal = CMatlabLibrary::loadMatVariable(fileName, "denoisedData_real");
            Assert::IsNotNull(matDenoisedDataReal);

            Assert::AreEqual(noOfDim, static_cast<size_t>(mxGetNumberOfDimensions(matDenoisedDataReal)));
            const size_t* dimDenoisedDataReal = new size_t[noOfDim];
            dimDenoisedDataReal = mxGetDimensions(matDenoisedDataReal);

            Assert::AreEqual(numberOfSpectralBins, dimDenoisedDataReal[0]);
            Assert::AreEqual(numberOfChannels, dimDenoisedDataReal[1]);
            Assert::AreEqual(numSamples, static_cast<size_t>(dimDenoisedDataReal[2]));

            double* dataElementsDenoisedReal = static_cast<double*>(mxGetData(matDenoisedDataReal));


            mxArray* matDenoisedDataImaginary = CMatlabLibrary::loadMatVariable(fileName, "denoisedData_imaginary");
            Assert::IsNotNull(matDenoisedDataImaginary);

            Assert::AreEqual(noOfDim, static_cast<size_t>(mxGetNumberOfDimensions(matDenoisedDataImaginary)));
            const size_t* dimDenoisedDataImaginary = new size_t[noOfDim];
            dimDenoisedDataImaginary = mxGetDimensions(matDenoisedDataImaginary);

            Assert::AreEqual(numberOfSpectralBins, dimDenoisedDataImaginary[0]);
            Assert::AreEqual(numberOfChannels, dimDenoisedDataImaginary[1]);
            Assert::AreEqual(numSamples, static_cast<size_t>(dimDenoisedDataImaginary[2]));

            double* dataElementsDenoisedImaginary = static_cast<double*>(mxGetData(matDenoisedDataImaginary));


            double validValue;
            double procValue;
            double testValue;
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                for (size_t channelIndex = 0; channelIndex < numberOfChannels; ++channelIndex)
                {
                    for (size_t binIndex = 0; binIndex < numberOfSpectralBins; ++binIndex)
                    {
                        validValue = dataElementsDenoisedReal[binIndex + channelIndex * numberOfSpectralBins + sampleIndex * numberOfChannels * numberOfSpectralBins];
                        procValue = outputStorageReal[sampleIndex]->at(binIndex, channelIndex);
                        if (validValue == 0 && procValue == 0)
                        {
                            Assert::AreEqual(validValue, procValue);
                        }
                        else
                        {
                            testValue = abs((validValue - procValue) / (validValue + procValue));
                            Assert::AreEqual(0, testValue, testTolerance);
                        }


                        validValue = dataElementsDenoisedImaginary[binIndex + channelIndex * numberOfSpectralBins + sampleIndex * numberOfChannels * numberOfSpectralBins];
                        procValue = outputStorageImaginary[sampleIndex]->at(binIndex, channelIndex);
                        if (validValue == 0 && procValue == 0)
                        {
                            Assert::AreEqual(validValue, procValue);
                        }
                        else
                        {
                            testValue = abs((validValue - procValue) / (validValue + procValue));
                            Assert::AreEqual(0, testValue, testTolerance);
                        }
                    }
                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBufferReal = new double[numberOfSpectralBins * numberOfChannels * numSamples];
            double* storageBufferImaginary = new double[numberOfSpectralBins * numberOfChannels * numSamples];
            for (size_t outIndex = 0; outIndex < numSamples; ++outIndex)
            {
                memcpy(storageBufferReal + outIndex * numberOfSpectralBins * numberOfChannels, outputStorageReal[outIndex]->getValues(), numberOfSpectralBins * numberOfChannels * sizeof(double));
                memcpy(storageBufferImaginary + outIndex * numberOfSpectralBins * numberOfChannels, outputStorageImaginary[outIndex]->getValues(), numberOfSpectralBins * numberOfChannels * sizeof(double));
            }

            CMatlabLibrary::saveMatDouble3dData("..\\TestData\\DenoisingAlgorithm_testResults.mat", "denoisedData_real", storageBufferReal, numberOfSpectralBins, numberOfChannels, numSamples);
            CMatlabLibrary::saveMatDouble3dData("..\\TestData\\DenoisingAlgorithm_testResults.mat", "denoisedData_imaginary", storageBufferImaginary, numberOfSpectralBins, numberOfChannels, numSamples);

            delete denoisingAlgorithm;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete testBufferReal[testIndex];
                testBufferReal[testIndex] = 0;
            }
            delete[] testBufferReal;
            testBufferReal = 0;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete testBufferImaginary[testIndex];
                testBufferImaginary[testIndex] = 0;
            }
            delete[] testBufferImaginary;
            testBufferImaginary = 0;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete outputStorageReal[testIndex];
                outputStorageReal[testIndex] = 0;
            }
            delete[] outputStorageReal;
            outputStorageReal = 0;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete outputStorageImaginary[testIndex];
                outputStorageImaginary[testIndex] = 0;
            }
            delete[] outputStorageImaginary;
            outputStorageImaginary = 0;

            delete[] storageBufferReal;
            storageBufferReal = 0;

            delete[] storageBufferImaginary;
            storageBufferImaginary = 0;

            Logger::WriteMessage("Unit Test Denoising Algorithm\t--> Ok");
        }

    };

    TEST_CLASS(UnitTestFrequencyBandSelector)
    {
    public:

        TEST_METHOD(UnitTestFrequencyBandSelectorProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-8;

            // Input file
            string fileName = "..\\TestData\\testData_FrequencyBandSelector.mat";

            // Create the class that is being unitested
            CFrequencyBandSelector* frequencyBandSelector = new CFrequencyBandSelector(fileName);

            const size_t numberOfChannels = frequencyBandSelector->getNumberOfChannels();
            const size_t numberOfSpectralBins = frequencyBandSelector->getNumberOfSpectralBins();
            const size_t numberOfSelectedBins = frequencyBandSelector->getNumberOfSelectedBins();


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load real test data from .mat file
            mxArray* matTestData = CMatlabLibrary::loadMatVariable(fileName, "data");
            Assert::IsNotNull(matTestData);

            const size_t noOfDim = mxGetNumberOfDimensions(matTestData);
            const size_t* dimTestData = new size_t[noOfDim];
            dimTestData = mxGetDimensions(matTestData);

            Assert::AreEqual(3, static_cast<int>(noOfDim));
            Assert::AreEqual(numberOfSpectralBins, dimTestData[0]);
            Assert::AreEqual(numberOfChannels, dimTestData[1]);
            const size_t numSamples = dimTestData[2];

            // Store real test data in data Buffer
            double* dataElements = static_cast<double*>(mxGetData(matTestData));
            CDoubleMatrix** testBuffer = new CDoubleMatrix*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBuffer[testIndex] = new CDoubleMatrix(numberOfSpectralBins, numberOfChannels, dataElements + testIndex * numberOfSpectralBins * numberOfChannels);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            CDoubleMatrix** outputStorage = new CDoubleMatrix*[numSamples];
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                outputStorage[sampleIndex] = new CDoubleMatrix(numberOfSelectedBins,numberOfChannels);
                frequencyBandSelector->process(outputStorage[sampleIndex], testBuffer[sampleIndex]);
            }



            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the process data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "processedData");
            Assert::IsNotNull(matValidOutput);

            Assert::AreEqual(noOfDim, mxGetNumberOfDimensions(matValidOutput));
            const size_t* dimValidData = new size_t[noOfDim];
            dimValidData = mxGetDimensions(matValidOutput);

            Assert::AreEqual(numberOfSelectedBins, dimValidData[0]);
            Assert::AreEqual(numberOfChannels, dimValidData[1]);
            Assert::AreEqual(numSamples, dimValidData[2]);

            dataElements = static_cast<double*>(mxGetData(matValidOutput));
            double validValue;
            double procValue;
            double testValue;
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                for (size_t channelIndex = 0; channelIndex < numberOfChannels; ++channelIndex)
                {
                    for (size_t binIndex = 0; binIndex < numberOfSelectedBins; ++binIndex)
                    {
                        validValue = dataElements[binIndex + channelIndex * numberOfSelectedBins + sampleIndex * numberOfChannels * numberOfSelectedBins];
                        procValue = outputStorage[sampleIndex]->at(binIndex, channelIndex);
                        if (validValue == 0 && procValue == 0)
                        {
                            Assert::AreEqual(validValue, procValue);
                        }
                        else
                        {
                            testValue = abs((validValue - procValue) / (validValue + procValue));
                            Assert::AreEqual(0, testValue, testTolerance);
                        }
                    }
                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBuffer = new double[numberOfSelectedBins * numberOfChannels * numSamples];
            for (size_t outIndex = 0; outIndex < numSamples; ++outIndex)
            {
                memcpy(storageBuffer + outIndex * numberOfSelectedBins * numberOfChannels, outputStorage[outIndex]->getValues(), numberOfSelectedBins * numberOfChannels * sizeof(double));
            }

            CMatlabLibrary::saveMatDouble3dData("..\\TestData\\FrequencyBandSelector_testResults.mat", "denoisedData_real", storageBuffer, numberOfSelectedBins, numberOfChannels, numSamples);

            delete frequencyBandSelector;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete testBuffer[testIndex];
                testBuffer[testIndex] = 0;
            }
            delete[] testBuffer;
            testBuffer = 0;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete outputStorage[testIndex];
                outputStorage[testIndex] = 0;
            }
            delete[] outputStorage;
            outputStorage = 0;

            delete[] storageBuffer;
            storageBuffer = 0;

            Logger::WriteMessage("Unit Test Frequency Band Selector\t--> Ok");
        }
    };

    TEST_CLASS(UnitTestSpectralSourceReconstruction)
    {
    public:

        TEST_METHOD(UnitTestSpectralSourceReconstructionProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-8;

            // Input file
            string fileName = "..\\TestData\\testData_SpectralSourceReconstruction.mat";

            // Create the class that is being unitested
            CSpectralSourceReconstruction* spectralSourceReconstruction = new CSpectralSourceReconstruction(fileName);

            const size_t numberOfChannels = spectralSourceReconstruction->getNumberOfChannels();
            const size_t numberOfSpectralBins = spectralSourceReconstruction->getNumberOfSpectralBins();
            const size_t numberOfCorticalSources = spectralSourceReconstruction->getNumberOfCorticalSources();


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load real test data from .mat file
            mxArray* matTestData = CMatlabLibrary::loadMatVariable(fileName, "data");
            Assert::IsNotNull(matTestData);

            const size_t noOfDim = mxGetNumberOfDimensions(matTestData);
            const size_t* dimTestData = new size_t[noOfDim];
            dimTestData = mxGetDimensions(matTestData);

            Assert::AreEqual(3, static_cast<int>(noOfDim));
            Assert::AreEqual(numberOfSpectralBins, dimTestData[0]);
            Assert::AreEqual(numberOfChannels, dimTestData[1]);
            const size_t numSamples = dimTestData[2];

            // Store real test data in data Buffer
            double* dataElements = static_cast<double*>(mxGetData(matTestData));
            CDoubleMatrix** testBuffer = new CDoubleMatrix*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBuffer[testIndex] = new CDoubleMatrix(numberOfSpectralBins, numberOfChannels, dataElements + testIndex * numberOfSpectralBins * numberOfChannels);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            CDoubleMatrix** outputStorage = new CDoubleMatrix*[numSamples];
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                outputStorage[sampleIndex] = new CDoubleMatrix(numberOfSpectralBins, numberOfCorticalSources);
                spectralSourceReconstruction->process(outputStorage[sampleIndex], testBuffer[sampleIndex]);
            }



            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the process data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "sourceActivity");
            Assert::IsNotNull(matValidOutput);

            Assert::AreEqual(noOfDim, mxGetNumberOfDimensions(matValidOutput));
            const size_t* dimValidData = new size_t[noOfDim];
            dimValidData = mxGetDimensions(matValidOutput);

            Assert::AreEqual(numberOfSpectralBins, dimValidData[0]);
            Assert::AreEqual(numberOfCorticalSources, dimValidData[1]);
            Assert::AreEqual(numSamples, dimValidData[2]);

            dataElements = static_cast<double*>(mxGetData(matValidOutput));
            double validValue;
            double procValue;
            double testValue;
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                for (size_t sourceIndex = 0; sourceIndex < numberOfCorticalSources; ++sourceIndex)
                {
                    for (size_t binIndex = 0; binIndex < numberOfSpectralBins; ++binIndex)
                    {
                        validValue = dataElements[binIndex + sourceIndex * numberOfSpectralBins + sampleIndex * numberOfCorticalSources * numberOfSpectralBins];
                        procValue = outputStorage[sampleIndex]->at(binIndex, sourceIndex);
                        if (validValue == 0 && procValue == 0)
                        {
                            Assert::AreEqual(validValue, procValue);
                        }
                        else
                        {
                            testValue = abs((validValue - procValue) / (validValue + procValue));
                            Assert::AreEqual(0, testValue, testTolerance);
                        }
                    }
                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBuffer = new double[numberOfSpectralBins * numberOfCorticalSources * numSamples];
            for (size_t outIndex = 0; outIndex < numSamples; ++outIndex)
            {
                memcpy(storageBuffer + outIndex * numberOfSpectralBins * numberOfCorticalSources, outputStorage[outIndex]->getValues(), numberOfSpectralBins * numberOfCorticalSources * sizeof(double));
            }

            CMatlabLibrary::saveMatDouble3dData("..\\TestData\\SpectralSourceReconstruction_testResults.mat", "sourceActivity", storageBuffer, numberOfSpectralBins, numberOfCorticalSources, numSamples);

            delete spectralSourceReconstruction;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete testBuffer[testIndex];
                testBuffer[testIndex] = 0;
            }
            delete[] testBuffer;
            testBuffer = 0;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete outputStorage[testIndex];
                outputStorage[testIndex] = 0;
            }
            delete[] outputStorage;
            outputStorage = 0;

            delete[] storageBuffer;
            storageBuffer = 0;

            Logger::WriteMessage("Unit Test Cortical Source Reconstruction\t--> Ok");
        }

    };

    TEST_CLASS(UnitTestEogCorrectionAlgorithm)
    {
    public:

        TEST_METHOD(UnitTestEogCorrectionAlgorithmProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-8;

            // Input file
            string fileName = "..\\TestData\\testData_EogCorrectionAlgorithm.mat";

            // Create the class that is being unitested
            CEogCorrectionAlgorithm* eogCorrectionAlgorithm = new CEogCorrectionAlgorithm(fileName);

            const size_t numberOfNeuralChannels = eogCorrectionAlgorithm->getNumberOfNeuralChannels();
            const size_t numberOfEogChannels = eogCorrectionAlgorithm->getNumberOfEogChannels();


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load test data from .mat file
            mxArray* matEegData = CMatlabLibrary::loadMatVariable(fileName, "eegData");
            Assert::IsNotNull(matEegData);

            // Store data in data Buffer
            Assert::AreEqual(numberOfNeuralChannels, mxGetM(matEegData));
            size_t numSamples = mxGetN(matEegData);

            double* dataElements = static_cast<double*>(mxGetData(matEegData));
            CDoubleVector** eegBuffer = new CDoubleVector*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                eegBuffer[testIndex] = new CDoubleVector(numberOfNeuralChannels, dataElements + testIndex * numberOfNeuralChannels);
            }


            // Load test data from .mat file
            mxArray* matEogData = CMatlabLibrary::loadMatVariable(fileName, "eogData");
            Assert::IsNotNull(matEogData);

            // Store data in data Buffer
            Assert::AreEqual(numberOfEogChannels, mxGetM(matEogData));
            Assert::AreEqual(numSamples, mxGetN(matEogData));

            dataElements = static_cast<double*>(mxGetData(matEogData));
            CDoubleVector** eogBuffer = new CDoubleVector*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                eogBuffer[testIndex] = new CDoubleVector(numberOfEogChannels, dataElements + testIndex * numberOfEogChannels);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            CDoubleVector** outputStorage = new CDoubleVector*[numSamples];
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                outputStorage[sampleIndex] = new CDoubleVector(numberOfNeuralChannels);
                eogCorrectionAlgorithm->process(outputStorage[sampleIndex], eegBuffer[sampleIndex], eogBuffer[sampleIndex]);
            }



            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the process data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "eegCorrectedData");
            Assert::IsNotNull(matValidOutput);

            Assert::AreEqual(numberOfNeuralChannels, mxGetM(matValidOutput));
            Assert::AreEqual(numSamples, mxGetN(matValidOutput));

            dataElements = static_cast<double*>(mxGetData(matValidOutput));
            double validValue;
            double procValue;
            double testValue;
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                for (size_t channelIndex = 0; channelIndex < numberOfNeuralChannels; ++channelIndex)
                {
                    validValue = dataElements[channelIndex + sampleIndex * numberOfNeuralChannels];
                    procValue = outputStorage[sampleIndex]->getValues()[channelIndex];
                    if (validValue == 0 && procValue == 0)
                    {
                        Assert::AreEqual(validValue, procValue);
                    }
                    else
                    {
                        testValue = abs((validValue - procValue) / (validValue + procValue));
                        Assert::AreEqual(0, testValue, testTolerance);
                    }
                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBuffer = new double[numberOfNeuralChannels * numSamples];
            for (size_t outIndex = 0; outIndex < numSamples; ++outIndex)
            {
                memcpy(storageBuffer + outIndex * numberOfNeuralChannels, outputStorage[outIndex]->getValues(), numberOfNeuralChannels * sizeof(double));
            }

            CMatlabLibrary::saveMatDoubleMatrixData("..\\TestData\\EogCorrectionAlgorithm_testResults.mat", "eegCorrectedData", storageBuffer, numberOfNeuralChannels, numSamples);

            delete eogCorrectionAlgorithm;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete eegBuffer[testIndex];
                eegBuffer[testIndex] = 0;
            }
            delete[] eegBuffer;
            eegBuffer = 0;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete eogBuffer[testIndex];
                eogBuffer[testIndex] = 0;
            }
            delete[] eogBuffer;
            eogBuffer = 0;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete outputStorage[testIndex];
                outputStorage[testIndex] = 0;
            }
            delete[] outputStorage;
            outputStorage = 0;

            delete[] storageBuffer;
            storageBuffer = 0;

            Logger::WriteMessage("Unit Test EOG Correction Algorithm\t--> Ok");
        }

    };

    TEST_CLASS(UnitTestChannelInterpolationAlgorithm)
    {
    public:

        TEST_METHOD(UnitTestChannelInterpolationAlgorithmProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-8;

            // Input file
            string fileName = "..\\TestData\\testData_ChannelInterpolationAlgorithm.mat";

            // Create the class that is being unitested
            CChannelInterpolationAlgorithm* channelInterpolationAlgorithm = new CChannelInterpolationAlgorithm(fileName);

            const size_t numberOfNeuralChannels = channelInterpolationAlgorithm->getNumberOfNeuralChannels();


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load test data from .mat file
            mxArray* matTestData = CMatlabLibrary::loadMatVariable(fileName, "eegData");
            Assert::IsNotNull(matTestData);

            // Store data in data Buffer
            Assert::AreEqual(numberOfNeuralChannels, mxGetM(matTestData));
            size_t numSamples = mxGetN(matTestData);

            double* dataElements = static_cast<double*>(mxGetData(matTestData));
            CDoubleVector** testBuffer = new CDoubleVector*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBuffer[testIndex] = new CDoubleVector(numberOfNeuralChannels, dataElements + testIndex * numberOfNeuralChannels);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            CDoubleVector** outputStorage = new CDoubleVector*[numSamples];
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                outputStorage[sampleIndex] = new CDoubleVector(numberOfNeuralChannels);
                channelInterpolationAlgorithm->process(outputStorage[sampleIndex], testBuffer[sampleIndex]);
            }



            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the process data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "eegInterpolatedData");
            Assert::IsNotNull(matValidOutput);

            Assert::AreEqual(numberOfNeuralChannels, mxGetM(matValidOutput));
            Assert::AreEqual(numSamples, mxGetN(matValidOutput));

            dataElements = static_cast<double*>(mxGetData(matValidOutput));
            double validValue;
            double procValue;
            double testValue;
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                for (size_t channelIndex = 0; channelIndex < numberOfNeuralChannels; ++channelIndex)
                {
                    validValue = dataElements[channelIndex + sampleIndex * numberOfNeuralChannels];
                    procValue = outputStorage[sampleIndex]->getValues()[channelIndex];
                    if (validValue == 0 && procValue == 0)
                    {
                        Assert::AreEqual(validValue, procValue);
                    }
                    else
                    {
                        testValue = abs((validValue - procValue) / (validValue + procValue));
                        Assert::AreEqual(0, testValue, testTolerance);
                    }
                }
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBuffer = new double[numberOfNeuralChannels * numSamples];
            for (size_t outIndex = 0; outIndex < numSamples; ++outIndex)
            {
                memcpy(storageBuffer + outIndex * numberOfNeuralChannels, outputStorage[outIndex]->getValues(), numberOfNeuralChannels * sizeof(double));
            }

            CMatlabLibrary::saveMatDoubleMatrixData("..\\TestData\\ChannelInterpolationAlgorithm_testResults.mat", "eegInterpolatedData", storageBuffer, numberOfNeuralChannels, numSamples);

            delete channelInterpolationAlgorithm;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete testBuffer[testIndex];
                testBuffer[testIndex] = 0;
            }
            delete[] testBuffer;
            testBuffer = 0;

            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                delete outputStorage[testIndex];
                outputStorage[testIndex] = 0;
            }
            delete[] outputStorage;
            outputStorage = 0;

            delete[] storageBuffer;
            storageBuffer = 0;

            Logger::WriteMessage("Unit Test Channel Interpolation Algorithm\t--> Ok");
        }

    };

    TEST_CLASS(UnitTestSerialRationalTransferFunctionFilter)
    {
    public:

        TEST_METHOD(UnitTestSerialRationalTransferFunctionFilterProcess)
        {
            // Tolerance
            const double testTolerance = 1.0e-3;

            // Input file
            string fileName = "..\\TestData\\testData_SerialRationalTransferFunctionFilter.mat";

            // Create the class that is being unitested
            CSerialRationalTransferFunctionFilter* serialRationalTransferFunctionFilter = new CSerialRationalTransferFunctionFilter(fileName);

            const size_t numberOfChannels = serialRationalTransferFunctionFilter->getNumberOfChannels();


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load test data from .mat file
            mxArray* matTestData = CMatlabLibrary::loadMatVariable(fileName, "rawData");
            Assert::IsNotNull(matTestData);

            // Store data in data Buffer
            Assert::AreEqual(numberOfChannels, mxGetM(matTestData));
            size_t numSamples = mxGetN(matTestData);

            double* dataElements = static_cast<double*>(mxGetData(matTestData));
            CDoubleVector** testBuffer = new CDoubleVector*[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testBuffer[testIndex] = new CDoubleVector(numberOfChannels, dataElements + testIndex * numberOfChannels);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            vector<CDoubleVector*> outputStorage;
            CDoubleVector tempFilteredSignal(numberOfChannels,0.0);
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                if (serialRationalTransferFunctionFilter->process(&tempFilteredSignal, testBuffer[sampleIndex]))
                {
                    outputStorage.push_back(new CDoubleVector(tempFilteredSignal));
                }
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the processed data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "rezData");
            Assert::IsNotNull(matValidOutput);

            Assert::AreEqual(numberOfChannels, mxGetM(matValidOutput));
            Assert::AreEqual(outputStorage.size(), mxGetN(matValidOutput));

            dataElements = static_cast<double*>(mxGetData(matValidOutput));
            double validValue;
            double procValue;
            double testValue;
            for (size_t procIndex = 0; procIndex < outputStorage.size(); ++procIndex)
            {
                for (size_t chIndex = 0; chIndex < numberOfChannels; ++chIndex)
                {
                    validValue = dataElements[chIndex + procIndex * numberOfChannels];
                    procValue = outputStorage.at(procIndex)->getValues()[chIndex];
                    testValue = abs(validValue - procValue);
                    //Assert::AreEqual(0, testValue, testTolerance);

                    // The standard test of accuracy doesn't work due to the propagation of rounding errors
                    /*
                    if (validValue == 0 && procValue == 0)
                    {
                        Assert::AreEqual(validValue, procValue);
                    }
                    else
                    {
                        testValue = abs((validValue - procValue) / (validValue + procValue));
                        Assert::AreEqual(0, testValue, testTolerance);
                    }
                    */
                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBuffer = new double[numberOfChannels * outputStorage.size()];
            for (size_t outIndex = 0; outIndex < outputStorage.size(); ++outIndex)
            {
                memcpy(storageBuffer + numberOfChannels * outIndex, outputStorage.at(outIndex)->getValues(), numberOfChannels * sizeof(double));
                delete outputStorage.at(outIndex);
                outputStorage.at(outIndex) = 0;
            }

            CMatlabLibrary::saveMatDoubleMatrixData("..\\TestData\\SerialRationalTransferFunctionFilter_testResults.mat", "filtData", storageBuffer, numberOfChannels, outputStorage.size());

            delete serialRationalTransferFunctionFilter;

            delete[] storageBuffer;
            storageBuffer = 0;

            outputStorage.clear();

            Logger::WriteMessage("Unit Test Serial Rational Transfer Function Filter\t--> Ok");
        }
    };

    TEST_CLASS(UnitTestConsolidateDecisionAlgorithm)
    {
    public:

        TEST_METHOD(UnitTestConsolidateDecisionAlgorithmProcess)
        {
            // Input file
            string fileName = "..\\TestData\\testData_ConsolidateDecisionAlgorithm.mat";

            // Create the class that is being unitested
            CConsolidateDecisionAlgorithm* consolidateDecisionAlgorithm = new CConsolidateDecisionAlgorithm(fileName);


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Read the test data

            // Load decoder decision data from .mat file
            mxArray* matTestDecoderDecision = CMatlabLibrary::loadMatVariable(fileName, "testDecoderDecision");
            Assert::IsNotNull(matTestDecoderDecision);

            // Store data in data Buffer
            size_t numSamples = mxGetM(matTestDecoderDecision);
            Assert::AreEqual(1, static_cast<int>(mxGetN(matTestDecoderDecision)));

            double* dataElements = static_cast<double*>(mxGetData(matTestDecoderDecision));
            size_t* testDecoderDecision = new size_t[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                testDecoderDecision[testIndex] = static_cast<size_t>(dataElements[testIndex]);
            }


            // Load the number of sample points from .mat file
            mxArray* matNumberOfSamplePoints = CMatlabLibrary::loadMatVariable(fileName, "numberOfSamplePoints");
            Assert::IsNotNull(matNumberOfSamplePoints);

            // Store data in data Buffer
            Assert::AreEqual(numSamples, mxGetM(matNumberOfSamplePoints));
            Assert::AreEqual(1, static_cast<int>(mxGetN(matNumberOfSamplePoints)));

            dataElements = static_cast<double*>(mxGetData(matNumberOfSamplePoints));
            size_t* numberOfSamplePoints = new size_t[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                numberOfSamplePoints[testIndex] = static_cast<size_t>(dataElements[testIndex]);
            }


            // Load artificat presence data from the .mat file
            mxArray* matArtifactPresence = CMatlabLibrary::loadMatVariable(fileName, "artifactPresence");
            Assert::IsNotNull(matArtifactPresence);

            // Store data in data Buffer
            Assert::AreEqual(numSamples, mxGetM(matArtifactPresence));
            Assert::AreEqual(1, static_cast<int>(mxGetN(matArtifactPresence)));

            dataElements = static_cast<double*>(mxGetData(matArtifactPresence));
            bool* artifactPresence = new bool[numSamples];
            for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
            {
                artifactPresence[testIndex] = static_cast<bool>(dataElements[testIndex]);
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Process data

            vector<size_t> outputStorage;
            for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                outputStorage.push_back(consolidateDecisionAlgorithm->process(testDecoderDecision[sampleIndex], artifactPresence[sampleIndex], numberOfSamplePoints[sampleIndex]));
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Compare the processed data with the MATLAB version

            // Load the real spectrum data from the .mat file
            mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "consolidatedDecoderDecision");
            Assert::IsNotNull(matValidOutput);

            Assert::AreEqual(outputStorage.size(), mxGetM(matValidOutput));
            Assert::AreEqual(1, static_cast<int>(mxGetN(matValidOutput)));

            dataElements = static_cast<double*>(mxGetData(matValidOutput));

            for (size_t procIndex = 0; procIndex < outputStorage.size(); ++procIndex)
            {
                Assert::AreEqual(outputStorage.at(procIndex), static_cast<size_t>(dataElements[procIndex]));
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Save results

            double* storageBuffer = new double[outputStorage.size()];
            for (size_t outIndex = 0; outIndex < outputStorage.size(); ++outIndex)
            {
                storageBuffer[outIndex] = static_cast<double>(outputStorage.at(outIndex));
            }

            CMatlabLibrary::saveMatDoubleMatrixData("..\\TestData\\ConsolidateDecisionAlgorithm_testResults.mat", "consolidatedDecoderDecision", storageBuffer, outputStorage.size(), 1);

            delete consolidateDecisionAlgorithm;

            delete[] testDecoderDecision;
            testDecoderDecision = 0;

            delete[] numberOfSamplePoints;
            numberOfSamplePoints = 0;

            delete[] artifactPresence;
            artifactPresence = 0;

            delete[] storageBuffer;
            storageBuffer = 0;

            outputStorage.clear();

            Logger::WriteMessage("Unit Test Consolidate Decision Algorithm\t--> Ok");
        }
    };
}

namespace EegDataUnitTest
{
    TEST_CLASS(UnitTestEegData)
    {
    public:
        TEST_METHOD(UnitTestArtefactThreshold)
        {
			// EEG data class
			string fileName = "..\\TestData\\testData_calibrateChannelSelector.mat";
			CEegData eegData(fileName);

			///////////////////////////////////////////////////////////////////////////////////////////////
			// Read the test data

			// Load test data from .mat file
			mxArray* matTestData = CMatlabLibrary::loadMatVariable(fileName, "testData");
			Assert::IsNotNull(matTestData);

			// Store data in data Buffer
			size_t numChannels = mxGetM(matTestData);
			size_t numSamples = mxGetN(matTestData);

			double* dataElements = static_cast<double*>(mxGetData(matTestData));
			CDoubleVector** dataBuffer = new CDoubleVector*[numSamples];
			for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
			{
				dataBuffer[sampleIndex] = new CDoubleVector(numChannels, dataElements + sampleIndex * numChannels);
			}

			///////////////////////////////////////////////////////////////////////////////////////////////
			// Process data

			double* outputStorage = new double[numSamples];
			for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
			{
				eegData.artefactThreshold(dataBuffer[sampleIndex]->getValues(), numChannels);
				outputStorage[sampleIndex] = static_cast<double>(eegData.GetIsArtefactDetected());
			}

			///////////////////////////////////////////////////////////////////////////////////////////////
			// Compare the processed data with the MATLAB version

			// Load the artifact presence data from the .mat file
			mxArray* matValidOutput = CMatlabLibrary::loadMatVariable(fileName, "artifactPresence");
			Assert::IsNotNull(matValidOutput);

			Assert::AreEqual(1, static_cast<int>(mxGetM(matValidOutput)));
			Assert::AreEqual(numSamples, mxGetN(matValidOutput));

			dataElements = static_cast<double*>(mxGetData(matValidOutput));
			for (size_t sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
			{
				Assert::AreEqual(static_cast<bool>(dataElements[sampleIndex]), static_cast<bool>(outputStorage[sampleIndex]));
			}

			///////////////////////////////////////////////////////////////////////////////////////////////
			// Save results

			CMatlabLibrary::saveMatDoubleMatrixData("..\\TestData\\ArtefactThreshold_testResults.mat", "artifactPresence", outputStorage, 1, numSamples);

			for (size_t testIndex = 0; testIndex < numSamples; ++testIndex)
			{
				delete dataBuffer[testIndex];
				dataBuffer[testIndex] = 0;
			}
			delete[] dataBuffer;
			dataBuffer = 0;

			delete[] outputStorage;
			outputStorage = 0;

            Logger::WriteMessage("Unit Test Artefact Threshold\t--> Ok");
        }
    };
}